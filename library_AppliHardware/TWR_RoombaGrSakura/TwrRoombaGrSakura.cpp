/*
 * TwrRoombaGrSakura.cpp
 *
 *  Created on: 2018/12/04
 *      Author: khashimoto
 */

#include "TwrRoombaGrSakura.h"

TwrRoombaGrSakura::TwrRoombaGrSakura(void){
	position_ctrl = TwrRunPositionCtrl::getInstance();
	velocity_ctrl = TwrRunVelocityCtrl::getInstance();
	linetrace_ctrl = TwrRunLinetraceCtrl::getInstance();
}

TwrRoombaGrSakura::~TwrRoombaGrSakura(void)
{

}


uint8_t TwrRoombaGrSakura::init(void)
{
	uint16_t i;

	/* 制御ボード初期化 */
	GrEx.begin();		/* 拡張基板初期化 */
	GrExOled.begin();	/* 拡張基板のOLED初期化 */


	/* ルンバ初期化 */
	rmb.begin(&Sci2);

	/* 二輪制御ドライバ初期化 */
	twr_driver_if.init(&rmb);

	/* 各制御クラスを初期化 */
	run_ctrl.init(&twr_driver_if, &state_est, &vehicle_ctrl);		/* 走行指令制御 */
	vehicle_ctrl.init(&twr_driver_if, &state_est);	/* 走行制御 */
	state_est.init(&twr_driver_if);		/* 状態推定 */


	/* 走行制御の制御係数をセット */
	vehicle_ctrl.pid_vel.setParam(0.0f, 0.0f, 0.0f, 0.0f, 100.0f, 0.0f, 20.0f);
	vehicle_ctrl.pid_angvel.setParam(0.0f, 0.0f, 0.0f, 0.0f, 100.0f, 0.0f, 20.0f);


	/* ルンバ初期設定 */
	rmb.setMode(ROI_MODE_FULL);	// フルモードにする
	// ルンバモード変更待ち
	for(i=0;i<65534;i++);	// ちょい待ち
	for(i=0;i<65534;i++);	// ちょい待ち
	// 受信したいデータ設定
	const ROI_SENSPACKET_ID SensDataReq[] = {
				ROI_SENSPACKET_BUMPS_WHEELDROPS,
				ROI_SENSPACKET_BATTERY_CHARGE,
				ROI_SENSPACKET_BATTERY_CAPACITY,
				ROI_SENSPACKET_ENCODER_COUNTS_LEFT,
				ROI_SENSPACKET_ENCODER_COUNTS_RIGHT,
				ROI_SENSPACKET_BUTTONS,
				ROI_SENSPACKET_SONG_PLAYING
	};
	rmb.setRcvStream(sizeof(SensDataReq) / sizeof(SensDataReq[0]), SensDataReq);

	return 0;
}

void TwrRoombaGrSakura::task_1ms(void)
{
	GrEx.task();		// 拡張基板タスク実行
	GrExOled.task();	// 拡張基板OLED基板タスク実行
}

void TwrRoombaGrSakura::task_10ms(void)
{
	/* 通信系処理 */
	/* I2C */
	I2c_t::task();

	/* 二輪制御ドライバ更新 */
	twr_driver_if.task();

	/* ルンバ処理 */
	rmb.task();

	/* 二輪ロボット制御処理 */
	state_est.task();		/* 状態推定 */
	run_ctrl.task();		/* 走行指令制御 */
	vehicle_ctrl.task();	/* 走行制御 */

	/* 次ターン向け処理 */
	GrEx.Imu->measure();

}




/* 独立二輪ロボット制御用インタフェース */
TwrRoombaGrSakura::TwrRoombaGrSakuraIf::TwrRoombaGrSakuraIf(void)
{
	rmb = NULL;
	enc_l_last = 0;
	enc_r_last = 0;
	enc_cnt_l = 0;
	enc_cnt_r = 0;
	duty_l = 0.0f;
	duty_r = 0.0f;
};


uint8_t TwrRoombaGrSakura::TwrRoombaGrSakuraIf::init(RoombaOi* rmb){
	this->rmb = rmb;

	// ラインセンサ
	setPinMode(LINESENS_PORT_LED, PIN_OUTPUT);	// LED光らせるポート
	// AD変換開始
	GrEx.startAD();

	// ラインセンサ初期化
	initLineSens();





	GrEx.Imu->setAxis(1,1,1,1,1,1,1,1,1);	/* 軸設定(ロボット搭載向き的に全部逆) */
	return 0;
}

/* 二輪制御ドライバ更新 */
void TwrRoombaGrSakura::TwrRoombaGrSakuraIf::task(void){


	/* ラインセンサ更新 */
	updateLineSense();
	/* ライン位置計算 */
	calcLinePos(false);

}

uint8_t TwrRoombaGrSakura::TwrRoombaGrSakuraIf::setWheelDuty(const float duty_l_pct, const float duty_r_pct)
{

	duty_l = duty_l_pct;
	duty_r = duty_r_pct;

	rmb->drivePwm((int16_t)(-duty_l_pct*2.55f), (int16_t)(-duty_r_pct*2.55f));	/* 0~255で指定 */

	return 0;
}

uint8_t TwrRoombaGrSakura::TwrRoombaGrSakuraIf::getWheelAng(float& ang_l_rad, float& ang_r_rad)
{
	uint16_t enc_r, enc_l;

	/* エンコーダ値取得 */
	rmb->getEncoderCounts(&enc_l, &enc_r);	/* 0~65535で取得。逆向きなのでタイヤ逆 */

	/* カウント値更新 */
	if(duty_l>0.0f){		/* 指令は正回転 */
							/* →正逆切替時に誤差が乗るので，ゼロクロスを検出して反転を検知したい */
		enc_cnt_l += ((int32_t)enc_l_last - (int32_t)enc_l);	/* 逆向きなのでマイナス */
	}else{
		enc_cnt_l -= ((int32_t)enc_l_last - (int32_t)enc_l);
	}
	if(duty_r>0.0f){		/* 指令は正回転 */
		enc_cnt_r += ((int32_t)enc_r_last - (int32_t)enc_r);
	}else{
		enc_cnt_r -= ((int32_t)enc_r_last - (int32_t)enc_r);
	}
	enc_l_last = enc_l;
	enc_r_last = enc_r;

	ang_l_rad = (float)enc_cnt_l / (float)ROI_ENCODER_COUNT;
	ang_r_rad = (float)enc_cnt_r / (float)ROI_ENCODER_COUNT;

	return 0;
}

uint8_t TwrRoombaGrSakura::TwrRoombaGrSakuraIf::getGyroYaw(float& yawrate_radps)
{
	yawrate_radps = GrEx.Imu->getGyroZ() * M_PI / 180.0f;

	return GrEx.Imu->isComuErr()||(!GrEx.Imu->isCalibrated());
}

uint8_t TwrRoombaGrSakura::TwrRoombaGrSakuraIf::getLine(float& line_pos_mm)
{
	line_pos_mm = LinePos;
	return 0;
}


/*********************
ラインセンサ初期化
概要：
引数：

**********************/
int8_t TwrRoombaGrSakura::TwrRoombaGrSakuraIf::initLineSens(void){
	/*
	LineSensAd[0].EepIdMin = MEM_LINE_0_MIN;
	LineSensAd[0].EepIdMax = MEM_LINE_0_MAX;
	LineSensAd[1].EepIdMin = MEM_LINE_1_MIN;
	LineSensAd[1].EepIdMax = MEM_LINE_1_MAX;
	LineSensAd[2].EepIdMin = MEM_LINE_2_MIN;
	LineSensAd[2].EepIdMax = MEM_LINE_2_MAX;
	LineSensAd[3].EepIdMin = MEM_LINE_3_MIN;
	LineSensAd[3].EepIdMax = MEM_LINE_3_MAX;

	LineSensAd[0].PosOffset = LINESENS_POS_Y_OFFSET_0;
	LineSensAd[1].PosOffset = LINESENS_POS_Y_OFFSET_1;
	LineSensAd[2].PosOffset = LINESENS_POS_Y_OFFSET_2;
	LineSensAd[3].PosOffset = LINESENS_POS_Y_OFFSET_3;

	for(uint8_t SensNum = 0; SensNum < LINESENS_NUM_SENSE; ++SensNum){
		readMem(LineSensAd[SensNum].EepIdMin, &LineSensAd[SensNum].Range.Min);
		readMem(LineSensAd[SensNum].EepIdMax, &LineSensAd[SensNum].Range.Max);
	}
	*/
	LinePos = 0;

	return 0;
}
/*********************
ラインセンサアップデート
概要：
 ラインセンサ値をアップデート
 LEDonした時とoffした時の差分を取る
引数：

**********************/
void TwrRoombaGrSakura::TwrRoombaGrSakuraIf::updateLineSense(void){

	if(fLineSensOn){	// onだった
		for(uint8_t i=0; i<LINESENS_NUM_SENSE; i++){
			LineSensAd[i].OnVal = GrEx.getAD10((grex_ad_ch)i);	// 10bit
		}
		// LED消す
		outPin(LINESENS_PORT_LED, 0);
		fLineSensOn = 0;
	}else{
		for(uint8_t i=0; i<LINESENS_NUM_SENSE; i++){
			LineSensAd[i].OffVal = GrEx.getAD10((grex_ad_ch)i);	// 10bit
		}

		// LED光らす
		outPin(LINESENS_PORT_LED, 1);
		fLineSensOn = 1;

	}

	// off時-on時でセンサ値とする
	for(uint8_t i=0; i<LINESENS_NUM_SENSE; i++){
		LineSensAd[i].Val = LineSensAd[i].OffVal - LineSensAd[i].OnVal;
	}

}


/*********************
ライン位置アップデート
概要：
 ラインセンサ値から，ラインがどの位置にいるかを計算する
 一番センサ値低いセンサの位置に近いとして，
 隣のセンサ値との割合で位置を出す。
引数：
 updateSensValRange ラインセンサのAD値範囲更新
戻値：
 位置
前提条件：
 各センサの最小・最大値がセットされていること
**********************/
int8_t TwrRoombaGrSakura::TwrRoombaGrSakuraIf::calcLinePos(const bool_t updateSensValRange){
	float NormalizedSensVal[LINESENS_NUM_SENSE];
	int8_t idx[2] = {-1, -1};
	float MinVal = 65535.0;

	// ラインセンサキャリブ有効なら
	// ラインセンサの最大値最小値を更新する
	/*
	if(updateSensValRange){
		for(int i = 0; i<LINESENS_NUM_SENSE; ++i){
			setLinePosAdRange(i, LineSensAd[i].Val);
		}
	}
	 */

	// 0~1に正規化
	for(int i = 0; i<LINESENS_NUM_SENSE; ++i){
		NormalizedSensVal[i] = ((float)LineSensAd[i].Val - (float)LineSensAd[i].Range.Min) / (float)(LineSensAd[i].Range.Max - LineSensAd[i].Range.Min);

		// 一番低い値，センサを取得
		if(MinVal > NormalizedSensVal[i]){
			MinVal = NormalizedSensVal[i];
			idx[0] = i;
		}
	}

	if(0.9 >= MinVal){	// センサ値有効(ラインに乗ってる)

		// 端っこのセンサが最小だったらその隣が二番目とする
		if(0 == idx[0]){	// はじっこ
			idx[1] = 1;
			if(0.8 < NormalizedSensVal[idx[1]]){
				idx[1] = -1;
			}
		}else if((LINESENS_NUM_SENSE-1) == idx[0]){	// はじっこ
			idx[1] = LINESENS_NUM_SENSE-2;
			if(0.8 < NormalizedSensVal[idx[1]]){
				idx[1] = -1;
			}
		}else{
			if(NormalizedSensVal[idx[0]-1] < NormalizedSensVal[idx[0]+1]){
				idx[1] = idx[0] - 1;
			}else{
				idx[1] = idx[0] + 1;
			}
		}

		// センサ位置演算
		if(0<=idx[1]){	// センサ幅内

			LinePos = ((float)LineSensAd[idx[1]].PosOffset - (float)LineSensAd[idx[0]].PosOffset) * NormalizedSensVal[idx[0]] / NormalizedSensVal[idx[1]] + (float)LineSensAd[idx[0]].PosOffset;

		}else{	// 端っこより外側

			//Pos = (float)LineSensAd[idx[0]].PosOffset;	// とりあえず端っこの位置にしておく。
			if(0 == idx[0]){
				LinePos = (float)LineSensAd[idx[0]].PosOffset - LINESENS_POS_Y_PITCH*NormalizedSensVal[idx[0]];
			}else if(LINESENS_NUM_SENSE == idx[0]){
				LinePos = (float)LineSensAd[idx[0]].PosOffset + LINESENS_POS_Y_PITCH*NormalizedSensVal[idx[0]];
			}else{
				/* error*/
			}
		}

		return 0;
	}

	return -1;
}

