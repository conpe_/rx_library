/*
 * TwrRoombaGrSakura.h
 *
 *  Created on: 2018/12/04
 *      Author: khashimoto
 */

/*
ルンバをベースとしてGR-SAKURAを使って制御するロボ
・エンコーダ，ラインセンサを使った走行制御
・GR-SAKURA処理，I2C，ルンバ処理タスクあたりまで実行

その他のセンサ類とか通信類は継承して拡張して使ってね。
*/



#ifndef TWRROOMBAGRSAKURA_H_
#define TWRROOMBAGRSAKURA_H_

#include "TwoWheelRobotCtrl.h"
#include "RoombaOi.h"
#include "GR_Ex15_v3.h"
#include "GR_Ex15_OLED.h"


/* ラインセンサ */
// 中心位置
#define LINESENS_POS_X	110.0F	// タイヤ位置からの距離
#define LINESENS_POS_Y	0.0F
// 各センサの位置オフセット
#define LINESENS_POS_Y_OFFSET_0	(-30)		// 左
#define LINESENS_POS_Y_OFFSET_1	(-6)		// センサが一番暗くなる位置この辺ぽい
#define LINESENS_POS_Y_OFFSET_2	(6)
#define LINESENS_POS_Y_OFFSET_3	(30)		// 右
#define LINESENS_POS_Y_PITCH	(20)		// センサ間
// 赤外LEDポート
#define LINESENS_PORT_LED	GREX_IO7
// センサ数
#define LINESENS_NUM_SENSE	4	// インデックスは左から順に0, 1, 2, 3
								// ADポートの0~3に対応
// センサ閾値
#define LINESENS_THRESHOLD_SIDE 400
#define LINESENS_THRESHOLD_CENTER 400





class TwrRoombaGrSakura{
public:
	RoombaOi rmb;

	TwrRunCtrl run_ctrl;
	TwrRunPositionCtrl* position_ctrl;
	TwrRunVelocityCtrl* velocity_ctrl;
	TwrRunLinetraceCtrl* linetrace_ctrl;

	TwrVehicleCtrl vehicle_ctrl;
	TwrStateEst state_est;

public:
	TwrRoombaGrSakura(void);
	~TwrRoombaGrSakura(void);
	uint8_t init(void);			/* 初回に実行する */
	void task_1ms(void);		/* 1msごとに実行するタスク */
	void task_10ms(void);		/* 10msごとに実行するタスク */



/* TWR用インタフェース */
private:
	class TwrRoombaGrSakuraIf : public TwrDriverIf{
	public:
		TwrRoombaGrSakuraIf(void);
		uint8_t init(RoombaOi* rmb);
		void task(void);

		float getCtrlSampleTime_ms(void){return 5.0f;};
		uint8_t setWheelDuty(const float duty_l_pct, float duty_r_pct);
		uint8_t getWheelAng(float& ang_l_rad, float& ang_r_rad);
		uint8_t getGyroYaw(float& yawrate_radps);
		uint8_t getLine(float& line_pos_mm);
	private:
		RoombaOi* rmb;
		uint16_t enc_l_last, enc_r_last;
		int32_t enc_cnt_l, enc_cnt_r;
		float duty_l, duty_r;

		/* ラインセンサ */
		typedef struct{
			uint16_t Min;
			uint16_t Max;
		} MinMax_t;
		typedef struct{
			uint16_t Val;
			uint16_t OnVal;		// ラインセンサAD値 LED on時 10bit
			uint16_t OffVal;		// ラインセンサAD値 LED off時 10bit
			MinMax_t Range;
			int16_t PosOffset;
			uint16_t EepIdMin;
			uint16_t EepIdMax;
		} LineSensAd_t;
		LineSensAd_t LineSensAd[LINESENS_NUM_SENSE];	// ラインセンサAD値
		float LinePos;								// ライン位置
		bool_t fLineSensOn;							// LED光らせてるフラグ
		int8_t initLineSens(void);
		void updateLineSense(void);
		int8_t calcLinePos(const bool_t updateSensValRange);
	};

	TwrRoombaGrSakuraIf twr_driver_if;
};



#endif /* TWRROOMBAGRSAKURA_H_ */
