/*
 * TwrStateEst.h
 *
 *  Created on: 2018/12/04
 *      Author: khashimoto
 */

/*
 【概要】
エンコーダやジャイロから車体の状態(位置，姿勢，速度)とかを推定する。

【使い方】
はじめに
・initを呼ぶ
・オドメトリ用のパラメータをセットする
  odometry_param

*/

#ifndef TWRSTATEEST_H_
#define TWRSTATEEST_H_

#include "TwrDriverIf.h"

class TwrStateEst{
public:
	TwrOdometryParam odometry_param;
public:
	TwrStateEst(void);
	uint8_t init(TwrDriverIf* const twr_driver);
	void task(void);

	void setMachinePos(const Position& pos){TwrStateEst::pos = pos;};
	void getMachinePos(Position& pos){pos = TwrStateEst::pos;};
	void getMachineSpd(Velocity& vel){vel = TwrStateEst::vel;};

protected:
	bool is_init;

	float ctrl_sample_time_ms;

	TwrDriverIf* twr_driver;
	Position pos;	/* 位置 */
	Velocity vel;	/* 速度 */

/* 速度推定 */
	float ang_l_last, ang_r_last;
	void updateVel(void);

/* 位置推定 */
	void updatePos(void);
};

#endif /* TWRSTATEEST_H_ */
