/*
 * TwrVehicleCtrl.h
 *
 *  Created on: 2018/12/04
 *      Author: khashimoto
 */

/* 車体制御
【概要】
速度・角速度指令を受けてそのとおり車体を制御する
PID制御

【使い方】
はじめに
・initを呼ぶ
・PID係数などをセットする。
  pid_vel, pid_angvel

*/
#ifndef TWRVEHICLECTRL_H_
#define TWRVEHICLECTRL_H_

#include "TwrDriverIf.h"
#include "TwrStateEst.h"
#include "AppliCtrl.h"

class TwrVehicleCtrl{
public:
	TwrVehicleCtrl(void);
	uint8_t init(TwrDriverIf* const twr_driver, TwrStateEst* const state_est);
	uint8_t task(void);		/* 実行 (dutyセット) */
	uint8_t setTgtVel(const Velocity& tgt_vel);		/* 目標速度設定 */
	uint8_t setMaxVel(const Velocity& max_vel);		/* 最大速度設定 (FFに必要) */

	ctrl_pid pid_vel;		/* 並進速度PID制御 */
	ctrl_pid pid_angvel;	/* 角速度PID制御 */
	float ff_gain_vel;		/* 並進速度FFゲイン */
	float ff_gain_angvel;	/* 角速度FFゲイン */
protected:
	bool is_init;
	bool is_set_tgtvel;
	bool is_set_maxvel;

	TwrDriverIf* twr_driver;
	TwrStateEst* state_est;

	Velocity tgt_vel;
	Velocity max_vel;
};

#endif /* TWRVEHICLECTRL_H_ */
