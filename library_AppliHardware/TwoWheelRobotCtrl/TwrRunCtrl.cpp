/*
 * TwrRunCtrl.cpp
 *
 *  Created on: 2018/12/04
 *      Author: khashimoto
 */

#include "TwrRunCtrl.h"

/**************/
/* 走行制御 */
/**************/

TwrRunCtrl* TwrRunCtrl::ctrl_method;
TwrDriverIf* TwrRunCtrl::twr_driver;
TwrStateEst* TwrRunCtrl::state_est;
TwrVehicleCtrl* TwrRunCtrl::vehicle_ctrl;


TwrRunCtrl::TwrRunCtrl(void)
{
	ctrl_method = NULL;

	twr_driver = NULL;
	state_est = NULL;
	vehicle_ctrl = NULL;

	ctrl_sample_time_ms = 1.0f;

	is_init = false;
}

TwrRunCtrl::~TwrRunCtrl(void)
{

}

uint8_t TwrRunCtrl::init(TwrDriverIf* const twr_driver, TwrStateEst* const state_est, TwrVehicleCtrl* const vehicle_ctrl)
{
	if((NULL!=twr_driver) && (NULL!=state_est) && (NULL!=vehicle_ctrl)){
		this->twr_driver = twr_driver;
		this->state_est = state_est;
		this->vehicle_ctrl = vehicle_ctrl;

		ctrl_sample_time_ms = twr_driver->getCtrlSampleTime_ms();	/* 制御周期取得 */

		is_init = true;

		return 0;
	}
	return 1;
}

void TwrRunCtrl::task(void)
{
	Velocity tgt_vel;

	if(NULL != ctrl_method){	/* 制御方法指定ありなら */
		if(0 == ctrl_method->ctrl(tgt_vel)){		/* 指定された制御方法で走行速度計算 */
			/* 計算おっけー */
		}else{
			/* 計算だめー */
			tgt_vel.vel = 0.0f;
			tgt_vel.ang_vel = 0.0f;
		}
	}else{	/* 制御方法指定なしなら */
		tgt_vel.vel = 0.0f;
		tgt_vel.ang_vel = 0.0f;
	}
	/* 走行速度セット */
	vehicle_ctrl->setTgtVel(tgt_vel);
}

/* 制御方法セット */
void TwrRunCtrl::run(void)
{
	ctrl_method = this;
}

/* 指令速度演算 */
uint8_t TwrRunCtrl::ctrl(Velocity& tgt_vel)
{
	return 1;
}

/* 動作完了 */
bool TwrRunCtrl::isFinish(void){

	return false;
}




/*
 * 個別の制御方法
 */

/**************/
/* 位置制御 */
/**************/
TwrRunPositionCtrl TwrRunPositionCtrl::RunCtrl;
/* 指令速度演算 */
uint8_t TwrRunPositionCtrl::ctrl(Velocity& tgt_vel)
{


	return 1;
}
/* 動作完了 */
bool TwrRunPositionCtrl::isFinish(void)
{
	return false;
}

/**************/
/* 速度制御 */
/**************/
TwrRunVelocityCtrl TwrRunVelocityCtrl::RunCtrl;
TwrRunVelocityCtrl::TwrRunVelocityCtrl(void){
	IsValidTgt = false;
}

/* 目標速度セット */
void TwrRunVelocityCtrl::setTgtVel(Velocity& tgt_vel)
{
	this->tgt_vel = tgt_vel;
}

/* 指令速度演算 */
uint8_t TwrRunVelocityCtrl::ctrl(Velocity& tgt_vel)
{
	if(IsValidTgt){
		tgt_vel = this->tgt_vel;

		return 0;
	}else{
		tgt_vel.vel = 0.0f;
		tgt_vel.ang_vel = 0.0f;

		return 1;
	}
}
/* 動作完了 */
bool TwrRunVelocityCtrl::isFinish(void)
{
	return false;
}

/**************/
/* ライントレース制御 */
/**************/
TwrRunLinetraceCtrl TwrRunLinetraceCtrl::RunCtrl;
/* 指令速度演算 */
uint8_t TwrRunLinetraceCtrl::ctrl(Velocity& tgt_vel)
{
	return 1;
}
/* 動作完了 */
bool TwrRunLinetraceCtrl::isFinish(void)
{
	return false;
}

