/*
 * TwrDataType.h
 *
 *  Created on: 2018/12/07
 *      Author: khashimoto
 */

/*
 * 共通データ型の定義
 *
 */


#ifndef TWRDATATYPE_H_
#define TWRDATATYPE_H_

#include "CommonDataType.h"
#include "aplGeometry.h"



/*********************
オドメトリ用定数
 独立二輪
概要：
引数：
**********************/
class TwrOdometryParam{
public:
	float Tread_mm;		/* トレッド */
	float TyreDir_mm;	/* 直径 */
	float GearRatio;	/* エンコーダまでのギアレシオ */
	float EncCPR;		/* エンコーダ分解能/rev */
};


#endif /* TWRDATATYPE_H_ */
