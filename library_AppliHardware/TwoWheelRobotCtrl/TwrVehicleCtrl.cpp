/*
 * TwrVehicleCtrl.cpp
 *
 *  Created on: 2018/12/04
 *      Author: khashimoto
 */



#include "TwrVehicleCtrl.h"

TwrVehicleCtrl::TwrVehicleCtrl(void)
{
	is_init = false;
	twr_driver = NULL;
	state_est = NULL;

	is_set_tgtvel = false;
	is_set_maxvel = false;

	tgt_vel.vel = 0.0f;
	tgt_vel.ang_vel = 0.0f;

	max_vel.vel = 0.0f;
	max_vel.ang_vel = 0.0f;

	ff_gain_vel = 1.0f;
	ff_gain_angvel = 1.0f;
}

uint8_t TwrVehicleCtrl::init(TwrDriverIf* const twr_driver, TwrStateEst* const state_est)
{
	if((NULL!=twr_driver) && (NULL!=state_est)){
		this->twr_driver = twr_driver;
		this->state_est = state_est;

		is_init = true;

		return 0;
	}
	return 1;
}

uint8_t TwrVehicleCtrl::task(void){
	Velocity current_vel;

	float duty_straight_pct;
	float duty_turn_pct;

	float set_duty_l_pct;
	float set_duty_r_pct;

	if(false == is_set_tgtvel){
		return 1;
	}


	/* FF */
	/* duty100%で最大速度になるとする。間はまっすぐ補間 */
	if(is_set_maxvel){
		duty_straight_pct = ff_gain_vel * (tgt_vel.vel / max_vel.vel);
		duty_turn_pct = ff_gain_angvel * (tgt_vel.ang_vel / max_vel.ang_vel);
	}

	/* FB */
	/* 目標速度になるようPID制御 */
	/* 直進と旋回のduty量を算出 */
	/* 現在の速度を取得 */
	state_est->getMachineSpd(current_vel);
	duty_straight_pct += pid_vel.calc(current_vel.vel - tgt_vel.vel);
	duty_turn_pct += pid_vel.calc(current_vel.ang_vel - tgt_vel.ang_vel);


	/* 直進量と旋回量を調停 */
	set_duty_l_pct = duty_straight_pct - duty_turn_pct;
	set_duty_r_pct = duty_straight_pct + duty_turn_pct;

	/* dutyが100%を超えるなら調整 */
	float adj_gain = 1.0f;
	if(fabs(set_duty_l_pct) > fabs(set_duty_r_pct)){	/* lが大きい */
		if(100.0f < fabs(set_duty_l_pct)){				/* lが100%超えてる */
			adj_gain = 100.0f/fabs(set_duty_l_pct);		/* lが100%になるようにゲインをセット */
		}else{
			/* どちらも100%超えていないのでOK */
			adj_gain = 1.0f;
		}
	}else{												/* rが大きい */
		if(100.0f < fabs(set_duty_r_pct)){				/* lが100%超えてる */
			adj_gain = 100.0f/fabs(set_duty_r_pct);		/* lが100%になるようにゲインをセット */
		}else{
			/* どちらも100%超えていないのでOK */
			adj_gain = 1.0f;
		}
	}
	set_duty_l_pct *= adj_gain;
	set_duty_r_pct *= adj_gain;

	/* dutyセット */
	twr_driver->setWheelDuty(set_duty_l_pct, set_duty_r_pct);

	return 0;
}

uint8_t TwrVehicleCtrl::setTgtVel(const Velocity& tgt_vel)
{
	this->tgt_vel = tgt_vel;
	is_set_tgtvel = true;
	return 0;
}

uint8_t TwrVehicleCtrl::setMaxVel(const Velocity& max_vel)
{
	this->max_vel = max_vel;
	is_set_maxvel = true;
	return 0;
}



