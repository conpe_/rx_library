/*
 * TwrStateEst.cpp
 *
 *  Created on: 2018/12/04
 *      Author: khashimoto
 */

#include <mathf.h>
#include "TwrStateEst.h"

TwrStateEst::TwrStateEst(void)
{
	is_init = false;
	twr_driver = NULL;
	ctrl_sample_time_ms = 1.0f;

	ang_r_last = 0.0f;
	ang_l_last = 0.0f;
}

uint8_t TwrStateEst::init(TwrDriverIf* const twr_driver)
{

	if(NULL != twr_driver){
		this->twr_driver = twr_driver;

		ctrl_sample_time_ms = twr_driver->getCtrlSampleTime_ms();	/* 制御周期取得 */

		is_init = true;

		return 0;
	}
	return 1;
}


void TwrStateEst::task(void)
{

	updateVel();
	updatePos();

}

/* 速度更新 */
void TwrStateEst::updateVel(void)
{
	float ang_l_rad, ang_r_rad;
	float angvel_l_radps, angvel_r_radps;

	twr_driver->getWheelAng(ang_l_rad, ang_r_rad);

	angvel_l_radps = (ang_l_rad - ang_l_last) / ctrl_sample_time_ms * 1000.0f;
	angvel_r_radps = (ang_r_rad - ang_r_last) / ctrl_sample_time_ms * 1000.0f;

	vel.vel = (angvel_l_radps + angvel_r_radps) / 2.0f;
	vel.ang_vel = (angvel_r_radps - angvel_l_radps) / odometry_param.Tread_mm;

}

/* 位置更新 */
void TwrStateEst::updatePos(void)
{

	float delta_dist = 0.0;
	float theta = 0.0;

	/* 制御周期中の移動距離 */
	delta_dist = vel.vel * ctrl_sample_time_ms / 1000.0f;

	Angle dir_last = pos.dir;
	/* 角度変化分 */
	Angle d_dir;
	d_dir.dir = vel.ang_vel * ctrl_sample_time_ms / 1000.0f;
	/* 新しい角度 */
	pos.dir.dir = dir_last + d_dir;
	/* 進んだ向き */
	theta = (pos.dir - dir_last) / 2.0f;	/* 前の向きと今回の向きの間 */

	/* 新しい位置 */
	pos.pt.x += delta_dist * cosf(theta);
	pos.pt.y += delta_dist * sinf(theta);

}


