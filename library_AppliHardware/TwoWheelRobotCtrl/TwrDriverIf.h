/*
 * TwrDriverIf.h
 *
 *  Created on: 2018/12/04
 *      Author: khashimoto
 */

#ifndef TWRDRIVERIF_H_
#define TWRDRIVERIF_H_

#include "TwrDataType.h"

class TwrDriverIf{
public:
	virtual ~TwrDriverIf(void){};

	/* 制御周期[ms] */
	virtual float getCtrlSampleTime_ms(void) = 0;
	/* タイヤのデューティー[%]をセット */
	virtual uint8_t setWheelDuty(float duty_l_pct, float duty_r_pct) = 0;
	/* タイヤの角度[rad]を取得 */
	virtual uint8_t getWheelAng(float& ang_l_rad, float& ang_r_rad) = 0;
	/* ジャイロのヨーレート[rad/s]を取得 */
	virtual uint8_t getGyroYaw(float& yawrate_radps) = 0;
	/* ラインセンサで検出した中央からのズレ[mm]を取得 */
	virtual uint8_t getLine(float& line_pos_mm) = 0;

};

#endif /* TWRDRIVERIF_H_ */
