/*
 * TwrRunCtrl.h
 *
 *  Created on: 2018/12/04
 *      Author: khashimoto
 */

/* 走行制御
【概要】


*/

#ifndef TWRRUNCTRL_H_
#define TWRRUNCTRL_H_

#include "TwrDriverIf.h"
#include "TwrStateEst.h"
#include "TwrVehicleCtrl.h"

class TwrRunCtrl{
public:
	TwrRunCtrl(void);
	virtual ~TwrRunCtrl(void);

	uint8_t init(TwrDriverIf* const twr_driver, TwrStateEst* const state_est, TwrVehicleCtrl* const vehicle_ctrl);
	void task(void);

	virtual void run(void);

protected:
	bool is_init;

	float ctrl_sample_time_ms;

	static TwrRunCtrl* ctrl_method;

	static TwrDriverIf* twr_driver;
	static TwrStateEst* state_est;
	static TwrVehicleCtrl* vehicle_ctrl;

	virtual uint8_t ctrl(Velocity& tgt_vel);
	virtual bool isFinish(void);
};

/**************/
/* 位置制御 */
/**************/
class TwrRunPositionCtrl : public TwrRunCtrl{
private:
	TwrRunPositionCtrl(void){};
	static TwrRunPositionCtrl RunCtrl;
public:
	static TwrRunPositionCtrl* getInstance(void){return &RunCtrl;};
protected:
	virtual uint8_t ctrl(Velocity& tgt_vel);
	virtual bool isFinish(void);
};

/**************/
/* 速度制御 */
/**************/
class TwrRunVelocityCtrl : public TwrRunCtrl{
private:
	TwrRunVelocityCtrl(void);
	static TwrRunVelocityCtrl RunCtrl;
public:
	static TwrRunVelocityCtrl* getInstance(void){return &RunCtrl;};
public:
	virtual void setTgtVel(Velocity& tgt_vel);
protected:
	Velocity tgt_vel;
	bool IsValidTgt;
	virtual uint8_t ctrl(Velocity& tgt_vel);
	virtual bool isFinish(void);
};

/**************/
/* ライントレース制御 */
/**************/
class TwrRunLinetraceCtrl : public TwrRunCtrl{
private:
	TwrRunLinetraceCtrl(void){};
	static TwrRunLinetraceCtrl RunCtrl;
public:
	static TwrRunLinetraceCtrl* getInstance(void){return &RunCtrl;};
protected:
	virtual uint8_t ctrl(Velocity& tgt_vel);
	virtual bool isFinish(void);
};
















#endif /* TWRRUNCTRL_H_ */
