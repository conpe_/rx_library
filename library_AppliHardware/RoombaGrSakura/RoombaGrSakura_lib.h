/**************************************************
RoombaGrSakura_lib.h
	ルンバロボ用ライブラリ
	知ロボ，先端ものづくりチャレンジ，Robocon30th
**************************************************/


#ifndef __ROOMBA_GRSAKURA_H__
#define __ROOMBA_GRSAKURA_H__

#include <math.h>

#include "GR_Ex15_v3.h"
#include "GR_Ex15_OLED.h"
#include "AppliCtrl.h"

#include "RoombaOi.h"
#include "RunEleSbdbt.h"
#include "aplSerialCom.h"
#include "aplMemCtrl.h"

extern RoombaOi Rmb;
extern runele_sbdbt_t Sbdbt;

// define

// 制御周期
#define SAMPLETIME_CTRL_S 0.020	// 動作制御周期 20ms

// 制御定数
#define PI 3.1415

// 角度
#define DEG_TO_RAD 0.017453F	// [rad/deg]
#define RAD_TO_DEG 57.2957F	// [deg/rad]

#define BATT_LOW_RMBSOC 60	// ルンバのSOC 60%

// 
#define RMB_VEL_MAX	400.0F	// 最高速度[mm/s]
#define RMB_ANGVEL_MAX	3.0F	// 最高角速度[rad/s]


// type
typedef struct calc_odmetry{
	float REncCntDiff[2];
	float LEncCntDiff[2];
	float CosTh[2];
	float SinTh[2];
	float dpos[2];		// 前回との位置の差 [mm]
	float dth[2];		// 前回との角度の差 [rad/s]
}calc_odmetry;

enum inrof_orientation{
	ORIENT_NONE, 
	ORIENT_EAST,
	ORIENT_NORTH,
	ORIENT_WEST,
	ORIENT_SOUTH
};

class roomba_grsakura_t{
public:
	roomba_grsakura_t(void);	// 
	virtual void begin(void);	// 
	virtual void step(void);	// 
private:
	virtual void initParamRoombaGrSakura(void);	// アプリケーション系フラグとかリセット
public:
	// Machine Constant
	two_wheel_robot_parameter_t WheelParam;
	
	
	// Machine State
	position_t* getMachinePos(void){return &MachinePos;};
	void initMachinePos(float x = 0.0, float y = -200.0, float th = PI/2.0);	// マシン位置初期化
	
	// peripheral module
	// IMU
	void enableImu(uint8_t AD0=0){ OdoImu->begin(AD0); EnableImu = true; };
	void disableImu(void){EnableImu = false;};
	bool_t isEnableImu(void){return EnableImu;};
	
	// 走行モータ
	void enableDriveMotor(void){EnableDriveMotor = true;};
	void disableDriveMotor(void){EnableDriveMotor = false;};
	bool_t isEnableDriveMotor(void){return EnableDriveMotor;};
	

	
	// ルンバでお掃除
	bool_t fCleaning;
	int8_t startClean(void);
	int8_t stopClean(void);
	bool_t isCleaning(void){return fCleaning;};
	
	
	// ユーティリティ
	// TPU6を使ったフリーカウンタ
	void setupFreeCounter(void);
	uint16_t getFreeCounter(void){return TPU6.TCNT;};
	bool_t isFreeCounterOvf(void){return TPU6.TSR.BIT.TCFV;};
	void clearFreeCounterOvfFlag(void){TPU6.TSR.BIT.TCFV = 0;};
	
	
/***********
 PS3コン関係
************/
	void enablePs3Con(void){
		SerialCom_t::setMode(SELCOM_MODE_THROUGH);	// スルーモード
		Sbdbt.setMode(runele_sbdbt_t::PS3);
	};
	void disablePs3Con(void){
		SerialCom_t::setMode(SELCOM_MODE_COMMAND);	// コマンドモード
		Sbdbt.setMode(runele_sbdbt_t::PC_THROUGH);
	};
	bool_t isEnablePs3Con(void){
		return Sbdbt.isMode(runele_sbdbt_t::PS3);
	};
	
public:
	void isr1ms(void);		// call 1ms cyclic

	float getAngVelTgt(void){return AngVelTgt;};
	float getSpdTgt(void){return SpdTgt;};
	
protected:
	// Machine State
	position_t MachinePos;			// 位置
	float MachineSpd;			// マシン速度
	inrof_orientation MachineOrientation;	// マシンの向き(4方向)
	// Roomba
	void initRoomba(void);
	//オドメトリ計算用
	calc_odmetry CalcOdmetry;		// オドメトリ計算用
	uint16_t REncCnt, LEncCnt;		// エンコーダ値
	int16_t REncCntDiff, LEncCntDiff;	// エンコーダ前回との差
	float RTirePos_r, LTirePos_r;		// タイヤ角度[rev]
	float RTireSpd_rps, LTireSpd_rps;	// タイヤ角速度[rps]
	
	// Machine Controll
	/// 走行制御
	float SpdTgt, AngVelTgt;			// 目標速度[mm/s], 角速度[rad/s]
	float SpdTgtRated, AngVelTgtRated;		//  レートリミット後
	int16_t RTireDutyTgt, LTireDutyTgt;		// 出力duty
	ctrl_pid* PidCtrlRTire;				// タイヤPID処理用
	ctrl_pid* PidCtrlLTire;				//  速度→デューティー
	ctrl_ratelimiter_abs* RateLimitSpdTgt;		// 車体速度レートリミッタ [mm/s/s]
	ctrl_ratelimiter_abs* RateLimitAngVelTgt;	// 車体回転速度レートリミッタ [rad/s/s]
	
	// 走行制御
	void setTgtSpdAng(float Spd = 0.0F, float AngVel = 0.0F);	// 速度[mm/s]，角速度[rad/s]
	
	//
	virtual void stepTask_1ms(void);	// 1msごと処理
	void stepTask_20ms(void);	// メイン制御(20msごと。stepTask_Input, stepTask_Calc, stepTask_Outputを呼び出すこと)
	virtual void stepTask_100ms(void);	// 100msごと処理
	// メイン制御
	virtual void stepTask_Input(void);
	virtual void stepTask_Calc(void);
	virtual void stepTask_Output(void);
	
	
/***********
 PS3コン関係
************/
	// PS3コンでルンバを動かす
	runele_sbdbt_t::ps3data_t Ps3ConDataLast;
	float Ps3StrOut;	// +-255
	float Ps3TurnOut;	// +-255
	virtual void drivePs3Con(void);
	
	// task
protected:
	bool_t fExeReqTask_1ms;
	bool_t fExeReqTask_20ms;
	bool_t fExeReqTask_100ms;
	
	uint16_t CntTask_20ms;
	uint16_t CntTask_100ms;
	
	
	// peripheral module
protected:
	// オドメトリ用IMU
	invensense_MPU* OdoImu;
	bool_t EnableImu;		// is enable Imu
	// 走行モータ
	bool_t EnableDriveMotor;
	
	/*********************
	エンコーダ更新
	概要：	エンコーダの前回値との差分をセット
		初回実行時は差分0をセットする
	**********************/
protected:
	virtual void updateEncoder(void);
	bool_t fResetEncoder;	// 立ってる間差分0になる
private:
	/*********************
	タイヤパラメータ更新
	概要：	角度[rev]、角速度[rps]
	引数：
	**********************/
	void updateTyre(void);
	
	/*********************
	オドメトリ更新
	概要：
	引数：
	メンバ変数：MachinePos,MachineOrientation,Spd,CalcOdmetry
	**********************/
	virtual void updateOdmetry(void);
	
/***********
 制御
************/
	
	/*********************
	走行制御
	概要：
	 目標速度、角速度に追従する
	引数：
	 目標速度[mm/s]、角速度[rad/s]
	**********************/
protected:
	virtual void ctrlRun(float SpdTgt_mmps, float AngVelTgt_radps, int16_t* DutyR, int16_t* DutyL, bool_t ctrlEnable = 1);
	
	
/***********
 出力
************/
	/*********************
	モーター出力
	概要：
	 PWM値を出力する
	引数：
	 Duty
	**********************/
protected:
	virtual void outTyres(int16_t RTireDutyTgt, int16_t LTireDutyTgt, bool_t fMotorOut=1);
	
	
	
	
	
	
	
	
	
private:
// ユーティリティ
	void setup1msInterrupt(void);
	
};



#endif
