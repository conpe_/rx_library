/************************************************************************/
/*	ルンバとGR-SAKURAボードだけでできることをライブラリにしてます。	*/
/************************************************************************/


#include "RoombaGrSakura_lib.h"



// ルンバ
// タイヤエンコーダ関係
//#define TIRE_TREAD_MM	235.0F		// トレッド
#define TIRE_TREAD_MM	248.0F		// トレッド
//#define TIRE_DIA_MM	70.0F		// 直径
#define TIRE_DIA_MM	72.0F		// 直径
#define GEAR_RATIO	1.0F		// ギアヘッドの減速比
#define ENC_CPR		490.0F		// エンコーダ一周あたりのカウント

#define ROI_MAX_SPD_RPS ROI_MAX_SPD_MMPS/TIRE_DIA_MM/PI	// 最大速度[rps]

// ラインセンサ
// 中心位置
#define LINESENS_POS_X	110.0F	// タイヤ位置からの距離
#define LINESENS_POS_Y	0.0F
// 赤外LEDポート
#define LINESENS_PORT_LED	GREX_IO7
// センサ数
#define LINESENS_NUM_SENSE	4	// インデックスは左から順に0, 1, 2, 3
					// ADポートの0〜3に対応	
// センサ閾値
#define LINESENS_THRESHOLD_SIDE 550
#define LINESENS_THRESHOLD_CENTER 500





RoombaOi Rmb;
runele_sbdbt_t Sbdbt(&Sci0);		// SBDBT基板との通信(PC, PS3コン)


roomba_grsakura_t::roomba_grsakura_t(void){
	// オドメトリ演算パラメータ
	WheelParam.Tread_mm = TIRE_TREAD_MM;
	WheelParam.TyreDir_mm = TIRE_DIA_MM;
	WheelParam.GearRatio = GEAR_RATIO;
	WheelParam.EncCPR = ENC_CPR;
	
	
	EnableImu = false;	// デフォルトoff
}

// 基本処理、ルンバ通信初期化
void roomba_grsakura_t::begin(void){
	volatile uint16_t i;
	
	GrEx.begin();		// 拡張基板初期化
	GrExOled.begin();	// 拡張基板のOLED初期化
	
	
	// データフラッシュ初期化
	initMem();
	
	
	
// 外部通信
	Sci0.begin(115200, true, true, 1024, 128);	// PC use Tx, Rx
	//Sci1.begin(115200, true, true, 255, 128);	// デバッガ use Tx, Rx
	SerialCom_t::init(&Sci0, SELCOM_MODE_COMMAND);	// 通信取りまとめ
	
// オブジェクト用意
	// ルンバ
	Rmb.begin(&Sci2);
	
	
	// 測距センサ
	//beginBallSensor();
	
	// Imu
	OdoImu = GrEx.Imu;
	//enableImu();		// アプリケーションに任せるため，ここでは有効にしない
	
	
//初期化	
	
	
	// ラインセンサ
	//setPinMode(LINESENS_PORT_LED, PIN_OUTPUT);	// LED光らせるポート
	// AD変換開始
	//GrEx.startAD();
	
	
	// ルンバ初期化
	initRoomba();
	
	// フラグ系リセット
	initParamRoombaGrSakura();
	
	setup1msInterrupt();	//1msごと割り込み設定
}


void roomba_grsakura_t::initParamRoombaGrSakura(void){
	
	
	// 制御用
	// タイヤ
	if(PidCtrlRTire){ delete PidCtrlRTire; }
	PidCtrlRTire = new ctrl_pid(SAMPLETIME_CTRL_S, 20.0F, 0.5F, 0.5F, -255,255, -200, 200);	// 速度->デューティー
	if(PidCtrlLTire){ delete PidCtrlLTire; }
	PidCtrlLTire = new ctrl_pid(SAMPLETIME_CTRL_S, 20.0F, 0.5F, 0.5F, -255,255, -200, 200);
	// 速度レートリミッタ
	if(RateLimitSpdTgt){ delete RateLimitSpdTgt; }
	RateLimitSpdTgt = new ctrl_ratelimiter_abs(SAMPLETIME_CTRL_S, 1000.0F, 1000.0F);	// 1000 mm/s/s
	
	if(RateLimitAngVelTgt){ delete RateLimitAngVelTgt; }
	//RateLimitAngVelTgt = new ctrl_ratelimiter_abs(SAMPLETIME_CTRL_S, 3.0F, 3.0F);		// 3.0 rad/s/s	
	RateLimitAngVelTgt = new ctrl_ratelimiter_abs(SAMPLETIME_CTRL_S, 10.0F, 10.0F);		// 10.0 rad/s/s	
	
	
	if(NULL==PidCtrlRTire) __heap_chk_fail();
	if(NULL==PidCtrlLTire) __heap_chk_fail();
	if(NULL==RateLimitSpdTgt) __heap_chk_fail();
	if(NULL==RateLimitAngVelTgt) __heap_chk_fail();
	
	
	fExeReqTask_1ms = true;
	fExeReqTask_20ms = true;
	fExeReqTask_100ms = true;
	
	
	fResetEncoder = true;
	
	SpdTgt = 0;
	AngVelTgt = 0;
	RTireDutyTgt = 0;
	LTireDutyTgt = 0;
	
	// 車体初期位置
	initMachinePos(0.0F, 0.0F, 0.0F);
	MachinePos.dX = 0.0;
	MachinePos.dY = 0.0;
	MachinePos.dTh = 0.0;
	
	
	// IMU軸方向
	OdoImu->setAxis(1,1,1,1,1,1,1,1,1);	// 軸設定(全部逆)
	
	// オドメトリ
	CalcOdmetry.REncCntDiff[0] = 0.0;
	CalcOdmetry.LEncCntDiff[0] = 0.0;
	CalcOdmetry.CosTh[0] = 0.0;
	CalcOdmetry.SinTh[0] = 1.0;
	CalcOdmetry.dpos[0] = 0.0;
	CalcOdmetry.dth[0] = 0.0;
	CalcOdmetry.REncCntDiff[1] = 0.0;
	CalcOdmetry.LEncCntDiff[1] = 0.0;
	CalcOdmetry.CosTh[1] = 0.0;
	CalcOdmetry.SinTh[1] = 1.0;
	CalcOdmetry.dpos[1] = 0.0;
	CalcOdmetry.dth[1] = 0.0;
	
	// PS3コンモード
	Ps3StrOut = 0.0F;	// +-255
	Ps3TurnOut = 0.0F;	// +-255
}

// ルンバ初期化
void roomba_grsakura_t::initRoomba(void){
	uint16_t i;
	
	Rmb.setMode(ROI_MODE_FULL);	// フルモードにする
	// ルンバモード変更待ち
	for(i=0;i<65534;i++);	// ちょい待ち
	for(i=0;i<65534;i++);	// ちょい待ち
	// 受信したいデータ設定
	const ROI_SENSPACKET_ID SensDataReq[] = {
				ROI_SENSPACKET_BUMPS_WHEELDROPS,
				ROI_SENSPACKET_BATTERY_CHARGE,
				ROI_SENSPACKET_BATTERY_CAPACITY,
				ROI_SENSPACKET_ENCODER_COUNTS_LEFT,
				ROI_SENSPACKET_ENCODER_COUNTS_RIGHT,
				ROI_SENSPACKET_BUTTONS,
				ROI_SENSPACKET_CLIFF_LEFT,
				ROI_SENSPACKET_CLIFF_FRONT_LEFT,
				ROI_SENSPACKET_CLIFF_FRONT_RIGHT,
				ROI_SENSPACKET_CLIFF_RIGHT,
				ROI_SENSPACKET_CLIFF_LEFT_SIGNAL,
				ROI_SENSPACKET_CLIFF_FRONT_LEFT_SIGNAL,
				ROI_SENSPACKET_CLIFF_FRONT_RIGHT_SIGNAL,
				ROI_SENSPACKET_CLIFF_RIGHT_SIGNAL,
				ROI_SENSPACKET_SONG_PLAYING
	};
	Rmb.setRcvStream(sizeof(SensDataReq) / sizeof(SensDataReq[0]), SensDataReq);
	
}

void roomba_grsakura_t::step(void){
	if(fExeReqTask_1ms){
		fExeReqTask_1ms = false;
		this->stepTask_1ms();		// 派生クラスのtaskを呼び出す(派生クラスではこのクラスのタスクを呼び出すこと)
	}
	if(fExeReqTask_20ms){
		fExeReqTask_20ms = false;
		this->stepTask_20ms();
	}
	if(fExeReqTask_100ms){
		fExeReqTask_100ms = false;
		this->stepTask_100ms();
	}
}


void roomba_grsakura_t::stepTask_1ms(void){
	fExeReqTask_1ms = false;
	
	GrEx.task();		// 拡張基板タスク実行
	GrExOled.task();	// 拡張基板OLED基板タスク実行
}



void roomba_grsakura_t::stepTask_20ms(void){
	fExeReqTask_20ms = false;
	
	this->stepTask_Input();		// 派生クラスの各処理メソッドを実行する
	
	this->stepTask_Calc();
	
	this->stepTask_Output();
	
}

void roomba_grsakura_t::stepTask_100ms(void){
	fExeReqTask_100ms = false;
}



// 入力・通信系タスク
void roomba_grsakura_t::stepTask_Input(void){
	// シリアル通信タスク
	SerialCom_t::task();
	
	// EEPROM書き込み
	drvDataFlash_WriteTask();
	
	// I2Cタスク
	I2c_t::task();
	
	// ルンバタスク
	Rmb.task();
	
	// PS3コン通信処理
	Sbdbt.task();
	

	/***********
	 入力処理
	************/
	// エンコーダ更新
	updateEncoder();
	// タイヤ状態更新
	updateTyre();
	// オドメトリ更新
	updateOdmetry();
	
	
}

// 計算系タスク
void roomba_grsakura_t::stepTask_Calc(){
	
	
}
	
// 出力系タスク
void roomba_grsakura_t::stepTask_Output(){
	
	
// 足回り
	// 目標速度，角速度にレートリミット
	//SpdTgtRated = RateLimitSpdTgt->limitRate(SpdTgt);
	//AngVelTgtRated = RateLimitAngVelTgt->limitRate(AngVelTgt);
	SpdTgtRated = SpdTgt;
	AngVelTgtRated = AngVelTgt;
	// デューティ演算
	ctrlRun(SpdTgtRated, AngVelTgtRated, &RTireDutyTgt, &LTireDutyTgt, EnableDriveMotor);
	// 出力
	outTyres(RTireDutyTgt, LTireDutyTgt, EnableDriveMotor);
	
	
	// センサ計測要求出力
	// IMU
	if(isEnableImu()){
		OdoImu->measure();
	}
	
}



/*********************
// マシン位置初期化
**********************/
void roomba_grsakura_t::initMachinePos(float x, float y, float th){
	MachinePos.X = x;
	MachinePos.Y = y;
	MachinePos.Th = th;
	if(isEnableImu()){
		OdoImu->resetCalcAttitudeEstimation(0.0F, 0.0F, RAD_TO_DEG * MachinePos.Th);
	}
}


/*********************
エンコーダアップデート
概要：
 エンコーダカウントの前回との差を計算
引数：

**********************/
void roomba_grsakura_t::updateEncoder(void){
	uint16_t REncCntLast, LEncCntLast;
	
	REncCntLast = this->REncCnt;
	LEncCntLast = this->LEncCnt;
	
	// ルンバからエンコーダ値取得
	Rmb.getEncoderCounts(&this->LEncCnt, &this->REncCnt);	// ルンバから見たら逆向き
	
	if(fResetEncoder){	// 初回は同じの入れる。差をゼロにするため。
		REncCntLast = this->REncCnt;
		LEncCntLast = this->LEncCnt;
		fResetEncoder = false;
	}
	
	// 差を計算
	REncCntDiff = REncCntLast - REncCnt;	// ルンバから見たら逆向き
	LEncCntDiff = LEncCntLast - LEncCnt;	
	
	// ↑オーバーフローとかでいい感じに計算される
	// int16_t = uint16_t - uint16_t
	// int16_tで計算できる値より大きい差がでたら逆方向に回転したと判断した結果になる。
}

/*********************
タイヤ状態アップデート
概要：
 エンコーダ値から角度と角速度をアップデート
引数：

**********************/
void roomba_grsakura_t::updateTyre(void){
	
	// タイヤ角速度[rps]
	RTireSpd_rps = (float)REncCntDiff/WheelParam.EncCPR/WheelParam.GearRatio/SAMPLETIME_CTRL_S;
	LTireSpd_rps = (float)LEncCntDiff/WheelParam.EncCPR/WheelParam.GearRatio/SAMPLETIME_CTRL_S;
	
	// タイヤ角度[rev]
	RTirePos_r = RTirePos_r + (float)REncCntDiff/WheelParam.EncCPR/WheelParam.GearRatio;
	LTirePos_r = LTirePos_r + (float)LEncCntDiff/WheelParam.EncCPR/WheelParam.GearRatio;
	
}
	
/*********************
自己位置更新
**********************/
void roomba_grsakura_t::updateOdmetry(void){
	CalcOdmetry.REncCntDiff[1] = CalcOdmetry.REncCntDiff[0];
	CalcOdmetry.REncCntDiff[0] = REncCntDiff;
	CalcOdmetry.LEncCntDiff[1] = CalcOdmetry.LEncCntDiff[0];
	CalcOdmetry.LEncCntDiff[0] = LEncCntDiff;
	
	
	// マシン角度・角速度
	CalcOdmetry.dth[1] = CalcOdmetry.dth[0]; 
	if(EnableImu && OdoImu->isCalibrated() && !OdoImu->isComuErr()){	// ジャイロキャリブ済み, IMU問題なし
		#if 0
		// ジャイロセンサを使って角度を得る
		MachinePos.dTh = DEG_TO_RAD * OdoImu->getGyroZ();
		MachinePos.Th = DEG_TO_RAD * OdoImu->getAeYaw();			// [rad]
		CalcOdmetry.dth[0] = MachinePos.dTh * SAMPLETIME_CTRL_S;
		#endif
		
		
		
		// ジャイロセンサを使って角度を得る
		float dThEnc = ((CalcOdmetry.REncCntDiff[0] - CalcOdmetry.LEncCntDiff[0])*WheelParam.TyreDir_mm*PI/WheelParam.Tread_mm/WheelParam.EncCPR/WheelParam.GearRatio) / SAMPLETIME_CTRL_S;		// [rad/s]
		float dThImu = DEG_TO_RAD * OdoImu->getGyroZ();
		float gain = fabs(dThImu);
		
		// ジャイロのオフセットがあるかもなので，下の方は切る(0.1〜1.0の範囲を0.0〜1.0に広げる)
		gain = (gain - 0.1F);
		if(gain < 0.0F){	
			gain = 0.0F;
		}
		gain = gain*10.0F/9.0F;
		if(gain > 1.0F){
			gain = 1.0F;
		}
		// 角速度大きいほどジャイロ使う
		MachinePos.dTh = gain * dThImu + (1.0F - gain) * dThEnc;		// [rad/s]
		// 角度積算
		MachinePos.Th += SAMPLETIME_CTRL_S * MachinePos.dTh;	// [rad]
		CalcOdmetry.dth[0] = MachinePos.dTh * SAMPLETIME_CTRL_S;
	}else{
		// エンコーダから角度を得る
		CalcOdmetry.dth[0] = (CalcOdmetry.REncCntDiff[0] - CalcOdmetry.LEncCntDiff[0])*WheelParam.TyreDir_mm*PI/WheelParam.Tread_mm/WheelParam.EncCPR/WheelParam.GearRatio;		//[rad]
		MachinePos.dTh = CalcOdmetry.dth[0] / SAMPLETIME_CTRL_S;		// [rad/s]
		MachinePos.Th = MachinePos.Th + CalcOdmetry.dth[0];			// [rad]
	}
	
	CalcOdmetry.CosTh[1] = CalcOdmetry.CosTh[0];
	CalcOdmetry.SinTh[1] = CalcOdmetry.SinTh[0];
	CalcOdmetry.CosTh[0] = cosf(MachinePos.Th);
	CalcOdmetry.SinTh[0] = sinf(MachinePos.Th);
	
	CalcOdmetry.dpos[1] = CalcOdmetry.dpos[0];
	//if(EnableImu && OdoImu->isCalibrated() && !OdoImu->isComuErr()){	// ジャイロキャリブ済み, IMU問題なし
		// テスト的に，右のエンコーダだけ使うVer
	//	CalcOdmetry.dpos[0] = (CalcOdmetry.REncCntDiff[0] + CalcOdmetry.REncCntDiff[1])/2/WheelParam.EncCPR/WheelParam.GearRatio * PI*WheelParam.TyreDir_mm - (CalcOdmetry.dth[0] + CalcOdmetry.dth[1])/2 * WheelParam.Tread_mm/2;
	//}else{
		CalcOdmetry.dpos[0] = (CalcOdmetry.REncCntDiff[0] + CalcOdmetry.REncCntDiff[1] + CalcOdmetry.LEncCntDiff[0] + CalcOdmetry.LEncCntDiff[1])/4/WheelParam.EncCPR/WheelParam.GearRatio * PI*WheelParam.TyreDir_mm;
	//}
	
	// マシン速度(フィールド座標)
	float XDiff = (CalcOdmetry.dpos[0] * CalcOdmetry.CosTh[0] + CalcOdmetry.dpos[1] * CalcOdmetry.CosTh[1]) / 2;
	float YDiff = (CalcOdmetry.dpos[0] * CalcOdmetry.SinTh[0] + CalcOdmetry.dpos[1] * CalcOdmetry.SinTh[1]) / 2;		// [mm]
	MachinePos.dX = XDiff / SAMPLETIME_CTRL_S;	
	MachinePos.dY = YDiff / SAMPLETIME_CTRL_S;	// [mm/s]
	// マシン位置
	MachinePos.X = MachinePos.X + XDiff;
	MachinePos.Y = MachinePos.Y + YDiff;		// [mm]
	
	// マシン速度
	MachineSpd = CalcOdmetry.dpos[0]/SAMPLETIME_CTRL_S;	// [mm/s]
	// マシン方向
	float NormalizedTh = MachinePos.getNormalizeTh();
	if((NormalizedTh < PI/4)&&(NormalizedTh >= -PI/4))	this->MachineOrientation = ORIENT_EAST;
	else if((NormalizedTh < PI*3/4)&&(NormalizedTh >= PI/4))	this->MachineOrientation = ORIENT_NORTH;
	else if((NormalizedTh >= PI*3/4)||(NormalizedTh < -PI*3/4))	this->MachineOrientation = ORIENT_WEST;
	else if((NormalizedTh < -PI/4)&&(NormalizedTh >= -PI*3/4))	this->MachineOrientation = ORIENT_SOUTH;
}



/*********************
走行制御
概要：
 PID制御で、直進成分±回転成分のDutyを出力する。
 Dutyは、signedのプラスマイナスで前進後進を表す。
**********************/
void roomba_grsakura_t::ctrlRun(float SpdTgt_mmps, float AngVelTgt_radps, int16_t* DutyR, int16_t* DutyL, bool_t ctrlEnable){
	float RTireSpdMes_rps, LTireSpdMes_rps;
	float RTireSpdTgt_rps, LTireSpdTgt_rps;
	int16_t PidOutTmp[2];
	
	
	// 各タイヤの現在速度
	if(EnableImu && OdoImu->isCalibrated() && !OdoImu->isComuErr()){	// ジャイロキャリブ済み, IMU問題なし
		// IMUで計測した角速度と，別途計測した速度からタイヤ角速度を計算
		RTireSpdMes_rps = (MachineSpd + WheelParam.Tread_mm * MachinePos.dTh / 2.0F) / (PI * WheelParam.TyreDir_mm);
		LTireSpdMes_rps = (MachineSpd - WheelParam.Tread_mm * MachinePos.dTh / 2.0F) / (PI * WheelParam.TyreDir_mm);
	}else{
		RTireSpdMes_rps = RTireSpd_rps;	// エンコーダから計算した回転数
		LTireSpdMes_rps = LTireSpd_rps;
	}
	
	// 各タイヤの目標速度
	//  目標直進速度[mm/s]と角速度[rad/s]から各タイヤの目標角速度[r/s]を求める
	RTireSpdTgt_rps = (SpdTgt_mmps + AngVelTgt_radps*(WheelParam.Tread_mm/2.0f))/(PI*WheelParam.TyreDir_mm);
	LTireSpdTgt_rps = (SpdTgt_mmps - AngVelTgt_radps*(WheelParam.Tread_mm/2.0f))/(PI*WheelParam.TyreDir_mm);
	
	
	// 最大速度制限
	RTireSpdTgt_rps = (ROI_MAX_SPD_RPS < RTireSpdTgt_rps)?ROI_MAX_SPD_RPS:RTireSpdTgt_rps;
	LTireSpdTgt_rps = (ROI_MAX_SPD_RPS < LTireSpdTgt_rps)?ROI_MAX_SPD_RPS:LTireSpdTgt_rps;
	RTireSpdTgt_rps = (-ROI_MAX_SPD_RPS > RTireSpdTgt_rps)?(-ROI_MAX_SPD_RPS):RTireSpdTgt_rps;
	LTireSpdTgt_rps = (-ROI_MAX_SPD_RPS > LTireSpdTgt_rps)?(-ROI_MAX_SPD_RPS):LTireSpdTgt_rps;
	
	
	// フィードバック制御
	if(false != ctrlEnable){
		// PID制御
		PidOutTmp[0] = (int16_t)PidCtrlRTire->calc(RTireSpdTgt_rps - RTireSpdMes_rps);
		PidOutTmp[1] = (int16_t)PidCtrlLTire->calc(LTireSpdTgt_rps - LTireSpdMes_rps);
		
		*DutyR += PidOutTmp[0];
		*DutyL += PidOutTmp[1];
		
		if(*DutyR>255){
			*DutyR = 255;
		}else if(*DutyR< (-255)){
			*DutyR = -255;
		}
		if(*DutyL>255){
			*DutyL = 255;
		}else if(*DutyL< (-255)){
			*DutyL = -255;
		}
	}else{
		// モーター出力無効時は、PID制御にリセットをかけ、目標デューティーを0とする。
		PidCtrlRTire->resetStatus(RTireSpdTgt_rps - RTireSpdMes_rps);
		PidCtrlLTire->resetStatus(LTireSpdTgt_rps - LTireSpdMes_rps);
		*DutyR = 0;
		*DutyL = 0;
	}
	
}

/*********************
モーター出力(PWM duty)
概要：
 Duty正負(+-255)に応じて正転逆転を切り替える。
 fMotorOutがfalseの時、モーターをフリー状態とする。
**********************/
void roomba_grsakura_t::outTyres(int16_t RTireDutyTgt, int16_t LTireDutyTgt, bool_t fMotorOut){
	
	if(fMotorOut){
		// ルンバ的にはバックなので逆にする
		Rmb.drivePwm(-LTireDutyTgt, -RTireDutyTgt);
	}else{
		Rmb.drivePwm(0, 0);	// 止まれ！
	}
	
}

/*********************
走行制御
**********************/

// 速度[mm/s]，角速度[rad/s]
void roomba_grsakura_t::setTgtSpdAng(float Spd, float AngVel){
	SpdTgt = Spd;
	AngVelTgt = AngVel;
};



// ネタ系アプリ
// ルンバでお掃除
// Vacume, SideBrush
int8_t roomba_grsakura_t::startClean(void){
	if(false != fCleaning){ return -1;};
	fCleaning = true;
	return Rmb.sendCommand(ROI_OPCODE_MOTORS, (uint8_t)0x03);
};

int8_t roomba_grsakura_t::stopClean(void){
	if(false == fCleaning){ return -1;};
	fCleaning = false;
	return Rmb.sendCommand(ROI_OPCODE_MOTORS, (uint8_t)0x00);
};

// PS3コンで操作
void roomba_grsakura_t::drivePs3Con(void){
	int16_t outDutyR,outDutyL;	// -255 ~ 255
	float RateLimit;
	
	if(Sbdbt.isPs3ConAvailable()){
		
		// 掃除モーター駆動on/off
		// STARTボタン(立ち上がり)
		if(Sbdbt.ControllerData.button.Start && !Ps3ConDataLast.button.Start){
			if(isCleaning()){
				stopClean();
			}else{
				startClean();
			}
		}
		// Fullモードへ
		// ○ボタン(立ち上がり)
		if(Sbdbt.ControllerData.button.Circle && !Ps3ConDataLast.button.Circle){
			Rmb.setMode(ROI_MODE_PASSIVE);
			Rmb.setMode(ROI_MODE_FULL);
		}
		// パッシブモードへ
		// Selectボタン(立ち上がり)
		if(Sbdbt.ControllerData.button.Select && !Ps3ConDataLast.button.Select){
			Rmb.setMode(ROI_MODE_PASSIVE);
		}
		// お家へ帰る
		// □ボタン(立ち上がり)
		if(Sbdbt.ControllerData.button.Square && !Ps3ConDataLast.button.Square){
			Rmb.sendCommand(ROI_OPCODE_FORCE_SEEKING_DOG);
		}
		
		
		
		
		// 左右スティックで走る(+-128 -> +-255)
		//outDutyL = ((int16_t)0x0080 - (int16_t)Sbdbt.ControllerData.button.LStickVertical)*2;
		//outDutyR = ((int16_t)0x0080 - (int16_t)Sbdbt.ControllerData.button.RStickVertical)*2;
		
		
		int16_t Straight = 2 * ((int16_t)0x0080 - (int16_t)Sbdbt.ControllerData.button.RStickVertical);
		int16_t Turn = ((int16_t)0x0080 - (int16_t)Sbdbt.ControllerData.button.LStickHrizontal);
		
		if((Straight<20) && (Straight>-20)){
			Straight = 0;
		}
		if((Turn<30) && (Turn>-30)){
			Turn = 0;
		}
		
		// レートリミット
		// 強くボタンを押しているほど速く目標値に達する
		// 255のとき、0.5秒(25カウント)で255になる。
		// 最低でも1秒(50カウント)で止まるようにする
		RateLimit = abs(Ps3StrOut - Straight) / 25.0F;
		if(RateLimit < (255 / 50.0F)){
			RateLimit = 255 / 50.0F;
		}
		
		if(Ps3StrOut > (Straight + RateLimit)){
			Ps3StrOut -= RateLimit;
		}else if((Ps3StrOut + RateLimit) < Straight){
			Ps3StrOut += RateLimit;
		}else{
			Ps3StrOut = Straight;
		}
		
		// 旋回優先
		if((Ps3StrOut + Turn)>255){
			Ps3StrOut -= (Ps3StrOut + Turn - 255);
		}
		if((Ps3StrOut + Turn)< -255){
			Ps3StrOut += (Ps3StrOut + Turn + 255);
		}
		
		
		outDutyR = Ps3StrOut + Turn;
		outDutyL = Ps3StrOut - Turn;
		/*
		uint16_t Amari = 0;
		if(outDutyR>255){
			Amari += outDutyR-255;
		}
		if(outDutyR<-255){
			Amari += outDutyR+255;
		}
		if(outDutyL>255){
			Amari -= outDutyL-255;
		}
		if(outDutyL<-255){
			Amari -= outDutyL+255;
		}
		outDutyR -= Amari;
		outDutyL -= Amari;
		*/
		// 最大値
		outDutyR = outDutyR > 255 ? 255 : outDutyR;
		outDutyR = outDutyR < -255 ? -255 : outDutyR;
		outDutyL = outDutyL > 255 ? 255 : outDutyL;
		outDutyL = outDutyL < -255 ? -255 : outDutyL;
		
		// 中央に不感帯
		if((outDutyR<30) && (outDutyR>-30)){
			outDutyR = 0;
		}
		if((outDutyL<30) && (outDutyL>-30)){
			outDutyL = 0;
		}
		
		outTyres(outDutyR, outDutyL, 1);
		
		// 今回のボタン情報を記憶(立ち上がり判定のため)
		Ps3ConDataLast = Sbdbt.ControllerData;
	}else{
		outTyres(0, 0, 0);
	}
}


// 1ms割り込み
void roomba_grsakura_t::isr1ms(void){
	GR_cnt();
	
	fExeReqTask_1ms = true;	// 1msごと
	
	if(CntTask_20ms<19){		// 20msごと
		CntTask_20ms ++;
	}else{
		CntTask_20ms = 0;
		fExeReqTask_20ms = true;
	}
	
	if(CntTask_100ms<99){	// 100msごと
		CntTask_100ms++;
	}else{
		CntTask_100ms = 0;
		fExeReqTask_100ms = true;
	}

}

// 1msごとの割り込み設定
// TMR0
// 割り込み周期 46 / (PCLK/1024) = 0.981msec
void roomba_grsakura_t::setup1msInterrupt(void){
	uint16_t SamplingFreq = 1000;

	SYSTEM.PRCR.WORD = 0xA503u;		// 書き込み許可
	MSTP(TMR0) = 0;					// モジュール起動
	
	TMR0.TCR.BYTE = 0x00;
	TMR0.TCR.BIT.CCLR = 1;			// コンペアマッチAでカウンタクリア
	TMR0.TCR.BIT.CMIEA = 1;			// コンペアマッチA割り込み許可(CMIAn)
	
	
	TMR0.TCSR.BYTE = 0x00;
	
	
	// カウンタ
	uint16_t cnt = PCLK/1024/SamplingFreq;
	TMR0.TCNT = 0;
	TMR0.TCORA = cnt;		//コンペアマッチ用(使わない)
	TMR0.TCORB = cnt+1;		//コンペアマッチ用(使わない)
	
	// 割り込み許可
	IEN(TMR0,CMIA0) = 1;
	IPR(TMR0,CMIA0) = 2;
	
	// クロック設定
	TMR0.TCCR.BYTE = 0x00;
	TMR0.TCCR.BIT.CKS = 0x05;	// クロック選択			PCLK/1024
	TMR0.TCCR.BIT.CSS = 0x01;	// クロックソース選択 分周クロック
	
	// 動作止めるのはこれしかない？
	//TMR0.TCCR.BIT.CKS = 0x00;
	//TMR0.TCCR.BIT.CSS = 0x00;	
}

/*************
TPU6を使ったフリーカウンタ。
時間計測用
16bit, PCLK/1 -> 48MHz/64 = 750kHz
タイマカウンタ : TPU6.TCNT
オーバーフローすると TPU6.TSR.BIT.TCFVが立つ
*************/
void roomba_grsakura_t::setupFreeCounter(void){
	MSTP(TPU6) = 0;					// モジュール起動
	TPU6.TCR.BIT.TPSC = 3;	// PCLK/4
	TPU6.TCR.BIT.CKEG = 0;	// 立ち上がりエッジでカウント
	TPU6.TCR.BIT.CCLR = 0;	// クリアなし
	TPU6.TMDR.BIT.MD = 0;	// 通常動作
	
	TPUB.TSTR.BIT.CST6 = 1;	// カウント開始
	
}






