/********************************************/
/*				カラー液晶関数				*/
/*					for RX621				*/
/*					Wrote by conpe_			*/
/*							2014/07/01		*/
/********************************************/

// aitendoカラー液晶ZYFGD用関数です。

#ifndef ZYFGDBUFF
#define ZYFGDBUFF

#include <CommonDataType.h>
//#include <HardwareSetting.h>

void initST7735S(void);
void initPortST7725S(void);
void writeCommandST7725S(uint8_t com);
void writeDataST7725S(uint8_t data);
void writeDataBurstST7725S(uint8_t data);




#endif
