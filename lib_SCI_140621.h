/********************************************/
/*				SCI1関数					*/
/*					for SH7125 @ HEW		*/
/*					Wrote by conpe_			*/
/*							2011/03/26		*/
/********************************************/

//SCI1用の送受信関数です。

// バッファを使って順次割り込みにより送信
// intprg.cのINT_SCI1_TXI1からisrTxBuffSCI(1)を呼び出してね。
// intprg.cのINT_SCI0_TXI0からisrTxBuffSCI(0)を呼び出してね。
// resetprg.cの#define SR_Initは0x00000000にする

// バッファがあふれそうになったら、空くまで待つので注意。


#ifndef SCIBUFF
#define SCIBUFF

	#include <stdarg.h>	//可変長引数
	#include <stdio.h>


#include <CommonDataType.h>


#define DMA		0
#define NONE	1


#define SCI0_OFF		0
#define SCI0A_ENABLE	1
#define SCI1_OFF		2
#define SCI1A_ENABLE	3
#define SCI1B_ENABLE	4
#define SCI2_OFF		5
#define SCI2A_ENABLE	6
#define SCI2B_ENABLE	7
#define SCI3_OFF		8
#define SCI3A_ENABLE	9
#define SCI3B_ENABLE	10
#define SCI5_OFF		11
#define SCI5A_ENABLE	12
#define SCI6_OFF		13
#define SCI6A_ENABLE	14
#define SCI6B_ENABLE	15

#define SCINum0	0
#define SCINum1	1
#define SCINum2	2
#define SCINum3	3
#define SCINum5	5
#define SCINum6	6


// SCIステータス
typedef struct scistate{
	int Enable			:2;		// 0:disable, 1:A, 2:B
	int TxdEnable		:1;
	int RxdEnable		:1;
	int TxdDmaEnable	:1;
	int RxdDmaEnable	:1;
} scistate;

// 各バッファ
typedef struct scibuff{
	unsigned int BuffSize;	// バッファサイズ
	unsigned int WriteP;	// 書き込みポインタ (次書くとこ)
	unsigned int ReadP;		// 読み込みポインタ (次読むとこ)
	uint8_t *pData;				// バッファへのポインタ
}scibuff;

typedef struct scibufftxrx{
	scibuff Tx;
	scibuff Rx;
}scibufftxrx;


/*********************
SCI初期化
引数：ボーレート 4800〜115200
**********************/
void initSCI(uint8_t SCIEnable, unsigned long BaudRate);

/*********************
SCI有効化
引数：

**********************/
void enableTxSCI(uint8_t scinum);

/*********************
SCI TX無効化
引数：

**********************/
void disableTxSCI(uint8_t scinum);

/*********************
SCI1 文字送信
引数：なし
返値：なし
TDREの割り込みによって動作
**********************/
void isrTxBuffSCI(uint8_t scinum);

/*********************
SCI1 bufferを読む
引数：なし
返値：文字 バッファがないときは0x15
**********************/
static int8_t readTxBuffSCI(uint8_t scinum, uint8_t *data);

/*********************
SCI1 bufferに書く
引数：文字
返値：0x00(正常), 0x15(バッファいっぱい)
**********************/
static int8_t writeTxBuffSCI(uint8_t scinum, uint8_t moji);

/*********************
SCI1 printf風
引数：文字列, 変数

対応変換： %c,%s,%d,%x,%X
**********************/
uint16_t printfSCI(uint8_t scinum, uint8_t *, ...);

/*********************
SCI1 printf風 バッファいっぱいだったら送信しない
引数：文字列, 変数

対応変換： %c,%s,%d,%x,%X
**********************/
//void SCI_printf_break(uint8_t scinum, uint8_t *, ...);

/*********************
SCI1 1文字送信
引数：文字
返値：バッファがいっぱいだったら0x15
**********************/
int8_t txCharSCI(uint8_t scinum, uint8_t moji);
int8_t txCharSCI0(uint8_t moji);
int8_t txCharSCI1(uint8_t moji);

/*********************
SCI1 1文字送信
引数：文字
返値：バッファがいっぱいだったら0x15
**********************/
int8_t txCharBreakSCI(uint8_t scinum, uint8_t ch);
int8_t txCharBreakSCI1(uint8_t ch);
int8_t txCharBreakSCI0(uint8_t ch);

/*********************
SCI1 1文字送信  バッファを使わない(送信あくまで待つ)
引数：文字
**********************/
int8_t txCharNoBuffSCI(uint8_t scinum, uint8_t ch);
int8_t txCharNoBuffSCI0(uint8_t ch);
int8_t txCharNoBuffSCI1(uint8_t ch);

/*********************
SCI1 文字列送信
引数：文字列
**********************/
int8_t txStrSCI(uint8_t scinum, uint8_t *str);

/*********************
SCI1 10進数字送信
引数：数字,桁数
返値：なし
longまでの数
**********************/
int8_t txDecSCI(uint8_t scinum, long d);

/*********************
SCI1 16進数字送信(小文字)
引数：数字,桁数
返値：なし
longまでの数
**********************/
int8_t txHexSCI(uint8_t scinum, long x);

/*********************
SCI1 16進数字送信(大文字)
引数：数字,桁数
返値：なし
longまでの数
**********************/
int8_t txHEXSCI(uint8_t scinum, long X);


/*********************
SCI1 バッファポインタのインクリメント
引数：今のポインタ
返値：次のポインタ
**********************/
static uint16_t incTxBuffP(uint8_t scinum, uint16_t p);


/*********************
SCI1 送信バッファがクリアされるまで待つ
引数：SCInum
返値：なし
**********************/
void waitTxEndSCI(uint8_t scinum);




///////////////////////////////////////////
// 受信
///////////////////////////////////////////

/*********************
SCI 1文字受信
引数：
	uint8_t scinum : SCIモジュール番号
返値：
	受信データ
来るまで待つので注意
**********************/
int8_t rxCharSCI(uint8_t scinum, uint8_t *data); //printf

#define SCI0_rx_char() rxCharSCI(0)
#define SCI1_rx_char() rxCharSCI(1)

/*********************
SCI 1文字受信
引数：
	uint8_t scinum : SCIモジュール番号
返値：
	受信データ(来てなければ0x15(NAK))
**********************/
int8_t rxCharBreakSCI(uint8_t scinum, uint8_t *data);

	


/*********************
SCI 受信bufferに書く
引数：文字
返値：0x00(正常), 0x15(バッファいっぱい)
**********************/
static int8_t writeRxBuffSCI(uint8_t scinum, uint8_t moji);


/*********************
SCI1 rx_bufferを読む
引数：
	uint8_t scinum : SCI番号
返値：
	受信データ バッファがないときは0x15(NAK)
**********************/
static int8_t readRxBuffSCI(uint8_t scinum, uint8_t *data);


/*********************
SCI1 1文字受信
引数：なし
返値：文字
来るまで待つので注意
**********************/
int8_t rxCharNoBuffSCI(uint8_t scinum, uint8_t *data);

/*********************
SCI1 1文字受信(待ちなし)
引数：なし
返値：文字
呼び出しもとで来てるか確認する必要あり
**********************/
int8_t rxCharBreakNoBuffSCI(uint8_t scinum, uint8_t *data);

/*********************
SCI1 複数ケタ数字受信
引数：なし
返値：数
enter押すまで読み続け。
unsigned intまでの数
**********************/
uint16_t rxDecSCI(uint8_t scinum);
/*********************
SCI1 複数ケタ数字受信
引数：なし
返値：数
enter押すまで読み続け。
unsigned longまでの数
**********************/
uint32_t rxLongDecSCI(uint8_t scinum);


/*********************
SCI 受信バッファポインタのインクリメント
引数：今のポインタ
返値：次のポインタ
**********************/
static uint16_t incRxBuffP(uint8_t scinum, uint16_t p);


/*********************
SCI ここまでの受信データを捨てる
引数：
	uint8_t scinum : SCIモジュール番号
返値：
	なし
**********************/
void purgeRxBufferSCI(uint8_t scinum);



/*********************
SCI 送信バッファが空かチェック
引数：
	uint8_t scinum : SCIモジュール番号
返値：
	1 : 空
	0 : 空じゃない
**********************/
bool_t isEmptyTxBuffSCI(uint8_t scinum);

/*********************
SCI 受信バッファが空かチェック
引数：
	uint8_t scinum : SCIモジュール番号
返値：
	1 : 空
	0 : 空じゃない
**********************/
bool_t isEmptyRxBuffSCI(uint8_t scinum);


/*********************
SCI 送信バッファに溜ってるデータ数
引数：
	uint8_t scinum : SCIモジュール番号
返値：
	int : データ数
**********************/
uint16_t countTxBuffDataSCI(uint8_t scinum);




/*********************
SCI 受信割り込み
引数：なし
返値：なし
TDREの割り込みによって動作
**********************/
void isrRxBuffSCI(uint8_t scinum);


/*********************
DMA0 割り込み
引数：
	なし
返値：
	なし
SCI0送信用
バッファにデータがなかったら割り込み終わり	
データあったらセットしなおして送信。
**********************/
/*
void isrDMAC_CMAC0I(void);	// TXD0用
void isrDMAC_CMAC1I(void);	// RXD0用
void isrDMAC_CMAC2I(void);	// TXD1用
void isrDMAC_CMAC3I(void);	// RXD1用
*/


#endif
