/********************************************/
/*				カメラOV7670関数			*/
/*					for RX621				*/
/*					Wrote by conpe_			*/
/*							2014/07/26		*/
/********************************************/


// 【仕様】
// コントローラ：OV7670


//【更新履歴】
// Ver.140726
//  2014.07.26 書き始め


//【使い方】
// initPortCamOV7670();をできるだけ早く実行
// initCamOV7670();を実行


/*
HardWareSetting.h内で指定するもの

// -------- lib_CamOV7670 --------
#include <lib_CamOV7670_140726.h>

// 入力
#define OV7670_HREFF	PORT5.PORT.BIT.B2
#define OV7670_VSYNC	PORT3.PORT.BIT.B1
#define OV7670_PCLK		PORT3.PORT.BIT.B2
#define OV7670_D0		PORTB.PORT.BIT.B0
#define OV7670_D1		PORTB.PORT.BIT.B1
#define OV7670_D2		PORTB.PORT.BIT.B2
#define OV7670_D3		PORTB.PORT.BIT.B3
#define OV7670_D4		PORTB.PORT.BIT.B4
#define OV7670_D5		PORTB.PORT.BIT.B5
#define OV7670_D6		PORTB.PORT.BIT.B6
#define OV7670_D7		PORTB.PORT.BIT.B7
// 入出力方向設定
#define OV7670_HREFF_DDR	PORT5.DDR.BIT.B2
#define OV7670_VSYNC_DDR	PORT3.DDR.BIT.B1
#define OV7670_PCLK_DDR		PORT3.DDR.BIT.B2
#define OV7670_D0_DDR		PORTB.DDR.BIT.B0
#define OV7670_D1_DDR		PORTB.DDR.BIT.B1
#define OV7670_D2_DDR		PORTB.DDR.BIT.B2
#define OV7670_D3_DDR		PORTB.DDR.BIT.B3
#define OV7670_D4_DDR		PORTB.DDR.BIT.B4
#define OV7670_D5_DDR		PORTB.DDR.BIT.B5
#define OV7670_D6_DDR		PORTB.DDR.BIT.B6
#define OV7670_D7_DDR		PORTB.DDR.BIT.B7
// 入力バッファ設定
#define OV7670_HREFF_ICR	PORT5.ICR.BIT.B2
#define OV7670_VSYNC_ICR	PORT3.ICR.BIT.B1
#define OV7670_PCLK_ICR		PORT3.ICR.BIT.B2
#define OV7670_D0_ICR		PORTB.ICR.BIT.B0
#define OV7670_D1_ICR		PORTB.ICR.BIT.B1
#define OV7670_D2_ICR		PORTB.ICR.BIT.B2
#define OV7670_D3_ICR		PORTB.ICR.BIT.B3
#define OV7670_D4_ICR		PORTB.ICR.BIT.B4
#define OV7670_D5_ICR		PORTB.ICR.BIT.B5
#define OV7670_D6_ICR		PORTB.ICR.BIT.B6
#define OV7670_D7_ICR		PORTB.ICR.BIT.B7

#define DDR_IN 0
#define DDR_OUT 1

#define OV7670_DEVICE_ADDRESS	0x42
#define OV7670_I2C_CHNO	0

*/

/********************************************************************************************************************
Includes <System Includes> , "Project Includes"
*********************************************************************************************************************/
#include <machine.h>
#include <string.h>
#include <stddef.h>
#include "iodefine.h"

#include <HardwareSetting.h>
#include <CommonDataType.h>


/********************************************************************************************************************
Macro definitions
*********************************************************************************************************************/
/* IIC Information */
//#define CH_NO               (uint8_t)(0)                        /* IIC channel no.                                  */  /** SET **/
//#define SLVADR              (uint8_t)(0xA0)                     /* Slave address for EEPROM                         */  /** SET **/
#define INDEX_SLVADR        (uint8_t)(1)                        /* Index of slave address filed                     */  /** SET **/
//#define INDEX_DATA1ST       (uint8_t)(2)                        /* Index of 1st data filed                          */  /** SET **/
//#define INDEX_DATA2ND       (uint32_t)(3)                       /* Index of 2nd data filed                          */  /** SET **/



/********************************************************************************************************************
Typedef definitions
*********************************************************************************************************************/


/********************************************************************************************************************
Exported global variables (to be accessed by other files)
*********************************************************************************************************************/
void tsetmain(void);

#ifdef RIIC0_ENABLE
r_iic_drv_info_t            g_iic_Info_ch0;                     /* Channel 0 IIC driver information                 */
#endif /* #ifdef RIIC0_ENABLE */
#ifdef RIIC1_ENABLE
r_iic_drv_info_t            g_iic_Info_ch1;                     /* Channel 1 IIC driver information                 */
#endif /* #ifdef RIIC1_ENABLE */
#ifdef RIIC2_ENABLE
r_iic_drv_info_t            g_iic_Info_ch2;                     /* Channel 2 IIC driver information                 */
#endif /* #ifdef RIIC2_ENABLE */
#ifdef RIIC3_ENABLE
r_iic_drv_info_t            g_iic_Info_ch3;                     /* Channel 3 IIC driver information                 */
#endif /* #ifdef RIIC3_ENABLE */


// カメラバッファ
uint8_t CamData[CAMDATA_HEIGHT][CAMDATA_WIDTH*2];

/********************************************************************************************************************
Inported global variables
*********************************************************************************************************************/


/********************************************************************************************************************
Private global variables and functions
*********************************************************************************************************************/
static void trap(void);
static void DriverInitI2C(r_iic_drv_info_t * pIic_info);
static void MasterTxI2C(r_iic_drv_info_t * pIic_info);
static void MasterRxI2C(r_iic_drv_info_t * pIic_info, uint8_t SlaveAddress, uint8_t RcvDataNum, uint8_t *RcvData);
static void RecoveryI2C(r_iic_drv_info_t * pIic_info);
static void callbackW(void);
static void callbackR(void);
static void PushRIICInterruptI2C(r_iic_drv_info_t * pIic_info);
static void PopRIICInterruptI2C(r_iic_drv_info_t * pIic_info);

static uint8_t              gs_SlvAdr[INDEX_SLVADR];            /* Slave address field                              */
static error_t              gs_DevStatus;                       /* Device status flag field                         */
static volatile error_t     gs_Wflag;                           /* Transmission completion flag                     */
static volatile error_t     gs_Rflag;                           /* Reception completion flag                        */
static uint8_t              gs_EEI_Interrupt_Level;             /* EEI0 interrupt priority level Temporary          */
static uint8_t              gs_RXI_Interrupt_Level;             /* RXI0 interrupt priority level Temporary          */
static uint8_t              gs_TXI_Interrupt_Level;             /* TXI0 interrupt priority level Temporary          */
static uint8_t              gs_TEI_Interrupt_Level;             /* TEI0 interrupt priority level Temporary          */


/*********************
 OV7670 初期化
**********************/
void initCamOV7670(void){
	
	InitDefaultReg();

	InitRGB565();
	 
	InitQQVGA();
	
	// DMA設定
	initDmaCamOV7670();
}

/*********************
 OV7670 ポート初期化
**********************/
void initPortCamOV7670(void){
	
	
    r_iic_drv_info_t        *Iic_info;                          /* IIC driver information pointer                   */
	
	
	OV7670_HREFF_DDR = DDR_IN;
	OV7670_VSYNC_DDR = DDR_IN;
	OV7670_PCLK_DDR = DDR_IN;
	OV7670_D0_DDR = DDR_IN;
	OV7670_D1_DDR = DDR_IN;
	OV7670_D2_DDR = DDR_IN;
	OV7670_D3_DDR = DDR_IN;
	OV7670_D4_DDR = DDR_IN;
	OV7670_D5_DDR = DDR_IN;
	OV7670_D6_DDR = DDR_IN;
	OV7670_D7_DDR = DDR_IN;
	
	//入力のバッファ有効化
	OV7670_HREFF_ICR = 1;
	OV7670_VSYNC_ICR = 1;
	OV7670_PCLK_ICR = 1;
	OV7670_D0_ICR = 1;
	OV7670_D1_ICR = 1;
	OV7670_D2_ICR = 1;
	OV7670_D3_ICR = 1;
	OV7670_D4_ICR = 1;
	OV7670_D5_ICR = 1;
	OV7670_D6_ICR = 1;
	OV7670_D7_ICR = 1;
	
	
    /* Selects using channel IIC Information structure. */
    switch (OV7670_I2C_CHNO)
    {
        case 0:
#ifdef RIIC0_ENABLE
            Iic_info = &g_iic_Info_ch0;			// グローバルのやつのアドレスセット
#endif /* #ifdef RIIC0_ENABLE */
        break;
        case 1:
#ifdef RIIC1_ENABLE
            Iic_info = &g_iic_Info_ch1;
#endif /* #ifdef RIIC1_ENABLE */
        break;
        case 2:
#ifdef RIIC2_ENABLE
            Iic_info = &g_iic_Info_ch2;
#endif /* #ifdef RIIC2_ENABLE */
        break;
        case 3:
#ifdef RIIC3_ENABLE
            Iic_info = &g_iic_Info_ch3;
#endif /* #ifdef RIIC3_ENABLE */
        break;
        default:
            /* Does nothing. */
        break;
    }

    memset(Iic_info, 0x00, sizeof(Iic_info));
	
	// I2C初期化
	DriverInitI2C(Iic_info);
}


	
/*********************
 OV7670 DMA初期化
**********************/
void initDmaCamOV7670(void){
	
	// DMAC 0 : フリーランニングモードで。
	// IRQ 2 : PCLK (DMAC0起動要因)
	// IRQ 1 : VSYNC割込 (転送先アドレスリセット)
	
	
	
	//DMAC起こす
	MSTP(DMAC0) = 0;


	// DMAC起動要因設定
	ICU.DMRSR0 = VECT(ICU,IRQ2);
	
	// DMA転送禁止
	DMAC0.DMCNT.BIT.DTE = 0;
	
	//転送先アドレス更新モード設定
	DMAC0.DMAMD.BIT.DM = 2;		// インクリメント
	
	//転送元アドレス更新モード設定
	DMAC0.DMAMD.BIT.SM = 0;		// アドレス固定
	
	//転送先アドレス拡張リピートエリア設定
	DMAC0.DMAMD.BIT.DARA = 0;	// 拡張リピート設定なし
	
	//転送元アドレス拡張リピートエリア設定
	DMAC0.DMAMD.BIT.SARA = 0;	// 拡張リピート設定なし
	
	
	// 転送要求選択
	DMAC0.DMTMD.BIT.DCTG = 1;	// 周辺モジュール, 外部割込みで転送要求
	
	// データ転送サイズ
	DMAC0.DMTMD.BIT.SZ = 0;		// 8bit転送	
	
	// リピート領域選択
	DMAC0.DMTMD.BIT.DTS = 2;	// 設定なし
	
	// 転送モード設定
	DMAC0.DMTMD.BIT.MD = 0;		// ノーマル転送
	
	// 転送元開始アドレス
	DMAC0.DMSAR = (void *)&PORTB.PORT.BYTE;
	
	
	DMAC0.DMCSL.BIT.DISEL = 0;	// IRQ2割込実行しない
	
	// ↓VSYNC割込毎に設定しよう
	
	// 転送先開始アドレス
	DMAC0.DMDAR = CamData;		//CamDataの頭から
	
	// 転送データ数
	DMAC0.DMCRA = 0;		// フリーランニングモード
	
	// ブロック転送数
	//DMAC0.DMCRB = ;
	
	// DMA転送終了割込許可
	DMAC0.DMINT.BIT.DTIE = 0;	// 割込なし
	
	// DMA転送エスケープ割込設定
	// 設定なし
	
	
	// DMAC動作
	DMAC.DMAST.BIT.DMST = 1;
	
	// DMAC転送許可
	DMAC0.DMCNT.BIT.DTE = 1;
	
	
	
	
	// IRQ設定
	
	// 割込検出設定
	ICU.IRQCR[2].BIT.IRQMD = 2;		//立ち上がりエッジで割込
	ICU.IRQCR[1].BIT.IRQMD = 2;		//立ち上がりエッジで割込
	
	// 割込優先度
	IPR(ICU,IRQ2) = 2;				//ICU.IPR DMAoff時にPCLK来ても割込はいらない
	IPR(ICU,IRQ1) = 15;				// DMA起動要因として使う場合はこの設定は不要
	
	//割込ステータスクリア
	IR(ICU,IRQ2) = 0;				//ICU.IR
	IR(ICU,IRQ1) = 0;
	// IRQ割込許可
	IEN(ICU, IRQ2) = 1;				//ICU.IER
	IEN(ICU, IRQ1) = 1;
	
}


/*********************
 OV7670 レジスタ読み込み
**********************/
error_t readRegCamOV7670(uint8_t RegID, uint8_t *RegData){

    r_iic_drv_info_t        *Iic_info;                          /* IIC driver information pointer                   */
    
    /* Selects using channel IIC Information structure. */
    switch (OV7670_I2C_CHNO)
    {
        case 0:
#ifdef RIIC0_ENABLE
            Iic_info = &g_iic_Info_ch0;			// グローバルのやつのアドレスセット
#endif /* #ifdef RIIC0_ENABLE */
        break;
        case 1:
#ifdef RIIC1_ENABLE
            Iic_info = &g_iic_Info_ch1;
#endif /* #ifdef RIIC1_ENABLE */
        break;
        case 2:
#ifdef RIIC2_ENABLE
            Iic_info = &g_iic_Info_ch2;
#endif /* #ifdef RIIC2_ENABLE */
        break;
        case 3:
#ifdef RIIC3_ENABLE
            Iic_info = &g_iic_Info_ch3;
#endif /* #ifdef RIIC3_ENABLE */
        break;
        default:
            /* Does nothing. */
        break;
    }
	
	
	
	// 読み込みアドレスの書き込み
    
    Iic_info->pSlvAdr      = gs_SlvAdr;
    Iic_info->Cnt1st       = 0;				// 1stデータのみの送信はできない模様。
    Iic_info->pData1st     = NULL;			// 1byteだけ送りたいなら2ndデータを使う。
    Iic_info->Cnt2nd       = 1;
    Iic_info->pData2nd     = &RegID;
    Iic_info->CallBackFunc = callbackW;

    // Set Data
    gs_SlvAdr[0]            = OV7670_DEVICE_ADDRESS;
	MasterTxI2C(Iic_info);
	
	// 読み込み
    Iic_info->CallBackFunc = callbackR;	// 通信終了時に読み出す関数
	MasterRxI2C(Iic_info, OV7670_DEVICE_ADDRESS, 1, RegData);
	
	
	/*
	// スレーブアドレス、アドレス1byte送って再スタート、スレーブアドレス送信、1バイト受信
	//サンプルではアドレス送った後ストップして、スタート、スレーブアドレス送信からやってる。
	
    // Set Data 
	
    Iic_info->pSlvAdr      = gs_SlvAdr;	// スレーブアドレス
    Iic_info->Cnt1st       = 1;			// 読み込みアドレス長さ
    Iic_info->pData1st     = &RegID;		// 読込みアドレス
    Iic_info->Cnt2nd       = 1;			// 1バイト読み込み
    Iic_info->pData2nd     = RegData;		// 読み込みデータ格納アドレス
    Iic_info->CallBackFunc = callbackR;	// 通信終了時に読み出す関数
	
	
    // Set Data 
    *gs_SlvAdr			= OV7670_DEVICE_ADDRESS;
	MasterTRxI2C(Iic_info);
	*/
	return gs_Rflag;
}


/*********************
 OV7670 レジスタ書き込み
**********************/
error_t writeRegCamOV7670(uint8_t RegID, uint8_t RegData){

    r_iic_drv_info_t        *Iic_info;                          /* IIC driver information pointer                   */
    
    /* Selects using channel IIC Information structure. */
    switch (OV7670_I2C_CHNO)
    {
        case 0:
#ifdef RIIC0_ENABLE
            Iic_info = &g_iic_Info_ch0;			// グローバルのやつのアドレスセット
#endif /* #ifdef RIIC0_ENABLE */
        break;
        case 1:
#ifdef RIIC1_ENABLE
            Iic_info = &g_iic_Info_ch1;
#endif /* #ifdef RIIC1_ENABLE */
        break;
        case 2:
#ifdef RIIC2_ENABLE
            Iic_info = &g_iic_Info_ch2;
#endif /* #ifdef RIIC2_ENABLE */
        break;
        case 3:
#ifdef RIIC3_ENABLE
            Iic_info = &g_iic_Info_ch3;
#endif /* #ifdef RIIC3_ENABLE */
        break;
        default:
            /* Does nothing. */
        break;
    }
	
	
	
	// 読み込みアドレスの書き込み
    
    Iic_info->pSlvAdr      = gs_SlvAdr;
    Iic_info->Cnt1st       = 1;				// 1stデータのみの送信はできない模様。
    Iic_info->pData1st     = &RegID;			// 1byteだけ送りたいなら2ndデータを使う。
    Iic_info->Cnt2nd       = 1;
    Iic_info->pData2nd     = &RegData;
    Iic_info->CallBackFunc = callbackW;

    // Set Data
    gs_SlvAdr[0]            = OV7670_DEVICE_ADDRESS;
	
	// 書き込み
	MasterTxI2C(Iic_info);
	
	
	
	return gs_Rflag;
}

/*********************
 OV7670 レジスタ書き込み
**********************/
void printRegCamOV7670(uint8_t SciNum){
	uint8_t CntPrint;
	uint8_t *RcvData;
	
	printfSCI(SciNum, "adrs : +0 +1 +2 +3 +4 +5 +6 +7 +8 +9 +A +B +C +D +E +F");
	for(CntPrint=0;CntPrint<REGNUM_OV7670;CntPrint++){
		if(CntPrint%0x10 == 0){
			printfSCI(SciNum, "\r\n0x%1X0 : ", CntPrint/0x0f);
		}
		readRegCamOV7670(CntPrint, RcvData);
		printfSCI(SciNum, "%02X ", *RcvData);
		
	}
}



/********************************************************************************************************************
* Outline      : Recovery error processing
* Function Name: trap
* Description  : Recovery error
* Arguments    : None
* return Value : None
*********************************************************************************************************************/
static void trap(void)
{
	
	printfSCI(SCINum1, "Error@lib_Cam\r\n");
    //while(1);
}


/********************************************************************************************************************
* Outline      : Initialization processing
* Function Name: DriverInitSample
* Description  : Initializes the RIIC driver sample.
* Arguments    : None
* return Value : None
*********************************************************************************************************************/
void DriverInitI2C(r_iic_drv_info_t * pIic_info)
{
    error_t                 ret;

    /* Sets IIC Information. */
    pIic_info->ChNo          = OV7670_I2C_CHNO;
    pIic_info->pDevStatus    = &gs_DevStatus;                    /* Device status flag buffer.                       */

    /* Sets status flag. */
    g_iic_ChStatus[pIic_info->ChNo] = R_IIC_NO_INIT;
    *(pIic_info->pDevStatus)        = R_IIC_NO_INIT;
    
    /* Calls initialization processing. */
    ret =  R_IIC_Drv_Init(pIic_info);
    if (R_IIC_IDLE != ret)
    {
        RecoveryI2C(pIic_info);
    }
}




/********************************************************************************************************************
* Outline      : Master transmission pattern 1 processing
* Function Name: MasterTxPattern1Sample
* Description  : Master transmission pattern 1 sample.
* Arguments    : None
* return Value : None
*********************************************************************************************************************/
static void MasterTxI2C(r_iic_drv_info_t * pIic_info)
{
    error_t                 ret;
    volatile uint32_t       cnt;


    gs_Wflag = COMU_INIT;
    PushRIICInterruptI2C(pIic_info);
    ret =  R_IIC_Drv_MasterTx(pIic_info);
    if (R_IIC_COMMUNICATION != ret)
    {
        gs_Wflag = COMU_ERR;
    }
    PopRIICInterruptI2C(pIic_info);

    while(COMU_INIT == gs_Wflag);                             /* Wait transmission completion.                    */
    if (gs_Wflag != COMU_FINISH)
    {
        RecoveryI2C(pIic_info);
    }

    for (cnt=0; cnt<100000; cnt++);                             /* Wait EEPROM writing completion.                  */  /** SET **/
}


/********************************************************************************************************************
* Outline      : Master reception processing
* Function Name: MasterRxSample
* Description  : Master reception sample.
* Arguments    : None
* return Value : None
*********************************************************************************************************************/
static void MasterRxI2C(r_iic_drv_info_t * pIic_info, uint8_t SlaveAddress, uint8_t RcvDataNum, uint8_t *RcvData)
{
    error_t                 ret;
    volatile uint32_t       cnt;

	
	
    /*** Performs master reception. ***/
    pIic_info->pSlvAdr      = gs_SlvAdr;
    pIic_info->Cnt1st       = 0;
    pIic_info->pData1st     = NULL;
    pIic_info->Cnt2nd       = RcvDataNum;
    pIic_info->pData2nd     = RcvData;
    pIic_info->CallBackFunc = callbackR;

    /* Set Data */
    gs_SlvAdr[0]            = SlaveAddress;
	
	
    gs_Rflag = COMU_INIT;
    PushRIICInterruptI2C(pIic_info);
    ret =  R_IIC_Drv_MasterRx(pIic_info);
    if (R_IIC_COMMUNICATION != ret)
    {
        gs_Rflag = COMU_ERR;
    }
    PopRIICInterruptI2C(pIic_info);

    while(COMU_INIT == gs_Rflag);                             /* Wait reception completion.                       */
    if (gs_Rflag != COMU_FINISH)
    {
        RecoveryI2C(pIic_info);
    }
}



/********************************************************************************************************************
* Outline      : Recovery processing
* Function Name: RecoverySample
* Description  : Performs iic recovery.
* Arguments    : None
* return Value : None
*********************************************************************************************************************/
static void RecoveryI2C(r_iic_drv_info_t * pIic_info)
{
    error_t                 ret;

    ret = R_IIC_Drv_Reset(pIic_info);                            /* Driver reset processing                          */
    if (0 > ret)
    {
        trap();
    }
    
    ret = R_IIC_Drv_GenClk(pIic_info, 9);                        /* SCL clock generation processing                  */
    if (0 > ret)
    {
        trap();
    }

    ret = R_IIC_Drv_Init(pIic_info);                             /* Initialization processing                        */
    if (0 > ret)
    {
        trap();
    }

    /* Sets IIC Information. */
    pIic_info->pSlvAdr      = NULL;
    pIic_info->Cnt1st       = 0;
    pIic_info->pData1st     = NULL;
    pIic_info->Cnt2nd       = 0;
    pIic_info->pData2nd     = NULL;
    pIic_info->CallBackFunc = callbackW;

    gs_Wflag = COMU_INIT;
    PushRIICInterruptI2C(pIic_info);
    ret =  R_IIC_Drv_MasterTx(pIic_info);
    if (R_IIC_COMMUNICATION != ret)
    {
        trap();
    }
    PopRIICInterruptI2C(pIic_info);

    while(COMU_INIT == gs_Wflag);                             /* Wait transmission completion.                    */
    if (gs_Wflag != COMU_FINISH)
    {
        trap();
    }
}


/********************************************************************************************************************
* Outline      : Transmission callback processing
* Function Name: callbackW
* Description  : Transmission callback sample.
* Arguments    : None
* return Value : None
*********************************************************************************************************************/
void callbackW(void)
{
    r_iic_drv_info_t        *Iic_info;                          /* IIC driver information pointer                   */
    
    /* Check using channel. */
    switch (OV7670_I2C_CHNO)
    {
        case 0:
#ifdef RIIC0_ENABLE
            Iic_info = &g_iic_Info_ch0;
#endif /* #ifdef RIIC0_ENABLE */
        break;
        case 1:
#ifdef RIIC1_ENABLE
            Iic_info = &g_iic_Info_ch1;
#endif /* #ifdef RIIC1_ENABLE */
        break;
        case 2:
#ifdef RIIC2_ENABLE
            Iic_info = &g_iic_Info_ch2;
#endif /* #ifdef RIIC2_ENABLE */
        break;
        case 3:
#ifdef RIIC3_ENABLE
            Iic_info = &g_iic_Info_ch3;
#endif /* #ifdef RIIC3_ENABLE */
        break;
        default:
            /* Does nothing. */
        break;
    }

    /* Sets reception completion flag. */
    if (R_IIC_FINISH == g_iic_ChStatus[Iic_info->ChNo])
    {
        gs_Wflag = COMU_FINISH;
    }
    else
    {
        gs_Wflag = COMU_ERR;
    }
}


/********************************************************************************************************************
* Outline      : Reception callback processing
* Function Name: callbackR
* Description  : Reception callback sample.
* Arguments    : None
* return Value : None
*********************************************************************************************************************/
void callbackR(void)
{
    r_iic_drv_info_t        *Iic_info;                          /* IIC driver information pointer                   */

    /* Check using channel. */
    switch (OV7670_I2C_CHNO)
    {
        case 0:
#ifdef RIIC0_ENABLE
            Iic_info = &g_iic_Info_ch0;
#endif /* #ifdef RIIC0_ENABLE */
        break;
        case 1:
#ifdef RIIC1_ENABLE
            Iic_info = &g_iic_Info_ch1;
#endif /* #ifdef RIIC1_ENABLE */
        break;
        case 2:
#ifdef RIIC2_ENABLE
            Iic_info = &g_iic_Info_ch2;
#endif /* #ifdef RIIC2_ENABLE */
        break;
        case 3:
#ifdef RIIC3_ENABLE
            Iic_info = &g_iic_Info_ch3;
#endif /* #ifdef RIIC3_ENABLE */
        break;
        default:
            /* Does nothing. */
        break;
    }

    /* Sets reception completion flag. */
    if (R_IIC_FINISH == g_iic_ChStatus[Iic_info->ChNo])
    {
        gs_Rflag = COMU_FINISH;
    }
    else
    {
        gs_Rflag = COMU_ERR;
    }
}


/********************************************************************************************************************
* Outline      : Push RIIC interrupt processing
* Function Name: PushRIICInterrupt
* Description  : Gets RIIC interrupt priority level to temporary.
*              : Sets 0x00 to RIIC interrupt priority level registers.
* Arguments    : None
* return Value : None
*********************************************************************************************************************/
void PushRIICInterruptI2C(r_iic_drv_info_t * pIic_info)
{
    volatile uint8_t uctmp = 0U;

    /* Channel number? */
    /* Diesables RIIC interrupt. */
    switch (pIic_info->ChNo)
    {
        case 0U:                                                /* Channel 0                                        */
#ifdef RIIC0_ENABLE
            /* Temps interrupt source priority. */
            gs_EEI_Interrupt_Level = R_IIC_IPR_EEI0;
            gs_RXI_Interrupt_Level = R_IIC_IPR_RXI0;
            gs_TXI_Interrupt_Level = R_IIC_IPR_TXI0;
            gs_TEI_Interrupt_Level = R_IIC_IPR_TEI0;

            /* Sets interrupt source priority. */
            R_IIC_IPR_EEI0 = 0x00;
            R_IIC_IPR_RXI0 = 0x00;
            R_IIC_IPR_TXI0 = 0x00;
            R_IIC_IPR_TEI0 = 0x00;
            uctmp          = R_IIC_IPR_TEI0;                    /* Reads IPR.                                       */
#endif /* #ifdef RIIC0_ENABLE */
        break;

        case 1U:                                                /* Channel 1                                        */
#ifdef RIIC1_ENABLE
            /* Temps interrupt source priority. */
            gs_EEI_Interrupt_Level = R_IIC_IPR_EEI1;
            gs_RXI_Interrupt_Level = R_IIC_IPR_RXI1;
            gs_TXI_Interrupt_Level = R_IIC_IPR_TXI1;
            gs_TEI_Interrupt_Level = R_IIC_IPR_TEI1;

            /* Sets interrupt source priority. */
            R_IIC_IPR_EEI1 = 0x00;
            R_IIC_IPR_RXI1 = 0x00;
            R_IIC_IPR_TXI1 = 0x00;
            R_IIC_IPR_TEI1 = 0x00;
            uctmp          = R_IIC_IPR_TEI1;                    /* Reads IPR.                                       */
#endif /* #ifdef RIIC1_ENABLE */
        break;

        case 2U:                                                /* Channel 2                                        */
#ifdef RIIC2_ENABLE
            /* Temps interrupt source priority. */
            gs_EEI_Interrupt_Level = R_IIC_IPR_EEI2;
            gs_RXI_Interrupt_Level = R_IIC_IPR_RXI2;
            gs_TXI_Interrupt_Level = R_IIC_IPR_TXI2;
            gs_TEI_Interrupt_Level = R_IIC_IPR_TEI2;

            /* Sets interrupt source priority. */
            R_IIC_IPR_EEI2 = 0x00;
            R_IIC_IPR_RXI2 = 0x00;
            R_IIC_IPR_TXI2 = 0x00;
            R_IIC_IPR_TEI2 = 0x00;
            uctmp          = R_IIC_IPR_TEI2;                    /* Reads IPR.                                       */
#endif /* #ifdef RIIC2_ENABLE */
        break;

        case 3U:                                                /* Channel 3                                        */
#ifdef RIIC3_ENABLE
            /* Temps interrupt source priority. */
            gs_EEI_Interrupt_Level = R_IIC_IPR_EEI3;
            gs_RXI_Interrupt_Level = R_IIC_IPR_RXI3;
            gs_TXI_Interrupt_Level = R_IIC_IPR_TXI3;
            gs_TEI_Interrupt_Level = R_IIC_IPR_TEI3;

            /* Sets interrupt source priority. */
            R_IIC_IPR_EEI3 = 0x00;
            R_IIC_IPR_RXI3 = 0x00;
            R_IIC_IPR_TXI3 = 0x00;
            R_IIC_IPR_TEI3 = 0x00;
            uctmp          = R_IIC_IPR_TEI3;                    /* Reads IPR.                                       */
#endif /* #ifdef RIIC3_ENABLE */
        break;

        default:
            /* Please add a channel as needed. */
        break;
    }
}


/********************************************************************************************************************
* Outline      : Pop RIIC interrupt processing
* Function Name: PopRIICInterrupt
* Description  : Sets temporary level to RIIC interrupt priority level registers.
* Arguments    : None
* return Value : None
*********************************************************************************************************************/
void PopRIICInterruptI2C(r_iic_drv_info_t * pIic_info)
{
    volatile uint8_t uctmp = 0U;
	
	
    /* Channel number? */
    /* Enables RIIC interrupt. */
    switch (pIic_info->ChNo)
    {
        case 0U:                                                /* Channel 0                                        */
#ifdef RIIC0_ENABLE
            /* Sets interrupt source priority. */
            R_IIC_IPR_EEI0 = gs_EEI_Interrupt_Level;
            R_IIC_IPR_RXI0 = gs_RXI_Interrupt_Level;
            R_IIC_IPR_TXI0 = gs_TXI_Interrupt_Level;
            R_IIC_IPR_TEI0 = gs_TEI_Interrupt_Level;
			
            uctmp          = R_IIC_IPR_TEI0;                    /* Reads IPR.                                       */

#endif /* #ifdef RIIC0_ENABLE */
        break;

        case 1U:                                                /* Channel 1                                        */
#ifdef RIIC1_ENABLE
            /* Sets interrupt source priority. */
            R_IIC_IPR_EEI1 = gs_EEI_Interrupt_Level;
            R_IIC_IPR_RXI1 = gs_RXI_Interrupt_Level;
            R_IIC_IPR_TXI1 = gs_TXI_Interrupt_Level;
            R_IIC_IPR_TEI1 = gs_TEI_Interrupt_Level;
            uctmp          = R_IIC_IPR_TEI1;                    /* Reads IPR.                                       */
#endif /* #ifdef RIIC1_ENABLE */
        break;

        case 2U:                                                /* Channel 2                                        */
#ifdef RIIC2_ENABLE
            /* Sets interrupt source priority. */
            R_IIC_IPR_EEI2 = gs_EEI_Interrupt_Level;
            R_IIC_IPR_RXI2 = gs_RXI_Interrupt_Level;
            R_IIC_IPR_TXI2 = gs_TXI_Interrupt_Level;
            R_IIC_IPR_TEI2 = gs_TEI_Interrupt_Level;
            uctmp          = R_IIC_IPR_TEI2;                    /* Reads IPR.                                       */
#endif /* #ifdef RIIC2_ENABLE */
        break;

        case 3U:                                                /* Channel 3                                        */
#ifdef RIIC3_ENABLE
            /* Sets interrupt source priority. */
            R_IIC_IPR_EEI3 = gs_EEI_Interrupt_Level;
            R_IIC_IPR_RXI3 = gs_RXI_Interrupt_Level;
            R_IIC_IPR_TXI3 = gs_TXI_Interrupt_Level;
            R_IIC_IPR_TEI3 = gs_TEI_Interrupt_Level;
            uctmp          = R_IIC_IPR_TEI3;                    /* Reads IPR.                                       */
#endif /* #ifdef RIIC3_ENABLE */
        break;

        default:
            /* Please add a channel as needed. */
        break;
    }
}



/*********************
 OV7670 VSYNC割込処理
IRQで1画面ごとに割り込む。
**********************/
void isrVsyncCamOV7670(void){
	
	// DMAC動作停止
	DMAC.DMAST.BIT.DMST = 0;
	
	// DMAC転送停止
	DMAC0.DMCNT.BIT.DTE = 0;
	
	
	// 転送先開始アドレス
	DMAC0.DMDAR = CamData;		//CamDataの頭から
	
	// 転送データ数
	DMAC0.DMCRA = 0;		// フリーランニングモード
	
	// ブロック転送数
	//DMAC0.DMCRB = ;
	
	// DMA転送終了割込許可
	DMAC0.DMINT.BIT.DTIE = 0;	// 割込なし
	
	// DMA転送エスケープ割込設定
	// 設定なし
	
	
	// DMAC動作
	DMAC.DMAST.BIT.DMST = 1;
	
	// DMAC転送許可
	DMAC0.DMCNT.BIT.DTE = 1;
}





void isrPclkCamOV7670(void){
	
	if(PORTA.DR.BIT.B5){
		onLED5();
	}else{
		offLED5();
	}
	
}
