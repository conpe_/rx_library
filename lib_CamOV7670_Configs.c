

#include <lib_CamOV7670_140726.h>



void InitRGB565(void){
   uint8_t reg_com7;
	
	readRegCamOV7670(REG_COM7, &reg_com7);

	writeRegCamOV7670(REG_COM7, reg_com7|COM7_RGB);
	writeRegCamOV7670(REG_RGB444, RGB444_DISABLE);
	writeRegCamOV7670(REG_COM15, COM15_R00FF|COM15_RGB565);

	writeRegCamOV7670(REG_TSLB, 0x04);

	writeRegCamOV7670(REG_COM1, 0x00);
	writeRegCamOV7670(REG_COM9, 0x38);  	// 16x gain ceiling; 0x8 is reserved bit
	writeRegCamOV7670(0x4f, 0xb3);  		// "matrix coefficient 1"
	writeRegCamOV7670(0x50, 0xb3);  		// "matrix coefficient 2"
	writeRegCamOV7670(0x51, 0x00);  		// vb
	writeRegCamOV7670(0x52, 0x3d);  		// "matrix coefficient 4"
	writeRegCamOV7670(0x53, 0xa7);  		// "matrix coefficient 5"
	writeRegCamOV7670(0x54, 0xe4);  		// "matrix coefficient 6"
	writeRegCamOV7670(REG_COM13, COM13_GAMMA|COM13_UVSAT);
};


void InitQQVGA(void) {
    // QQVGA
    uint8_t reg_com7;
    
	readRegCamOV7670(REG_COM7, &reg_com7);
	
    writeRegCamOV7670(REG_COM7,reg_com7|COM7_QQVGA);

    writeRegCamOV7670(REG_HSTART,HSTART_QQVGA);
    writeRegCamOV7670(REG_HSTOP,HSTOP_QQVGA);
    writeRegCamOV7670(REG_HREF,HREF_QQVGA);
    writeRegCamOV7670(REG_VSTART,VSTART_QQVGA);
    writeRegCamOV7670(REG_VSTOP,VSTOP_QQVGA);
    writeRegCamOV7670(REG_VREF,VREF_QQVGA);
    writeRegCamOV7670(REG_COM3, COM3_QQVGA);
    writeRegCamOV7670(REG_COM14, COM14_QQVGA);
    writeRegCamOV7670(REG_SCALING_XSC, SCALING_XSC_QQVGA);
    writeRegCamOV7670(REG_SCALING_YSC, SCALING_YSC_QQVGA);
    writeRegCamOV7670(REG_SCALING_DCWCTR, SCALING_DCWCTR_QQVGA);
    writeRegCamOV7670(REG_SCALING_PCLK_DIV, SCALING_PCLK_DIV_QQVGA);
    writeRegCamOV7670(REG_SCALING_PCLK_DELAY, SCALING_PCLK_DELAY_QQVGA);
	
	writeRegCamOV7670(REG_COM10, COM10_PCLK_HB);
	
	//writeRegCamOV7670(REG_COM17, COM17_CBAR);	//カラーバー
}
	

void InitDefaultReg(void) {
	// Gamma curve values
	writeRegCamOV7670(0x7a, 0x20);
	writeRegCamOV7670(0x7b, 0x10);
	writeRegCamOV7670(0x7c, 0x1e);
	writeRegCamOV7670(0x7d, 0x35);
	writeRegCamOV7670(0x7e, 0x5a);
	writeRegCamOV7670(0x7f, 0x69);
	writeRegCamOV7670(0x80, 0x76);
	writeRegCamOV7670(0x81, 0x80);
	writeRegCamOV7670(0x82, 0x88);
	writeRegCamOV7670(0x83, 0x8f);
	writeRegCamOV7670(0x84, 0x96);
	writeRegCamOV7670(0x85, 0xa3);
	writeRegCamOV7670(0x86, 0xaf);
	writeRegCamOV7670(0x87, 0xc4);
	writeRegCamOV7670(0x88, 0xd7);
	writeRegCamOV7670(0x89, 0xe8);
	
	// AGC and AEC parameters.  Note we start by disabling those features,
	//then turn them only after tweaking the values.
	writeRegCamOV7670(REG_COM8, COM8_FASTAEC | COM8_AECSTEP | COM8_BFILT);
	writeRegCamOV7670(REG_GAIN, 0);
	writeRegCamOV7670(REG_AECH, 0);
	writeRegCamOV7670(REG_COM4, 0x40);
	// magic reserved bit
	writeRegCamOV7670(REG_COM9, 0x18);
	// 4x gain + magic rsvd bit
	writeRegCamOV7670(REG_BD50MAX, 0x05);
	writeRegCamOV7670(REG_BD60MAX, 0x07);
	writeRegCamOV7670(REG_AEW, 0x95);
	writeRegCamOV7670(REG_AEB, 0x33);
	writeRegCamOV7670(REG_VPT, 0xe3);
	writeRegCamOV7670(REG_HAECC1, 0x78);
	writeRegCamOV7670(REG_HAECC2, 0x68);
	writeRegCamOV7670(0xa1, 0x03);
	// magic
	writeRegCamOV7670(REG_HAECC3, 0xd8);
	writeRegCamOV7670(REG_HAECC4, 0xd8);
	writeRegCamOV7670(REG_HAECC5, 0xf0);
	writeRegCamOV7670(REG_HAECC6, 0x90);
	writeRegCamOV7670(REG_HAECC7, 0x94);
	writeRegCamOV7670(REG_COM8, COM8_FASTAEC|COM8_AECSTEP|COM8_BFILT|COM8_AGC|COM8_AEC);
	
	// Almost all of these are magic "reserved" values.
	writeRegCamOV7670(REG_COM5, 0x61);
	writeRegCamOV7670(REG_COM6, 0x4b);
	writeRegCamOV7670(0x16, 0x02);
	writeRegCamOV7670(REG_MVFP, 0x07);
	writeRegCamOV7670(0x21, 0x02);
	writeRegCamOV7670(0x22, 0x91);
	writeRegCamOV7670(0x29, 0x07);
	writeRegCamOV7670(0x33, 0x0b);
	writeRegCamOV7670(0x35, 0x0b);
	writeRegCamOV7670(0x37, 0x1d);
	writeRegCamOV7670(0x38, 0x71);
	writeRegCamOV7670(0x39, 0x2a);
	writeRegCamOV7670(REG_COM12, 0x78);
	writeRegCamOV7670(0x4d, 0x40);
	writeRegCamOV7670(0x4e, 0x20);
	writeRegCamOV7670(REG_GFIX, 0);
	writeRegCamOV7670(0x6b, 0x0a);
	writeRegCamOV7670(0x74, 0x10);
	writeRegCamOV7670(0x8d, 0x4f);
	writeRegCamOV7670(0x8e, 0);
	writeRegCamOV7670(0x8f, 0);
	writeRegCamOV7670(0x90, 0);
	writeRegCamOV7670(0x91, 0);
	writeRegCamOV7670(0x96, 0);
	writeRegCamOV7670(0x9a, 0);
	writeRegCamOV7670(0xb0, 0x84);
	writeRegCamOV7670(0xb1, 0x0c);
	writeRegCamOV7670(0xb2, 0x0e);
	writeRegCamOV7670(0xb3, 0x82);
	writeRegCamOV7670(0xb8, 0x0a);
	
	// More reserved magic, some of which tweaks white balance
	writeRegCamOV7670(0x43, 0x0a);
	writeRegCamOV7670(0x44, 0xf0);
	writeRegCamOV7670(0x45, 0x34);
	writeRegCamOV7670(0x46, 0x58);
	writeRegCamOV7670(0x47, 0x28);
	writeRegCamOV7670(0x48, 0x3a);
	writeRegCamOV7670(0x59, 0x88);
	writeRegCamOV7670(0x5a, 0x88);
	writeRegCamOV7670(0x5b, 0x44);
	writeRegCamOV7670(0x5c, 0x67);
	writeRegCamOV7670(0x5d, 0x49);
	writeRegCamOV7670(0x5e, 0x0e);
	writeRegCamOV7670(0x6c, 0x0a);
	writeRegCamOV7670(0x6d, 0x55);
	writeRegCamOV7670(0x6e, 0x11);
	writeRegCamOV7670(0x6f, 0x9f);
	// "9e for advance AWB"
	writeRegCamOV7670(0x6a, 0x40);
	writeRegCamOV7670(REG_BLUE, 0x40);
	writeRegCamOV7670(REG_RED, 0x60);
	writeRegCamOV7670(REG_COM8, COM8_FASTAEC|COM8_AECSTEP|COM8_BFILT|COM8_AGC|COM8_AEC|COM8_AWB);
	
	// Matrix coefficients
	writeRegCamOV7670(0x4f, 0x80);
	writeRegCamOV7670(0x50, 0x80);
	writeRegCamOV7670(0x51, 0);
	writeRegCamOV7670(0x52, 0x22);
	writeRegCamOV7670(0x53, 0x5e);
	writeRegCamOV7670(0x54, 0x80);
	writeRegCamOV7670(0x58, 0x9e);
	
	writeRegCamOV7670(REG_COM16, COM16_AWBGAIN);
	writeRegCamOV7670(REG_EDGE, 0);
	writeRegCamOV7670(0x75, 0x05);
	writeRegCamOV7670(0x76, 0xe1);
	writeRegCamOV7670(0x4c, 0);
	writeRegCamOV7670(0x77, 0x01);
	writeRegCamOV7670(0x4b, 0x09);
	writeRegCamOV7670(0xc9, 0x60);
	writeRegCamOV7670(REG_COM16, 0x38);
	writeRegCamOV7670(0x56, 0x40);
	
	writeRegCamOV7670(0x34, 0x11);
	writeRegCamOV7670(REG_COM11, COM11_EXP|COM11_HZAUTO_ON);
	writeRegCamOV7670(0xa4, 0x88);
	writeRegCamOV7670(0x96, 0);
	writeRegCamOV7670(0x97, 0x30);
	writeRegCamOV7670(0x98, 0x20);
	writeRegCamOV7670(0x99, 0x30);
	writeRegCamOV7670(0x9a, 0x84);
	writeRegCamOV7670(0x9b, 0x29);
	writeRegCamOV7670(0x9c, 0x03);
	writeRegCamOV7670(0x9d, 0x4c);
	writeRegCamOV7670(0x9e, 0x3f);
	writeRegCamOV7670(0x78, 0x04);
	
	// Extra-weird stuff.  Some sort of multiplexor register
	writeRegCamOV7670(0x79, 0x01);
	writeRegCamOV7670(0xc8, 0xf0);
	writeRegCamOV7670(0x79, 0x0f);
	writeRegCamOV7670(0xc8, 0x00);
	writeRegCamOV7670(0x79, 0x10);
	writeRegCamOV7670(0xc8, 0x7e);
	writeRegCamOV7670(0x79, 0x0a);
	writeRegCamOV7670(0xc8, 0x80);
	writeRegCamOV7670(0x79, 0x0b);
	writeRegCamOV7670(0xc8, 0x01);
	writeRegCamOV7670(0x79, 0x0c);
	writeRegCamOV7670(0xc8, 0x0f);
	writeRegCamOV7670(0x79, 0x0d);
	writeRegCamOV7670(0xc8, 0x20);
	writeRegCamOV7670(0x79, 0x09);
	writeRegCamOV7670(0xc8, 0x80);
	writeRegCamOV7670(0x79, 0x02);
	writeRegCamOV7670(0xc8, 0xc0);
	writeRegCamOV7670(0x79, 0x03);
	writeRegCamOV7670(0xc8, 0x40);
	writeRegCamOV7670(0x79, 0x05);
	writeRegCamOV7670(0xc8, 0x30);
	writeRegCamOV7670(0x79, 0x26);
}