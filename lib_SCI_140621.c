/********************************************/
/*				SCI1関数					*/
/*					for RX621				*/
/*					Wrote by conpe_			*/
/*							2014/06/21		*/
/********************************************/


// 【仕様】
// SCI用の送受信関数です。
// 調歩同期式, 8bit, パリティなし, ストップビット長1
// SCIは全6ch。1, 2, 3, 6はA, Bの端子が選べる
// 種類によっては選べないよん(AKI-621はSCI2のみ)
// リングバッファを使用して割込にて順次送受信する
//
// 送信(受信)毎の割り込みでリングバッファから送る(溜め込む)

// 割込関数の設定
// 送受信割り込みを以下の関数に飛ばす
// SCI0_TXI0 → isrTxBuffSCI(0);
// SCI0_RXI0 → isrRxBuffSCI(0);
// SCI1_TXI1 → isrTxBuffSCI(1);
// SCI1_RXI1 → isrRxBuffSCI(1);
// SCI2_TXI2 → isrTxBuffSCI(2);
// SCI2_RXI2 → isrRxBuffSCI(2);
// SCI3_TXI3 → isrTxBuffSCI(3);
// SCI3_RXI3 → isrRxBuffSCI(3);
// SCI5_TXI5 → isrTxBuffSCI(5);
// SCI5_RXI5 → isrRxBuffSCI(5);
// SCI6_TXI6 → isrTxBuffSCI(6);
// SCI6_RXI6 → isrRxBuffSCI(6);

// 割り込み関数の設定方法 ： intprg.cから呼び出す。

// printfSCIを使う場合は、<lib_function_***>も読み込んでおく。


//更新履歴
// Ver.140621
//  2014.06.21 SH7125のlibraryから移植
//  2014.06.28 DMA実装 → 評価の結果使わないほうが良さそうなので設定消去
//  2014.06.30 SCI1以外にも展開 (レジスタのアドレスを配列に。)

/*
HardWareSetting.h内で指定するもの

// -------- lib_SCI --------
#include <lib_SCI_140621.h>

#define PCLK	48000000	// モジュールへの供給クロック([MHz])

#define USE_TXD0		1			// TXD0使用する
#define SCI0_TXBUFFSIZE 128			// 送信リングバッファサイズ
#define USE_RXD0		1			// RXD0使用する
#define SCI0_RXBUFFSIZE 128			// 受信リングバッファサイズ

#define USE_TXD1		1
#define SCI1_TXBUFFSIZE 128
#define USE_RXD1		1
#define SCI1_RXBUFFSIZE 128

#define USE_TXD2		1
#define SCI2_TXBUFFSIZE 128
#define USE_RXD2		1
#define SCI2_RXBUFFSIZE 128

#define USE_TXD3		1
#define SCI3_TXBUFFSIZE 128
#define USE_RXD3		1
#define SCI3_RXBUFFSIZE 128

#define USE_TXD5		1
#define SCI5_TXBUFFSIZE 128
#define USE_RXD5		1
#define SCI5_RXBUFFSIZE 128

#define USE_TXD6		1
#define SCI6_TXBUFFSIZE 128
#define USE_RXD6		1
#define SCI6_RXBUFFSIZE 128


*/

#include "iodefine.h"
#include <HardwareSetting.h>


/********* グローバル変数 *********/

// レジスタ
volatile __evenaccess struct st_sci *SCIreg[7] = {&SCI0, &SCI1, &SCI2, &SCI3, &SCI0, &SCI5, &SCI6};

// バッファ関係
scibufftxrx SciBuffs[7];	// バッファ 0:SCI0, 1:SCI1, 2:SCI2 … 6:SCI6 ([4]は欠番 SCI4は無いよ)

#ifdef USE_TXD0
	static scibuff Sci0TxBuff;					// バッファ変数
	static uint8_t Sci0TxBuffer[SCI0_TXBUFFSIZE];	// バッファ実体
#endif

#ifdef USE_RXD0
	static scibuff Sci0RxBuff;					// バッファ変数
	static uint8_t Sci0RxBuffer[SCI0_RXBUFFSIZE];	// バッファ実体
#endif

#ifdef USE_TXD1
	static scibuff Sci1TxBuff;					// バッファ変数
	static uint8_t Sci1TxBuffer[SCI1_TXBUFFSIZE];	// バッファ実体
#endif

#ifdef USE_RXD1
	static scibuff Sci1RxBuff;					// バッファ変数
	static uint8_t Sci1RxBuffer[SCI1_RXBUFFSIZE];	// バッファ実体
#endif

#ifdef USE_TXD2
	static scibuff Sci2TxBuff;					// バッファ変数
	static uint8_t Sci2TxBuffer[SCI2_TXBUFFSIZE];	// バッファ実体
#endif

#ifdef USE_RXD2
	static scibuff Sci2RxBuff;					// バッファ変数
	static uint8_t Sci2RxBuffer[SCI2_RXBUFFSIZE];	// バッファ実体
#endif

#ifdef USE_TXD3
	static scibuff Sci3TxBuff;					// バッファ変数
	static uint8_t Sci3TxBuffer[SCI3_TXBUFFSIZE];	// バッファ実体
#endif

#ifdef USE_RXD3
	static scibuff Sci3RxBuff;					// バッファ変数
	static uint8_t Sci3RxBuffer[SCI3_RXBUFFSIZE];	// バッファ実体
#endif

#ifdef USE_TXD5
	static scibuff Sci5TxBuff;					// バッファ変数
	static uint8_t Sci5TxBuffer[SCI5_TXBUFFSIZE];	// バッファ実体
#endif

#ifdef USE_RXD5
	static scibuff Sci5RxBuff;					// バッファ変数
	static uint8_t Sci5RxBuffer[SCI5_RXBUFFSIZE];	// バッファ実体
#endif

#ifdef USE_TXD6
	static scibuff Sci6TxBuff;					// バッファ変数
	static uint8_t Sci6TxBuffer[SCI6_TXBUFFSIZE];	// バッファ実体
#endif

#ifdef USE_RXD6
	static scibuff Sci6RxBuff;					// バッファ変数
	static uint8_t Sci6RxBuffer[SCI6_RXBUFFSIZE];	// バッファ実体
#endif



// SCI state
static scistate SciState[7];

// プチ関数 lib_function_***で定義してるやつに飛ぶはず
extern double myPow(int num, signed int pow);
extern unsigned long myAbs(signed long a);
extern int myItoa_d(long integer, uint8_t *asci);
extern int myItoa_x(long integer, uint8_t *asci);
extern int myItoa_X(long integer, uint8_t *asci);

/*********************
SCI1初期化
引数：ボーレート 4800〜115200
**********************/
void initSCI(uint8_t SCIEnable, unsigned long BaudRate){
	uint16_t i;
	uint16_t SciNum;
	uint8_t ValueSMR;
	unsigned short n;
	
	// シリアルモードレジスタに書く値
	ValueSMR = 0x00;	// CKS=0, stopbit=1, parity=none, length=8bit, 調歩同期式


	// ポート設定と設定レジスタアドレス取得
	switch(SCIEnable){
	// SCI0
	case SCI0_OFF:
		SciNum = 0;
		SciState[SciNum].Enable = 0;
		SciState[SciNum].TxdEnable = 0;
		SciState[SciNum].RxdEnable = 0;
		break;
	case SCI0A_ENABLE:
		SciNum = 0;
		SciState[SciNum].Enable = 1;
		#if USE_TXD0
			SciState[SciNum].TxdEnable = 1;
		#else
			SciState[SciNum].TxdEnable = 0;
		#endif
		#if USE_RXD0
			SciState[SciNum].RxdEnable = 1;
		#else
			SciState[SciNum].RxdEnable = 0;
		#endif
		break;
	// SCI1
	case SCI1_OFF:
		SciNum = 1;
		SciState[SciNum].Enable = 0;
		SciState[SciNum].TxdEnable = 0;
		SciState[SciNum].RxdEnable = 0;
		break;
	case SCI1A_ENABLE:
		SciNum = 1;
		SciState[SciNum].Enable = 1;
		#if USE_TXD1
			SciState[SciNum].TxdEnable = 1;
		#else
			SciState[SciNum].TxdEnable = 0;
		#endif
		#if USE_RXD1
			SciState[SciNum].RxdEnable = 1;
		#else
			SciState[SciNum].RxdEnable = 0;
		#endif
		IOPORT.PFFSCI.BIT.SCI1S = 0;	// SCI1Aを使う
		break;
	case SCI1B_ENABLE:
		SciNum = 1;
		SciState[SciNum].Enable = 2;
		#if USE_TXD1
			SciState[SciNum].TxdEnable = 1;
		#else
			SciState[SciNum].TxdEnable = 0;
		#endif
		#if USE_RXD1
			SciState[SciNum].RxdEnable = 1;
		#else
			SciState[SciNum].RxdEnable = 0;
		#endif
		IOPORT.PFFSCI.BIT.SCI1S = 1;	// SCI1Bを使う
		break;
	// SCI2
	case SCI2_OFF:
		SciNum = 2;
		SciState[SciNum].Enable = 0;
		SciState[SciNum].TxdEnable = 0;
		SciState[SciNum].RxdEnable = 0;
		break;
	case SCI2A_ENABLE:
		SciNum = 2;
		SciState[SciNum].Enable = 1;
		#if USE_TXD2
			SciState[SciNum].TxdEnable = 1;
		#else
			SciState[SciNum].TxdEnable = 0;
		#endif
		#if USE_RXD2
			SciState[SciNum].RxdEnable = 1;
		#else
			SciState[SciNum].RxdEnable = 0;
		#endif
		IOPORT.PFFSCI.BIT.SCI2S = 0;
		break;
	case SCI2B_ENABLE:
		SciNum = 2;
		SciState[SciNum].Enable = 2;
		#if USE_TXD2
			SciState[SciNum].TxdEnable = 1;
		#else
			SciState[SciNum].TxdEnable = 0;
		#endif
		#if USE_RXD2
			SciState[SciNum].RxdEnable = 1;
		#else
			SciState[SciNum].RxdEnable = 0;
		#endif
		IOPORT.PFFSCI.BIT.SCI2S = 1;
		break;
	// SCI3
	case SCI3_OFF:
		SciNum = 3;
		SciState[SciNum].Enable = 0;
		SciState[SciNum].TxdEnable = 0;
		SciState[SciNum].RxdEnable = 0;
		break;
	case SCI3A_ENABLE:
		SciNum = 3;
		SciState[SciNum].Enable = 1;
		#if USE_TXD3
			SciState[SciNum].TxdEnable = 1;
		#else
			SciState[SciNum].TxdEnable = 0;
		#endif
		#if USE_RXD3
			SciState[SciNum].RxdEnable = 1;
		#else
			SciState[SciNum].RxdEnable = 0;
		#endif
		IOPORT.PFFSCI.BIT.SCI3S = 0;
		break;
	case SCI3B_ENABLE:
		SciNum = 3;
		SciState[SciNum].Enable = 2;
		#if USE_TXD3
			SciState[SciNum].TxdEnable = 1;
		#else
			SciState[SciNum].TxdEnable = 0;
		#endif
		#if USE_RXD3
			SciState[SciNum].RxdEnable = 1;
		#else
			SciState[SciNum].RxdEnable = 0;
		#endif
		IOPORT.PFFSCI.BIT.SCI3S = 1;
		break;
	// SCI5
	case SCI5_OFF:
		SciNum = 5;
		SciState[SciNum].Enable = 0;
		SciState[SciNum].TxdEnable = 0;
		SciState[SciNum].RxdEnable = 0;
		break;
	case SCI5A_ENABLE:
		SciNum = 5;
		SciState[SciNum].Enable = 1;
		#if USE_TXD5
			SciState[SciNum].TxdEnable = 1;
		#else
			SciState[SciNum].TxdEnable = 0;
		#endif
		#if USE_RXD5
			SciState[SciNum].RxdEnable = 1;
		#else
			SciState[SciNum].RxdEnable = 0;
		#endif
		break;
	// SCI6
	case SCI6_OFF:
		SciNum = 6;
		SciState[SciNum].Enable = 0;
		SciState[SciNum].TxdEnable = 0;
		SciState[SciNum].RxdEnable = 0;
		break;
	case SCI6A_ENABLE:
		SciNum = 6;
		SciState[SciNum].Enable = 1;
		#if USE_TXD6
			SciState[SciNum].TxdEnable = 1;
		#else
			SciState[SciNum].TxdEnable = 0;
		#endif
		#if USE_RXD6
			SciState[SciNum].RxdEnable = 1;
		#else
			SciState[SciNum].RxdEnable = 0;
		#endif
		IOPORT.PFFSCI.BIT.SCI6S = 0;
		break;
	case SCI6B_ENABLE:
		SciNum = 6;
		SciState[SciNum].Enable = 2;
		#if USE_TXD6
			SciState[SciNum].TxdEnable = 1;
		#else
			SciState[SciNum].TxdEnable = 0;
		#endif
		#if USE_RXD6
			SciState[SciNum].RxdEnable = 1;
		#else
			SciState[SciNum].RxdEnable = 0;
		#endif
		IOPORT.PFFSCI.BIT.SCI6S = 1;
		break;
	}

	

	if(SciState[SciNum].Enable == 0){	// SCI disable処理
		switch(SciNum){
			case 0:
				MSTP(SCI0) = 1;	// モジュールスタンバイ
				break;
			case 1:
				MSTP(SCI1) = 1;
				break;
			case 2:
				MSTP(SCI2) = 1;
				break;
			case 3:
				MSTP(SCI3) = 1;
				break;
			case 5:
				MSTP(SCI5) = 1;
				break;
			case 6:
				MSTP(SCI6) = 1;
				break;
		}
		
		SCIreg[SciNum]->SCR.BYTE = 0x00;	// 送受信disable, 割り込みdisable
		
	}else{			// SCI enable処理
		
		
		switch(SciNum){
			case 0:
				MSTP(SCI0) = 0;			// モジュール起動
				IPR(SCI0,    ) = 3;		// interrupt level = 1
				break;
			case 1:
				MSTP(SCI1) = 0;
				IPR(SCI1,    ) = 3;
				break;
			case 2:
				MSTP(SCI2) = 0;
				IPR(SCI2,    ) = 3;
				break;
			case 3:
				MSTP(SCI3) = 0;
				IPR(SCI3,    ) = 3;
				break;
			case 5:
				MSTP(SCI5) = 0;
				IPR(SCI5,    ) = 3;
				break;
			case 6:
				MSTP(SCI6) = 0;
				IPR(SCI6,    ) = 3;
				break;
		}
		
		SCIreg[SciNum]->SCR.BYTE = 0x00;		// SCRレジスタ初期化
		SCIreg[SciNum]->SMR.BYTE = ValueSMR;	// シリアルモードレジスタ
		SCIreg[SciNum]->SCMR.BYTE = 0xF2;		// SCMR.BCP2=1
		
		// ボーレートの設定
		//ボーレート計算
		n = SCIreg[SciNum]->SMR.BIT.CKS;
		
		if( (uint16_t)((double)PCLK / (unsigned long)((double)(32) * (double)myPow(2, (2*n-1))*(double)BaudRate) - 1) > 255) {
			SCIreg[SciNum]->SEMR.BIT.ABCS = 0;
		}else{
			SCIreg[SciNum]->SEMR.BIT.ABCS = 1;
		}
		
		SCIreg[SciNum]->BRR = (uint8_t)( (double)PCLK / ((double)(((uint8_t)SCIreg[SciNum]->SEMR.BIT.ABCS&0x01)?32:64) * (double)myPow(2, (2*n-1))*(double)BaudRate) - 1);
		// 例1
		// SCIreg[SciNum]->BRR = 38;		// Bitrate(38400bps)
		// 例2
		//SCIreg[SciNum]->SEMR.BIT.ABCS = 1;
		//SCIreg[SciNum]->BRR = 51;			// Bitrate(115200bps)
		// 例3
		// SCIreg[SciNum]->BRR = 155;		// Bitrate(9600bps)
	
		for (i = 0; i <= 1000; i++);        // 1ビット期間待つ
		
		
		// TXD設定
		if(SciState[SciNum].TxdEnable==1){
			
			// バッファ変数初期化
			switch(SciNum){
				case 0:
					#if USE_TXD0
					SciBuffs[SciNum].Tx = Sci0TxBuff;
					SciBuffs[SciNum].Tx.BuffSize = SCI0_TXBUFFSIZE;	// バッファサイズ
					SciBuffs[SciNum].Tx.pData = Sci0TxBuffer;		// バッファ実体へのポインタ
					#endif
					break;
				case 1:
					#if USE_TXD1
					SciBuffs[SciNum].Tx = Sci1TxBuff;
					SciBuffs[SciNum].Tx.BuffSize = SCI1_TXBUFFSIZE;
					SciBuffs[SciNum].Tx.pData = Sci1TxBuffer;
					#endif
					break;
				case 2:
					#if USE_TXD2
					SciBuffs[SciNum].Tx = Sci2TxBuff;
					SciBuffs[SciNum].Tx.BuffSize = SCI2_TXBUFFSIZE;
					SciBuffs[SciNum].Tx.pData = Sci2TxBuffer;
					#endif
					break;
				case 3:
					#if USE_TXD3
					SciBuffs[SciNum].Tx = Sci3TxBuff;
					SciBuffs[SciNum].Tx.BuffSize = SCI3_TXBUFFSIZE;
					SciBuffs[SciNum].Tx.pData = Sci3TxBuffer;
					#endif
					break;
				case 5:
					#if USE_TXD5
					SciBuffs[SciNum].Tx = Sci5TxBuff;
					SciBuffs[SciNum].Tx.BuffSize = SCI5_TXBUFFSIZE;
					SciBuffs[SciNum].Tx.pData = Sci5TxBuffer;
					#endif
					break;
				case 6:
					#if USE_TXD6
					SciBuffs[SciNum].Tx = Sci6TxBuff;
					SciBuffs[SciNum].Tx.BuffSize = SCI6_TXBUFFSIZE;
					SciBuffs[SciNum].Tx.pData = Sci6TxBuffer;
					#endif
					break;
			}
			
			// DMA設定 TXD
			if(0){				// DMA enable
				SciState[1].TxdDmaEnable = 1;		// DMA Enable Flag on
				
				SCIreg[SciNum]->SCR.BIT.TIE = 1;				// 送信バッファエンプティ割り込み enable
				SCIreg[SciNum]->SCR.BIT.TE = 1;				// 送信許可
				IEN(SCI1,TXI1) = 0;					// interrupt request enable
				
		    	ICU.DMRSR2 = VECT(SCI1,TXI1);		// DMAC2をSCI1_TXI1に割り当て 
				
				IPR(DMAC,DMAC2I) = 1;				// interrupt level = 1
				DMAC2.DMAMD.WORD = 0x8000;			// DM:fix, SM:plus
				DMAC2.DMCNT.BIT.DTE = 0x00;			// DTE disable
				DMAC2.DMTMD.WORD = 0x0001;			// DCTG:TXI1, SZ:8bit, DTS:no use, MD:normal
	    		DMAC2.DMDAR = (void *)&SCIreg[SciNum]->TDR;	// destination address : TDR
				DMAC2.DMCRB = 0x03FF;				
				
				DMAC.DMAST.BIT.DMST = 0x01;			// DMAC start
				IEN(DMAC,DMAC2I) = 1;   			// interrupt request enable
				
			}else{									// DMA disable
			
				SciState[1].TxdDmaEnable = 0;		// DMA Enable Flag off
					
				SCIreg[SciNum]->SCR.BIT.TIE = 0;	// 送信バッファエンプティ割り込み
				SCIreg[SciNum]->SCR.BIT.TEIE = 0;	// 送信完了割り込み
				SCIreg[SciNum]->SCR.BIT.TE = 1;	// 送信許可
				switch(SciNum){
					case 0:
						IEN(SCI0,TXI0) = 1;		
						break;
					case 1:
						IEN(SCI1,TXI1) = 1;		
						break;
					case 2:
						IEN(SCI2,TXI2) = 1;		
						break;
					case 3:
						IEN(SCI3,TXI3) = 1;		
						break;
					case 5:
						IEN(SCI5,TXI5) = 1;		
						break;
					case 6:
						IEN(SCI6,TXI6) = 1;		
						break;
				}
			}
		}
		
		// RXD設定
		if(SciState[SciNum].RxdEnable==1){
			
			// バッファ変数初期化
			SciBuffs[SciNum].Rx = Sci1RxBuff;
			SciBuffs[SciNum].Rx.BuffSize = SCI1_RXBUFFSIZE;	// バッファサイズ
			SciBuffs[SciNum].Rx.pData = Sci1RxBuffer;		// バッファ実体へのポインタ
			
			switch(SciNum){
				case 0:
					if(SciState[SciNum].Enable == 1){
						PORT2.ICR.BIT.B1 = 1;	// ポート設定 入力バッファ有
					}
					break;
				case 1:
					if(SciState[SciNum].Enable == 1){
						PORT3.ICR.BIT.B0 = 1;	// ポート設定 入力バッファ有
					}else if(SciState[SciNum].Enable == 2){
						PORTF.ICR.BIT.B2 = 1;	// ポート設定 入力バッファ有
					}
					break;
				case 2:
					if(SciState[SciNum].Enable == 1){
						PORT1.ICR.BIT.B2 = 1;	// ポート設定 入力バッファ有
					}else if(SciState[SciNum].Enable == 2){
						PORT5.ICR.BIT.B2 = 1;	// ポート設定 入力バッファ有
					}
					break;
				case 3:
					if(SciState[SciNum].Enable == 1){
						PORT1.ICR.BIT.B6 = 1;	// ポート設定 入力バッファ有
					}else if(SciState[SciNum].Enable == 2){
						PORT2.ICR.BIT.B5 = 1;	// ポート設定 入力バッファ有
					}
					break;
				case 5:
					if(SciState[SciNum].Enable == 1){
						PORTC.ICR.BIT.B2 = 1;	// ポート設定 入力バッファ有
					}
					break;
				case 6:
					if(SciState[SciNum].Enable == 1){
						PORT0.ICR.BIT.B1 = 1;	// ポート設定 入力バッファ有
					}else if(SciState[SciNum].Enable == 2){
						PORT3.ICR.BIT.B3 = 1;	// ポート設定 入力バッファ有
					}
					break;
			}
				
				
			// DMA設定 RXD
			if(0){				// DMA enable
				// 動作未確認
				SciState[SciNum].RxdDmaEnable = 1;		// DMA Enable Flag on
				
				SCIreg[SciNum]->SCR.BIT.RIE = 1;	// 受信バッファ割り込み enable
				SCIreg[SciNum]->SCR.BIT.RE = 1;	// TEと同時じゃないとenableにならないかも
				
				IEN(SCI1,RXI1) = 0;		// interrupt request enable
				
	    		ICU.DMRSR3 = VECT(SCI1,RXI1);		// DMAC3をSCI1_RXI1に割り当て
				
				IPR(DMAC,DMAC3I) = 1;				// interrupt level = 1
				DMAC3.DMAMD.WORD = 0x8000;			// DM:fix, SM:plus
				DMAC3.DMCNT.BIT.DTE = 0x00;			// DTE (disable)
				DMAC3.DMTMD.WORD = 0x0001;			// DCTG:TXI1, SZ:8bit, DTS:no use, MD:normal
				DMAC3.DMCRB = 0x03FF;				
				
				DMAC3.DMINT.BYTE = 0x10;            // interrupt enable
				DMAC.DMAST.BIT.DMST = 0x01;             // DMAC start
				DMAC3.DMCNT.BIT.DTE = 0x01;			// DTE (enable)
				IEN(DMAC,DMAC3I) = 1;   			// interrupt request enable
				
			}else{									// DMA disable
			
				SciState[SciNum].RxdDmaEnable = 0;		// DMA Enable Flag off
				SCIreg[SciNum]->SCR.BIT.RIE = 1;	// 受信完了割り込み
				SCIreg[SciNum]->SCR.BIT.RE = 1;	// TEと同時じゃないとenableにならないかも
				switch(SciNum){
					case 0:
						IEN(SCI0,RXI0) = 1;		// interrupt request enable
						break;
					case 1:
						IEN(SCI1,RXI1) = 1;		
						break;
					case 2:
						IEN(SCI2,RXI2) = 1;		
						break;
					case 3:
						IEN(SCI3,RXI3) = 1;		
						break;
					case 5:
						IEN(SCI5,RXI5) = 1;		
						break;
					case 6:
						IEN(SCI6,RXI6) = 1;		
						break;
				}
			}
		}
		
		
	}


}


/*********************
SCI有効化
引数：
disablaTxSCIをした後に再開したいとき。
**********************/
void enableTxSCI(uint8_t SciNum){
	
	// 割り込み許可
	SCIreg[SciNum]->SCR.BIT.TIE = 1;
	SCIreg[SciNum]->SCR.BIT.TEIE = 0;
	// 送受信許可
	SCIreg[SciNum]->SCR.BIT.TE = 1;	//TEとREは一旦両方offにして、同時にonしないとダメよ
	
	SciState[SciNum].TxdEnable = 1;
}

/*********************
SCI無効化
	出力ピンをハイインピーダンスにする
引数：
半二重通信したいときに使う。受信中はハイインピーダンスに。
**********************/
void disableTxSCI(uint8_t SciNum){
	
	// 割り込みdisable
	SCIreg[SciNum]->SCR.BIT.TIE = 0;
	SCIreg[SciNum]->SCR.BIT.TEIE = 0;
	
	// 送信disable
	SCIreg[SciNum]->SCR.BIT.TE = 0;
	
	// 端子を入力に
	switch(SciNum){
		case 0:
			if(SciState[SciNum].Enable == 1){
				PORT2.DDR.BIT.B0 = 0;	// ポート設定 入力バッファ有
			}
			break;
		case 1:
			if(SciState[SciNum].Enable == 1){
				PORT2.DDR.BIT.B6 = 0;	// ポート設定 入力バッファ有
			}else if(SciState[SciNum].Enable == 2){
				PORTF.DDR.BIT.B0 = 0;	// ポート設定 入力バッファ有
			}
			break;
		case 2:
			if(SciState[SciNum].Enable == 1){
				PORT1.DDR.BIT.B3 = 0;	// ポート設定 入力バッファ有
			}else if(SciState[SciNum].Enable == 2){
				PORT5.DDR.BIT.B0 = 0;	// ポート設定 入力バッファ有
			}
			break;
		case 3:
			if(SciState[SciNum].Enable == 1){
				PORT1.DDR.BIT.B7 = 0;	// ポート設定 入力バッファ有
			}else if(SciState[SciNum].Enable == 2){
				PORT2.DDR.BIT.B3 = 0;	// ポート設定 入力バッファ有
			}
			break;
		case 5:
			if(SciState[SciNum].Enable == 1){
				PORTC.DDR.BIT.B3 = 0;	// ポート設定 入力バッファ有
			}
			break;
		case 6:
			if(SciState[SciNum].Enable == 1){
				PORT0.DDR.BIT.B0 = 0;	// ポート設定 入力バッファ有
			}else if(SciState[SciNum].Enable == 2){
				PORT3.DDR.BIT.B2 = 0;	// ポート設定 入力バッファ有
			}
			break;
	}
	
	
	SciState[SciNum].TxdEnable = 0;
}


/*********************
SCI1 tx_bufferを読む
引数：なし
返値：バッファがないときは-1
**********************/
static int8_t readTxBuffSCI(uint8_t SciNum, uint8_t* data){
	if(isEmptyTxBuffSCI(SciNum)){	//バッファもう無い
		return -1;	// エラー
	}else{							//バッファ有り
		*data = SciBuffs[SciNum].Tx.pData[SciBuffs[SciNum].Tx.ReadP];
		SciBuffs[SciNum].Tx.ReadP = incTxBuffP(1, SciBuffs[SciNum].Tx.ReadP);
	}
	
	return 0x00;
}

/*********************
SCI1 tx_bufferに書く
引数：文字
返値：0x00(正常), -1(バッファいっぱい)
バッファいっぱいの時はバッファに貯めずにreturn
**********************/
static int8_t writeTxBuffSCI(uint8_t SciNum, uint8_t moji){
	int8_t ack;
	
	if(incTxBuffP(1, SciBuffs[SciNum].Tx.WriteP) != SciBuffs[SciNum].Tx.ReadP){		// 送信バッファ空き有り
		SciBuffs[SciNum].Tx.pData[SciBuffs[SciNum].Tx.WriteP] = moji;				// 送信バッファに書く
		SciBuffs[SciNum].Tx.WriteP = incTxBuffP(1, SciBuffs[SciNum].Tx.WriteP);		// 書いたらポインタ更新
		
		// 割り込み開始処理
		//if(SciState[1].TxdDmaEnable == 0){
		if(1){
			if( SCIreg[SciNum]->SCR.BIT.TIE==0 ){	//割り込みオフだったら
				SCIreg[SciNum]->SCR.BIT.TIE = 1;		// TXI割り込みオンにする
			//	if((SCIreg[SciNum]->SSR.BIT.TDRE==1)){		// もうTDRE書き込まれてない → 割込入らない
					isrTxBuffSCI(SciNum);			// 初めの1byteを送信
			//	}
			}
		}else{
			/*
			if( DMAC2.DMINT.BIT.DTIE==0 ){	//割り込みオフ(DMA動いてない)
				isrDMAC_CMAC2I();			// 初めの1byteを送信
			}
			*/
		}
		
		
		ack = 0;
	}else{		//バッファいっぱいだよ
		ack = -1;	// エラー
	}
	
	return ack;
}




/*********************
SCI1 printf風
引数：文字列, 変数

引数：
	出力フォーマット format
		%% : %
		%a__ : 文字コード指定(16進2ケタ)
		%c : 文字(uint8_t)
		%s : 文字列(uint8_t*)
		%ld : 10進(long)
		%lx : 16進小文字(long)
		%lX : 16進大文字(long)
		%d : 10進(int)
		%x : 16進小文字(int)
		%X : 16進大文字(int)
		
		2
		%3d  →   2
		%03d → 002		
		桁数指定は9ケタまで
		
	変数たち ...
返値:
	文字数
**********************/	
uint16_t printfSCI(uint8_t SciNum, uint8_t *format, ...){

	va_list list;
	
/*		
	va_start(list, format);
	myPrintf(txCharSCI1(, format, list);
	va_end(list);
*/	
/*
	uint8_t *s;
	int num;
	
	va_start(list, format);
	num = sprintf(s, format, list);
	va_end(list);
	
	while((*s != '\0') && (num>0)){
		txCharSCI1((*s);
		s++;
		num--;
	}
*/


	signed int i;
	int moji_cnt = 0;	//文字数カウント
	int moji_cnt_tmp = 0;
	uint8_t outtmp[32];
	uint8_t *str;	//文字列受取り用
	uint8_t code;
	signed int digit;	// 整形用 桁数
	uint8_t zero;	// 整形用 頭にゼロつけるフラグ
	signed int digit_cnt;	// 整形用 通常時の桁数カウント
	uint8_t headchar;	// '0' or ' '
	
	
	
	
	va_start(list, in_str);
		
	while(*format)	//文字列続く限り
	{
		digit = 0;
		digit_cnt = 0;
		zero = 0;
		switch(*format)
		{
		case '%':
			format++;
			switch(*format)
			{
			case 'a':
				format++;
				code = (*format&0x0F)<<4;
				format++;
				outtmp[0] = code|(*format&0x0F);
				moji_cnt_tmp = 1;
				break;
			case '%':	//%%
				outtmp[0] = '%';
				moji_cnt_tmp = 1;
				break;
			case 'c':	//文字
				outtmp[0] = (uint8_t)va_arg(list, int);
				moji_cnt_tmp = 1;
				break;
			case 's':	//文字列
				/*
				str = va_arg(ap, uint8_t*);
				myPrintf(pfunc, str, ap);
				moji_cnt_tmp = 0;
				*/
				moji_cnt_tmp = 0;
				while(*(str+moji_cnt_tmp) != '\0')
				{
					outtmp[moji_cnt_tmp] = *(str+moji_cnt_tmp);
					moji_cnt_tmp++;
				}
				break;
			//整形 %03dとか
			case '0':
				zero = 1;	// 頭にゼロつけるフラグ
				format++;
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				digit = (*format)-'0';
				format++;
			case 'l':
			case 'd':
			case 'x':
			case 'X':
				switch(*format)
				{
				case 'l':	//数値(long)
					format++;
					switch(*format)
					{
					case 'd':	//10進数字
						digit_cnt = myItoa_d(va_arg(list, long), outtmp);
						break;
					case 'x':	//16進小文字
						digit_cnt = myItoa_x(va_arg(list, long), outtmp);
						break;
					case 'X':	//16進大文字
						digit_cnt = myItoa_X(va_arg(list, long), outtmp);
						break;
					default:
						outtmp[0] = '%';
						outtmp[1] = 'l';
						outtmp[2] = *format;
						moji_cnt_tmp = 3;
						break;
					}
					break;
				//数値(int)
				case 'd':	//10進数字(int)
					digit_cnt = myItoa_d(va_arg(list, int), outtmp);
					break;
				case 'x':	//16進小文字
					digit_cnt = myItoa_x(va_arg(list, int), outtmp);
					break;
				case 'X':	//16進大文字
					digit_cnt = myItoa_X(va_arg(list, int), outtmp);
					break;
				default:
					outtmp[0] = '%';
					outtmp[1] = *format;
					moji_cnt_tmp = 2;
					break;
				}
				break;
			default:
				outtmp[0] = '%';
				outtmp[1] = *format;
				moji_cnt_tmp = 2;
				break;
			}
			
			// 整形
			if(digit_cnt!=0)	//これは数字
			{
				if((digit!=digit_cnt)&&(digit!=0))	//指定digitとItoaしたdigitがあってない&&指定digitがゼロじゃなければ整形
				{
					moji_cnt_tmp = digit;
					if(zero)
					{
						headchar = '0';
					}
					else
					{
						headchar = ' ';
					}
					
					if(digit>digit_cnt)		//digitの方が大きいときは右のケタからずらしていく
					{
						for(i=0;i<digit_cnt;i++)	//ずらす
						{
						//	if(((digit-i) > 0)&&(digit_cnt-i)>0)
						//	{
								outtmp[digit-1-i] = outtmp[digit_cnt-1-i];
						//	}
						}
					}
					else					//digit_cntの方が大きいときは左のケタからずらしていく
					{
						for(i=0;i<digit;i++)	//ずらす
						{
						//	if(((digit-i) > 0)&&(digit_cnt-i)>0)
						//	{
								outtmp[i] = outtmp[digit_cnt-digit+i];
						//	}
						}
					}
					i = digit-digit_cnt;
					while(i>0)					//頭埋める
					{
						outtmp[i-1] = headchar;
						i--;
					}
				}
				else	//数字だけど整形なし
				{
					moji_cnt_tmp = digit_cnt;
				}
			}
			break;	// end of case '%'
			
		default :	//ただの文字
			outtmp[0] = *format;
			moji_cnt_tmp = 1;
			
			break;
		}
		
		i=0;
		while(i<moji_cnt_tmp)
		{
			//(*pfunc)(outtmp[i]);	//目的の関数に1文字ずつ出力
			txCharSCI1(outtmp[i]);	//バッファあくまで待つよ
			i++;
		}
		
		
		format++;
		moji_cnt = moji_cnt + moji_cnt_tmp;
	}
	
	va_end(list);
	
	
	return moji_cnt;
}

/*********************
SCI1 printf風 バッファいっぱいだったら送信しない
引数：文字列, 変数

対応変換： %c,%s,%d,%x,%X
**********************/
/*
void SCI_printf_break(uint8_t SciNum, uint8_t *in_str, ...){
	va_list list;
	
	
//	va_start(list, in_str);
//	myPrintf(txCharSCI1(_break, in_str, list);
	
//	va_end(list);

	uint8_t *s;
	int num;
	
	va_start(list, in_str);
	num = sprintf(s, in_str, list);
	va_end(list);
	
	while((*s != '\0') && (num>0)){
		txCharSCI1(_break;(*s);
		s++;
		num--;
	}

}
*/

/*********************
SCI1 1文字送信
引数：文字
返値：なし
**********************/
int8_t txCharSCI(uint8_t SciNum, uint8_t moji){
	uint8_t ack = 1;
	
	while(ack!=0){	//バッファに書くのが成功するまで(バッファが空くまで)
		ack = writeTxBuffSCI(SciNum, moji);	
	}
	
	return ack; 
}
int8_t txCharSCI1(uint8_t moji){
	return txCharSCI(1, moji);
}
int8_t txCharSCI0(uint8_t moji){
	return txCharSCI(0, moji);
}



/*********************
SCI1 1文字送信 バッファいっぱいだったらbreak
引数：文字
返値：なし
バッファがいっぱいだったらbreak
**********************/
int8_t txCharBreakSCI(uint8_t SciNum, uint8_t moji){
	return writeTxBuffSCI(SciNum, moji);	
}
int8_t txCharBreakSCI1(uint8_t moji){
	return txCharBreakSCI(1, moji);
}
int8_t txCharBreakSCI0(uint8_t moji){
	return txCharBreakSCI(0, moji);
}

/*********************
SCI1 1文字送信 バッファを使わない(送信あくまで待つ)
引数：文字
返値：なし
**********************/
int8_t txCharNoBuffSCI(uint8_t SciNum, uint8_t moji){
	
	while (SCIreg[SciNum]->SSR.BIT.TDRE==0);	// SCTDRへのデータ書き込み可能待ち
	SCIreg[SciNum]->SSR.BIT.TDRE = 0;	
	SCIreg[SciNum]->TDR = moji;

	return 0;
}
int8_t txCharNoBuffSCI1(uint8_t moji){
	return txCharNoBuffSCI(1, moji);
}
int8_t txCharNoBuffSCI0(uint8_t moji){
	return txCharNoBuffSCI(0, moji);
}

/*********************
SCI1 文字列送信
引数：文字列
\0が来るまで繰り返し
**********************/
int8_t txStrSCI(uint8_t SciNum, uint8_t *in_str){
	uint8_t ack = 1;

	while(*in_str != '\0'){			//\0にしたけど大丈夫？まえは0
		ack = txCharSCI(SciNum, *in_str);
    	in_str++;
	}

	return ack;
}


/*********************
SCI1 10進数字送信
引数：数字,桁数
返値：なし
longまでの数
**********************/
int8_t txDecSCI(uint8_t SciNum, long fig){
	uint16_t i;
	uint8_t buf[16];
	uint8_t ack = 1;
	
	i=0;
	do{		//とりあえず下のケタから埋めていく
		buf[i]=( (fig%10) | '0');
		i++;
		if(i>15)	fig=0;
		fig = fig/10;	//その桁を1の位に持ってくる
	}while(fig);		//割り切ったら終わり
	
	while(i>0){			//bufの中身を後ろから出力
		ack = txCharSCI(SciNum, buf[i-1]);
		i--;
	}
	
	return ack;
}


/*********************
SCI1 16進数字送信(小文字)
引数：数字,桁数
返値：なし
longまでの数
**********************/
int8_t txHexSCI(uint8_t SciNum, long fig){
	uint16_t i;
	uint8_t buf[16];
	uint8_t tmp;
	uint8_t ack = 1; 
	
	i=0;
	do{		//とりあえず下のケタから埋めていく
		tmp = (fig & 0xf); 	//下のケタを抽出
		if(tmp<10){			//0〜9
			buf[i]=( tmp | '0');
		}else{
			buf[i]=( (tmp-10) + 'a');
		}
		i++;
		if(i>15)	fig=0;
		fig = fig>>4;		//その桁を1の位に持ってくる
	}while(fig);			//割り切ったら終わり
	
	while(i>0){			//bufの中身を後ろから出力
		ack = txCharSCI(SciNum, buf[i-1]);
		i--;
	}
	
	return ack;
}

/*********************
SCI1 16進数字送信(大文字)
引数：数字,桁数
返値：なし
longまでの数
**********************/
int8_t txHEXSCI(uint8_t SciNum, long fig){
	uint16_t i;
	uint8_t buf[16];
	uint8_t tmp;
	uint8_t ack = 1; 
	
	i=0;
	do{		//とりあえず下のケタから埋めていく
		tmp = (fig & 0xf); 	//下のケタを抽出
		if(tmp<10){			//0〜9
			buf[i]=( tmp | '0');
		}else{
			buf[i]=( (tmp-10) + 'A');
		}
		i++;
		if(i>15)	fig = 0;
		fig = fig>>4;		//その桁を1の位に持ってくる
	}while(fig);			//割り切ったら終わり
	
	while(i>0){			//bufの中身を後ろから出力
		ack = txCharSCI(SciNum, buf[i-1]);
		i--;
	}

	return ack;
}




/*********************
SCI1 バッファポインタのインクリメント
引数：今のポインタ
返値：次のポインタ
**********************/
static uint16_t incTxBuffP(uint8_t SciNum, uint16_t p){
	
	if(p < (SciBuffs[SciNum].Tx.BuffSize-1) ){
		return p+1;
	}else{
		return 0;
	}
	
	return p;
}



/*********************
SCI1 送信バッファがクリアされるまで待つ
引数：SCInum
返値：なし
**********************/
void waitTxEndSCI(uint8_t SciNum){

	while(SCIreg[SciNum]->SSR.BIT.TEND == 0);	//送信が終わるまで待つ

}







///////////////////////////////////////////
// 受信
///////////////////////////////////////////


/*********************
SCI1 1文字受信
引数：
	uint8_t SciNum : SCIモジュール番号
返値：
	バッファから読んだ受信データ
来るまで待つので注意
**********************/
int8_t rxCharSCI(uint8_t SciNum, uint8_t *data){
	uint8_t ack = (uint8_t)-1;
	
	while(ack){	//受信するまで待つ
		ack = readRxBuffSCI(1, data);
	}
	
	/*
	// バッファ使わない時
		*data = rxCharNoBuffSCI(1, data);
		ack = 0;
	*/
	

	return ack;	
}


/*********************
SCI 1文字受信
引数：
	uint8_t SciNum : SCIモジュール番号
返値：
	受信データ(来てなければ-1(NAK))
**********************/
int8_t rxCharBreakSCI(uint8_t SciNum, uint8_t *data){
	
	//uint8_t ack;
	
	return readRxBuffSCI(SciNum, data);
	
	/*
	// バッファ使わない時
		return rxCharBreakNoBuffSCI(SciNum, data);
	*/

		
}



/*********************
SCI 受信bufferに書く
引数：文字
返値：0x00(正常), -1(バッファいっぱいだった)
バッファいっぱいだったら、古いのを捨てていく。
**********************/
static int8_t writeRxBuffSCI(uint8_t SciNum, uint8_t moji){
	
		if(incRxBuffP(1, SciBuffs[SciNum].Rx.WriteP) != SciBuffs[SciNum].Rx.ReadP){	//バッファに追いついてない
			SciBuffs[SciNum].Rx.pData[SciBuffs[SciNum].Rx.WriteP] = moji;
			SciBuffs[SciNum].Rx.WriteP = incRxBuffP(1, SciBuffs[SciNum].Rx.WriteP);
			
			return 0;
		}else{		//バッファいっぱいだよ
			SciBuffs[SciNum].Rx.pData[SciBuffs[SciNum].Rx.WriteP] = moji;
			SciBuffs[SciNum].Rx.WriteP = incRxBuffP(1, SciBuffs[SciNum].Rx.WriteP);
			SciBuffs[SciNum].Rx.ReadP = incRxBuffP(1, SciBuffs[SciNum].Rx.ReadP);	// バッファ読み込みバッファ進める(=古いの捨てる

			return -1;	// エラー
		}
		
	
}



/*********************
SCI1 rx_bufferを読む
引数：
	uint8_t SciNum : SCI番号
返値：
	受信データ バッファがないときは-1(NAK)
**********************/
static int8_t readRxBuffSCI(uint8_t SciNum, uint8_t *data){

	if(SciBuffs[SciNum].Rx.WriteP != SciBuffs[SciNum].Rx.ReadP){	//バッファがある
		*data = SciBuffs[SciNum].Rx.pData[SciBuffs[SciNum].Rx.ReadP];
		SciBuffs[SciNum].Rx.ReadP = incRxBuffP(1, SciBuffs[SciNum].Rx.ReadP);
		return 0;
	}else{	//もう全部読み切ってるよ
		return -1;	// エラー
	}
	
	return -1;
}

/*********************
SCI1 1文字受信(バッファ使わない)
引数：なし
返値：文字
来るまで待つので注意
**********************/
int8_t rxCharNoBuffSCI(uint8_t SciNum, uint8_t *data){
	
	
	//バッファオーバーランエラー解除
	if(SCIreg[SciNum]->SSR.BIT.ORER==1){
		SCIreg[SciNum]->SSR.BIT.ORER = 0;
	}
	while ((SCIreg[SciNum]->SSR.BIT.RDRF) == 0);	// 受信待ち
	SCIreg[SciNum]->SSR.BIT.RDRF = 0;	// RDRFクリア
	*data = (uint8_t)SCIreg[SciNum]->RDR;
	return 0;
	
}


/*********************
SCI1 1文字受信(待ちなし)
引数：なし
返値：文字
呼び出しもとで来てるか確認する必要あり
**********************/
int8_t rxCharBreakNoBuffSCI(uint8_t SciNum, uint8_t *data){
	
	
		//バッファオーバーランエラー解除
		if(SCIreg[SciNum]->SSR.BIT.ORER==1){
			SCIreg[SciNum]->SSR.BIT.ORER = 0;
		}
		if(SCIreg[SciNum]->SSR.BIT.RDRF){
			SCIreg[SciNum]->SSR.BIT.RDRF = 0;	// RDRFクリア
			*data = SCIreg[SciNum]->RDR; 
			return 0;
		}else{
			return -1;
		}
}


/*********************
SCI1 複数ケタ数字受信
引数：なし
返値：数
enter押すまで読み続け。
uint16_tまでの数
**********************/
uint16_t rxDecSCI(uint8_t SciNum){
	uint8_t data_tmp = 0;
	uint16_t data_int = 0;
	
	do{
		rxCharSCI(SciNum, &data_tmp);	//受信待ち
		if(data_tmp != 0x0d){	//エンターキーじゃなければ、
			if((data_tmp>='0')&&(data_tmp<='9')){	//0〜9なら
				data_int *= 10;
				data_int += (data_tmp - '0');
			}
		}
		txCharSCI(SciNum, data_tmp);		//エコー
	}while(data_tmp != 0x0d);		//エンターじゃない限り続ける
	txStrSCI(SciNum, "\n\r");
	
	return data_int;
}

/*********************
SCI1 複数ケタ数字受信
引数：なし
返値：数
enter押すまで読み続け。
unsigned longまでの数
**********************/
unsigned long rxLongDecSCI(uint8_t SciNum){
	uint8_t data_tmp = 0;
	unsigned long data_int = 0;
			
	do{
		rxCharSCI(SciNum, &data_tmp);	//受信待ち
		if(data_tmp != 0x0d){	//エンターキーじゃなければ、
			if((data_tmp>='0')&&(data_tmp<='9')){	//0〜9なら
				data_int *= 10;
				data_int += (data_tmp - '0');
			}
		}
		txCharSCI(SciNum, data_tmp);		//エコー
	}while(data_tmp != 0x0d);		//エンターじゃない限り続ける
	txStrSCI(SciNum, "\n\r");
	
	return data_int;
}

/*********************
SCI 受信バッファポインタのインクリメント
引数：今のポインタ
返値：次のポインタ
**********************/
static uint16_t incRxBuffP(uint8_t SciNum, uint16_t p){
	
		if(p < (SciBuffs[SciNum].Rx.BuffSize-1) ){
			return p+1;
		}else{
			return 0;
		}
	
	return p;
}



/*********************
SCI ここまでの受信データを捨てる
引数：
	uint8_t SciNum : SCIモジュール番号
返値：
	なし
**********************/
void purgeRxBufferSCI(uint8_t SciNum){
	
		SciBuffs[SciNum].Rx.ReadP = SciBuffs[SciNum].Rx.WriteP;
	
}



/*********************
SCI 送信バッファが空かチェック
引数：
	uint8_t SciNum : SCIモジュール番号
返値：
	1 : 空
	0 : 空じゃない
**********************/
bool_t isEmptyTxBuffSCI(uint8_t SciNum){
	return SciBuffs[SciNum].Tx.WriteP == SciBuffs[SciNum].Tx.ReadP;
}

/*********************
SCI 受信バッファが空かチェック
引数：
	uint8_t SciNum : SCIモジュール番号
返値：
	1 : 空
	0 : 空じゃない
**********************/
bool_t isEmptyRxBuffSCI(uint8_t SciNum){
	return SciBuffs[SciNum].Rx.WriteP == SciBuffs[SciNum].Rx.ReadP;
}


/*********************
SCI 送信バッファに溜ってるデータ数
引数：
	uint8_t SciNum : SCIモジュール番号
返値：
	int : データ数
**********************/
uint16_t countTxBuffDataSCI(uint8_t SciNum){
	
	if(SciBuffs[SciNum].Tx.WriteP<SciBuffs[SciNum].Tx.ReadP){
		return (int)(SciBuffs[SciNum].Tx.BuffSize - SciBuffs[SciNum].Tx.ReadP + SciBuffs[SciNum].Tx.WriteP);
	}else{
		return (int)(SciBuffs[SciNum].Tx.WriteP-SciBuffs[SciNum].Tx.ReadP);
	}
	
	return 0;
}







// 以下割り込み

/*********************
SCI1 文字送信
引数：SCIモジュールNo.
返値：なし
バッファからTDRへ書く
TDREの割り込みによって動作
**********************/
void isrTxBuffSCI(uint8_t SciNum){
	uint8_t data;
	
	
	if(SCIreg[SciNum]->SSR.BIT.TDRE){		// TDRエンプティ
		
		if(readTxBuffSCI(SciNum, &data)==0){	// 送信データ有
			SCIreg[SciNum]->TDR = data;					// 送信
		}else{		// 貯まってるバッファ数が0なら割込disable
			SCIreg[SciNum]->SCR.BIT.TIE = 0;	// 割り込みoff
		}
		/*
		if(countTxBuffDataSCI(SciNum)<=0){		// 貯まってるバッファ数が0なら割込disable
			SCIreg[SciNum]->SCR.BIT.TIE = 0;	// 割り込みoff
		}
		*/
	}
}

/*********************
SCI1 受信割り込み
引数：
	uint8_t SciNum : SCIモジュール番号
返値：なし
受信割り込みによって動作
受信したら受信バッファに書く
バッファいっぱいだったら、古いのを捨てていく。
**********************/
void isrRxBuffSCI(uint8_t SciNum){
	
	SCIreg[SciNum]->SSR.BIT.RDRF = 0;
	writeRxBuffSCI(1, SCIreg[SciNum]->RDR);
	
}



/*********************
DMA0 割り込み
引数：
	なし
返値：
	なし
SCI0送信用
バッファにデータがなかったら割り込み終わり	
データあったらセットしなおして送信。
**********************/
/*
void isrDMAC_CMAC0I(void){// TXD0用

	DMAC0.DMCNT.BIT.DTE = 0x00;		// DTE disable
	DMAC0.DMINT.BYTE = 0x00;		// DMA interrupt disable
	IEN(SCI0,TXI0) = 0;				// SCI interrupt disable
}

void isrDMAC_CMAC1I(void){	// RXD0用

	DMAC1.DMCNT.BIT.DTE = 0x00;		// DTE disable
	DMAC1.DMINT.BYTE = 0x00;		// DMA interrupt disable
	IEN(SCI0,RXI0) = 0;				// SCI interrupt disable
	
}

void isrDMAC_CMAC2I(void){ 	// TXD1用
	static uint16_t DmaTxNum = 0;	
	uint16_t SciNum;
	
	SciNum = 1;
	
	// ポインタ更新 ここまでのDMAで送信した分をreadpを進める
	if( (SciBuffs[SciNum].Tx.ReadP+DmaTxNum) < SciBuffs[SciNum].Tx.BuffSize){
		SciBuffs[SciNum].Tx.ReadP = SciBuffs[SciNum].Tx.ReadP + DmaTxNum;
	}else{
		SciBuffs[SciNum].Tx.ReadP = SciBuffs[SciNum].Tx.ReadP + DmaTxNum - SciBuffs[SciNum].Tx.BuffSize;
	}
		
	if(isEmptyTxBuffSCI(1)){		// 送信終わり
		DmaTxNum = 0;
		// 割込
		DMAC2.DMCNT.BIT.DTE = 0x00;		// DTE disable
		DMAC2.DMINT.BYTE = 0x00;		// DMA interrupt disable
		IEN(SCI1,TXI1) = 0;				// SCI interrupt disable
	}else{
		
		DMAC2.DMCNT.BIT.DTE = 0x00;		// DTE disable
		
		DMAC2.DMSAR = &SciBuffs[SciNum].Tx.pData[SciBuffs[SciNum].Tx.ReadP];	// source address : ポインタの場所
		if( (SciBuffs[SciNum].Tx.ReadP + countTxBuffDataSCI(1)) < SciBuffs[SciNum].Tx.BuffSize ){	// 送信データ数設定
			DmaTxNum = countTxBuffDataSCI(SciNum);
		}else{
			DmaTxNum = SciBuffs[SciNum].Tx.BuffSize - SciBuffs[SciNum].Tx.ReadP;
		}
		DMAC2.DMCRA = DmaTxNum;			// Transfer size : バッファに溜まってる分orリングバッファの最後まで
		DMAC2.DMCNT.BIT.DTE = 0x01;		// DTE enable
		DMAC2.DMINT.BYTE = 0x10;		// DMA interrupt enable
			
		IEN(SCI1,TXI1) = 1;				// SCI interrupt enable
	}
	
}

void isrDMAC_CMAC3I(void){	// RXD1用

	DMAC3.DMCNT.BIT.DTE = 0x00;		// DTE (disable)
	DMAC3.DMINT.BYTE = 0x00;		// DMA interrupt disable
	IEN(SCI1,RXI1) = 0;				// SCI interrupt disable


}
*/