/********************************************/
/*				SCI1関数					*/
/*					for RX621				*/
/*					Wrote by conpe_			*/
/*							2014/06/21		*/
/********************************************/


// library作成作戦
// まずSCI1だけで実装。
// その後レジスタをglobalな配列に突っ込んで他chに展開


// 【仕様】
// SCI用の送受信関数です。
// 調歩同期式, 8bit, パリティなし, ストップビット長1
// SCIは全6ch。1, 2, 3, 6はA, Bの端子が選べる
// 種類によっては選べないよん(AKI-621はSCI2のみ)
// リングバッファを使用して割込にて順次送受信する
//
// ・DMA
// DMA使って極力CPU処理をせずにシリアル転送。
// 4本あるDMAの割り当て TXD0:DMA0, RXD0:DMA1, TXD1:DMA2, RXD1:DMA3
// → ちょい足しちょい出しちょい処理の組み合わせだと劇的にメインの処理が重くなるので使わないほうが良さげ
// 
// ・DMA使わないVer. 
// 送信(受信)毎の割り込みでリングバッファから送る(溜め込む)

// 割込関数の設定
// 【DMAの割り込み設定】
// 使用するDMAの割り込みを以下の関数に飛ばす
// void intDMAC_CMAC0I(void);	// TXD0用
// void intDMAC_CMAC1I(void); 	// RXD0用
// void intDMAC_CMAC2I(void); 	// TXD1用
// void intDMAC_CMAC3I(void);	// RXD1用

// 【DMA非使用時のSCI割り込み設定】
// DMAを使用しない場合の送受信割り込みを以下の関数に飛ばす
// void intSCI0_TXI0(void);
// void intSCI0_RXI0(void);
// void intSCI1_TXI1(void);
// void intSCI1_RXI1(void);
// void intSCI2_TXI2(void);
// void intSCI2_RXI2(void);
// void intSCI3_TXI3(void);
// void intSCI3_RXI3(void);
// void intSCI5_TXI5(void);
// void intSCI5_RXI5(void);
// void intSCI6_TXI6(void);
// void intSCI6_RXI6(void);


// 割り込み関数を飛ばす方法 ： vect.hに記述するか(←オススメ)、intprg.cから呼び出すか。



//更新履歴
// Ver.140621
//  2014.06.21 SH7125のlibraryから移植
//  2014.06.28 DMA実装 → 評価の結果使わないほうが良さそう

/*
HardWareSetting.h内で指定するもの

#include <lib_SCI_RX621def_140621.h>
#include <lib_SCI_DMA_140621.h>

#define PCLK	48	// モジュールへの供給クロック([MHz])

#define USE_TXD0		1			// TXD0使用する
#define TXD0_ATTACH_DMA	NONE		// DMA割り当て DMA:DMA使う NONE:DMA使わない(オススメ)
#define SCI0_TxBuffSize 128			// 送信リングバッファサイズ
#define USE_RXD0		1			// RXD0使用する
#define RXD0_ATTACH_DMA	NONE		// DMA割り当て DMA:DMA使う NONE:DMA使わない(オススメ)
#define SCI0_RxBuffSize 128			// 受信リングバッファサイズ

#define USE_TXD1		1
#define TXD1_ATTACH_DMA	DMA
#define SCI1_TXBUFFSIZE 128
#define USE_RXD1		1
#define RXD1_ATTACH_DMA	DMA
#define SciBuffs[1].Rx.BuffSize 128

#define USE_TXD2		1
#define SCI2_TxBuffSize 128
#define USE_RXD2		1
#define SCI2_RxBuffSize 128

#define USE_TXD3		1
#define SCI3_TxBuffSize 128
#define USE_RXD3		1
#define SCI3_RxBuffSize 128

#define USE_TXD5		1
#define SCI5_TxBuffSize 128
#define USE_RXD5		1
#define SCI5_RxBuffSize 128

#define USE_TXD6		1
#define SCI6_TxBuffSize 128
#define USE_RXD6		1
#define SCI6_RxBuffSize 128


*/

#include "iodefine.h"
#include <HardwareSetting.h>
#include <lib_SCI_RX621def_140621.h>


/********* グローバル変数 *********/

// レジスタ
volatile __evenaccess struct st_sci *SCIreg[7] = {&SCI0, &SCI1, &SCI2, &SCI3, &SCI0, &SCI5, &SCI6};

// バッファ関係
scibufftxrx SciBuffs[7];	// バッファ 0:SCI0, 1:SCI1, 2:SCI2 … 6:SCI6 ([4]は欠番 SCI4は無いよ)

#ifdef USE_TXD0
	static scibuff Sci0TxBuff;					// バッファ変数
	static char Sci0TxBuffer[SCI0_TXBUFFSIZE];	// バッファ実体
#endif

#ifdef USE_RXD0
	static scibuff Sci0RxBuff;					// バッファ変数
	static char Sci0RxBuffer[SCI0_RXBUFFSIZE];	// バッファ実体
#endif

#ifdef USE_TXD1
	static scibuff Sci1TxBuff;					// バッファ変数
	static char Sci1TxBuffer[SCI1_TXBUFFSIZE];	// バッファ実体
#endif

#ifdef USE_RXD1
	static scibuff Sci1RxBuff;					// バッファ変数
	static char Sci1RxBuffer[SCI1_RXBUFFSIZE];	// バッファ実体
#endif

#ifdef USE_TXD2
	static scibuff Sci2TxBuff;					// バッファ変数
	static char Sci2TxBuffer[SCI2_TXBUFFSIZE];	// バッファ実体
#endif

#ifdef USE_RXD2
	static scibuff Sci2RxBuff;					// バッファ変数
	static char Sci2RxBuffer[SCI2_RXBUFFSIZE];	// バッファ実体
#endif

#ifdef USE_TXD3
	static scibuff Sci3TxBuff;					// バッファ変数
	static char Sci3TxBuffer[SCI3_TXBUFFSIZE];	// バッファ実体
#endif

#ifdef USE_RXD3
	static scibuff Sci3RxBuff;					// バッファ変数
	static char Sci3RxBuffer[SCI3_RXBUFFSIZE];	// バッファ実体
#endif

#ifdef USE_TXD5
	static scibuff Sci5TxBuff;					// バッファ変数
	static char Sci5TxBuffer[SCI5_TXBUFFSIZE];	// バッファ実体
#endif

#ifdef USE_RXD5
	static scibuff Sci5RxBuff;					// バッファ変数
	static char Sci5RxBuffer[SCI5_RXBUFFSIZE];	// バッファ実体
#endif

#ifdef USE_TXD6
	static scibuff Sci6TxBuff;					// バッファ変数
	static char Sci6TxBuffer[SCI6_TXBUFFSIZE];	// バッファ実体
#endif

#ifdef USE_RXD6
	static scibuff Sci6RxBuff;					// バッファ変数
	static char Sci6RxBuffer[SCI6_RXBUFFSIZE];	// バッファ実体
#endif



//
static scistate SciState[6];

// プチ関数
static double myPow(int num, signed int pow);
static unsigned long myAbs(signed long a);
static int myItoa_d(long integer, char *asci);
static int myItoa_x(long integer, char *asci);
static int myItoa_X(long integer, char *asci);

/*********************
SCI1初期化
引数：ボーレート 4800〜115200
**********************/
void SCI_setup(unsigned char SCIEnable, unsigned long BaudRate){
	unsigned int i;
	unsigned int SCINum;
	_UBYTE ValueSMR;
	unsigned short n;
	
	
	// 使ってるレジスタ一覧
	/*
	MSTP(SCI1)
	SCIreg[1]->SCR.BYTE
	IOPORT.PFFSCI.BIT.SCI1S
	SCIreg[1]->SMR.BYTE
	SCIreg[1]->SCMR.BYTE
	SCIreg[1]->SMR.BIT.CKS
	SCIreg[1]->SEMR.BIT.ABCS
	SCIreg[1]->BRR
	IPR(SCI1,    )
	SCIreg[1]->SCR.BIT.TIE
	SCIreg[1]->SCR.BIT.TEIE
	SCIreg[1]->SCR.BIT.TE
	IEN(SCI1,TXI1)
	ICU.DMRSR2
	IPR(DMAC,DMAC2I)
	DMAC2.DMAMD.WORD
	DMAC2.DMCNT.BIT.DTE
	DMAC2.DMINT.BIT.DTIE
	DMAC2.DMTMD.WORD
	DMAC2.DMDAR
	DMAC2.DMCRB
	DMAC.DMAST.BIT.DMST
	IEN(DMAC,DMAC2I)
	SCIreg[1]->SCR.BIT.TEIE
	PORT_RXD1A_INPUTBUFF
	SCIreg[1]->SCR.BIT.RIE
	SCIreg[1]->SCR.BIT.RE
	IEN(SCI1,RXI1)
	ICU.DMRSR3 = VECT(SCI1,RXI1)
	IPR(DMAC,DMAC3I)
	DMAC3.DMAMD.WORD
	DMAC3.DMCNT.BIT.DTE
	DMAC3.DMTMD.WORD
	DMAC3.DMCRB
	DMAC3.DMINT.BYTE
	DMAC.DMAST.BIT.DMST
	DMAC3.DMCNT.BIT.DTE
	IEN(DMAC,DMAC3I)
	SCIreg[1]->SCR.BIT.RIE
	SCIreg[1]->SCR.BIT.RE
	IEN(SCI1,RXI1)
	SCIreg[1]->TDR
	SCIreg[1]->SSR.BIT.TDRE
	SCIreg[1]->SSR.BIT.TEND
	SCIreg[1]->SSR.BIT.ORER
	SCIreg[1]->SSR.BIT.RDRF
	SCIreg[1]->RDR
	DMAC2.DMCNT.BIT.DTE
	DMAC2.DMSAR
	DMAC2.DMCRA
	DMAC2.DMINT.BYTE
	IEN(SCI1,TXI1)
	*/
	
	// シリアルモードレジスタに書く値
	ValueSMR = 0x00;	// CKS=0, stopbit=1, parity=none, length=8bit, 調歩同期式


	// ポート設定と設定レジスタアドレス取得
	switch(SCIEnable){
	// SCI0
	case SCI0_OFF:
		SCINum = 0;
		break;
	case SCI0A_ENABLE:
		SCINum = 0;
		break;
	// SCI1
	case SCI1_OFF:
		SCINum = 1;
		
		SciState[1].Enable = 0;
		
		MSTP(SCI1) = 1;	// モジュールスタンバイ
		SCIreg[1]->SCR.BYTE = 0x00;	// 送受信disable, 割り込みdisable

		break;
	case SCI1A_ENABLE:
		
		SCINum = 1;
		SciState[1].Enable = 1;
		SciState[1].TxdEnable = USE_TXD1;
		SciState[1].RxdEnable = USE_RXD1;
		
		IOPORT.PFFSCI.BIT.SCI1S = 0;	// SCI1Aを使う

		MSTP(SCI1) = 0;				// モジュール起動
		//SCIreg[1]->SCR.BYTE = 0x00;		// SCRレジスタ初期化
		//SCIreg[1]->SMR.BYTE = ValueSMR;	// シリアルモードレジスタ
		//SCIreg[1]->SCMR.BYTE = 0xF2;		// SCMR.BCP2=1
		SCIreg[1]->SCR.BYTE = 0x00;		// SCRレジスタ初期化
		SCIreg[1]->SMR.BYTE = ValueSMR;	// シリアルモードレジスタ
		SCIreg[1]->SCMR.BYTE = 0xF2;		// SCMR.BCP2=1
		
		// ボーレートの設定
		//ボーレート計算
		n = SCIreg[1]->SMR.BIT.CKS;
		
		if(( (unsigned long)PCLK * (unsigned long)1000000 / ((unsigned long)(32) * myPow(2, (2*n-1))*(unsigned long)BaudRate) - 1) > 255) {
			onLED7();
			SCIreg[1]->SEMR.BIT.ABCS = 0;
		}else{
			onLED6();
			SCIreg[1]->SEMR.BIT.ABCS = 1;
		}
		
		SCIreg[1]->BRR = (unsigned char)( (unsigned long)PCLK * (unsigned long)1000000 / ((unsigned long)(((unsigned char)SCIreg[1]->SEMR.BIT.ABCS&0x01)?32:64) * myPow(2, (2*n-1))*(unsigned long)BaudRate) - 1);
		
		// SCIreg[1]->BRR = 38;		// Bitrate(38400bps)
		//SCIreg[1]->SEMR.BIT.ABCS = 1;
		//SCIreg[1]->BRR = 25;			// Bitrate(115200bps)
		// SCIreg[1]->BRR = 155;		// Bitrate(9600bps)
	
		for (i = 0; i <= 1000; i++);        // 1ビット期間待つ
		
		// 割り込みレベル
		IPR(SCI1,    ) = 1;                 // interrupt level = 1
		
		// TXD設定
		if(SciState[1].TxdEnable==1){
			
			// バッファ変数初期化
			SciBuffs[1].Tx = Sci1TxBuff;
			SciBuffs[1].Tx.BuffSize = SCI1_TXBUFFSIZE;	// バッファサイズ
			SciBuffs[1].Tx.pData = Sci1TxBuffer;		// バッファ実体へのポインタ
			
			// DMA設定 TXD
			if(TXD1_ATTACH_DMA==DMA){				// DMA enable
				SciState[1].TxdDmaEnable = 1;		// DMA Enable Flag on
				
				SCIreg[1]->SCR.BIT.TIE = 1;				// 送信バッファエンプティ割り込み enable
				SCIreg[1]->SCR.BIT.TE = 1;				// 送信許可
				IEN(SCI1,TXI1) = 0;					// interrupt request enable
				
		    	ICU.DMRSR2 = VECT(SCI1,TXI1);		// DMAC2をSCI1_TXI1に割り当て 
				
				IPR(DMAC,DMAC2I) = 1;				// interrupt level = 1
				DMAC2.DMAMD.WORD = 0x8000;			// DM:fix, SM:plus
				DMAC2.DMCNT.BIT.DTE = 0x00;			// DTE disable
				DMAC2.DMTMD.WORD = 0x0001;			// DCTG:TXI1, SZ:8bit, DTS:no use, MD:normal
	    		DMAC2.DMDAR = (void *)&SCIreg[1]->TDR;	// destination address : TDR
				DMAC2.DMCRB = 0x03FF;				
				
				DMAC.DMAST.BIT.DMST = 0x01;			// DMAC start
				IEN(DMAC,DMAC2I) = 1;   			// interrupt request enable
				
			}else{									// DMA disable
			
				SciState[1].TxdDmaEnable = 0;		// DMA Enable Flag off
					
				SCIreg[1]->SCR.BIT.TIE = 0;	// 送信バッファエンプティ割り込み
				SCIreg[1]->SCR.BIT.TEIE = 0;	// 送信完了割り込み
				SCIreg[1]->SCR.BIT.TE = 1;	// 送信許可
				IEN(SCI1,TXI1) = 1;		
			}
		}
		
		// RXD設定
		if(SciState[1].RxdEnable==1){
			
			// バッファ変数初期化
			SciBuffs[1].Rx = Sci1RxBuff;
			SciBuffs[1].Rx.BuffSize = SCI1_RXBUFFSIZE;	// バッファサイズ
			SciBuffs[1].Rx.pData = Sci1RxBuffer;		// バッファ実体へのポインタ
			
			PORT_RXD1A_INPUTBUFF = 1;	// ポート設定 入力バッファ有
			// DMA設定 RXD
			if(RXD1_ATTACH_DMA==DMA){				// DMA enable
				// 動作未確認
				SciState[1].RxdDmaEnable = 1;		// DMA Enable Flag on
				
				SCIreg[1]->SCR.BIT.RIE = 1;	// 受信バッファ割り込み enable
				SCIreg[1]->SCR.BIT.RE = 1;	// TEと同時じゃないとenableにならないかも
				IEN(SCI1,RXI1) = 0;		// interrupt request enable
				
	    		ICU.DMRSR3 = VECT(SCI1,RXI1);		// DMAC3をSCI1_RXI1に割り当て
				
				IPR(DMAC,DMAC3I) = 1;				// interrupt level = 1
				DMAC3.DMAMD.WORD = 0x8000;			// DM:fix, SM:plus
				DMAC3.DMCNT.BIT.DTE = 0x00;			// DTE (disable)
				DMAC3.DMTMD.WORD = 0x0001;			// DCTG:TXI1, SZ:8bit, DTS:no use, MD:normal
				DMAC3.DMCRB = 0x03FF;				
				
				DMAC3.DMINT.BYTE = 0x10;            // interrupt enable
				DMAC.DMAST.BIT.DMST = 0x01;             // DMAC start
				DMAC3.DMCNT.BIT.DTE = 0x01;			// DTE (enable)
				IEN(DMAC,DMAC3I) = 1;   			// interrupt request enable
				
			}else{									// DMA disable
			
				SciState[1].RxdDmaEnable = 0;		// DMA Enable Flag off
				SCIreg[1]->SCR.BIT.RIE = 1;	// 受信完了割り込み
				SCIreg[1]->SCR.BIT.RE = 1;	// TEと同時じゃないとenableにならないかも
				IEN(SCI1,RXI1) = 0;		// interrupt request enable
			}
		}
		
		
		break;
	case SCI1B_ENABLE:
		SCINum = 1;
		SciState[1].Enable = 2;
		IOPORT.PFFSCI.BIT.SCI1S = 1;	// SCI1Bを使う
		
		
		break;
	// SCI2
	case SCI2_OFF:
		SCINum = 2;
		break;
	case SCI2A_ENABLE:
		IOPORT.PFFSCI.BIT.SCI2S = 0;
		SCINum = 2;
		break;
	case SCI2B_ENABLE:
		IOPORT.PFFSCI.BIT.SCI2S = 1;
		SCINum = 2;
		break;
	// SCI3
	case SCI3_OFF:
		SCINum = 3;
		break;
	case SCI3A_ENABLE:
		IOPORT.PFFSCI.BIT.SCI3S = 0;
		SCINum = 3;
		break;
	case SCI3B_ENABLE:
		IOPORT.PFFSCI.BIT.SCI3S = 1;
		SCINum = 3;
		break;
	// SCI5
	case SCI5_OFF:
		SCINum = 5;
		break;
	case SCI5A_ENABLE:
		SCINum = 5;
		break;
	// SCI6
	case SCI6_OFF:
		SCINum = 6;
		break;
	case SCI6A_ENABLE:
		IOPORT.PFFSCI.BIT.SCI6S = 0;
		SCINum = 6;
		break;
	case SCI6B_ENABLE:
		IOPORT.PFFSCI.BIT.SCI6S = 1;
		SCINum = 6;
		break;
	}

	switch(SCINum){
	case 0:

		break;


	}



}


/*********************
SCI有効化
引数：

**********************/
void enableTxSCI(unsigned char scinum){
	
	// 割り込み許可
	SCIreg[1]->SCR.BIT.TIE = 1;
	SCIreg[1]->SCR.BIT.TEIE = 0;
	// 送受信許可
	SCIreg[1]->SCR.BIT.TE = 1;	//TEとREは一旦両方offにして、同時にonしないとダメよ
	
}

/*********************
SCI無効化
	出力ピンをハイインピーダンスにする
引数：

**********************/
void disableTxSCI(unsigned char scinum){
	
	// 割り込みdisable
	SCIreg[1]->SCR.BIT.TIE = 0;
	SCIreg[1]->SCR.BIT.TEIE = 0;
	
	// 送信disable
	SCIreg[1]->SCR.BIT.TE = 0;
	
	// 端子を入力に
	PORT_TXD1A_DATADIRECTION = 0;
	
	
}


/*********************
SCI1 tx_bufferを読む
引数：なし
返値：バッファがないときは0x15
**********************/
static char SCI_read_tx_buff(unsigned char scinum, char* data){
	if(isEmptyTxBuff(scinum)==0){	//バッファがある
		*data = SciBuffs[1].Tx.pData[SciBuffs[1].Tx.ReadP];
		SciBuffs[1].Tx.ReadP = inc_tx_buff_p(1, SciBuffs[1].Tx.ReadP);
	}else{	//もう全部読み切ってるよ
		return 0x15;	//NAK
	}
	
	return 0x00;
}

/*********************
SCI1 tx_bufferに書く
引数：文字
返値：0x00(正常), 0x15(バッファいっぱい)
バッファいっぱいの時はバッファに貯めずにreturn
**********************/
static char SCI_write_tx_buff(unsigned char scinum, char moji){
	char ack;
	
	if(inc_tx_buff_p(1, SciBuffs[1].Tx.WriteP) != SciBuffs[1].Tx.ReadP){	// 送信バッファ空き有り → 送信バッファ空き数で判断しよう→それでincの関数は直接書き換えるようにしちゃおう
		SciBuffs[1].Tx.pData[SciBuffs[1].Tx.WriteP] = moji;				// 送信バッファに書く
		SciBuffs[1].Tx.WriteP = inc_tx_buff_p(1, SciBuffs[1].Tx.WriteP);	// 書いたらポインタ更新
		
		// 割り込み開始処理
		if(SciState[1].TxdDmaEnable == 0){
			if( SCIreg[1]->SCR.BIT.TIE==0 ){	//割り込みオフ
				SCIreg[1]->SCR.BIT.TIE = 1;		// TXI割り込みオンにする
			//	if((SCIreg[1]->SSR.BIT.TDRE==1)){		// もうTDRE書き込まれてない → 割込入らない
					intSCI_tx_buff(1);			// 初めの1byteを送信
			//	}
			}
		}else{
			if( DMAC2.DMINT.BIT.DTIE==0 ){	//割り込みオフ(DMA動いてない)
				intDMAC_CMAC2I();			// 初めの1byteを送信
			}
		}
		
		
		ack = 0;
	}else{		//バッファいっぱいだよ
		ack = 0x15;	//NAK
	}
	
	return ack;
}




/*********************
SCI1 printf風
引数：文字列, 変数

引数：
	出力フォーマット format
		%% : %
		%a__ : 文字コード指定(16進2ケタ)
		%c : 文字(char)
		%s : 文字列(char*)
		%ld : 10進(long)
		%lx : 16進小文字(long)
		%lX : 16進大文字(long)
		%d : 10進(int)
		%x : 16進小文字(int)
		%X : 16進大文字(int)
		
		2
		%3d  →   2
		%03d → 002		
		桁数指定は9ケタまで
		
	変数たち ...
返値:
	文字数
**********************/	
int SCI_printf(unsigned char scinum, char *format, ...){

	va_list list;
	
/*		
	va_start(list, format);
	myPrintf(SCI1_tx_char, format, list);
	va_end(list);
*/	
/*
	char *s;
	int num;
	
	va_start(list, format);
	num = sprintf(s, format, list);
	va_end(list);
	
	while((*s != '\0') && (num>0)){
		SCI1_tx_char(*s);
		s++;
		num--;
	}
*/


	signed int i;
	int moji_cnt = 0;	//文字数カウント
	int moji_cnt_tmp = 0;
	char outtmp[32];
	char *str;	//文字列受取り用
	char code;
	signed int digit;	// 整形用 桁数
	char zero;	// 整形用 頭にゼロつけるフラグ
	signed int digit_cnt;	// 整形用 通常時の桁数カウント
	char headchar;	// '0' or ' '
	
	
	
	
	va_start(list, in_str);
		
	while(*format)	//文字列続く限り
	{
		digit = 0;
		digit_cnt = 0;
		zero = 0;
		switch(*format)
		{
		case '%':
			format++;
			switch(*format)
			{
			case 'a':
				format++;
				code = (*format&0x0F)<<4;
				format++;
				outtmp[0] = code|(*format&0x0F);
				moji_cnt_tmp = 1;
				break;
			case '%':	//%%
				outtmp[0] = '%';
				moji_cnt_tmp = 1;
				break;
			case 'c':	//文字
				outtmp[0] = (char)va_arg(list, int);
				moji_cnt_tmp = 1;
				break;
			case 's':	//文字列
				/*
				str = va_arg(ap, char*);
				myPrintf(pfunc, str, ap);
				moji_cnt_tmp = 0;
				*/
				moji_cnt_tmp = 0;
				while(*(str+moji_cnt_tmp) != '\0')
				{
					outtmp[moji_cnt_tmp] = *(str+moji_cnt_tmp);
					moji_cnt_tmp++;
				}
				break;
			//整形 %03dとか
			case '0':
				zero = 1;	// 頭にゼロつけるフラグ
				format++;
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				digit = (*format)-'0';
				format++;
			case 'l':
			case 'd':
			case 'x':
			case 'X':
				switch(*format)
				{
				case 'l':	//数値(long)
					format++;
					switch(*format)
					{
					case 'd':	//10進数字
						digit_cnt = myItoa_d(va_arg(list, long), outtmp);
						break;
					case 'x':	//16進小文字
						digit_cnt = myItoa_x(va_arg(list, long), outtmp);
						break;
					case 'X':	//16進大文字
						digit_cnt = myItoa_X(va_arg(list, long), outtmp);
						break;
					default:
						outtmp[0] = '%';
						outtmp[1] = 'l';
						outtmp[2] = *format;
						moji_cnt_tmp = 3;
						break;
					}
					break;
				//数値(int)
				case 'd':	//10進数字(int)
					digit_cnt = myItoa_d(va_arg(list, int), outtmp);
					break;
				case 'x':	//16進小文字
					digit_cnt = myItoa_x(va_arg(list, int), outtmp);
					break;
				case 'X':	//16進大文字
					digit_cnt = myItoa_X(va_arg(list, int), outtmp);
					break;
				default:
					outtmp[0] = '%';
					outtmp[1] = *format;
					moji_cnt_tmp = 2;
					break;
				}
				break;
			default:
				outtmp[0] = '%';
				outtmp[1] = *format;
				moji_cnt_tmp = 2;
				break;
			}
			
			// 整形
			if(digit_cnt!=0)	//これは数字
			{
				if((digit!=digit_cnt)&&(digit!=0))	//指定digitとItoaしたdigitがあってない&&指定digitがゼロじゃなければ整形
				{
					moji_cnt_tmp = digit;
					if(zero)
					{
						headchar = '0';
					}
					else
					{
						headchar = ' ';
					}
					
					if(digit>digit_cnt)		//digitの方が大きいときは右のケタからずらしていく
					{
						for(i=0;i<digit_cnt;i++)	//ずらす
						{
						//	if(((digit-i) > 0)&&(digit_cnt-i)>0)
						//	{
								outtmp[digit-1-i] = outtmp[digit_cnt-1-i];
						//	}
						}
					}
					else					//digit_cntの方が大きいときは左のケタからずらしていく
					{
						for(i=0;i<digit;i++)	//ずらす
						{
						//	if(((digit-i) > 0)&&(digit_cnt-i)>0)
						//	{
								outtmp[i] = outtmp[digit_cnt-digit+i];
						//	}
						}
					}
					i = digit-digit_cnt;
					while(i>0)					//頭埋める
					{
						outtmp[i-1] = headchar;
						i--;
					}
				}
				else	//数字だけど整形なし
				{
					moji_cnt_tmp = digit_cnt;
				}
			}
			break;	// end of case '%'
			
		default :	//ただの文字
			outtmp[0] = *format;
			moji_cnt_tmp = 1;
			
			break;
		}
		
		i=0;
		while(i<moji_cnt_tmp)
		{
			//(*pfunc)(outtmp[i]);	//目的の関数に1文字ずつ出力
			SCI1_tx_char(outtmp[i]);	//バッファあくまで待つよ
			i++;
		}
		
		
		format++;
		moji_cnt = moji_cnt + moji_cnt_tmp;
	}
	
	va_end(list);
	
	
	return moji_cnt;
}

/*********************
SCI1 printf風 バッファいっぱいだったら送信しない
引数：文字列, 変数

対応変換： %c,%s,%d,%x,%X
**********************/
/*
void SCI_printf_break(unsigned char scinum, char *in_str, ...){
	va_list list;
	
	
//	va_start(list, in_str);
//	myPrintf(SCI1_tx_char_break, in_str, list);
	
//	va_end(list);

	char *s;
	int num;
	
	va_start(list, in_str);
	num = sprintf(s, in_str, list);
	va_end(list);
	
	while((*s != '\0') && (num>0)){
		SCI1_tx_char_break;(*s);
		s++;
		num--;
	}

}
*/

/*********************
SCI1 1文字送信
引数：文字
返値：なし
**********************/
char SCI_tx_char(unsigned char scinum, char moji){
	char ack = 1;
	
	while(ack!=0){	//バッファに書くのが成功するまで(バッファが空くまで)
		ack = SCI_write_tx_buff(scinum, moji);	
	}
	
	return ack; 
}
char SCI1_tx_char(char moji){
	return SCI_tx_char(1, moji);
}
char SCI0_tx_char(char moji){
	return SCI_tx_char(0, moji);
}



/*********************
SCI1 1文字送信 バッファいっぱいだったらbreak
引数：文字
返値：なし
バッファがいっぱいだったらbreak
**********************/
char SCI_tx_char_break(unsigned char scinum, char moji){
	return SCI_write_tx_buff(scinum, moji);	
}
char SCI1_tx_char_break(char moji){
	return SCI_tx_char_break(1, moji);
}
char SCI0_tx_char_break(char moji){
	return SCI_tx_char_break(0, moji);
}

/*********************
SCI1 1文字送信 バッファを使わない(送信あくまで待つ)
引数：文字
返値：なし
**********************/
char SCI_tx_char_nobuff(unsigned char scinum, char moji){
	
	while (SCIreg[1]->SSR.BIT.TDRE==0);	// SCTDRへのデータ書き込み可能待ち
	SCIreg[1]->SSR.BIT.TDRE = 0;	
	SCIreg[1]->TDR = moji;

	return 0;
}
char SCI1_tx_char_nobuff(char moji){
	return SCI_tx_char_nobuff(1, moji);
}
char SCI0_tx_char_nobuff(char moji){
	return SCI_tx_char_nobuff(0, moji);
}

/*********************
SCI1 文字列送信
引数：文字列
\0が来るまで繰り返し
**********************/
char SCI_tx_str(unsigned char scinum, char *in_str){
	char ack = 1;

	while(*in_str != '\0'){			//\0にしたけど大丈夫？まえは0
		ack = SCI_tx_char(scinum, *in_str);
    	in_str++;
	}

	return ack;
}


/*********************
SCI1 10進数字送信
引数：数字,桁数
返値：なし
longまでの数
**********************/
char SCI_tx_d(unsigned char scinum, long fig){
	unsigned int i;
	char buf[16];
	char ack = 1;
	
	i=0;
	do{		//とりあえず下のケタから埋めていく
		buf[i]=( (fig%10) | '0');
		i++;
		if(i>15)	fig=0;
		fig = fig/10;	//その桁を1の位に持ってくる
	}while(fig);		//割り切ったら終わり
	
	while(i>0){			//bufの中身を後ろから出力
		ack = SCI_tx_char(scinum, buf[i-1]);
		i--;
	}
	
	return ack;
}


/*********************
SCI1 16進数字送信(小文字)
引数：数字,桁数
返値：なし
longまでの数
**********************/
char SCI_tx_x(unsigned char scinum, long fig){
	unsigned int i;
	char buf[16];
	char tmp;
	char ack = 1; 
	
	i=0;
	do{		//とりあえず下のケタから埋めていく
		tmp = (fig & 0xf); 	//下のケタを抽出
		if(tmp<10){			//0〜9
			buf[i]=( tmp | '0');
		}else{
			buf[i]=( (tmp-10) + 'a');
		}
		i++;
		if(i>15)	fig=0;
		fig = fig>>4;		//その桁を1の位に持ってくる
	}while(fig);			//割り切ったら終わり
	
	while(i>0){			//bufの中身を後ろから出力
		ack = SCI_tx_char(scinum, buf[i-1]);
		i--;
	}
	
	return ack;
}

/*********************
SCI1 16進数字送信(大文字)
引数：数字,桁数
返値：なし
longまでの数
**********************/
char SCI_tx_X(unsigned char scinum, long fig){
	unsigned int i;
	char buf[16];
	char tmp;
	char ack = 1; 
	
	i=0;
	do{		//とりあえず下のケタから埋めていく
		tmp = (fig & 0xf); 	//下のケタを抽出
		if(tmp<10){			//0〜9
			buf[i]=( tmp | '0');
		}else{
			buf[i]=( (tmp-10) + 'A');
		}
		i++;
		if(i>15)	fig = 0;
		fig = fig>>4;		//その桁を1の位に持ってくる
	}while(fig);			//割り切ったら終わり
	
	while(i>0){			//bufの中身を後ろから出力
		ack = SCI_tx_char(scinum, buf[i-1]);
		i--;
	}

	return ack;
}




/*********************
SCI1 バッファポインタのインクリメント
引数：今のポインタ
返値：次のポインタ
**********************/
static unsigned short inc_tx_buff_p(unsigned char scinum, unsigned short p){
	
	if(p < (SciBuffs[1].Tx.BuffSize-1) ){
		//p++;
		return p+1;
	}else{
		//p = 0;
		return 0;
	}
	
	return p;
}



/*********************
SCI1 送信バッファがクリアされるまで待つ
引数：SCInum
返値：なし
**********************/
void SCI_wait_end_transmit(unsigned char scinum){

	while(SCIreg[1]->SSR.BIT.TEND == 0);	//送信が終わるまで待つ

}







///////////////////////////////////////////
// 受信
///////////////////////////////////////////


/*********************
SCI1 1文字受信
引数：
	unsigned char scinum : SCIモジュール番号
返値：
	バッファから読んだ受信データ
来るまで待つので注意
**********************/
char SCI_rx_char(unsigned char scinum, char *data){
	char ack = 0x15;
	
	#ifdef USE_RXD1
		while(ack){	//受信するまで待つ
			ack = SCI_read_rx_buff(1, data);
		}
	#else 
		*data = SCI_rx_char_nobuff(1, data);
		ack = 0;
	#endif
	

	return ack;	
}


/*********************
SCI 1文字受信
引数：
	unsigned char scinum : SCIモジュール番号
返値：
	受信データ(来てなければ0x15(NAK))
**********************/
char SCI_rx_char_nowait(unsigned char scinum, char *data){
	
	//char ack;
	
	#ifdef USE_RXD1
		return SCI_read_rx_buff(1, data);
	#else
		return SCI_rx_char_nobuff_nowait(1, data);
	#endif

		
}



/*********************
SCI 受信bufferに書く
引数：文字
返値：0x00(正常), 0x15(バッファいっぱいだった)
バッファいっぱいだったら、古いのを捨てていく。
**********************/
static char SCI_write_rx_buff(unsigned char scinum, char moji){
	char ack = 1;
	
	#ifdef USE_RXD1
		if(inc_rx_buff_p(1, SciBuffs[1].Rx.WriteP) != SciBuffs[1].Rx.ReadP){	//バッファに追いついてない
			SciBuffs[1].Rx.pData[SciBuffs[1].Rx.WriteP] = moji;
			SciBuffs[1].Rx.WriteP = inc_rx_buff_p(1, SciBuffs[1].Rx.WriteP);
			
			ack = 0;
		}else{		//バッファいっぱいだよ
			SciBuffs[1].Rx.pData[SciBuffs[1].Rx.WriteP] = moji;
			SciBuffs[1].Rx.WriteP = inc_rx_buff_p(1, SciBuffs[1].Rx.WriteP);
			SciBuffs[1].Rx.ReadP = inc_rx_buff_p(1, SciBuffs[1].Rx.ReadP);	// バッファ読み込みバッファ進める(=古いの捨てる

			ack = 0x15;	//NAK
		}
	#endif
		
	
	return ack;
}



/*********************
SCI1 rx_bufferを読む
引数：
	unsigned char scinum : SCI番号
返値：
	受信データ バッファがないときは0x15(NAK)
**********************/
static char SCI_read_rx_buff(unsigned char scinum, char *data){
	
	char ack = 0;

	#ifdef USE_RXD1
		if(SciBuffs[1].Rx.WriteP != SciBuffs[1].Rx.ReadP){	//バッファがある
			*data = SciBuffs[1].Rx.pData[SciBuffs[1].Rx.ReadP];
			SciBuffs[1].Rx.ReadP = inc_rx_buff_p(1, SciBuffs[1].Rx.ReadP);
			ack = 0;
		}else{	//もう全部読み切ってるよ
			ack = 0x15;	//NAK
		}
	#endif
	
	return ack;
}

/*********************
SCI1 1文字受信(バッファ使わない)
引数：なし
返値：文字
来るまで待つので注意
**********************/
char SCI_rx_char_nobuff(unsigned char scinum, char *data){
	
	
		//バッファオーバーランエラー解除
		if(SCIreg[1]->SSR.BIT.ORER==1){
			SCIreg[1]->SSR.BIT.ORER = 0;
		}
		while ((SCIreg[1]->SSR.BIT.RDRF) == 0);	// 受信待ち
		SCIreg[1]->SSR.BIT.RDRF = 0;	// RDRFクリア
		*data = (char)SCIreg[1]->RDR;
		return 0;
	
}


/*********************
SCI1 1文字受信(待ちなし)
引数：なし
返値：文字
呼び出しもとで来てるか確認する必要あり
**********************/
char SCI_rx_char_nobuff_nowait(unsigned char scinum, char *data){
	
	
		//バッファオーバーランエラー解除
		if(SCIreg[1]->SSR.BIT.ORER==1){
			SCIreg[1]->SSR.BIT.ORER = 0;
		}
		if(SCIreg[1]->SSR.BIT.RDRF){
			SCIreg[1]->SSR.BIT.RDRF = 0;	// RDRFクリア
			*data = SCIreg[1]->RDR; 
			return 0;
		}else{
			return 0x15;
		}
}


/*********************
SCI1 複数ケタ数字受信
引数：なし
返値：数
enter押すまで読み続け。
unsigned intまでの数
**********************/
unsigned int SCI_rx_d(unsigned char scinum){
	char data_tmp = 0;
	unsigned int data_int = 0;
	
	do{
		SCI_rx_char(scinum, &data_tmp);	//受信待ち
		if(data_tmp != 0x0d){	//エンターキーじゃなければ、
			if((data_tmp>='0')&&(data_tmp<='9')){	//0〜9なら
				data_int *= 10;
				data_int += (data_tmp - '0');
			}
		}
		SCI_tx_char(scinum, data_tmp);		//エコー
	}while(data_tmp != 0x0d);		//エンターじゃない限り続ける
	SCI_tx_str(scinum, "\n\r");
	
	return data_int;
}

/*********************
SCI1 複数ケタ数字受信
引数：なし
返値：数
enter押すまで読み続け。
unsigned longまでの数
**********************/
unsigned long SCI_rx_ld(unsigned char scinum){
	char data_tmp = 0;
	unsigned long data_int = 0;
			
	do{
		SCI_rx_char(scinum, &data_tmp);	//受信待ち
		if(data_tmp != 0x0d){	//エンターキーじゃなければ、
			if((data_tmp>='0')&&(data_tmp<='9')){	//0〜9なら
				data_int *= 10;
				data_int += (data_tmp - '0');
			}
		}
		SCI_tx_char(scinum, data_tmp);		//エコー
	}while(data_tmp != 0x0d);		//エンターじゃない限り続ける
	SCI_tx_str(scinum, "\n\r");
	
	return data_int;
}

/*********************
SCI 受信バッファポインタのインクリメント
引数：今のポインタ
返値：次のポインタ
**********************/
static unsigned short inc_rx_buff_p(unsigned char scinum, unsigned short p){
	
		if(p < (SciBuffs[1].Rx.BuffSize-1) ){
			return p+1;
		}else{
			return 0;
		}
	
	return p;
}



/*********************
SCI ここまでの受信データを捨てる
引数：
	unsigned char scinum : SCIモジュール番号
返値：
	なし
**********************/
void SCI_breakReceivedBuffer(unsigned char scinum){
	
	#ifdef USE_RXD1
		SciBuffs[1].Rx.ReadP = SciBuffs[1].Rx.WriteP;
	#endif
	
	
}



/*********************
SCI 送信バッファが空かチェック
引数：
	unsigned char scinum : SCIモジュール番号
返値：
	1 : 空
	0 : 空じゃない
**********************/
char isEmptyTxBuff(unsigned char scinum){
	return SciBuffs[1].Tx.WriteP == SciBuffs[1].Tx.ReadP;
}

/*********************
SCI 受信バッファが空かチェック
引数：
	unsigned char scinum : SCIモジュール番号
返値：
	1 : 空
	0 : 空じゃない
**********************/
char isEmptyRxBuff(unsigned char scinum){
	return SciBuffs[1].Rx.WriteP == SciBuffs[1].Rx.ReadP;
}


/*********************
SCI 送信バッファに溜ってるデータ数
引数：
	unsigned char scinum : SCIモジュール番号
返値：
	int : データ数
**********************/
int countTxBuffData(unsigned char scinum){
	
	//SciBuffs[1].Rx.WriteP == SciBuffs[1].Rx.ReadPだとempty
	if(SciBuffs[1].Tx.WriteP<SciBuffs[1].Tx.ReadP){
		return (int)(SciBuffs[1].Tx.BuffSize - SciBuffs[1].Tx.ReadP + SciBuffs[1].Tx.WriteP);
	}else{
		return (int)(SciBuffs[1].Tx.WriteP-SciBuffs[1].Tx.ReadP);
	}
	
	return 0;
}





// 汎用関数：他と被らないよう注意

/*********************
乗数
	
**********************/	
double myPow(int num, signed int pow){
	unsigned int i;
	double ret = 1;
	
	if(pow>0){
		for(i=0;i<pow;i++){
			ret = ret*(double)num;
		}
	}else if(pow<0){
		pow = - pow;
		for(i=0;i<pow;i++){
			ret = ret/(double)num;
		}
	}else{
		ret = 1;
	}
	
	return ret;
}

/*********************
my絶対値
引数：
	数値 a
返値:
	絶対値とった値
**********************/	
static unsigned long myAbs(signed long a)
{
	
	if(a<0){
		a = -a;
	}
	
	return (unsigned long)a;
}

/*********************
数字を文字へ_d
引数：
	数字 integer
	格納配列 asci 10進
返値:
	文字数
**********************/	
static int myItoa_d(long integer, char *asci)
{
	int i;
	int num = 0;	//桁ポインタ
	char buf[20];	//20文字まで
	
	if(integer<0)	
	{
		integer = myAbs(integer);
		*asci = '-';
		asci++;
		num++;
	}
	
	do{		//とりあえず下のケタからbufに埋めていく
		buf[num]=(char)( (integer%10) + '0');
		num++;
		if(num>20)	integer = 0;
		integer = integer/10;	//次の桁を1の位に持ってくる
	}while(integer);			//割り切ったら終わり
	
	i = num;
	
	while(i>0){			//bufの中身を後ろから出力
		i--;
		*asci = buf[i];
		asci++;
	}
	*asci = '\0';
	
	return num;
}


/*********************
数字を文字へ_x
引数：
	数字 integer
	格納配列 asci 16進小文字
返値:
	文字数
**********************/	
static int myItoa_x(long integer, char *asci)
{
	int i;
	int num = 0;	//桁ポインタ
	char buf[20];	//20文字まで
	char tmp;
	
	if(integer<0)	
	{
		integer = myAbs(integer);
		*asci = '-';
		asci++;
		num++;
	}
	
	
	do{		//とりあえず下のケタから埋めていく
		tmp = (integer & 0xf); 	//下のケタを抽出
		if(tmp<10){			//0〜9
			buf[num]=(char)( tmp + '0');
		}else{				//A〜F
			buf[num]=(char)( (tmp-10) + 'a');
		}
		num++;
		if(num>20)	integer = 0;
		integer = integer>>4;	//次の桁を1の位に持ってくる
	}while(integer);			//割り切ったら終わり
	
	i = num;
	
	while(i>0){			//bufの中身を後ろから出力
		i--;
		*asci = buf[i];
		asci++;
	}
	*asci = '\0';
	
	return num;
}


/*********************
数字を文字へ_X
引数：
	数字 integer
	格納配列 asci 10進大文字
返値:
	文字数
**********************/	
static int myItoa_X(long integer, char *asci)
{
	int i;
	int num = 0;	//桁ポインタ
	char buf[20];	//20文字まで
	char tmp;
	
	if(integer<0)	
	{
		integer = myAbs(integer);
		*asci = '-';
		asci++;
		num++;
	}
	
	
	do{		//とりあえず下のケタから埋めていく
		tmp = (integer & 0xf); 	//下のケタを抽出
		if(tmp<10){			//0〜9
			buf[num]=(char)( tmp + '0');
		}else{				//A〜F
			buf[num]=(char)( (tmp-10) + 'A');
		}
		num++;
		if(num>20)	integer = 0;
		integer = integer>>4;	//次の桁を1の位に持ってくる
	}while(integer);			//割り切ったら終わり
	
	i = num;
	
	while(i>0){			//bufの中身を後ろから出力
		i--;
		*asci = buf[i];
		asci++;
	}
	*asci = '\0';
	
	return num;
}






// 以下割り込み

/*********************
SCI1 文字送信
引数：なし
返値：なし
TDREの割り込みによって動作
**********************/
void intSCI_tx_buff(unsigned char scinum){
	char data;
	
	
	if(SCIreg[1]->SSR.BIT.TDRE){		// TDRエンプティ
	
		
		if(SCI_read_tx_buff(scinum, &data)==0){	// 送信データ有
			SCIreg[1]->TDR = data;					// 送信
		}else{
		//	SCIreg[1]->SCR.BIT.TIE = 0;			//割り込みoff
			// DMACoff
			//IEN(DMAC,DMAC0I) = 0;
		}
		
		if(countTxBuffData(1)<=0){		// 貯まってるバッファ数が1以下なら割込disable
			SCIreg[1]->SCR.BIT.TIE = 0;	// 割り込みoff
		}
		
	}
}

/*********************
SCI1 受信割り込み
引数：
	unsigned char scinum : SCIモジュール番号
返値：なし
受信割り込みによって動作
受信したら受信バッファに書く
バッファいっぱいだったら、古いのを捨てていく。
**********************/
void intSCI_rx_buff(unsigned char scinum){
	
	SCIreg[1]->SSR.BIT.RDRF = 0;
	SCI_write_rx_buff(1, SCIreg[1]->RDR);
	
}



/*********************
DMA0 割り込み
引数：
	なし
返値：
	なし
SCI0送信用
バッファにデータがなかったら割り込み終わり	
データあったらセットしなおして送信。
**********************/

void intDMAC_CMAC0I(void){// TXD0用

	DMAC0.DMCNT.BIT.DTE = 0x00;		// DTE disable
	DMAC0.DMINT.BYTE = 0x00;		// DMA interrupt disable
	IEN(SCI0,TXI0) = 0;				// SCI interrupt disable
}

void intDMAC_CMAC1I(void){	// RXD0用

	DMAC1.DMCNT.BIT.DTE = 0x00;		// DTE disable
	DMAC1.DMINT.BYTE = 0x00;		// DMA interrupt disable
	IEN(SCI0,RXI0) = 0;				// SCI interrupt disable
	
}

void intDMAC_CMAC2I(void){ 	// TXD1用
	static unsigned int DmaTxNum = 0;	
	
	// ポインタ更新 ここまでのDMAで送信した分をreadpを進める
	if( (SciBuffs[1].Tx.ReadP+DmaTxNum) < SciBuffs[1].Tx.BuffSize){
		SciBuffs[1].Tx.ReadP = SciBuffs[1].Tx.ReadP + DmaTxNum;
	}else{
		SciBuffs[1].Tx.ReadP = SciBuffs[1].Tx.ReadP + DmaTxNum - SciBuffs[1].Tx.BuffSize;
	}
		
	if(isEmptyTxBuff(1)){		// 送信終わり
		DmaTxNum = 0;
		// 割込
		DMAC2.DMCNT.BIT.DTE = 0x00;		// DTE disable
		DMAC2.DMINT.BYTE = 0x00;		// DMA interrupt disable
		IEN(SCI1,TXI1) = 0;				// SCI interrupt disable
	}else{
		
		DMAC2.DMCNT.BIT.DTE = 0x00;		// DTE disable
		
		DMAC2.DMSAR = &SciBuffs[1].Tx.pData[SciBuffs[1].Tx.ReadP];	// source address : ポインタの場所
		if( (SciBuffs[1].Tx.ReadP + countTxBuffData(1)) < SciBuffs[1].Tx.BuffSize ){	// 送信データ数設定
			DmaTxNum = countTxBuffData(1);
		}else{
			DmaTxNum = SciBuffs[1].Tx.BuffSize - SciBuffs[1].Tx.ReadP;
		}
		DMAC2.DMCRA = DmaTxNum;			// Transfer size : バッファに溜まってる分orリングバッファの最後まで
		DMAC2.DMCNT.BIT.DTE = 0x01;		// DTE enable
		DMAC2.DMINT.BYTE = 0x10;		// DMA interrupt enable
			
		IEN(SCI1,TXI1) = 1;				// SCI interrupt enable
	}
	
}

void intDMAC_CMAC3I(void){	// RXD1用

	DMAC3.DMCNT.BIT.DTE = 0x00;		// DTE (disable)
	DMAC3.DMINT.BYTE = 0x00;		// DMA interrupt disable
	IEN(SCI1,RXI1) = 0;				// SCI interrupt disable


}