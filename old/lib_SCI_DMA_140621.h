/********************************************/
/*				SCI1関数					*/
/*					for SH7125 @ HEW		*/
/*					Wrote by conpe_			*/
/*							2011/03/26		*/
/********************************************/

//SCI1用の送受信関数です。

// バッファを使って順次割り込みにより送信
// intprg.cのINT_SCI1_TXI1からintSCI_tx_buff(1)を呼び出してね。
// intprg.cのINT_SCI0_TXI0からintSCI_tx_buff(0)を呼び出してね。
// resetprg.cの#define SR_Initは0x00000000にする

// バッファがあふれそうになったら、空くまで待つので注意。


#ifndef SCIBUFF
#define SCIBUFF

	#include <stdarg.h>	//可変長引数
	#include <stdio.h>

#define DMA		0
#define NONE	1


#define SCI0_OFF		0
#define SCI0A_ENABLE	1
#define SCI1_OFF		2
#define SCI1A_ENABLE	3
#define SCI1B_ENABLE	4
#define SCI2_OFF		5
#define SCI2A_ENABLE	6
#define SCI2B_ENABLE	7
#define SCI3_OFF		8
#define SCI3A_ENABLE	9
#define SCI3B_ENABLE	10
#define SCI5_OFF		11
#define SCI5A_ENABLE	12
#define SCI6_OFF		13
#define SCI6A_ENABLE	14
#define SCI6B_ENABLE	15


// SCIステータス
typedef struct scistate{
	int Enable			:2;		// 0:disable, 1:A, 2:B
	int TxdEnable		:1;
	int RxdEnable		:1;
	int TxdDmaEnable	:1;
	int RxdDmaEnable	:1;
} scistate;

// 各バッファ
typedef struct scibuff{
	unsigned int BuffSize;	// バッファサイズ
	unsigned int WriteP;	// 書き込みポインタ (次書くとこ)
	unsigned int ReadP;		// 読み込みポインタ (次読むとこ)
	char *pData;				// バッファへのポインタ
}scibuff;

typedef struct scibufftxrx{
	scibuff Tx;
	scibuff Rx;
}scibufftxrx;


/*********************
SCI初期化
引数：ボーレート 4800〜115200
**********************/
void SCI_setup(unsigned char SCIEnable, unsigned long BaudRate);

/*********************
SCI有効化
引数：

**********************/
void enableTxSCI(unsigned char scinum);

/*********************
SCI TX無効化
引数：

**********************/
void disableTxSCI(unsigned char scinum);

/*********************
SCI1 文字送信
引数：なし
返値：なし
TDREの割り込みによって動作
**********************/
void intSCI_tx_buff(unsigned char scinum);

/*********************
SCI1 bufferを読む
引数：なし
返値：文字 バッファがないときは0x15
**********************/
static char SCI_read_tx_buff(unsigned char scinum, char *data);

/*********************
SCI1 bufferに書く
引数：文字
返値：0x00(正常), 0x15(バッファいっぱい)
**********************/
static char SCI_write_tx_buff(unsigned char scinum, char moji);

/*********************
SCI1 printf風
引数：文字列, 変数

対応変換： %c,%s,%d,%x,%X
**********************/
int SCI_printf(unsigned char scinum, char *, ...);

/*********************
SCI1 printf風 バッファいっぱいだったら送信しない
引数：文字列, 変数

対応変換： %c,%s,%d,%x,%X
**********************/
//void SCI_printf_break(unsigned char scinum, char *, ...);

/*********************
SCI1 1文字送信
引数：文字
返値：バッファがいっぱいだったら0x15
**********************/
char SCI_tx_char(unsigned char scinum, char moji);
char SCI0_tx_char(char moji);
char SCI1_tx_char(char moji);

/*********************
SCI1 1文字送信
引数：文字
返値：バッファがいっぱいだったら0x15
**********************/
char SCI_tx_char_break(unsigned char scinum, char ch);
char SCI1_tx_char_break(char ch);
char SCI0_tx_char_break(char ch);

/*********************
SCI1 1文字送信  バッファを使わない(送信あくまで待つ)
引数：文字
**********************/
char SCI_tx_char_nobuff(unsigned char scinum, char ch);
char SCI0_tx_char_nobuff(char ch);
char SCI1_tx_char_nobuff(char ch);

/*********************
SCI1 文字列送信
引数：文字列
**********************/
char SCI_tx_str(unsigned char scinum, char *str);

/*********************
SCI1 10進数字送信
引数：数字,桁数
返値：なし
longまでの数
**********************/
char SCI_tx_d(unsigned char scinum, long d);

/*********************
SCI1 16進数字送信(小文字)
引数：数字,桁数
返値：なし
longまでの数
**********************/
char SCI_tx_x(unsigned char scinum, long x);

/*********************
SCI1 16進数字送信(大文字)
引数：数字,桁数
返値：なし
longまでの数
**********************/
char SCI_tx_X(unsigned char scinum, long X);


/*********************
SCI1 バッファポインタのインクリメント
引数：今のポインタ
返値：次のポインタ
**********************/
static unsigned short inc_tx_buff_p(unsigned char scinum, unsigned short p);


/*********************
SCI1 送信バッファがクリアされるまで待つ
引数：SCInum
返値：なし
**********************/
void SCI_wait_end_transmit(unsigned char scinum);




///////////////////////////////////////////
// 受信
///////////////////////////////////////////

/*********************
SCI 1文字受信
引数：
	unsigned char scinum : SCIモジュール番号
返値：
	受信データ
来るまで待つので注意
**********************/
char SCI_rx_char(unsigned char scinum, char *data); //printf

#define SCI0_rx_char() SCI_rx_char(0)
#define SCI1_rx_char() SCI_rx_char(1)

/*********************
SCI 1文字受信
引数：
	unsigned char scinum : SCIモジュール番号
返値：
	受信データ(来てなければ0x15(NAK))
**********************/
char SCI_rx_char_nowait(unsigned char scinum, char *data);

	


/*********************
SCI 受信bufferに書く
引数：文字
返値：0x00(正常), 0x15(バッファいっぱい)
**********************/
static char SCI_write_rx_buff(unsigned char scinum, char moji);


/*********************
SCI1 rx_bufferを読む
引数：
	unsigned char scinum : SCI番号
返値：
	受信データ バッファがないときは0x15(NAK)
**********************/
static char SCI_read_rx_buff(unsigned char scinum, char *data);


/*********************
SCI1 1文字受信
引数：なし
返値：文字
来るまで待つので注意
**********************/
char SCI_rx_char_nobuff(unsigned char scinum, char *data);

/*********************
SCI1 1文字受信(待ちなし)
引数：なし
返値：文字
呼び出しもとで来てるか確認する必要あり
**********************/
char SCI_rx_char_nobuff_nowait(unsigned char scinum, char *data);

/*********************
SCI1 複数ケタ数字受信
引数：なし
返値：数
enter押すまで読み続け。
unsigned intまでの数
**********************/
unsigned int SCI_rx_d(unsigned char scinum);
/*********************
SCI1 複数ケタ数字受信
引数：なし
返値：数
enter押すまで読み続け。
unsigned longまでの数
**********************/
unsigned long SCI_rx_ld(unsigned char scinum);


/*********************
SCI 受信バッファポインタのインクリメント
引数：今のポインタ
返値：次のポインタ
**********************/
static unsigned short inc_rx_buff_p(unsigned char scinum, unsigned short p);


/*********************
SCI ここまでの受信データを捨てる
引数：
	unsigned char scinum : SCIモジュール番号
返値：
	なし
**********************/
void SCI_breakReceivedBuffer(unsigned char scinum);



/*********************
SCI 送信バッファが空かチェック
引数：
	unsigned char scinum : SCIモジュール番号
返値：
	1 : 空
	0 : 空じゃない
**********************/
char isEmptyTxBuff(unsigned char scinum);

/*********************
SCI 受信バッファが空かチェック
引数：
	unsigned char scinum : SCIモジュール番号
返値：
	1 : 空
	0 : 空じゃない
**********************/
char isEmptyRxBuff(unsigned char scinum);


/*********************
SCI 送信バッファに溜ってるデータ数
引数：
	unsigned char scinum : SCIモジュール番号
返値：
	int : データ数
**********************/
int countTxBuffData(unsigned char scinum);




/*********************
SCI 受信割り込み
引数：なし
返値：なし
TDREの割り込みによって動作
**********************/
void intSCI_rx_buff(unsigned char scinum);


/*********************
DMA0 割り込み
引数：
	なし
返値：
	なし
SCI0送信用
バッファにデータがなかったら割り込み終わり	
データあったらセットしなおして送信。
**********************/

void intDMAC_CMAC0I(void);	// TXD0用
void intDMAC_CMAC1I(void);	// RXD0用
void intDMAC_CMAC2I(void);	// TXD1用
void intDMAC_CMAC3I(void);	// RXD1用



#endif
