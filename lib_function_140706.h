/***********************************************************************/
/*                                                                     */
/*  ちょっと便利な関数群                                               */
/*  DATE        :Oct 17, 2011                                     */
/*  CPU TYPE    :SH7125                                                */
/*                                                                     */
/***********************************************************************/

#ifndef MYFUNCTION
#define MYFUNCTION


//#ifndef _STDARG
//#define _STDARG
	#include <stdarg.h>	//可変長引数
//#endif

#include <CommonDataType.h>

#define my_abs(a) a<0?-a:a
#define my_max(a, b) a<b?b:a
#define my_min(a, b) a<b?a:b


void delayMs(uint16_t);
void setupDelay(uint16_t CmtNum);
unsigned short int checkBit(uint16_t, uint16_t);
uint16_t setBit(uint16_t, uint16_t);
uint16_t clearBit(uint16_t, uint16_t);
uint16_t writeBit(uint16_t data, uint8_t writedata,uint16_t);
double myPow(int, signed int);
signed int saturateValue(signed int value, int max);
float saturateValuef(float value, float max);

int myPrintf(void(*pfunc)(char), char *format, va_list ap);

int myItoa_d(long integer, char *asci);
int myItoa_x(long integer, char *asci);
int myItoa_X(long integer, char *asci);

uint16_t my_abs_int16_t(int16_t num);

#endif
