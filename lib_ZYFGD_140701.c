/********************************************/
/*				カラー液晶関数				*/
/*					for RX621				*/
/*					Wrote by conpe_			*/
/*							2014/07/01		*/
/********************************************/


// 【仕様】
// コントローラ：ST7735S


//【更新履歴】
// Ver.140701
//  2014.07.01 書き始め


//【使い方】
// initPortST7725S();をできるだけ早く実行
// initST7725S();を実行


/*
HardWareSetting.h内で指定するもの

// -------- lib_ST7725S --------
#include <lib_ZYFGD_140701.h>

#define ST7735_CS	PORTE.DR.BIT.B4
#define ST7735_CD	PORTE.DR.BIT.B3
#define ST7735_RD	PORTE.DR.BIT.B2
#define ST7735_WR	PORTE.DR.BIT.B1
#define ST7735_RES	PORTE.DR.BIT.B0
#define ST7735_D0	PORTD.DR.BIT.B0
#define ST7735_D1	PORTD.DR.BIT.B1
#define ST7735_D2	PORTD.DR.BIT.B2
#define ST7735_D3	PORTD.DR.BIT.B3
#define ST7735_D4	PORTD.DR.BIT.B4
#define ST7735_D5	PORTD.DR.BIT.B5
#define ST7735_D6	PORTD.DR.BIT.B6
#define ST7735_D7	PORTD.DR.BIT.B7

#define ST7735_CS_DDR	PORTE.DDR.BIT.B4
#define ST7735_CD_DDR	PORTE.DDR.BIT.B3
#define ST7735_RD_DDR	PORTE.DDR.BIT.B2
#define ST7735_WR_DDR	PORTE.DDR.BIT.B1
#define ST7735_RES_DDR	PORTE.DDR.BIT.B0
#define ST7735_D0_DDR	PORTD.DDR.BIT.B0
#define ST7735_D1_DDR	PORTD.DDR.BIT.B1
#define ST7735_D2_DDR	PORTD.DDR.BIT.B2
#define ST7735_D3_DDR	PORTD.DDR.BIT.B3
#define ST7735_D4_DDR	PORTD.DDR.BIT.B4
#define ST7735_D5_DDR	PORTD.DDR.BIT.B5
#define ST7735_D6_DDR	PORTD.DDR.BIT.B6
#define ST7735_D7_DDR	PORTD.DDR.BIT.B7


*/

#include "iodefine.h"
#include <HardwareSetting.h>


/********* グローバル変数 *********/






/*********************
ZYFGD初期化
**********************/
void initST7735S(void){
	
	// ポート設定
	//initPortST7725S();	// HardwareSetup()から実行。
	
	ST7735_RES = 1;// RESX
	ST7735_RD = 1;
	ST7735_WR = 1;
	ST7735_CS = 1;
	ST7735_CD = 0;
	
    delayMs(100);
	
	//RESET
	ST7735_RES = 0;
	delayMs(5);
	ST7735_RES = 1;
	delayMs(120);

	writeCommandST7725S(0x01);//SOFTWARE RESET
	delayMs(50);

	writeCommandST7725S(0x11);//SLEEP OUT
	delayMs(200);

	writeCommandST7725S(0xFF);//VCOM4 level
	writeDataST7725S(0x40);//TC2=4, TC1=0 clock delay
	writeDataST7725S(0x03);//TC3=3 clock delay
	writeDataST7725S(0x1A);

	writeCommandST7725S(0xd9);//EEPROM control status?
	writeDataST7725S(0x60);//30h?
	writeCommandST7725S(0xc7);//Set VCOM offset control?
	writeDataST7725S(0x90);// vmf = vmh (same as 10h)
	delayMs(200);

	writeCommandST7725S(0xB1);//Frame Rate Control(In normal mode/ Full colors)
	//Frame rate= f_osc/((RTNA + 20)*(LINE+FPA+BPA))
	writeDataST7725S(0x04);//RTNA = 4h
	writeDataST7725S(0x25);//FPA=25h = 37
	writeDataST7725S(0x18);//BPA=18h = 24

	writeCommandST7725S(0xB2);//Frame Rate Control (In Idle mode/ 8-colors)
	writeDataST7725S(0x04);
	writeDataST7725S(0x25);
	writeDataST7725S(0x18);

	writeCommandST7725S(0xB3);//Frame Rate Control (In Partial mode/ full colors)
	//1st parameter to 3rd parameter are used in line inversion mode.
	writeDataST7725S(0x04);
	writeDataST7725S(0x25);
	writeDataST7725S(0x18);
	//4th parameter to 6th parameter are used in frame inversion mode.
	writeDataST7725S(0x04);
	writeDataST7725S(0x25);
	writeDataST7725S(0x18);

	writeCommandST7725S(0xB4);//Display Inversion Control(0:line inversion, 1:frame inversion)
	//[3:0] in full colors normal mode, in idle mode, in full colors partial mode (Partial mode on / Idle mode off)
	writeDataST7725S(0x03);// 011

	writeCommandST7725S(0xB6);//Display Function set 5
	writeDataST7725S(0x15);//NO=1,SDT=1,EQ=1 (default)
	writeDataST7725S(0x02);//PTG=0,PT=2

	writeCommandST7725S(0xC0);// POWER CONTROL 1 GVDD&AVDD
	writeDataST7725S(0x02);//4.7V (default)
	writeDataST7725S(0x70);//1.0uA (default)

	writeCommandST7725S(0xC1);// POWER CONTROL 2 VGHH&VCLL
	writeDataST7725S(0x07);// 14.7V, -12.25V

	writeCommandST7725S(0xC2);// POWER CONTROL 3 (in Normal mode/ Full colors)
	writeDataST7725S(0x01);// (default)
	writeDataST7725S(0x01);// (default)

	writeCommandST7725S(0xC3);// POWER CONTROL 4 (in Idle mode/ 8-colors)
	writeDataST7725S(0x02);// (default)
	writeDataST7725S(0x07);// (default)

	writeCommandST7725S(0xC4);// POWER CONTROL 5 (in Partial mode/ full-colors)
	writeDataST7725S(0x02);// (default)
	writeDataST7725S(0x04);// (default)

	writeCommandST7725S(0xFC);// POWER CONTROL 6 (in Partial mode + Idle mode)
	writeDataST7725S(0x11);// (default)
	writeDataST7725S(0x17);// 15h (default) -> 17h dcd

	writeCommandST7725S(0xC5);// VCOMH&VCOML
	writeDataST7725S(0x3c);// 4V (default) 
	writeDataST7725S(0x4f);// 3a:-1.050V(default) -> 4f:-0.525V

	writeCommandST7725S(0x36);//Memory data access control
	writeDataST7725S(0xC8);//MY-MX-MV-ML-RGB-MH-0-0 = 11001000 , (mirror xy, BGR)


	writeCommandST7725S(0x3a);//Interface pixel format
	writeDataST7725S(0x05);//16bit mode

	//GAMMA SET BY REGISTER
	//***********************GAMMA*************************
	writeCommandST7725S(0xE0);// + polarity
	writeDataST7725S(0x06);
	writeDataST7725S(0x0E);
	writeDataST7725S(0x05);
	writeDataST7725S(0x20);
	writeDataST7725S(0x27);
	writeDataST7725S(0x23);
	writeDataST7725S(0x1C);
	writeDataST7725S(0x21);
	writeDataST7725S(0x20);
	writeDataST7725S(0x1C);
	writeDataST7725S(0x26);
	writeDataST7725S(0x2F);
	writeDataST7725S(0x00);
	writeDataST7725S(0x03);
	writeDataST7725S(0x00);
	writeDataST7725S(0x24);

	writeCommandST7725S(0xE1);// - polarity
	writeDataST7725S(0x06);
	writeDataST7725S(0x10);
	writeDataST7725S(0x05);
	writeDataST7725S(0x21);
	writeDataST7725S(0x27);
	writeDataST7725S(0x22);
	writeDataST7725S(0x1C);
	writeDataST7725S(0x21);
	writeDataST7725S(0x1F);
	writeDataST7725S(0x1D);
	writeDataST7725S(0x27);
	writeDataST7725S(0x2F);
	writeDataST7725S(0x05);
	writeDataST7725S(0x03);
	writeDataST7725S(0x00);
	writeDataST7725S(0x3F);

	//***************************RAM ADDRESS*******************
	writeCommandST7725S(0x2A);//column address set
	writeDataST7725S(0x00);//xs = 02h = 2
	writeDataST7725S(0x02);
	writeDataST7725S(0x00);//xe = 81h = 129
	writeDataST7725S(0x81);

	writeCommandST7725S(0x2B);//row address set
	writeDataST7725S(0x00);//ys = 03h = 3
	writeDataST7725S(0x03);
	writeDataST7725S(0x00);//ye = 82h = 130
	writeDataST7725S(0x82);

	writeCommandST7725S(0x29);//display on
	delayMs(100);

	writeCommandST7725S(0x2C); //memory write
	
}	

/*********************
ZYFGD ポート初期化
**********************/
void initPortST7725S(void){
	
	ST7735_CS_DDR = 1;	
	ST7735_CD_DDR = 1;	
	ST7735_RD_DDR = 1;
	ST7735_WR_DDR = 1;
	ST7735_RES_DDR = 1;
	ST7735_D0_DDR = 1;
	ST7735_D1_DDR = 1;
	ST7735_D2_DDR = 1;
	ST7735_D3_DDR = 1;
	ST7735_D4_DDR = 1;
	ST7735_D5_DDR = 1;
	ST7735_D6_DDR = 1;
	ST7735_D7_DDR = 1;
	
	
	ST7735_RES = 1;// RESX
	ST7735_RD = 1;
	ST7735_WR = 1;
	ST7735_CS = 1;
	ST7735_CD = 0;

}

void writeCommandST7725S(uint8_t Command){
	ST7735_CS = 0;
	ST7735_CD = 0;	// Command
	
	ST7735_WR = 0;
	
	ST7735_D0 = Command&0x01;
	ST7735_D1 = (Command>>1)&0x01;
	ST7735_D2 = (Command>>2)&0x01;
	ST7735_D3 = (Command>>3)&0x01;
	ST7735_D4 = (Command>>4)&0x01;
	ST7735_D5 = (Command>>5)&0x01;
	ST7735_D6 = (Command>>6)&0x01;
	ST7735_D7 = (Command>>7)&0x01;
	
	//PORTD.DR.BYTE = Command;
	
//delayMs(1);
	ST7735_WR = 1;
	ST7735_CS = 1;
	
}

void writeDataST7725S(uint8_t Data){
		
	ST7735_CS = 0;
	ST7735_CD = 1;	// Data
	
	ST7735_WR = 0;
	
	ST7735_D0 = Data&0x01;
	ST7735_D1 = (Data>>1)&0x01;
	ST7735_D2 = (Data>>2)&0x01;
	ST7735_D3 = (Data>>3)&0x01;
	ST7735_D4 = (Data>>4)&0x01;
	ST7735_D5 = (Data>>5)&0x01;
	ST7735_D6 = (Data>>6)&0x01;
	ST7735_D7 = (Data>>7)&0x01;
	
	//PORTD.DR.BYTE = Data;

	ST7735_WR = 1;
	ST7735_CS = 1;
	
}

void writeDataBurstST7725S(uint8_t Data){
		
	ST7735_CS = 0;
	ST7735_CD = 1;	// Data
	
	ST7735_WR = 0;
	
	ST7735_D0 = Data&0x01;
	ST7735_D1 = (Data>>1)&0x01;
	ST7735_D2 = (Data>>2)&0x01;
	ST7735_D3 = (Data>>3)&0x01;
	ST7735_D4 = (Data>>4)&0x01;
	ST7735_D5 = (Data>>5)&0x01;
	ST7735_D6 = (Data>>6)&0x01;
	ST7735_D7 = (Data>>7)&0x01;
	
	//PORTD.DR.BYTE = Data;
//delayMs(1);

	ST7735_WR = 1;
	ST7735_CS = 1;
	
}