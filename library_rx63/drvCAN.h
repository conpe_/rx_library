/********************************************/
/*		CAN通信							*/
/*					for RX63n @ CS+		*/
/*					Wrote by conpe_		*/
/*							2018/02/25	*/
/********************************************/
// RX63nのCANモジュールでCAN通信

// ひとまず単純な送受信
// 標準ID

// 使ってる外の関数
// getPCLK(); @ GR_define.h

//【更新履歴】
// 2018.02.25 新規作成

//【更新予定】
// 


#ifndef __DRVCAN_H__
#define __DRVCAN_H__

#include "iodefine.h"
#include "CommonDataType.h"
#include "RingBuffer.h"
#include "portReg.h"

typedef struct{
    uint16_t Id;
    uint8_t Len;
    uint8_t Data[8];
    bool_t RemoteFrame;
} CanData_t;

// 
//#define CAN_USE_TX_INTERRUPT

// CANモジュールの数
#define CAN_MODULE_NUM 3

// ポート
#define PORT_SCL0 P12
#define PORT_SDA0 P13
#define PORT_SCL2 P16
#define PORT_SDA2 P17

// ボーレート
#define CAN_BAUDRATE_DEFAULT 250	// kbps
// リングバッファのサイズ
#define CAN_TX_BUFFER_SIZE_DEFAULT 8		// メールボックスが32chあるので，そんなには必要ない
#define CAN_RX_BUFFER_SIZE_DEFAULT 8		// 

// エラーコード
#define CAN_ERR_OK	0
#define CAN_ERR_NG	-1
#define CAN_ERR_TIMEOUT	-2


// どっかに実体がいる関数
extern uint32_t getPCLK(void);

typedef enum {
	CAN_CAN0 = 0,		/* 配列のインデックスで使うので値は0〜にすること */
	CAN_CAN1 = 1,
	CAN_CAN2 = 2
} can_module_e;




// CAN処理(CAN モジュール)
class Can_t{
private:
	typedef enum {
		CAN_CLOCK_DIV1 = 0x00,  
		CAN_CLOCK_DIV2 = 0x01,  
		CAN_CLOCK_DIV4 = 0x02,  
		CAN_CLOCK_DIV8 = 0x03, 
	} can_clock_div_e;
	
public:
	// Constructor
	Can_t(can_module_e CanModule);
	~Can_t(void);
	
	// 準備
	int8_t begin(uint16_t Baud_kbps = CAN_BAUDRATE_DEFAULT, uint8_t TxBuffNum = CAN_TX_BUFFER_SIZE_DEFAULT, uint8_t RxBuffNum = CAN_RX_BUFFER_SIZE_DEFAULT);

	void task(void);
	
	// 送信
	int8_t send(CanData_t* TxData);
	int8_t send(uint16_t Id, uint8_t Len,  uint8_t* TxData, bool_t Rf=false);
	
	// 受信データ取得
	int8_t getRcvData(CanData_t* RcvData){return RxBuff->read(RcvData);};
	
	// isr
public:
	static void isrTx(can_module_e CanModule);
	static void isrRx(can_module_e CanModule);
	static void isrErr(void);
private:
	static Can_t* CanInstance[CAN_MODULE_NUM];
	void isrTxMod(void);
	void isrRxMod(void);
	void isrErrMod(void);
	bool_t fSending;
private:
	
	volatile __evenaccess struct st_can *CANn;	// レジスタ
	can_module_e CanModule;				// CANモジュール選択
	uint16_t Baud_kbps;				// 通信速度
	
	RingBuffer<CanData_t> *TxBuff;	// 通信処理記憶バッファ
	RingBuffer<CanData_t> *RxBuff;	// 通信処理記憶バッファ
	uint8_t TxBuffNumMax;
	uint8_t RxBuffNumMax;
	
	int8_t initRegister(uint16_t Baud_kbps);
	void setPinModeCAN(void);
	
	uint32_t LastSendTime;
	
};


#endif