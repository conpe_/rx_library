/********************************************/
/*		CAN通信							*/
/*					for RX63n @ CS+		*/
/*					Wrote by conpe_		*/
/*							2018/02/25	*/
/********************************************/

#include "drvCAN.h"
#include "GR_define.h"

//絶対beginするんだぞっ
/* 
Can_t CAN_0(CAN_CAN0);
Can_t CAN_1(CAN_CAN1);
Can_t CAN_2(CAN_CAN2);
*/
#define CAN_MB_NUM 32

Can_t* Can_t::CanInstance[CAN_MODULE_NUM] = {0};

/****************************
 CAN コンストラクタ
概要：
引数：
 モジュールNo.
返値：
 なし
****************************/
Can_t::Can_t(can_module_e CanModule)
{
	TxBuffNumMax = 0;
	RxBuffNumMax = 0;
	CanInstance[(uint8_t)CanModule] = this;
	
	switch(CanModule){
	case CAN_CAN0:
		CANn = &CAN0;
		break;
	case CAN_CAN1:
		CANn = &CAN1;
		break;
	case CAN_CAN2:
		CANn = &CAN2;
		break;
	default:
		/* do nothing */
		break;
	} 
	
	LastSendTime = 0;
}

/****************************
 CAN デストラクタ
概要：
 確保したメモリを開放する
引数：
 なし
返値：
 なし
****************************/
Can_t::~Can_t(void){
	
}


/****************************
 CAN通信初期化
概要：
 CANモジュールの初期化を行う。
 初回のみ。
引数：
	uint16_t Baud_kbps	ボーレート[kbps]単位
	uint16_t TxBuffNum	送信バッファの数
返値：
 送受信バッファ、通信内容バッファの領域を確保できなければ-1
****************************/
int8_t Can_t::begin(uint16_t Baud_kbps, uint8_t TxBuffNum, uint8_t RxBuffNum){
	CanData_t tmp;
	
	this->Baud_kbps = Baud_kbps;
	
	// pin mode setting
	setPinModeCAN();
	
	// CAN setting
	initRegister(Baud_kbps);
	
		
	
	//TxBuffer
	if(this->TxBuffNumMax < TxBuffNum){	// より多くのバッファを要求
		this->TxBuffNumMax = TxBuffNum;
		
		RingBuffer<CanData_t>* TxBuffOld = TxBuff;
		
		TxBuff = new RingBuffer<CanData_t>(TxBuffNum);
		if(TxBuff==NULL){
			// __heap_chk_fail();
			return -1;
		}
		if(NULL != TxBuffOld){
			while(BUFFER_READ_OK==TxBuffOld->read(&tmp)){
				TxBuff->add(tmp);
			}
			delete TxBuffOld;
		}
	}
	
	//RxBuffer
	if(this->RxBuffNumMax < RxBuffNum){	// より多くのバッファを要求
		this->RxBuffNumMax = RxBuffNum;
		
		RingBuffer<CanData_t>* RxBuffOld = RxBuff;
		
		RxBuff = new RingBuffer<CanData_t>(RxBuffNum);
		if(RxBuff==NULL){
			// __heap_chk_fail();
			return -1;
		}
		if(NULL != RxBuffOld){
			while(BUFFER_READ_OK==RxBuffOld->read(&tmp)){
				RxBuff->add(tmp);
			}
			delete RxBuffOld;
		}
	}
	
	
	
	return 0;
}

/****************************
 CANタスク
概要：
 通信が終わったオブジェクトのコールバック関数を実行し、
 オブジェクトを削除。
引数：
 なし
返値：
 なし
****************************/
void Can_t::task(void){
#if !defined(CAN_USE_TX_INTERRUPT)
	// 割り込みoff時
	isrTxMod();
#endif
}

/****************************
 CANレジスタ設定
概要：
引数：
 ボーレート[kbps] 現在は固定値
返値：
 なし
****************************/
int8_t Can_t::initRegister(uint16_t Baud_kbps){
	//uint8_t BaudCnt;
	
	// モジュール起こす
	SYSTEM.PRCR.WORD = 0xA503u;
	switch(CanModule){
	case CAN_CAN0:
		MSTP(CAN0) = 0u;
		break;
	case CAN_CAN1:
		MSTP(CAN1) = 0u;
		break;
	case CAN_CAN2:
		MSTP(CAN2) = 0u;
		break;
	default:
		/* do nothing */
		return 1;
		break;
	}
	
	CANn->CTLR.BIT.CANM = 1;	/* CAN動作モード リセットモード */
	while(1 != CANn->STR.BIT.RSTST);
	CANn->CTLR.BIT.SLPM = 0;	/* スリープ解除 */
	while(1 == CANn->STR.BIT.SLPST);
	
	CANn->CTLR.BIT.MBM = 0;	/* メールボックスモード 通常 */
	CANn->CTLR.BIT.IDFM = 0;	/* IDフォーマットモード ミックス */
	CANn->CTLR.BIT.MLM = 0;		/* メッセージロストモード オーバライト */
	CANn->CTLR.BIT.TPM = 0;		/* 送信優先度 ID優先 */
	CANn->CTLR.BIT.TSRC = 1;	/* タイムスタンプクリア */
	CANn->CTLR.BIT.TSPS = 0;	/* タイムスタンププリスケーラ 1 */
	//CANn->CTLR.BIT.CANM = 0;	/* CAN動作モード CANオペレーションモード */
	CANn->CTLR.BIT.SLPM = 0;	/* CANスリープモード notスリープ */
	CANn->CTLR.BIT.BOM = 0;		/* バスオフ復帰モード ノーマル */
	CANn->CTLR.BIT.RBOC = 0;	/* バスオフ強制復帰ビット なし */
	
	// ビットレート設定
	// 125kbps
	CANn->BCR.BIT.CCLKS = 0;	/* CANクロックソース選択 PCLK */
	CANn->BCR.BIT.TSEG2 = 1;	/* タイムセグメント2制御 2Tq (TSEG1より小さくする) */
	CANn->BCR.BIT.SJW = 0;		/* 再同期ジャンプ幅制御 1Tq */
//	CANn->BCR.BIT.BRP = 44;		/* プリスケーラ分周比選択 0〜1023 125kbps計算値  */
//	CANn->BCR.BIT.BRP = 47;		/* プリスケーラ分周比選択 0〜1023 125kpbs実測値 */
	CANn->BCR.BIT.BRP = 46;		/* プリスケーラ分周比選択 0〜1023 125kpbs dsPIC実力 */
//	CANn->BCR.BIT.BRP = 23;		/* プリスケーラ分周比選択 0〜1023 250kpbs */
	CANn->BCR.BIT.TSEG1 = 4;	/* タイムセグメント1制御 5Tq */
	
	
	// 割り込み設定
	
	// エラー割り込み許可
	CANn->EIER.BIT.BEIE = 0;		/* バスエラー */
	CANn->EIER.BIT.EWIE = 0;		/* エラーワーニング */
	CANn->EIER.BIT.EPIE = 0;		/* エラーパッシブエントリ */
	CANn->EIER.BIT.BOEIE = 0;		/* バスオフ開始 */
	CANn->EIER.BIT.BORIE = 0;		/* バスオフ復帰 */
	CANn->EIER.BIT.ORIE = 0;		/* オーバラン */
	CANn->EIER.BIT.OLIE = 0;		/* オーバロードフレーム送信 */
	CANn->EIER.BIT.BLIE = 0;		/* バスロック */
	
	
	
	// フィルタ設定
	
	
	// メールボックス設定
	// のちのち関数化
	
	CANn->CTLR.BIT.CANM = 2;	/* CAN Haltモードにする*/
	while(1 != CANn->STR.BIT.HLTST);	/* Haltモード待ち */
	
	#if defined(CAN_USE_TX_INTERRUPT)
	CANn->MIER.LONG = 0xFFFFFFFF;	/* 全メールボックスの割り込みを許可 */
	#else
	CANn->MIER.LONG = 0x000FFFF;
	#endif
	
	
	uint8_t i;
	
	for(i=0; i<CAN_MB_NUM; ++i){
		CANn->MCTL[i].BYTE = 0x00;	/* クリア */
	}
	
	// 受信
	for(i=0; i<CAN_MB_NUM/2; ++i){
		// フィルタ設定
		// 自IDとブロードキャスト
		if(i<CAN_MB_NUM/2/2){		// 半分
			CANn->MB[i].ID.BIT.SID = 0x00;	// 自分のIDのみ受ける
		}else{
			CANn->MB[i].ID.BIT.SID = 0x00FF;	// FFのみ受ける
		}
		CANn->MB[i].ID.BIT.IDE = 0;
		CANn->MB[i].ID.BIT.RTR = 0;
		
		CANn->MCTL[i].BIT.RX.ONESHOT = 0;	/* メールボックスを非ワンショットに設定 */
		CANn->MCTL[i].BIT.RX.RECREQ = 1;	/* メールボックスを受信に設定 */
	}
	// 送信
	for(i=CAN_MB_NUM/2; i<CAN_MB_NUM; ++i){
		CANn->MCTL[i].BIT.TX.ONESHOT = 0;	/* メールボックスを非ワンショットに設定 */
		//CANn->MCTL[i].BIT.TX.TRMREQ = 1;	/* メールボックスを送信に設定 */
		/* 送信時に設定する(要求を出す) */
	}
	
	// マスク
	// 下1byte完全一致
	for(i=0; i<8; ++i){
		if(i%2){
			CANn->MKR[i].BIT.SID = 0x00FF;
		}else{
			CANn->MKR[i].BIT.SID = 0x0000;
		}
		CANn->MKR[i].BIT.EID = 0x0000;	/* 拡張ID使わない */
	}
	CANn->MKIVLR.LONG = 0x0000;	/* マスク有効 */
	
	
	IEN(ICU,GROUP0) = 0;	// エラー割り込み off
	
	switch(CanModule){
	case CAN_CAN0:
		IEN(CAN0,TXF0) = 0;
		IPR(CAN0,TXF0) = 9;
		IEN(CAN0,RXF0) = 0;
		IPR(CAN0,RXF0) = 9;
		IEN(CAN0,TXM0) = 1;
		IPR(CAN0,TXM0) = 9;
		IEN(CAN0,RXM0) = 1;
		IPR(CAN0,RXM0) = 9;
		break;
	case CAN_CAN1:
		IEN(CAN1,TXF1) = 0;
		IPR(CAN1,TXF1) = 9;
		IEN(CAN1,RXF1) = 0;
		IPR(CAN1,RXF1) = 9;
		IEN(CAN1,TXM1) = 1;
		IPR(CAN1,TXM1) = 9;
		IEN(CAN1,RXM1) = 1;
		IPR(CAN1,RXM1) = 9;
		break;
	case CAN_CAN2:
		IEN(CAN2,TXF2) = 0;
		IPR(CAN2,TXF2) = 9;
		IEN(CAN2,RXF2) = 0;
		IPR(CAN2,RXF2) = 9;
		IEN(CAN2,TXM2) = 1;
		IPR(CAN2,TXM2) = 9;
		IEN(CAN2,RXM2) = 1;
		IPR(CAN2,RXM2) = 9;
		break;
	}
	
	
	// CANリセット解除
	CANn->CTLR.BIT.CANM = 0;	/* CAN動作モード CANオペレーションモード */
	while(1 == CANn->STR.BIT.RSTST);
	while(1 == CANn->STR.BIT.HLTST);
	
	return 0;
}

/****************************
 CAN関係入出力ポート設定
概要：
引数：
 なし
返値：
 なし
****************************/
void Can_t::setPinModeCAN(void){
	
	// ポート出力設定
	MPC.PWPR.BYTE = 0x00u;
	MPC.PWPR.BYTE = 0x40u;
	SYSTEM.PRCR.WORD = 0xA503u;
	
	switch(CanModule){
	case CAN_CAN0:	/* P32,P33  or  PD1,PD2 */
		setPinMode(P33, PIN_INPUT);		// CRX input
									// 受信バッファ有効
		MPC.P33PFS.BIT.PSEL = 0x10;		// CRX0
		PORT3.PMR.BIT.B3 = 1;			// 周辺機能	
		
		setPinMode(P32, PIN_OUTPUT);		// CTX output
									// 受信バッファ無効
		MPC.P32PFS.BIT.PSEL = 0x10;		// CTX0
		PORT3.PMR.BIT.B2 = 1;			// 周辺機能
		break;
	case CAN_CAN1:
		setPinMode(P55, PIN_INPUT);		// CRX input
									// 受信バッファ有効
		MPC.P55PFS.BIT.PSEL = 0x10;		// CRX0
		PORT5.PMR.BIT.B5 = 1;			// 周辺機能	
		
		setPinMode(P54, PIN_OUTPUT);		// CTX output
									// 受信バッファ無効
		MPC.P54PFS.BIT.PSEL = 0x10;		// CTX0
		PORT5.PMR.BIT.B4 = 1;			// 周辺機能
		break;
	case CAN_CAN2:
		setPinMode(P33, PIN_INPUT);		// CRX input
									// 受信バッファ有効
		MPC.P33PFS.BIT.PSEL = 0x10;		// CRX0
		PORT3.PMR.BIT.B3 = 1;			// 周辺機能	
		
		setPinMode(P32, PIN_OUTPUT);		// CTX output
									// 受信バッファ無効
		MPC.P32PFS.BIT.PSEL = 0x10;		// CTX0
		PORT3.PMR.BIT.B2 = 1;			// 周辺機能
		break;
	}
	
}

// CAN送信登録
int8_t Can_t::send(CanData_t* TxData){
	if(TxBuff->add(*TxData)){	// バッファに追加
		isrTxMod();			// 送る
		if(TxBuff->add(*TxData)){
			return 1;			// バッファいっぱい
		}
	}
	
	isrTxMod();			// 送る
	
	return 0;
}

int8_t Can_t::send(uint16_t Id, uint8_t Len,  uint8_t* TxData, bool_t Rf){
	CanData_t CanData;
	CanData.Id = Id;
	CanData.Len = Len;
	memcpy(CanData.Data, TxData, Len);
	CanData.RemoteFrame = Rf;
	return send(&CanData);
}
	

/****************************
 CAN割り込み 振り分け
 各モジュールインスタンスの割り込み関数を実行
****************************/
 void Can_t::isrTx(can_module_e CanModule){
	CanInstance[(uint8_t)CanModule]->isrTxMod();
 }
 void Can_t::isrRx(can_module_e CanModule){
	CanInstance[(uint8_t)CanModule]->isrRxMod();
 }
 void Can_t::isrErr(void){
	uint8_t i;
	 
	for(i = 0; i<CAN_MODULE_NUM; ++i){
		if(0!=CanInstance[i]){	/* インスタンス有効 */
			if(0!=CanInstance[i]->CANn->EIFR.BYTE){	/* エラーあり */
				CanInstance[i]->isrErrMod();
			}
		}
	}
	
 }


/****************************
 CAN送信割り込み
バッファにたまってる分を送る
呼ばれる条件：
****************************/
void Can_t::isrTxMod(void){
	CanData_t TxData;
	uint8_t i, j;
	
	if(!fSending){
		fSending = 1;
		// 各メールボックスメンテ
		// 送信データあれば送信
		for(i = CAN_MB_NUM/2; i<CAN_MB_NUM; ++i){
			// 送信メールボックス空いてる, 受信として使っていない
			if((0==CANn->MCTL[i].BIT.RX.RECREQ) && ((1==CANn->MCTL[i].BIT.TX.SENTDATA) || (0==CANn->MCTL[i].BIT.TX.TRMACTIVE))){	
				
				if(0x00 != CANn->MCTL[i].BYTE){		// さっき使った
					CANn->MCTL[i].BYTE = 0x00;		// クリア
				}else{
					#if !defined(CAN_USE_TX_INTERRUPT)
					//if((0==CANn->STR.BIT.TRMST) && (0==CANn->STR.BIT.RECST)){	// バスオフ状態
					if(1 < getTime_ms(LastSendTime)){				// 最後に送ってから4ms待てばさすがに送信終わってるでしょ
					#endif
						if(BUFFER_READ_OK == TxBuff->read(&TxData)){		// 送信するデータ有る
							
							LastSendTime = getTime_ms();
						
							CANn->MB[i].ID.BIT.SID = TxData.Id;	
							CANn->MB[i].DLC = TxData.Len;
							//memcpy( (void *)CANn->MB[i].DATA, TxData.Data, TxData.Len);	/* ずれてコピーされる事例が合ったのでforに変更 */
							for(j = 0; j<TxData.Len; ++j){
								CANn->MB[i].DATA[j] = TxData.Data[j];
							}						
							
							// データフレーム or リモートフレーム
							CANn->MB[i].ID.BIT.RTR = TxData.RemoteFrame;
							
							// 標準ID or 拡張ID
							CANn->MB[i].ID.BIT.IDE = 0;		/* 標準ID */
							
							CANn->MCTL[i].BIT.TX.ONESHOT = 0;	/* メールボックスを非ワンショットに設定 */
		
							CANn->MCTL[i].BIT.TX.TRMREQ = 1;	/* 送信開始 */
							
						}
					#if !defined(CAN_USE_TX_INTERRUPT)
					}
					#endif
				}
			}
		}
		fSending = 0;
	}
}

/****************************
 CAN受信割り込み
****************************/
void Can_t::isrRxMod(void){
	uint8_t i, j;
	CanData_t CanData;
	
	for(i=0; i<CAN_MB_NUM/2; ++i){
		// 受信確認
		if((1==CANn->MCTL[i].BIT.RX.RECREQ) && (CANn->MCTL[i].BIT.RX.NEWDATA && (0==CANn->MCTL[i].BIT.RX.INVALDATA))){
			// メッセージ受信完了
			CANn->MCTL[i].BYTE = 0x00;	// フラグクリア
		
			CanData.Id = CANn->MB[i].ID.BIT.SID;	
			CanData.Len = CANn->MB[i].DLC;
			//memcpy(CanData.Data, (const void *)CANn->MB[i].DATA, CanData.Len);	/* ずれてコピーされる事例が合ったのでforに変更 */
			for(j = 0; j<CanData.Len; ++j){
				CanData.Data[j] = CANn->MB[i].DATA[j];
			}
			
			RxBuff->add(CanData);
			
			
			/*
			// フィルタ設定 ここで設定し直さなくても，はじめに設定したマスクに一致したIDになるので不要
			if(i<CAN_MB_NUM/2/2){
				CANn->MB[i].ID.BIT.SID = MY_ID;	// 自分のIDのみ受ける
			}else{
				CANn->MB[i].ID.BIT.SID = 0x00FF;	// FFのみ受ける
			}
			*/
			// 標準ID or 拡張ID
			CANn->MB[i].ID.BIT.IDE = 0;		/* 標準ID */
			// データクリア
			for(j=0; j<8; ++j){
				CANn->MB[i].DATA[j] = 0x00;
			}
			
			// 受信開始
			CANn->MCTL[i].BIT.RX.RECREQ = 1;
			CANn->MCTL[i].BIT.RX.TRMREQ = 0;
		}
	}
	
}



/****************************
 CAN エラー検出
****************************/
void Can_t::isrErrMod(void){
	
	
}


// interrupt
#pragma section IntPRG

/* CAN0 */
#pragma interrupt (Excep_CAN0_TXF0(enable, vect=VECT(CAN0,TXF0)))
void Excep_CAN0_TXF0(void)	/* 送信FIFO(未使用) */
{
	Can_t::isrTx(CAN_CAN0);
}
#pragma interrupt (Excep_CAN0_RXF0(enable, vect=VECT(CAN0,RXF0)))
void Excep_CAN0_RXF0(void){	/* 受信FIFO(未使用) */
	Can_t::isrRx(CAN_CAN0);
}
#pragma interrupt (Excep_CAN0_TXM0(enable, vect=VECT(CAN0,TXM0)))
void Excep_CAN0_TXM0(void)	/* 送信メールボックス */
{
	Can_t::isrTx(CAN_CAN0);
}
#pragma interrupt (Excep_CAN0_RXM0(enable, vect=VECT(CAN0,RXM0)))
void Excep_CAN0_RXM0(void){	/* 受信メールボックス */
	Can_t::isrRx(CAN_CAN0);
}

/* CAN1 */
#pragma interrupt (Excep_CAN1_TXF1(enable, vect=VECT(CAN1,TXF1)))
void Excep_CAN1_TXF1(void)	/* 送信FIFO(未使用) */
{
	Can_t::isrTx(CAN_CAN1);
}
#pragma interrupt (Excep_CAN1_RXF1(enable, vect=VECT(CAN1,RXF1)))
void Excep_CAN1_RXF1(void){	/* 受信FIFO(未使用) */
	Can_t::isrRx(CAN_CAN1);
}
#pragma interrupt (Excep_CAN1_TXM1(enable, vect=VECT(CAN1,TXM1)))
void Excep_CAN1_TXM1(void)	/* 送信メールボックス */
{
	Can_t::isrTx(CAN_CAN1);
}
#pragma interrupt (Excep_CAN1_RXM1(enable, vect=VECT(CAN1,RXM1)))
void Excep_CAN1_RXM1(void){	/* 受信メールボックス */
	Can_t::isrRx(CAN_CAN1);
}

/* CAN2 */
#pragma interrupt (Excep_CAN2_TXF2(enable, vect=VECT(CAN2,TXF2)))
void Excep_CAN2_TXF2(void)	/* 送信FIFO(未使用) */
{
	Can_t::isrTx(CAN_CAN2);
}
#pragma interrupt (Excep_CAN2_RXF2(enable, vect=VECT(CAN2,RXF2)))
void Excep_CAN2_RXF2(void){	/* 受信FIFO(未使用) */
	Can_t::isrRx(CAN_CAN2);
}
#pragma interrupt (Excep_CAN2_TXM2(enable, vect=VECT(CAN2,TXM2)))
void Excep_CAN2_TXM2(void)	/* 送信メールボックス */
{
	Can_t::isrTx(CAN_CAN2);
}
#pragma interrupt (Excep_CAN2_RXM2(enable, vect=VECT(CAN2,RXM2)))
void Excep_CAN2_RXM2(void){	/* 受信メールボックス */
	Can_t::isrRx(CAN_CAN2);
}

/* CAN0〜2エラー */
#pragma interrupt (Excep_ICU_GROUP0(enable, vect=VECT(ICU,GROUP0)))
void Excep_ICU_GROUP0(void){	/* エラー */
	Can_t::isrErr();
}


#pragma section 

