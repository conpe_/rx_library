
#include "GR_Ex15_OLED.h"

gr_ex_oled GrExOled;
//OledDriverSSD1306 Oled;		// 旧ソフト向け


#define OLED_SW_CNT (50)	// スイッチ読み取り値がこれだけ連続したら確定


gr_ex_oled::gr_ex_oled(void):OledDriverSSD1306(){
	setFont(NULL);
}

gr_ex_oled::gr_ex_oled(font_t* SetFont):OledDriverSSD1306(){
	setFont(SetFont);
}

void gr_ex_oled::begin(void){
	
	// switch
	setPinMode(OLED_SW0, PIN_INPUT);
	setPinMode(OLED_SW1, PIN_INPUT);
	setPinMode(OLED_SW2, PIN_INPUT);
	setPinMode(OLED_SW3, PIN_INPUT);
	
	// 旧ソフト向け
	//Oled.begin(OLED_SPI_MODULE, MOSIB_PE6, RSPCKB_PE5, SSLB0_PE4, OLED_PIN_DC);
	
	// 親begin
	OledDriverSSD1306::begin(OLED_SPI_MODULE, MOSIB_PE6, RSPCKB_PE5, SSLB0_PE4, OLED_PIN_DC);
	
}

// タスク
// 1msで回す
void gr_ex_oled::task(void){
	uint8_t i;
	const pins SWPIN[OLED_SW_NUM] = {OLED_SW0, OLED_SW1, OLED_SW2, OLED_SW3};
	bool_t SwCurrent;
	
	
	for(i=0; i<OLED_SW_NUM; ++i){
		
		SwCurrent = readPin(SWPIN[i]);
		
		if(SwLast[i] != SwCurrent){	// 前回値と異なる
			SwCnt[i] = 0;
		}else{				// 前回値と同じ
			if(SwCnt[i] < OLED_SW_CNT){
				++SwCnt[i];
			}else{
				Sw[i] = !SwCurrent;	// スイッチ確定値更新(負論理)
			}
		}
		
		SwLast[i] = SwCurrent;
		
	}	
	
}

// 2byteフォントをセット
void gr_ex_oled::setFont(font_t* SetFont){
	fFullWidthChar = false;
	if(NULL!=SetFont){
		EnableFullWidthFont = true;
		Font = SetFont;
	}else{
		EnableFullWidthFont = false;
	}
}


// 2byte transmit
int8_t gr_ex_oled::writeChar(char* strchar){
	// 1byte transmit
	writeChar(strchar[0]);
	
	if(false != fFullWidthChar){	// if 2byte character
		return writeChar(strchar[1]);
	}
	
	return 0;
	
}

int8_t gr_ex_oled::writeChar(uint16_t sjis){	// shift jis(2byte)
	uint8_t i;
	const uint8_t* FontData;
	uint8_t j;
	uint8_t OutTmp;
	
	// check clearance
	if((OLED_WIDTH - _CurCol)<8){	// その行の終わり8列より少なかったら次の行にする
		setCurNextLine();
	}
	
	// transmit font
	if(0 == Font->getFontData(&FontData, sjis)){	// フォント取得ok
		for(i = 0; i<8; ++i){
			OutTmp = 0;
			for(j=0; j<8; ++j){
				OutTmp |= ((FontData[j]>>(7-i))&0x01) << j;
			}
			if(writeData(OutTmp)){
				return -1;
			}
			
		}
	}else{
		for(i = 0; i<8; i++){
			if(writeData(0x00)){
				return -1;
			}
		}
	}
	
	return 0;
}

int8_t gr_ex_oled::writeChar(char ch){		//
	
	if(false == fFullWidthChar){
		if(((0x81 <= ch) && (0x9F >= ch))
		   ||((0xE0 <= ch) && (0xEF >= ch))){
			fFullWidthChar = true;		// 2byte文字の範囲だったらフラグ立てて送信保留
			sjisUpperTmp = ch;
		}else{
			return OledDriverSSD1306::writeChar(ch);	// 1byte
		}
	}else{	// 2byte目
		fFullWidthChar = false;			// reset 2byte flag
		return writeChar((uint16_t)(((uint16_t)sjisUpperTmp<<8) | ch));	// 2byte
	}
	
	return 0;
}






