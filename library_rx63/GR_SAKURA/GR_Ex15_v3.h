/**************************************************
GR_Ex15_v3_SCI.h
	GR-SAKURA用の拡張基板GR_Ex15用のライブラリだよ。
	いろいろ初期化したりするよ。
	
	グローバルのインスタンス用意しておくよ。
	gr_ex GrEx;
	
	モータードライバ(ch0,ch1:noPWM, ch2,ch3:withPWM)
	ジャイロ
	スピーカ	gr_ex_sp SP;
	CAN通信
	シリアル通信	(要ソフトプルアップ)
	I2C
	エンコーダ
	サーボモータ
**************************************************/



//【更新予定】
// スピーカ処理をDMA化


#ifndef __GR_EX15_H__
#define __GR_EX15_H__


#include "portReg.h"
#include "RingBuffer.h"
#include "GR_define.h"

#include "InvensenseMPU.h"

enum grex_ad_ch{
	GREX_AD0,
	GREX_AD1,
	GREX_AD2,
	GREX_AD3,
	GREX_AD4,
	GREX_AD5,
	GREX_AD6,
	GREX_AD7
};

#define GREX_IO0 PD7
#define GREX_IO1 PD6
#define GREX_IO2 PD5
#define GREX_IO3 PD4
#define GREX_IO4 PD3
#define GREX_IO5 PD2
#define GREX_IO6 PD1
#define GREX_IO7 PD0
#define GREX_IO20 PE3
#define GREX_IO21 PE1
#define GREX_IO22 P53
#define GREX_IO23 P35

// スピーカ
#define GREX_SP_VOLUME			60		// [%]
#define GREX_SP_DUTY_MAX		255
#define GREX_SP_DUTY_CENTER		127
#define GREX_SP_DUTY_MIN		0
#define GREX_SP_DUTY_MUTE		GREX_SP_DUTY_CENTER
#define GREX_SPOUT			PE2
#define GREX_SP_BUFFSIZE		512
#define GREX_SP_SAMPLE_RATE_NOTE	11025		// スピーカサンプリング周波数[Hz] 44.1kHzの1/4
#define GREX_SP_RESOLUTION		255		// 分解能 8bit
enum gr_ex_sp_mode{
	GREX_SP_MODE_IDLE,	// 再生してない
	GREX_SP_MODE_WAVE,	// 波形モード
	GREX_SP_MODE_NOTE	// 音階モード
};

#define GREX_SP_DURATION_PER_SEC 	64	// 1秒あたりのDuration値
#define GREX_SP_SONGPACKET_NUM		32	// 音パケット最大数
typedef struct{
	uint8_t NoteNum;
	uint8_t NoteDuration;
}gr_ex_song_packet_t;	// 音階で鳴らす // ルンバと同じ
					// 0x00, 0x00は無効(データ終わりとみなす)

// モーター
#define GREX_MOTOR_PWMCNT_1KHz 47999
#define GREX_MOTOR_PWMCNT_10KHz 4799

#define GREX_PIN_MOTOR0P P20
#define GREX_PIN_MOTOR0A P05
#define GREX_PIN_MOTOR0B P07
#define GREX_PIN_MOTOR1P P21
#define GREX_PIN_MOTOR1A P17
#define GREX_PIN_MOTOR1B P51
#define GREX_PIN_MOTOR2P PC1
#define GREX_PIN_MOTOR2A P54
#define GREX_PIN_MOTOR2B P55
#define GREX_PIN_MOTOR3P PJ3
#define GREX_PIN_MOTOR3A PC4
#define GREX_PIN_MOTOR3B PE0

enum gr_ex_motor_num{
	MOTOR0,	
	MOTOR1,	
	MOTOR2,	
	MOTOR3,
};
enum gr_ex_motor_dir{
	CW,
	CCW,
	BRAKE,
	FREE,
};

// エンコーダ
#define GREX_PIN_ENC0A P24
#define GREX_PIN_ENC0B P25
#define GREX_PIN_ENC1A P22
#define GREX_PIN_ENC1B P23

enum gr_ex_encoder_num{
	ENC0,
	ENC1
};


class gr_ex_imu;
class gr_ex_encoder;
class gr_ex_motor;
class gr_ex_sp;

class gr_ex{
public:
	gr_ex_sp *Sp;
	gr_ex_motor *Motor0;
	gr_ex_motor *Motor1;
	gr_ex_motor *Motor2;
	gr_ex_motor *Motor3;
	gr_ex_encoder *Enc0;
	gr_ex_encoder *Enc1;
	gr_ex_imu *Imu;
	
	gr_ex(void);
	~gr_ex(void);
	void begin(void);
	void task(void);
	
	void startAD(void);
	void stopAD(void){S12AD.ADCSR.BIT.ADST = 0;};
	uint16_t getAD(grex_ad_ch AdCh);	// 12bit
	uint16_t getAD8(grex_ad_ch AdCh);	// 8bit
	uint16_t getAD10(grex_ad_ch AdCh);	// 10bit
	uint16_t getAD12(grex_ad_ch AdCh);	// 12bit
	
private:
	// privatemember

	// private function
	
	
	
};


/*********************
スピーカ
概要：
 バッファに用意した波形データを順次出力する。
  分解能 8bit
**********************/
class gr_ex_sp{
public:
	gr_ex_sp(void);
	~gr_ex_sp(void);
	
	void task(void);
	
	void setupPlay(uint16_t SamplingFreq);			// 再生設定(サンプリング周波数[Hz]設定)
	void stopPlay(void);					// 再生停止
	
	// 波形データセット
	int8_t setData(uint8_t data);	// データをセット (順次書き込めるだけ書き込む)
	bool_t isBuffFull(void){return SpBuff->isFull();};	// 波形バッファフル？
	// 音階データセット
	// SongPackets[] = {0x00, 0x00}になるまで再生する
	int8_t setData(gr_ex_song_packet_t* pSongPacket, bool_t fRepeat = false);
	int8_t playMusic(gr_ex_song_packet_t* pSongPacket, bool_t fRepeat = false){return setData(pSongPacket, fRepeat);};
	
	// モード管理
	int8_t setMode(gr_ex_sp_mode Mode){ this->Mode = Mode; return 0; };
	gr_ex_sp_mode getMode(void){ return Mode; };
	bool_t isIdle(void){ return (GREX_SP_MODE_IDLE==Mode); };
	
	// 単音
	int8_t noteSingle(uint8_t NoteNum=69);
	int8_t noteDouble(uint8_t NoteNum0=69, uint8_t NoteNum1=72);
	int8_t noteTriple(uint8_t NoteNum0=69, uint8_t NoteNum1=72, uint8_t NoteNum2=75);
	
	
	// バッファ操作 (デバッグ用)
	//bool_t isBuffEmpty(void){return SpBuff->isEmpty();};
	//int8_t addBuff(uint8_t data){return SpBuff->add(data);};
	//void clearBuff(void){SpBuff->clear();};
	//int16_t getNumElements(void){return SpBuff->getNumElements();};
	
	void isrSpPlay(void);					// 割り込み(SamplingFreq周期)
private:
	RingBuffer<uint8_t> *SpBuff;		// 波形バッファ
	gr_ex_song_packet_t* SongPackets;	// 音階バッファ
	gr_ex_sp_mode Mode;			// 再生モード
	
	uint16_t _Freq;
	void setPeriodCnt(uint16_t PeriodCnt);			// 周期カウント数セット(基本255)
	void setDutyCnt(uint16_t DutyCnt);			// Dutyカウント数セット(8bit)
	uint16_t getDutyCnt(void);				// Dutyカウント数取得
	void setupRegisterPwm(void);				// PWM関係レジスタ設定
	
	uint16_t DutyZeroCross;					// ゼロクロス判定用前回Duty
	
	// 音階再生モード
	bool_t IsRepeat;			// 繰り返す?
	gr_ex_song_packet_t* CurrentSongPacket;	// 再生中の音階データ	
	uint32_t NoteStartTime_ms;		// その音が始まった時刻
	uint32_t NoteFreqCnt;			// 波形カウント 
	float CurrentNoteFreq;			// 周波数
	float CurrentNoteFreqTmp;		// 周波数仮置き(0クロスするまで)
};


/*********************
// モーター
**********************/
class gr_ex_motor{
public:
	gr_ex_motor(gr_ex_motor_num MotorNum);
	
	void setPeriodCnt(uint16_t PeriodCnt);
	void setDutyCnt(uint16_t DutyCnt);
	void setDir(gr_ex_motor_dir);
	uint16_t getPeriodCnt(void){return PeriodCnt_;};
	
	void setDuty(int16_t Duty);	// +-255
	
	//void setSpd(uint16_t Spd);	//速度制御したいなーって
	
private:
	gr_ex_motor_num _Motor;
	//bool_t _Enable;
	gr_ex_motor_dir _Dir;
	uint16_t PeriodCnt_;
	
	void setRegister(void);
};


/*********************
// エンコーダ
**********************/
class gr_ex_encoder{
public:
	bool_t fOverflow_;
	bool_t fUnderflow_;
	
	gr_ex_encoder(gr_ex_encoder_num EncNum);
	void startCnt(void);
	void stopCnt(void);
	void setCnt(uint16_t Cnt);
	uint16_t getCnt(void);		// カウント値
	int16_t getCntDiff(void);	// 前回値との差
private:
	uint16_t CntLast_;
	gr_ex_encoder_num Enc_;
	void setRegister(void);

};

/*********************
// MPU9250
**********************/
class gr_ex_imu: public invensense_MPU{
public:
	gr_ex_imu(I2c_t* I2Cn, uint8_t I2cAddress):invensense_MPU(I2Cn, I2cAddress){};
};



extern gr_ex GrEx;





#endif
