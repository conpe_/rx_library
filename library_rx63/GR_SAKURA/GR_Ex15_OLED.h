/**************************************************
GR_Ex15_OLED.h
	GR-SAKURA用拡張基板GR_Ex15の上にちょうど載っちゃうOLED
	とタクトスイッチx4の基板用ライブラリだよ。
	
	生成インスタンス
	GrExOled
	Oled

**************************************************/

// 引数にFont_tを与えると2バイトフォント扱える
// 8x8フォントに対応


#ifndef __GR_EX15_OLED_H__
#define __GR_EX15_OLED_H__


#include "GR_Ex15.h"
#include "OledDriverSSD1306.h"
#include "Font.h"

#define OLED_SW_NUM	4

#define OLED_SW0 GREX_IO23
#define OLED_SW1 GREX_IO22
#define OLED_SW2 GREX_IO21
#define OLED_SW3 GREX_IO20

#define OLED_SPI_MODULE RSPI_SPI1
#define OLED_PIN_DC PE7


class gr_ex_oled:public OledDriverSSD1306{
public:
	gr_ex_oled(void);
	gr_ex_oled(font_t* SetFont);
	void begin(void);
	void task(void);	// スイッチなどの処理タスク(1msごとに回す)
	void setFont(font_t* SetFont);
	
	// 2byteフォント文字出力
	int8_t writeChar(char* strchar);	// shift jis(2byte)
	int8_t writeChar(uint16_t sjis);	// shift jis(2byte)
	int8_t writeChar(char ch);		// 

	// スイッチ
	bool_t isSw0(void){return Sw[0];};
	bool_t isSw1(void){return Sw[1];};
	bool_t isSw2(void){return Sw[2];};
	bool_t isSw3(void){return Sw[3];};
	
	
private:
	bool_t EnableFullWidthFont;
	font_t* Font;
	bool_t fFullWidthChar;
	uint8_t sjisUpperTmp;
	
	bool_t Sw[OLED_SW_NUM];
	uint8_t SwCnt[OLED_SW_NUM];
	bool_t SwLast[OLED_SW_NUM];
};




extern gr_ex_oled GrExOled;
extern OledDriverSSD1306 Oled;

#endif
