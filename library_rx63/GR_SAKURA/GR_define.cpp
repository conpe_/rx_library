
#include "GR_define.h"
#include <machine.h>

void GR_setup(void);

void GR_begin(void){
	GR_setup();
	
	setPinMode(GR_LED0, PIN_OUTPUT);
	setPinMode(GR_LED1, PIN_OUTPUT);
	setPinMode(GR_LED2, PIN_OUTPUT);
	setPinMode(GR_LED3, PIN_OUTPUT);
	//outPin(GR_LED0, 0);
	//outPin(GR_LED1, 0);
	//outPin(GR_LED2, 0);
	//outPin(GR_LED3, 0);
	setPinMode(GR_SW, PIN_INPUT);
}

// 動作クロックなどの設定
// 12MHz, 96MHz
void GR_setup(void){
	volatile int i;
	
    // Protection off
    SYSTEM.PRCR.WORD = 0xA503u;

    // Stop sub-clock
    SYSTEM.SOSCCR.BYTE = 0x01u;

    // Set main oscillator settling time to 10ms (131072 cycles @ 12MHz)
    SYSTEM.MOSCWTCR.BYTE = 0x0Du;

    // Set PLL circuit settling time to 10ms (2097152 cycles @ 192MHz)
    SYSTEM.PLLWTCR.BYTE = 0x0Eu;
	for( i=0 ; i<636  ; i++ )					// 20.1053ms/143.75kHz/5cyc=635.527
		nop( );

    // Set PLL circuit to x16
    SYSTEM.PLLCR.WORD = 0x0F00u;

    // Start the external 12Mhz oscillator
    SYSTEM.MOSCCR.BYTE = 0x00u;

    // Turn on the PLL
    SYSTEM.PLLCR2.BYTE = 0x00u;

    // Wait over 12ms (~2075op/s @ 125KHz)
    for(volatile uint16_t i = 0; i < 2075u; i++)
    {
        nop();
    }

    // Configure the clocks as follows -
    //Clock Description              Frequency
    //----------------------------------------
    //PLL Clock frequency...............192MHz
    //System Clock Frequency.............96MHz
    //Peripheral Module Clock B..........48MHz
    //FlashIF Clock......................48MHz
    //External Bus Clock.................48MHz
    SYSTEM.SCKCR.LONG = 0x21021211u;
	while( SYSTEM.SCKCR.LONG != 0x21021211 )  ;	// Wait Finish

    // Configure the clocks as follows -
    //Clock Description              Frequency
    //----------------------------------------
    //USB Clock..........................48MHz
    //IEBus Clock........................24MHz
    SYSTEM.SCKCR2.WORD = 0x0033u;
	while( SYSTEM.SCKCR2.WORD != 0x0033 )  ;	// Wait Finish

    // Set the clock source to PLL
    SYSTEM.SCKCR3.WORD = 0x0400u;
	while( SYSTEM.SCKCR3.WORD != 0x0400 )  ;	// Wait Finish

    // Stop external bus
    SYSTEM.SYSCR0.WORD  = 0x5A01;

    // Protection on
    SYSTEM.PRCR.WORD = 0xA500u;

}

uint32_t getPCLK(void){
	return (uint32_t)PCLK;
}


// 時間関係
static uint32_t GR_Time_msec = 0;

void GR_cnt(void){
	GR_Time_msec++;
}

uint32_t getTime_ms(uint32_t BaseTime_ms){
	return GR_Time_msec - BaseTime_ms;
}
uint32_t getTime_ms(void){
	return GR_Time_msec;
}
// 時間経ったか
bool_t isTimePass_ms(uint32_t PassTime, uint32_t BaseTime_ms){
	return PassTime <= getTime_ms(BaseTime_ms);		// 経ってる?
}
// 時間経ったか(経ってたらBaseTime更新)
bool_t isTimePass_ms(uint32_t PassTime, uint32_t* BaseTime_ms){
	if(isTimePass_ms(PassTime, *BaseTime_ms)){		// 経ってる？
		*BaseTime_ms = getTime_ms();				// 現在時刻で更新
		return true;
	}
	
	return false;
}

// システム時間をクリア
// いろんな箇所に影響するので注意
void resetTime(void){
	GR_Time_msec = 0;
}

