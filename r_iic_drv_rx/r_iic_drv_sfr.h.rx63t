/********************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2013 Renesas Electronics Corporation. All rights reserved.
*********************************************************************************************************/
/********************************************************************************************************
* File Name    : r_iic_drv_sfr.h
* Version      : 1.12
* Description  : IIC driver RIIC I/F module SFR definitions
*********************************************************************************************************/
/********************************************************************************************************
* History      : DD.MM.YYYY Version  Description
*              : 29.03.2013 1.10     First Release
*              : 10.05.2013 1.10.R01 Corrected comments.
*              : 17.06.2013 1.11     Corrected comments.
*              : 24.06.2013 1.12     Added #define R_IIC_ICSR2_NACKF_SET.
*              : 24.06.2013 1.12     Added r_iic_drv_check_nackf_bit() prototype.
*              : 24.06.2013 1.12     Changed ICMR3 register setting.
*********************************************************************************************************/
#ifndef __R_IIC_DRV_SFR_H__
#define __R_IIC_DRV_SFR_H__


/********************************************************************************************************
Macro definitions
*********************************************************************************************************/
/*------------------------------------------------------------------------------------------------------*/
/*   Define frequency as iic channel. (Please add a channel as needed.)                                 */
/*------------------------------------------------------------------------------------------------------*/
/* The I2C transfer rate is calculated using the following expression.                                  */
/* Transfer rate = 1 / {[(ICBRH + 1) + (ICBRL + 1)] / (PCLK*Division ratio)
                                            + SCLn line rising time [tr] + SCLn line falling time [tf]} */
/* Note1:Division ratio sets it by ICMR1.CKS[2:0].                                                      */

/* Freq = 400KHz at main system clock = 48MHz */
/* Sets ICBRL and ICBRH register. */
#define R_IIC_CH0_LCLK          (uint8_t)(0xED)             /* Channel 0 ICBRL register setting         */  /** SET **/
#define R_IIC_CH0_HCLK          (uint8_t)(0xE6)             /* Channel 0 ICBRH register setting         */  /** SET **/
#define R_IIC_CH1_LCLK          (uint8_t)(0xED)             /* Channel 1 ICBRL register setting         */  /** SET **/
#define R_IIC_CH1_HCLK          (uint8_t)(0xE6)             /* Channel 1 ICBRH register setting         */  /** SET **/

/* Sets ICMR1 register.*/
#define R_IIC_CH0_ICMR1_INIT    (uint8_t)(0x28)             /* Channel 0 ICMR1 register setting         */  /** SET **/
#define R_IIC_CH1_ICMR1_INIT    (uint8_t)(0x28)             /* Channel 1 ICMR1 register setting         */  /** SET **/

/*------------------------------------------------------------------------------------------------------*/
/*   Define interrupt priority as iic channel. (Please add a channel as needed.)                        */
/*------------------------------------------------------------------------------------------------------*/
/* Sets interrupt source priority initialization. */
#define R_IIC_IPR_CH0_EEI_INIT  (uint8_t)(0x02)         /* EEI0 interrupt source priority initialization*/  /** SET **/
#define R_IIC_IPR_CH0_RXI_INIT  (uint8_t)(0x02)         /* RXI0 interrupt source priority initialization*/  /** SET **/
#define R_IIC_IPR_CH0_TXI_INIT  (uint8_t)(0x02)         /* TXI0 interrupt source priority initialization*/  /** SET **/
#define R_IIC_IPR_CH0_TEI_INIT  (uint8_t)(0x02)         /* TEI0 interrupt source priority initialization*/  /** SET **/

#define R_IIC_IPR_CH1_EEI_INIT  (uint8_t)(0x02)         /* EEI1 interrupt source priority initialization*/  /** SET **/
#define R_IIC_IPR_CH1_RXI_INIT  (uint8_t)(0x02)         /* RXI1 interrupt source priority initialization*/  /** SET **/
#define R_IIC_IPR_CH1_TXI_INIT  (uint8_t)(0x02)         /* TXI1 interrupt source priority initialization*/  /** SET **/
#define R_IIC_IPR_CH1_TEI_INIT  (uint8_t)(0x02)         /* TEI1 interrupt source priority initialization*/  /** SET **/


/*------------------------------------------------------------------------------------------------------*/
/*   Define Channel 0 register.                                                                         */
/*------------------------------------------------------------------------------------------------------*/
#ifdef RIIC0_ENABLE

/* Define port registers */
#define R_IIC_PDR_SCL0          PORTB.PDR.BIT.B1            /* SCL0 Port direction register             */
#define R_IIC_PDR_SDA0          PORTB.PDR.BIT.B2            /* SDA0 Port direction register             */
#define R_IIC_PODR_SCL0         PORTB.PODR.BIT.B1           /* SCL0 Port output data register           */
#define R_IIC_PODR_SDA0         PORTB.PIDR.BIT.B2           /* SDA0 Port output data register           */
#define R_IIC_PIDR_SCL0         PORTB.PODR.BIT.B1           /* SCL0 Port input data register            */
#define R_IIC_PIDR_SDA0         PORTB.PIDR.BIT.B2           /* SDA0 Port input data register            */
#define R_IIC_PMR_SCL0          PORTB.PMR.BIT.B1            /* SCL0 Port mode register                  */
#define R_IIC_PMR_SDA0          PORTB.PMR.BIT.B2            /* SDA0 Port mode register                  */
#define R_IIC_DSCR_SCL0                                     /* SCL0 Drive capacity control register     */
#define R_IIC_DSCR_SDA0                                     /* SDA0 Drive capacity control register     */
#define R_IIC_PCR_SCL0          PORTB.PCR.BIT.B1            /* SCL0 Pull-up resistor control register   */
#define R_IIC_PCR_SDA0          PORTB.PCR.BIT.B2            /* SDA0 Pull-up resistor control register   */

/* Define Pin function control registers */
#define R_IIC_MPC_SCL0          MPC.PB1PFS.BYTE             /* SCL0 Pin function control register       */
#define R_IIC_MPC_SDA0          MPC.PB2PFS.BYTE             /* SDA0 Pin function control register       */

/* Define interrupt request registers */
#define R_IIC_IR_EEI0   ICU.IR[IR_RIIC0_EEI0].BIT.IR        /* EEI0 Interrupt request register          */
#define R_IIC_IR_RXI0   ICU.IR[IR_RIIC0_RXI0].BIT.IR        /* RXI0 Interrupt request register          */
#define R_IIC_IR_TXI0   ICU.IR[IR_RIIC0_TXI0].BIT.IR        /* TXI0 Interrupt request register          */
#define R_IIC_IR_TEI0   ICU.IR[IR_RIIC0_TEI0].BIT.IR        /* TEI0 Interrupt request register          */

/* Define interrupt request enable registers */
#define R_IIC_IER_EEI0  ICU.IER[IER_RIIC0_EEI0].BIT.IEN2    /* EEI0 Interrupt request enable register   */
#define R_IIC_IER_RXI0  ICU.IER[IER_RIIC0_RXI0].BIT.IEN3    /* RXI0 Interrupt request enable register   */
#define R_IIC_IER_TXI0  ICU.IER[IER_RIIC0_TXI0].BIT.IEN4    /* TXI0 Interrupt request enable register   */  
#define R_IIC_IER_TEI0  ICU.IER[IER_RIIC0_TEI0].BIT.IEN5    /* TEI0 Interrupt request enable register   */

/* Define interrupt source priority registers */
#define R_IIC_IPR_EEI0  ICU.IPR[IPR_RIIC0_EEI0].BYTE        /* EEI0 Interrupt source priority register  */
#define R_IIC_IPR_RXI0  ICU.IPR[IPR_RIIC0_RXI0].BYTE        /* RXI0 Interrupt source priority register  */
#define R_IIC_IPR_TXI0  ICU.IPR[IPR_RIIC0_TXI0].BYTE        /* TXI0 Interrupt source priority register  */
#define R_IIC_IPR_TEI0  ICU.IPR[IPR_RIIC0_TEI0].BYTE        /* TEI0 Interrupt source priority register  */

/* Define peripheral enable register */
#define R_IIC_MSTP0             MSTP_RIIC0                  /* RIIC0 peripheral enable register         */

/* Sets to use multi-function pin controller. */
#define R_IIC_MPC_SDA0_INIT     (uint8_t)(0x00)             /* Pin function select to Hi-Z              */
#define R_IIC_MPC_SCL0_INIT     (uint8_t)(0x00)             /* Pin function select to Hi-Z              */
#define R_IIC_MPC_SDA0_ENABLE   (uint8_t)(0x0F)             /* Pin function select to RIIC0 SDA pin     */
#define R_IIC_MPC_SCL0_ENABLE   (uint8_t)(0x0F)             /* Pin function select to RIIC0 SCL pin     */

#endif /* #ifdef RIIC0_ENABLE */

/*------------------------------------------------------------------------------------------------------*/
/*   Define Channel 1 register.                                                                         */
/*------------------------------------------------------------------------------------------------------*/
#ifdef RIIC1_ENABLE

/* Define port registers */
#define R_IIC_PDR_SCL1          PORT2.PDR.BIT.B5            /* SCL1 Port direction register             */
#define R_IIC_PDR_SDA1          PORT2.PDR.BIT.B6            /* SDA1 Port direction register             */
#define R_IIC_PODR_SCL1         PORT2.PODR.BIT.B5           /* SCL1 Port output data register           */
#define R_IIC_PODR_SDA1         PORT2.PODR.BIT.B6           /* SDA1 Port output data register           */
#define R_IIC_PIDR_SCL1         PORT2.PODR.BIT.B5           /* SCL1 Port input data register            */
#define R_IIC_PIDR_SDA1         PORT2.PIDR.BIT.B6           /* SDA1 Port input data register            */
#define R_IIC_PMR_SCL1          PORT2.PMR.BIT.B5            /* SCL1 Port mode register                  */
#define R_IIC_PMR_SDA1          PORT2.PMR.BIT.B6            /* SDA1 Port mode register                  */
#define R_IIC_MPC_SCL1          MPC.P25PFS.BYTE             /* SCL1 Pin function control register       */
#define R_IIC_MPC_SDA1          MPC.P26PFS.BYTE             /* SDA1 Pin function control register       */
#define R_IIC_DSCR_SCL1                                     /* SCL1 Drive capacity control register     */
#define R_IIC_DSCR_SDA1                                     /* SDA1 Drive capacity control register     */
#define R_IIC_PCR_SCL1          PORT2.PCR.BIT.B5            /* SCL1 Pull-up resistor control register   */
#define R_IIC_PCR_SDA1          PORT2.PCR.BIT.B6            /* SDA1 Pull-up resistor control register   */

/* Define interrupt request registers */
#define R_IIC_IR_EEI1   ICU.IR[IR_RIIC1_EEI1].BIT.IR        /* EEI1 Interrupt request register          */
#define R_IIC_IR_RXI1   ICU.IR[IR_RIIC1_RXI1].BIT.IR        /* RXI1 Interrupt request register          */
#define R_IIC_IR_TXI1   ICU.IR[IR_RIIC1_TXI1].BIT.IR        /* TXI1 Interrupt request register          */
#define R_IIC_IR_TEI1   ICU.IR[IR_RIIC1_TEI1].BIT.IR        /* TEI1 Interrupt request register          */

/* Define interrupt registers */
#define R_IIC_IER_EEI1  ICU.IER[IER_RIIC1_EEI1].BIT.IEN6    /* EEI1 Interrupt request enable register   */
#define R_IIC_IER_RXI1  ICU.IER[IER_RIIC1_RXI1].BIT.IEN7    /* RXI1 Interrupt request enable register   */
#define R_IIC_IER_TXI1  ICU.IER[IER_RIIC1_TXI1].BIT.IEN0    /* TXI1 Interrupt request enable register   */
#define R_IIC_IER_TEI1  ICU.IER[IER_RIIC1_TEI1].BIT.IEN1    /* TEI1 Interrupt request enable register   */

/* Define interrupt source priority registers */
#define R_IIC_IPR_EEI1  ICU.IPR[IPR_RIIC1_EEI1].BYTE        /* EEI1 Interrupt source priority register  */
#define R_IIC_IPR_RXI1  ICU.IPR[IPR_RIIC1_RXI1].BYTE        /* RXI1 Interrupt source priority register  */
#define R_IIC_IPR_TXI1  ICU.IPR[IPR_RIIC1_TXI1].BYTE        /* TXI1 Interrupt source priority register  */
#define R_IIC_IPR_TEI1  ICU.IPR[IPR_RIIC1_TEI1].BYTE        /* TEI1 Interrupt source priority register  */

/* Define peripheral enable register */
#define R_IIC_MSTP1             MSTP_RIIC1                  /* RIIC1 peripheral enable register         */

/* Sets to use multi-function pin controller. */
#define R_IIC_MPC_SDA1_INIT     (uint8_t)(0x00)             /* Pin function select to Hi-Z              */
#define R_IIC_MPC_SCL1_INIT     (uint8_t)(0x00)             /* Pin function select to Hi-Z              */
#define R_IIC_MPC_SDA1_ENABLE   (uint8_t)(0x0F)             /* Pin function select to RIIC1 SDA pin     */
#define R_IIC_MPC_SCL1_ENABLE   (uint8_t)(0x0F)             /* Pin function select to RIIC1 SCL pin     */

#endif /* #ifdef RIIC1_ENABLE */


/*------------------------------------------------------------------------------------------------------*/
/*   Define register setting.                                                                           */
/*------------------------------------------------------------------------------------------------------*/
/* Interrupt register setting */
#define R_IIC_IR_CLR            (uint8_t)(0x00)         /* Clears interrupt request register.           */
#define R_IIC_IR_SET            (uint8_t)(0x01)         /* Sets interrupt request register.             */
#define R_IIC_IER_DISABLE       (uint8_t)(0x00)         /* Disables interrupt request enable register.  */
#define R_IIC_IER_ENABLE        (uint8_t)(0x01)         /* Enables interrupt request enable register.   */

/* Common register setting */
#define R_IIC_PRCR_ENABLE       (uint16_t)(0xA502)      /* PRCR:Write enabled                           */
#define R_IIC_PRCR_DISABLE      (uint16_t)(0xA500)      /* PRCR:Write disabled                          */

/* RIIC register setting */
#define R_IIC_ICCR1_NOT_DRIVEN  (uint8_t)(0x7F)         /* Clears ICCR1.ICE bit.                        */
#define R_IIC_ICCR1_RIIC_RESET  (uint8_t)(0x40)         /* Sets ICCR1.IICRST bit.                       */
#define R_IIC_ICCR1_INTER_RESET (uint8_t)(0x80)         /* Sets ICCR1.ICE bit.                          */
#define R_IIC_ICCR1_CLO_SET     (uint8_t)(0x20)         /* Sets ICCR1.CLO bit.                          */
#define R_IIC_ICCR1_ENABLE      (uint8_t)(0xBF)         /* Clears ICCR1.IICRST bit.                     */
#define R_IIC_ICCR1_SDAO_SCLO_SET (uint8_t)(0x1F) /* Clears ICCR1.SOWP bit. Sets ICCR1.SDAO and SCL0 bit*/
#define R_IIC_SARL0_INIT        (uint8_t)(0x00)         /* Sets SARLy and SARUy.(y=0,1,2)               */
#define R_IIC_SARU0_INIT        (uint8_t)(0x00)         /* Sets SARLy and SARUy.(y=0,1,2)               */
#define R_IIC_SARL1_INIT        (uint8_t)(0x00)         /* Sets SARLy and SARUy.(y=0,1,2)               */
#define R_IIC_SARU1_INIT        (uint8_t)(0x00)         /* Sets SARLy and SARUy.(y=0,1,2)               */
#define R_IIC_SARL2_INIT        (uint8_t)(0x00)         /* Sets SARLy and SARUy.(y=0,1,2)               */
#define R_IIC_SARU2_INIT        (uint8_t)(0x00)         /* Sets SARLy and SARUy.(y=0,1,2)               */
#define R_IIC_ICSER_INIT        (uint8_t)(0x00)         /* Sets ICSER                                   */
/* #define R_IIC_ICMR1_INIT */                    /* Refer to "R_IIC_CHx_ICMR1_INIT" of r_iic drv_api.h */
#define R_IIC_ICMR1_MTWP_SET    (uint8_t)(0x80)         /* Sets ICMR1.MTWP bit.                         */
#define R_IIC_ICMR1_MTWP_CLR    (uint8_t)(0x7F)         /* Clears ICMR1.MTWP bit.                       */
#define R_IIC_ICMR2_INIT        (uint8_t)(0x00)         /* Sets ICMR2                                   */
#define R_IIC_ICMR3_INIT        (uint8_t)(0x00)         /* Sets ICMR3                                   */
#define R_IIC_ICFER_INIT        (uint8_t)(0x72)         /* Sets ICFER                                   */
#define R_IIC_ICFER_MALE_CLR    (uint8_t)(0x70)         /* Clears ICFER.MALE bit.                       */
#define R_IIC_ICCR2_ST          (uint8_t)(0x02)         /* Sets ICCR2.ST bit.                           */
#define R_IIC_ICCR2_RS          (uint8_t)(0x04)         /* Sets ICCR2.RS bit.                           */
#define R_IIC_ICCR2_SP          (uint8_t)(0x08)         /* Sets ICCR2.SP bit.                           */
#define R_IIC_ICCR2_MST_SET     (uint8_t)(0x40)         /* Sets ICCR2.MST bit.                          */
#define R_IIC_ICMR3_WAIT_SET    (uint8_t)(0x40)         /* Sets ICMR3.WAIT bit.                         */
#define R_IIC_ICMR3_WAIT_CLR    (uint8_t)(0xBF)         /* Clears ICMR3.WAIT bit.                       */
#define R_IIC_ICMR3_PRE_END_SET (uint8_t)(0x68)         /* Sets ICMR3.ACKBT bit.                        */
                                                        /* Sets ICMR3.RDRFS bit.                        */
#define R_IIC_ICMR3_END_SET     (uint8_t)(0x08)         /* Sets ICMR3.ACKBT bit.                        */
#define R_IIC_ICMR3_ACKWP_SET   (uint8_t)(0x10)         /* Sets ICMR3.ACKWP bit.                        */
#define R_IIC_ICSR2_START_SET   (uint8_t)(0x04)         /* Sets ICSR2.START bit.                        */
#define R_IIC_ICSR2_STOP_SET    (uint8_t)(0x08)         /* Sets ICSR2.STOP bit.                         */
#define R_IIC_ICSR2_START_CLR   (uint8_t)(0xFB)         /* Clears ICSR2.START bit.                      */
#define R_IIC_ICSR2_STOP_CLR    (uint8_t)(0xF7)         /* Clears ICSR2.STOP bit.                       */
#define R_IIC_ICSR2_DISABLE     (uint8_t)(0xE7)         /* Clears ICSR2.NACKF bit and STOP bit.         */
#define R_IIC_ICSR2_NACKF_SET   (uint8_t)(0x10)         /* Sets ICSR2.NACKF bit                         */

#define R_IIC_ICIER_INIT        (uint8_t)(0x00)         /* Initializes ICIER.                           */
#define R_IIC_ICIER_STIE        (uint8_t)(0x16)         /* Sets ICIER.STIE bit.                         */
                                                        /* Enable bit  : ALIE, STIE, NAKIE              */
                                                        /* Disable bit : TMOIE, SPIE, RIE, TEIE, TIE    */
#define R_IIC_ICIER_TEIE        (uint8_t)(0x52)         /* Sets ICIER.TEIE bit.                         */
                                                        /* Enable bit  : ALIE, NAKIE, TEIE              */
                                                        /* Disable bit : TMOIE, STIE, SPIE, RIE, TIE    */
#define R_IIC_ICIER_RIE         (uint8_t)(0x32)         /* Sets ICIER.RIE bit.                          */
                                                        /* Enable bit  : ALIE, NAKIE, RIE               */
                                                        /* Disable bit : TMOIE, STIE, SPIE, TEIE, TIE   */
#define R_IIC_ICIER_SPIE_NAKIE  (uint8_t)(0x1A)         /* Sets ICIER.SPIE bit and NAKIE.               */
                                                        /* Enable bit  : ALIE, SPIE, NAKIE              */
                                                        /* Disable bit : TMOIE, STIE, RIE, TEIE, TIE    */
#define R_IIC_ICIER_SPIE        (uint8_t)(0x0A)     /* Sets ICIER.SPIE bit and clears ICIER.NAKIE bit.  */
                                                    /* Enable bit  : ALIE, SPIE                         */
                                                    /* Disable bit : TMOIE, STIE, NAKIE, RIE, TEIE, TIE */
#define R_IIC_MSK_BBSY          (uint8_t)(0x80)         /* Mask ICCR2.BBSY bit                          */
#define R_IIC_MSK_SCLSDA        (uint8_t)(0x03)         /* Mask ICCR1.SDAI bit and SCLI bit             */


/* Common registers setting */
#define R_IIC_PRCR              SYSTEM.PRCR.WORD        /* Protect Register (PRCR)                      */
#define R_IIC_PWPR_BOWI         MPC.PWPR.BIT.B0WI       /* PWPR PFSWE Bit Write Disable                 */
#define R_IIC_PWPR_PFSWE        MPC.PWPR.BIT.PFSWE      /* PWPR PFS Register Write Enable               */

/* Control registers address defines */
#define ICCR1_ADR(n)    ( (volatile uint8_t *)&RIIC0 + 0x00 + ((32 * n) / sizeof(uint8_t)) )
#define ICCR2_ADR(n)    ( (volatile uint8_t *)&RIIC0 + 0x01 + ((32 * n) / sizeof(uint8_t)) )
#define ICMR1_ADR(n)    ( (volatile uint8_t *)&RIIC0 + 0x02 + ((32 * n) / sizeof(uint8_t)) )
#define ICMR2_ADR(n)    ( (volatile uint8_t *)&RIIC0 + 0x03 + ((32 * n) / sizeof(uint8_t)) )
#define ICMR3_ADR(n)    ( (volatile uint8_t *)&RIIC0 + 0x04 + ((32 * n) / sizeof(uint8_t)) )
#define ICFER_ADR(n)    ( (volatile uint8_t *)&RIIC0 + 0x05 + ((32 * n) / sizeof(uint8_t)) )
#define ICSER_ADR(n)    ( (volatile uint8_t *)&RIIC0 + 0x06 + ((32 * n) / sizeof(uint8_t)) )
#define ICIER_ADR(n)    ( (volatile uint8_t *)&RIIC0 + 0x07 + ((32 * n) / sizeof(uint8_t)) )
#define ICSR1_ADR(n)    ( (volatile uint8_t *)&RIIC0 + 0x08 + ((32 * n) / sizeof(uint8_t)) )
#define ICSR2_ADR(n)    ( (volatile uint8_t *)&RIIC0 + 0x09 + ((32 * n) / sizeof(uint8_t)) )
#define SARL0_ADR(n)    ( (volatile uint8_t *)&RIIC0 + 0x0A + ((32 * n) / sizeof(uint8_t)) )
#define SARU0_ADR(n)    ( (volatile uint8_t *)&RIIC0 + 0x0B + ((32 * n) / sizeof(uint8_t)) )
#define SARL1_ADR(n)    ( (volatile uint8_t *)&RIIC0 + 0x0C + ((32 * n) / sizeof(uint8_t)) )
#define SARU1_ADR(n)    ( (volatile uint8_t *)&RIIC0 + 0x0D + ((32 * n) / sizeof(uint8_t)) )
#define SARL2_ADR(n)    ( (volatile uint8_t *)&RIIC0 + 0x0E + ((32 * n) / sizeof(uint8_t)) )
#define SARU2_ADR(n)    ( (volatile uint8_t *)&RIIC0 + 0x0F + ((32 * n) / sizeof(uint8_t)) )
#define ICBRL_ADR(n)    ( (volatile uint8_t *)&RIIC0 + 0x10 + ((32 * n) / sizeof(uint8_t)) )
#define ICBRH_ADR(n)    ( (volatile uint8_t *)&RIIC0 + 0x11 + ((32 * n) / sizeof(uint8_t)) )
#define ICDRT_ADR(n)    ( (volatile uint8_t *)&RIIC0 + 0x12 + ((32 * n) / sizeof(uint8_t)) )
#define ICDRR_ADR(n)    ( (volatile uint8_t *)&RIIC0 + 0x13 + ((32 * n) / sizeof(uint8_t)) )

/* Interrupt registers address defines */
#define IR_RXI_ADR(n)   ( (volatile uint8_t *)&ICU   + 0xB7 + (( 4 * n) / sizeof(uint8_t)) )
#define IR_TXI_ADR(n)   ( (volatile uint8_t *)&ICU   + 0xB8 + (( 4 * n) / sizeof(uint8_t)) )


/********************************************************************************************************
Typedef definitions
*********************************************************************************************************/


/********************************************************************************************************
Exported global variables
*********************************************************************************************************/


/********************************************************************************************************
Exported global functions (to be accessed by other files)
*********************************************************************************************************/
/* SFR setting functions */
void r_iic_drv_io_open(r_iic_drv_info_t * pRIic_Info);
void r_iic_drv_init_sfr(r_iic_drv_info_t * pRIic_Info);
void r_iic_drv_int_disable(r_iic_drv_info_t * pRIic_Info);
void r_iic_drv_int_enable(r_iic_drv_info_t * pRIic_Info);
void r_iic_drv_int_icier_setting(r_iic_drv_info_t * pRIic_Info, uint8_t New_icier);
void r_iic_drv_set_frequency(r_iic_drv_info_t * pRIic_Info);
bool r_iic_drv_check_sda_level(r_iic_drv_info_t * pRIic_Info);
bool r_iic_drv_check_bus_busy(r_iic_drv_info_t * pRIic_Info);
void r_iic_drv_cancel_mstp(r_iic_drv_info_t * pRIic_Info);
void r_iic_drv_start_cond_generate(r_iic_drv_info_t * pRIic_Info);
void r_iic_drv_re_start_cond_generate(r_iic_drv_info_t * pRIic_Info);
void r_iic_drv_stop_cond_generate(r_iic_drv_info_t * pRIic_Info);
void r_iic_drv_set_sending_data(r_iic_drv_info_t * pRIic_Info, uint8_t * pData);
uint8_t r_iic_drv_get_receiving_data(r_iic_drv_info_t * pRIic_Info);
void r_iic_drv_receive_start_setting(r_iic_drv_info_t * pRIic_Info);
void r_iic_drv_receive_wait_setting(r_iic_drv_info_t * pRIic_Info);
void r_iic_drv_receive_pre_end_setting(r_iic_drv_info_t * pRIic_Info);
void r_iic_drv_receive_end_setting(r_iic_drv_info_t * pRIic_Info);
void r_iic_drv_next_comm_setting(r_iic_drv_info_t * pRIic_Info);
void r_iic_drv_iic_disable(r_iic_drv_info_t * pRIic_Info);
void r_iic_drv_iic_enable(r_iic_drv_info_t * pRIic_Info);
void r_iic_drv_mpc_disable(r_iic_drv_info_t * pRIic_Info);
void r_iic_drv_mpc_enable(r_iic_drv_info_t * pRIic_Info);
bool r_iic_drv_clk(r_iic_drv_info_t * pRIic_Info);
void r_iic_drv_clear_ir_flag(r_iic_drv_info_t * pRIic_Info);
void r_iic_drv_set_mst_bit(r_iic_drv_info_t * pRIic_Info);
bool r_iic_drv_check_nackf_bit(r_iic_drv_info_t * pRIic_Info);


#endif /* __R_IIC_DRV_SFR_H__ */


/* End of File */
