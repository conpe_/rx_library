/********************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2013 Renesas Electronics Corporation. All rights reserved.
*********************************************************************************************************/
/********************************************************************************************************
* File Name    : r_iic_drv_api.h
* Version      : 1.12
* Description  : IIC driver RIIC I/F module definitions
*********************************************************************************************************/
/********************************************************************************************************
* History      : DD.MM.YYYY Version  Description
*              : 29.03.2013 1.10     First Release
*              : 10.05.2013 1.10.R01 Corrected comments.
*              : 17.06.2013 1.11     Corrected comments.
*              : 24.06.2013 1.12     Corrected comments.
*********************************************************************************************************/
#ifndef __R_IIC_DRV_API_H__
#define __R_IIC_DRV_API_H__


//#include <HardWareSetting.h>
#include <CommonDataType.h>


/********************************************************************************************************
Macro definitions
*********************************************************************************************************/

//   Select channels to enable.
#define RIIC0_ENABLE
// #define RIIC1_ENABLE
// #define RIIC2_ENABLE
// #define RIIC3_ENABLE

//   Define channel No.(max) + 1
#define MAX_IIC_CH_NUM          1

/*------------------------------------------------------------------------------------------------------*/
/*   Define to use an advance processing by RIIC interrupt handler.                                     */
/*------------------------------------------------------------------------------------------------------*/
 #define CALL_ADVANCE_INTERRUPT                                                                         /** SET **/


/*------------------------------------------------------------------------------------------------------*/
/*   Define counter.                                                                                    */
/*------------------------------------------------------------------------------------------------------*/
#define REPLY_CNT               (uint32_t)(100000)      /* Counter of non-reply errors                  */  /** SET **/
#define STOP_COND_WAIT          (uint16_t)(1000)        /* Counter of waiting stop condition generation */  /** SET **/
#define BUSCHK_CNT              (uint16_t)(1000)        /* Counter of checking bus busy                 */  /** SET **/
#define SDACHK_CNT              (uint16_t)(1000)        /* Counter of checking SDA level                */  /** SET **/
#define GEN_SCLCLK_WAIT         (uint16_t)(1000)        /* Counter of waiting SCL clock setting         */  /** SET **/


/*------------------------------------------------------------------------------------------------------*/
/*   Define return values and values of channel state flag.                                             */
/*------------------------------------------------------------------------------------------------------*/
#define R_IIC_NO_INIT           (error_t)(0)            /* Uninitialized state                          */
#define R_IIC_IDLE              (error_t)(1)            /* Idle state                                   */
#define R_IIC_FINISH            (error_t)(2)            /* Idle state (Finished communication)          */
#define R_IIC_NACK              (error_t)(3)            /* Idle state (Occured nack)                    */
#define R_IIC_COMMUNICATION     (error_t)(4)            /* Communication state                          */
#define R_IIC_LOCK_FUNC         (error_t)(5)            /* Using other API function                     */
#define R_IIC_BUS_BUSY          (error_t)(6)            /* Bus busy                                     */
#define R_IIC_ERR_PARAM         (error_t)(-1)           /* Error parameter                              */
#define R_IIC_ERR_AL            (error_t)(-2)           /* Error arbitration-lost                       */
#define R_IIC_ERR_NON_REPLY     (error_t)(-3)           /* Error non reply                              */
#define R_IIC_ERR_SDA_LOW_HOLD  (error_t)(-4)           /* Error holding SDA low                        */
#define R_IIC_ERR_OTHER         (error_t)(-5)           /* Error other                                  */

/*------------------------------------------------------------------------------------------------------*/
/*   Define R/W code for slave address.                                                                 */
/*------------------------------------------------------------------------------------------------------*/
#define W_CODE                  (uint8_t)(0x00)         /* Write code for slave address                 */
#define R_CODE                  (uint8_t)(0x01)         /* Read code for slave address                  */

/*------------------------------------------------------------------------------------------------------*/
/*   Define of port control.                                                                            */
/*------------------------------------------------------------------------------------------------------*/
#define R_IIC_HI                (uint8_t)(0x01)         /* Port "H"                                     */
#define R_IIC_LOW               (uint8_t)(0x00)         /* Port "L"                                     */
#define R_IIC_OUT               (uint8_t)(0x01)         /* Port Output                                  */
#define R_IIC_IN                (uint8_t)(0x00)         /* Port Input                                   */

/*------------------------------------------------------------------------------------------------------*/
/*   Define bool type.                                                                                  */
/*------------------------------------------------------------------------------------------------------*/
#define bool                    _Bool
#define R_IIC_FALSE             (bool)(0x00)            /* False                                        */
#define R_IIC_TRUE              (bool)(0x01)            /* True                                         */


/********************************************************************************************************
Typedef definitions
*********************************************************************************************************/

/* Data Model. */

#ifndef __COMMON_DATA_TYPE_H__
	#define __COMMON_DATA_TYPE_H__
	typedef signed char     int8_t;
	typedef unsigned char   uint8_t;
	typedef signed short    int16_t;
	typedef unsigned short  uint16_t;
	typedef signed long     int32_t;
	typedef unsigned long   uint32_t;
#endif

typedef signed short    error_t;        /* For CISC MCU.                                                */

/* Definitions bool type. */
typedef uint8_t          _Bool;


/*----- Enumeration type. -----*/
/* Internal Status. */
typedef enum
{
    R_IIC_STS_NO_INIT = 0U,             /* None initialization state                                    */
    R_IIC_STS_IDLE,                     /* Idle state                                                   */
    R_IIC_STS_ST_COND_WAIT,             /* Start condition generation completion wait state             */
    R_IIC_STS_SEND_SLVADR_W_WAIT,       /* Slave address [Write] transmission completion wait state     */
    R_IIC_STS_SEND_SLVADR_R_WAIT,       /* Slave address [Read] transmission completion wait state      */
    R_IIC_STS_SEND_DATA_WAIT,           /* Data transmission completion wait state                      */
    R_IIC_STS_RECEIVE_DATA_WAIT,        /* Data reception completion wait state                         */
    R_IIC_STS_SP_COND_WAIT,             /* Stop condition generation completion wait state              */
    R_IIC_STS_MAX                       /* Prohibition of setup above here                              */

} r_iic_drv_internal_status_t;

/* Internal Event. */
typedef enum
{
    R_IIC_EV_INIT = 0U,                 /* Called function of Initializes IIC driver                    */
    R_IIC_EV_GEN_START_COND,            /* Called function of Start condition generation                */
    R_IIC_EV_INT_START,                 /* Interrupted start codition generation                        */
    R_IIC_EV_INT_ADD,                   /* Interrupted address sending                                  */
    R_IIC_EV_INT_SEND,                  /* Interrupted data sending                                     */
    R_IIC_EV_INT_RECEIVE,               /* Interrupted data receiving                                   */
    R_IIC_EV_INT_STOP,                  /* Interrupted Stop condition generation                        */
    R_IIC_EV_INT_AL,                    /* Interrupted Arbitration-Lost                                 */
    R_IIC_EV_INT_NACK,                  /* Interrupted No Acknowledge                                   */
    R_IIC_EV_MAX                        /* Prohibition of setup above here                              */

} r_iic_drv_internal_event_t;

/* Internal Mode. */
typedef enum
{
    R_IIC_MODE_NONE = 0U,               /* Non-communication mode                                       */
    R_IIC_MODE_WRITE,                   /* Single master transmission mode                              */
    R_IIC_MODE_READ,                    /* Single master reception mode                                 */
    R_IIC_MODE_COMBINED,                /* Single master combined mode                                  */

} r_iic_drv_internal_mode_t;

/*----- Callback function type. -----*/
typedef void (*r_iic_callback)(void);                   /* Callback function type                       */


/*----- Structure type. -----*/
/* IIC Information structure. */
typedef struct
{
    uint8_t       * pSlvAdr;                            /* Pointer for Slave address buffer             */
    uint8_t       * pData1st;                           /* Pointer for 1st Data buffer                  */
    uint8_t       * pData2nd;                           /* Pointer for 2nd Data buffer                  */
    error_t       * pDevStatus;                         /* Device status flag                           */
    uint32_t        Cnt1st;                             /* 1st Data Counter                             */
    uint32_t        Cnt2nd;                             /* 2nd Data Counter                             */
    r_iic_callback  CallBackFunc;                       /* Callback function                            */
    uint8_t         ChNo;                               /* Channel No.                                  */
    uint8_t         rsv1;
    uint8_t         rsv2;
    uint8_t         rsv3;
} r_iic_drv_info_t;


/* Internal IIC Information structure. */
typedef struct
{
    r_iic_drv_internal_mode_t       Mode;               /* Internal mode of control protocol            */
    r_iic_drv_internal_status_t     N_status;           /* Internal Status of Now                       */
    r_iic_drv_internal_status_t     B_status;           /* Internal Status of Before                    */

} r_iic_drv_internal_info_t;


/********************************************************************************************************
Exported global variables
*********************************************************************************************************/
extern volatile error_t                     g_iic_ChStatus[MAX_IIC_CH_NUM];     /* Channel state flag   */
extern volatile r_iic_drv_internal_event_t  g_iic_Event[MAX_IIC_CH_NUM];        /* Event flag           */
extern volatile r_iic_drv_internal_info_t   g_iic_InternalInfo[MAX_IIC_CH_NUM]; /* Internal status management */
extern volatile uint32_t                    g_iic_ReplyCnt[MAX_IIC_CH_NUM];     /* Counter of calling
                                                                                   "R_IIC_Drv_Advance()" upper limit */
extern int32_t                              g_iic_Api[MAX_IIC_CH_NUM];          /* API flag             */

/********************************************************************************************************
Exported global functions (to be accessed by other files)
*********************************************************************************************************/
/* IIC Driver API functions */
error_t R_IIC_Drv_Init(r_iic_drv_info_t * pRIic_Info);
error_t r_iic_drv_init(r_iic_drv_info_t * pRIic_Info);
error_t R_IIC_Drv_MasterTx(r_iic_drv_info_t * pRIic_Info);
error_t r_iic_drv_mastertx(r_iic_drv_info_t * pRIic_Info);
error_t R_IIC_Drv_MasterRx(r_iic_drv_info_t * pRIic_Info);
error_t r_iic_drv_masterrx(r_iic_drv_info_t * pRIic_Info);
error_t R_IIC_Drv_MasterTRx(r_iic_drv_info_t * pRIic_Info);
error_t r_iic_drv_mastertrx(r_iic_drv_info_t * pRIic_Info);
error_t R_IIC_Drv_Advance(r_iic_drv_info_t * pRIic_Info);
error_t r_iic_drv_advance(r_iic_drv_info_t * pRIic_Info);
error_t R_IIC_Drv_GenClk(r_iic_drv_info_t * pRIic_Info, uint8_t ClkCnt);
error_t r_iic_drv_genclk(r_iic_drv_info_t * pRIic_Info, uint8_t ClkCnt);
error_t R_IIC_Drv_Reset(r_iic_drv_info_t * pRIic_Info);
error_t r_iic_drv_reset(r_iic_drv_info_t * pRIic_Info);


#endif /* __R_IIC_DRV_API_H__ */


/* End of File */
