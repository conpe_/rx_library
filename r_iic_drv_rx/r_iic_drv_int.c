/********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2013 Renesas Electronics Corporation. All rights reserved.
*********************************************************************************************************************/
/********************************************************************************************************************
* System Name  : Serial Interface Single Master Control Software
* File Name    : r_iic_drv_int.c
* Version      : 1.12
* Device       : R5F563NBDDFC (RX63N Group)
* Abstract     : Interrupt module
* Tool-Chain   : For RX63N Group
*              :  High-performance Embedded Workshop (Version 4.09.01.007),
*                 C/C++ Compiler Package for RX family (V.1.02 Release 01)
* OS           : not use
* H/W Platform : Renesas Starter Kit for RX63N
* Description  : IIC driver RIIC interrupt module
* Limitation   : None
*********************************************************************************************************************/
/********************************************************************************************************************
* History      : DD.MM.YYYY Version  Description
*              : 29.03.2013 1.10     First Release
*              : 10.05.2013 1.10.R01 Corrected comments.
*              : 17.06.2013 1.11     Moved ICMR3 and ICSR2 setting to r_iic_drv_release() from the stop condition
*              :                     interrupt handler.
*              : 24.06.2013 1.12     Changed a method waiting register setting.
*********************************************************************************************************************/


/********************************************************************************************************************
Includes <System Includes> , "Project Includes"
*********************************************************************************************************************/
#include <stddef.h>                                             /* The basic macro and fixed number support         */

#include "r_iic_drv_api.h"
#include "r_iic_drv_sub.h"
#include "r_iic_drv_int.h"
#include "r_iic_drv_sfr.h"

#include "iodefine.h"

/********************************************************************************************************************
Macro definitions
*********************************************************************************************************************/


/********************************************************************************************************************
Typedef definitions
*********************************************************************************************************************/


/********************************************************************************************************************
Exported global variables (to be accessed by other files)
*********************************************************************************************************************/


/********************************************************************************************************************
Private global variables and functions
*********************************************************************************************************************/


/********************************************************************************************************************
* Outline      : Interrupt EEI handler for channel 0
* Function Name: r_iic_drv_intRIIC0_EEI_isr
* Description  : Types of interrupt requests transfer error or event generation.
*              : The event generations are arbitration-lost, NACK detection, timeout detection, 
*              : start condition detection, and stop condition detection.
* Arguments    : None
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_intRIIC0_EEI_isr(void)
{
#ifdef RIIC0_ENABLE

    /* Checks Arbitration Lost. */
    if ((0U != RIIC0.ICSR2.BIT.AL) && (0U != RIIC0.ICIER.BIT.ALIE))
    {
        RIIC0.ICIER.BIT.ALIE = 0U;
        RIIC0.ICSR2.BIT.AL = 0U;
        if ((0U != RIIC0.ICSR2.BIT.AL) || (0U != RIIC0.ICIER.BIT.ALIE))
        {
            /* wait until the write operation is completed */
        }
        g_iic_Event[0] = R_IIC_EV_INT_AL;
    }

    /* Checks stop condition detection. */
    if ((0U != RIIC0.ICSR2.BIT.STOP) && (0U != RIIC0.ICIER.BIT.SPIE))
    {
        /* Prohibits STOP interrupt. */
        RIIC0.ICIER.BIT.SPIE = 0U;
        if (0U != RIIC0.ICIER.BIT.SPIE)
        {
            /* wait until the write operation is completed */
        }
        /* Sets event flag. */
        g_iic_Event[0] = R_IIC_EV_INT_STOP;
    }

    /* Checks NACK reception. */
    if ((0U != RIIC0.ICSR2.BIT.NACKF) && (0U != RIIC0.ICIER.BIT.NAKIE))
    {
        /* Prohibits NACK interrupt to generate stop condition. */
        RIIC0.ICIER.BIT.NAKIE = 0U;
        
        /* Prohibits these interrupt. */
        /* After NACK interrupt, these interrupts will occur when they do not stop the following interrupts. */
        RIIC0.ICIER.BIT.TEIE = 0U;
        RIIC0.ICIER.BIT.TIE = 0U;
        RIIC0.ICIER.BIT.RIE = 0U;
        if ((0U != RIIC0.ICIER.BIT.TEIE) ||
            (0U != RIIC0.ICIER.BIT.TIE)  ||
            (0U != RIIC0.ICIER.BIT.RIE)     )
        {
            /* wait until the write operation is completed */
        }

        /* Sets event flag. */
        g_iic_Event[0] = R_IIC_EV_INT_NACK;
    }

    /* Checks start condition detection. */
    if ((0U != RIIC0.ICSR2.BIT.START) && (0U != RIIC0.ICIER.BIT.STIE))
    {
        /* Disable start condition detection interrupt. */
        /* Clears status flag. */
        RIIC0.ICIER.BIT.STIE = 0U;
        RIIC0.ICSR2.BIT.START = 0U;
        if ((0U != RIIC0.ICSR2.BIT.START) || (0U != RIIC0.ICIER.BIT.STIE))
        {
            /* wait until the write operation is completed */
        }

        /* Sets event flag. */
        g_iic_Event[0] = R_IIC_EV_INT_START;
    }

#ifdef CALL_ADVANCE_INTERRUPT
    R_IIC_Drv_Advance(&g_iic_Info_ch0);                         /* Calls advance function                           */
#endif  /*#ifdef CALL_ADVANCE_INTERRUPT */


#endif /* #ifdef RIIC0_ENABLE */
}


/********************************************************************************************************************
* Outline      : Interrupt TXI handler for channel 0
* Function Name: r_iic_drv_intRIIC0_TXI_isr
* Description  : Types of interrupt requests transmission data empty.
*              : Does not use.
* Arguments    : None
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_intRIIC0_TXI_isr(void)
{
#ifdef RIIC0_ENABLE

    /* Does nothing. */

#endif /* #ifdef RIIC0_ENABLE */
}


/********************************************************************************************************************
* Outline      : Interrupt RXI handler for channel 0
* Function Name: r_iic_drv_intRIIC0_RXI_isr
* Description  : Types of interrupt requests reception end.
* Arguments    : None
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_intRIIC0_RXI_isr(void)
{
#ifdef RIIC0_ENABLE

    /* Sets interrupted data receiving. */
    g_iic_Event[0] = R_IIC_EV_INT_RECEIVE;

#ifdef CALL_ADVANCE_INTERRUPT
    R_IIC_Drv_Advance(&g_iic_Info_ch0);                         /* Calls advance function                           */
#endif  /*#ifdef CALL_ADVANCE_INTERRUPT */

#endif /* #ifdef RIIC0_ENABLE */
}


/********************************************************************************************************************
* Outline      : Interrupt TEI handler for channel 0
* Function Name: r_iic_drv_intRIIC0_TEI_isr
* Description  : Types of interrupt requests transmission end.
* Arguments    : None
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_intRIIC0_TEI_isr(void)
{
#ifdef RIIC0_ENABLE

    /* Clears ICSR2.TEND. */
    RIIC0.ICSR2.BIT.TEND = 0U;
    if (0U != RIIC0.ICSR2.BIT.TEND)
    {
        /* wait until the write operation is completed */
    }

    /* Sets event. */
    switch (g_iic_InternalInfo[0].N_status)
    {
        case R_IIC_STS_SEND_SLVADR_W_WAIT:
        case R_IIC_STS_SEND_SLVADR_R_WAIT:

            /* Sets interrupted address sending. */
            g_iic_Event[0] = R_IIC_EV_INT_ADD;
        
        break;
        
        case R_IIC_STS_SEND_DATA_WAIT:

            /* Sets interrupted data sending. */
            g_iic_Event[0] = R_IIC_EV_INT_SEND;
        
        break;
        
        default:
            /* Does nothing. */
        break;
        
    }

#ifdef CALL_ADVANCE_INTERRUPT
    R_IIC_Drv_Advance(&g_iic_Info_ch0);                         /* Calls advance function                           */
#endif  /*#ifdef CALL_ADVANCE_INTERRUPT */

#endif /* #ifdef RIIC0_ENABLE */
}


/********************************************************************************************************************
* Outline      : Interrupt EEI handler for channel 1
* Function Name: r_iic_drv_intRIIC1_EEI_isr
* Description  : Types of interrupt requests transfer error or event generation.
*              : The event generations are arbitration-lost, NACK detection, timeout detection, 
*              : start condition detection, and stop condition detection.
* Arguments    : None
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_intRIIC1_EEI_isr(void)
{
#ifdef RIIC1_ENABLE

    /* Checks Arbitration Lost. */
    if ((0U != RIIC1.ICSR2.BIT.AL) && (0U != RIIC1.ICIER.BIT.ALIE))
    {
        RIIC1.ICIER.BIT.ALIE = 0U;
        RIIC1.ICSR2.BIT.AL = 0U;
        if ((0U != RIIC1.ICSR2.BIT.AL) || (0U != RIIC1.ICIER.BIT.ALIE))
        {
            /* wait until the write operation is completed */
        }
        g_iic_Event[1] = R_IIC_EV_INT_AL;
    }

    /* Checks stop condition detection. */
    if ((0U != RIIC1.ICSR2.BIT.STOP) && (0U != RIIC1.ICIER.BIT.SPIE))
    {
        /* Prohibits STOP interrupt. */
        RIIC1.ICIER.BIT.SPIE = 0U;
        if (0U != RIIC1.ICIER.BIT.SPIE)
        {
            /* wait until the write operation is completed */
        }
        /* Sets event flag. */
        g_iic_Event[1] = R_IIC_EV_INT_STOP;
    }

    /* Checks NACK reception. */
    if ((0U != RIIC1.ICSR2.BIT.NACKF) && (0U != RIIC1.ICIER.BIT.NAKIE))
    {
        /* Prohibits NACK interrupt to generate stop condition. */
        RIIC1.ICIER.BIT.NAKIE = 0U;
        
        /* Prohibits these interrupt. */
        /* After NACK interrupt, these interrupts will occur when they do not stop the following interrupts. */
        RIIC1.ICIER.BIT.TEIE = 0U;
        RIIC1.ICIER.BIT.TIE = 0U;
        RIIC1.ICIER.BIT.RIE = 0U;
        if ((0U != RIIC1.ICIER.BIT.TEIE) ||
               (0U != RIIC1.ICIER.BIT.TIE)  ||
               (0U != RIIC1.ICIER.BIT.RIE)     )
        {
            /* wait until the write operation is completed */
        }

        /* Sets event flag. */
        g_iic_Event[1] = R_IIC_EV_INT_NACK;
    }

    /* Checks start condition detection. */
    if ((0U != RIIC1.ICSR2.BIT.START) && (0U != RIIC1.ICIER.BIT.STIE))
    {
        /* Disable start condition detection interrupt. */
        /* Clears status flag. */
        RIIC1.ICIER.BIT.STIE = 0U;
        RIIC1.ICSR2.BIT.START = 0U;
        if ((0U != RIIC1.ICSR2.BIT.START) || (0U != RIIC1.ICIER.BIT.STIE))
        {
            /* wait until the write operation is completed */
        }

        /* Sets event flag. */
        g_iic_Event[1] = R_IIC_EV_INT_START;
    }

#ifdef CALL_ADVANCE_INTERRUPT
    R_IIC_Drv_Advance(&g_iic_Info_ch1);                         /* Calls advance function                           */
#endif  /*#ifdef CALL_ADVANCE_INTERRUPT */

#endif /* #ifdef RIIC1_ENABLE */
}


/********************************************************************************************************************
* Outline      : Interrupt TXI handler for channel 1
* Function Name: r_iic_drv_intRIIC1_TXI_isr
* Description  : Types of interrupt requests transmission data empty.
*              : Does not use.
* Arguments    : None
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_intRIIC1_TXI_isr(void)
{
#ifdef RIIC1_ENABLE

    /* Does nothing. */

#endif /* #ifdef RIIC1_ENABLE */
}


/********************************************************************************************************************
* Outline      : Interrupt RXI handler for channel 1
* Function Name: r_iic_drv_intRIIC1_RXI_isr
* Description  : Types of interrupt requests reception end.
* Arguments    : None
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_intRIIC1_RXI_isr(void)
{
#ifdef RIIC1_ENABLE

    /* Sets interrupted data receiving. */
    g_iic_Event[1] = R_IIC_EV_INT_RECEIVE;

#ifdef CALL_ADVANCE_INTERRUPT
    R_IIC_Drv_Advance(&g_iic_Info_ch1);                         /* Calls advance function                           */
#endif  /*#ifdef CALL_ADVANCE_INTERRUPT */

#endif /* #ifdef RIIC1_ENABLE */
}


/********************************************************************************************************************
* Outline      : Interrupt TEI handler for channel 1
* Function Name: r_iic_drv_intRIIC1_TEI_isr
* Description  : Types of interrupt requests transmission end.
* Arguments    : None
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_intRIIC1_TEI_isr(void)
{
#ifdef RIIC1_ENABLE

    /* Clears ICSR2.TEND. */
    RIIC1.ICSR2.BIT.TEND = 0U;
    if (0U != RIIC1.ICSR2.BIT.TEND)
    {
        /* wait until the write operation is completed */
    }

    /* Sets event. */
    switch (g_iic_InternalInfo[1].N_status)
    {
        case R_IIC_STS_SEND_SLVADR_W_WAIT:
        case R_IIC_STS_SEND_SLVADR_R_WAIT:

            /* Sets interrupted address sending. */
            g_iic_Event[1] = R_IIC_EV_INT_ADD;
        
        break;
        
        case R_IIC_STS_SEND_DATA_WAIT:

            /* Sets interrupted data sending. */
            g_iic_Event[1] = R_IIC_EV_INT_SEND;
        
        break;
        
        default:
            /* Does nothing. */
        break;
        
    }

#ifdef CALL_ADVANCE_INTERRUPT
    R_IIC_Drv_Advance(&g_iic_Info_ch1);                         /* Calls advance function                           */
#endif  /*#ifdef CALL_ADVANCE_INTERRUPT */

#endif /* #ifdef RIIC1_ENABLE */
}


/********************************************************************************************************************
* Outline      : Interrupt EEI handler for channel 2
* Function Name: r_iic_drv_intRIIC2_EEI_isr
* Description  : Types of interrupt requests transfer error or event generation.
*              : The event generations are arbitration-lost, NACK detection, timeout detection, 
*              : start condition detection, and stop condition detection.
* Arguments    : None
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_intRIIC2_EEI_isr(void)
{
#ifdef RIIC2_ENABLE

    /* Checks Arbitration Lost. */
    if ((0U != RIIC2.ICSR2.BIT.AL) && (0U != RIIC2.ICIER.BIT.ALIE))
    {
        RIIC2.ICIER.BIT.ALIE = 0U;
        RIIC2.ICSR2.BIT.AL = 0U;
        if ((0U != RIIC2.ICSR2.BIT.AL) || (0U != RIIC2.ICIER.BIT.ALIE))
        {
            /* wait until the write operation is completed */
        }
        g_iic_Event[2] = R_IIC_EV_INT_AL;
    }

    /* Checks stop condition detection. */
    if ((0U != RIIC2.ICSR2.BIT.STOP) && (0U != RIIC2.ICIER.BIT.SPIE))
    {
        /* Prohibits STOP interrupt. */
        RIIC2.ICIER.BIT.SPIE = 0U;
        if (0U != RIIC2.ICIER.BIT.SPIE)
        {
            /* wait until the write operation is completed */
        }
        /* Sets event flag. */
        g_iic_Event[2] = R_IIC_EV_INT_STOP;
    }

    /* Checks NACK reception. */
    if ((0U != RIIC2.ICSR2.BIT.NACKF) && (0U != RIIC2.ICIER.BIT.NAKIE))
    {
        /* Prohibits NACK interrupt to generate stop condition. */
        RIIC2.ICIER.BIT.NAKIE = 0U;
        
        /* Prohibits these interrupt. */
        /* After NACK interrupt, these interrupts will occur when they do not stop the following interrupts. */
        RIIC2.ICIER.BIT.TEIE = 0U;
        RIIC2.ICIER.BIT.TIE = 0U;
        RIIC2.ICIER.BIT.RIE = 0U;
        if ((0U != RIIC2.ICIER.BIT.TEIE) ||
               (0U != RIIC2.ICIER.BIT.TIE)  ||
               (0U != RIIC2.ICIER.BIT.RIE)     )
        {
            /* wait until the write operation is completed */
        }

        /* Sets event flag. */
        g_iic_Event[2] = R_IIC_EV_INT_NACK;
    }

    /* Checks start condition detection. */
    if ((0U != RIIC2.ICSR2.BIT.START) && (0U != RIIC2.ICIER.BIT.STIE))
    {
        /* Disable start condition detection interrupt. */
        /* Clears status flag. */
        RIIC2.ICIER.BIT.STIE = 0U;
        RIIC2.ICSR2.BIT.START = 0U;
        if ((0U != RIIC2.ICSR2.BIT.START) || (0U != RIIC2.ICIER.BIT.STIE))
        {
            /* wait until the write operation is completed */
        }

        /* Sets event flag. */
        g_iic_Event[2] = R_IIC_EV_INT_START;
    }

#ifdef CALL_ADVANCE_INTERRUPT
    R_IIC_Drv_Advance(&g_iic_Info_ch2);                         /* Calls advance function                           */
#endif  /*#ifdef CALL_ADVANCE_INTERRUPT */

#endif /* #ifdef RIIC2_ENABLE */
}


/********************************************************************************************************************
* Outline      : Interrupt TXI handler for channel 2
* Function Name: r_iic_drv_intRIIC2_TXI_isr
* Description  : Types of interrupt requests transmission data empty.
*              : Does not use.
* Arguments    : None
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_intRIIC2_TXI_isr(void)
{
#ifdef RIIC2_ENABLE

    /* Does nothing. */

#endif /* #ifdef RIIC2_ENABLE */
}


/********************************************************************************************************************
* Outline      : Interrupt RXI handler for channel 2
* Function Name: r_iic_drv_intRIIC2_RXI_isr
* Description  : Types of interrupt requests reception end.
* Arguments    : None
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_intRIIC2_RXI_isr(void)
{
#ifdef RIIC2_ENABLE

    /* Sets interrupted data receiving. */
    g_iic_Event[2] = R_IIC_EV_INT_RECEIVE;

#ifdef CALL_ADVANCE_INTERRUPT
    R_IIC_Drv_Advance(&g_iic_Info_ch2);                         /* Calls advance function                           */
#endif  /*#ifdef CALL_ADVANCE_INTERRUPT */

#endif /* #ifdef RIIC2_ENABLE */
}


/********************************************************************************************************************
* Outline      : Interrupt TEI handler for channel 2
* Function Name: r_iic_drv_intRIIC2_TEI_isr
* Description  : Types of interrupt requests transmission end.
* Arguments    : None
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_intRIIC2_TEI_isr(void)
{
#ifdef RIIC2_ENABLE

    /* Clears ICSR2.TEND. */
    RIIC2.ICSR2.BIT.TEND = 0U;
    if (0U != RIIC2.ICSR2.BIT.TEND)
    {
        /* wait until the write operation is completed */
    }

    /* Sets event. */
    switch (g_iic_InternalInfo[2].N_status)
    {
        case R_IIC_STS_SEND_SLVADR_W_WAIT:
        case R_IIC_STS_SEND_SLVADR_R_WAIT:

            /* Sets interrupted address sending. */
            g_iic_Event[2] = R_IIC_EV_INT_ADD;
        
        break;
        
        case R_IIC_STS_SEND_DATA_WAIT:

            /* Sets interrupted data sending. */
            g_iic_Event[2] = R_IIC_EV_INT_SEND;
        
        break;
        
        default:
            /* Does nothing. */
        break;
        
    }

#ifdef CALL_ADVANCE_INTERRUPT
    R_IIC_Drv_Advance(&g_iic_Info_ch2);                         /* Calls advance function                           */
#endif  /*#ifdef CALL_ADVANCE_INTERRUPT */

#endif /* #ifdef RIIC2_ENABLE */
}


/********************************************************************************************************************
* Outline      : Interrupt EEI handler for channel 3
* Function Name: r_iic_drv_intRIIC3_EEI_isr
* Description  : Types of interrupt requests transfer error or event generation.
*              : The event generations are arbitration-lost, NACK detection, timeout detection, 
*              : start condition detection, and stop condition detection.
* Arguments    : None
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_intRIIC3_EEI_isr(void)
{
#ifdef RIIC3_ENABLE

    /* Checks Arbitration Lost. */
    if ((0U != RIIC3.ICSR2.BIT.AL) && (0U != RIIC3.ICIER.BIT.ALIE))
    {
        RIIC3.ICIER.BIT.ALIE = 0U;
        RIIC3.ICSR2.BIT.AL = 0U;
        if ((0U != RIIC3.ICSR2.BIT.AL) || (0U != RIIC3.ICIER.BIT.ALIE))
        {
            /* wait until the write operation is completed */
        }
        g_iic_Event[3] = R_IIC_EV_INT_AL;
    }

    /* Checks stop condition detection. */
    if ((0U != RIIC3.ICSR2.BIT.STOP) && (0U != RIIC3.ICIER.BIT.SPIE))
    {
        /* Prohibits STOP interrupt. */
        RIIC3.ICIER.BIT.SPIE = 0U;
        if (0U != RIIC3.ICIER.BIT.SPIE)
        {
            /* wait until the write operation is completed */
        }
        /* Sets event flag. */
        g_iic_Event[3] = R_IIC_EV_INT_STOP;
    }

    /* Checks NACK reception. */
    if ((0U != RIIC3.ICSR2.BIT.NACKF) && (0U != RIIC3.ICIER.BIT.NAKIE))
    {
        /* Prohibits NACK interrupt to generate stop condition. */
        RIIC3.ICIER.BIT.NAKIE = 0U;
        
        /* Prohibits these interrupt. */
        /* After NACK interrupt, these interrupts will occur when they do not stop the following interrupts. */
        RIIC3.ICIER.BIT.TEIE = 0U;
        RIIC3.ICIER.BIT.TIE = 0U;
        RIIC3.ICIER.BIT.RIE = 0U;
        if ((0U != RIIC3.ICIER.BIT.TEIE) ||
               (0U != RIIC3.ICIER.BIT.TIE)  ||
               (0U != RIIC3.ICIER.BIT.RIE)     )
        {
            /* wait until the write operation is completed */
        }

        /* Sets event flag. */
        g_iic_Event[3] = R_IIC_EV_INT_NACK;
    }

    /* Checks start condition detection. */
    if ((0U != RIIC3.ICSR2.BIT.START) && (0U != RIIC3.ICIER.BIT.STIE))
    {
        /* Disable start condition detection interrupt. */
        /* Clears status flag. */
        RIIC3.ICIER.BIT.STIE = 0U;
        RIIC3.ICSR2.BIT.START = 0U;
        if ((0U != RIIC3.ICSR2.BIT.START) || (0U != RIIC3.ICIER.BIT.STIE))
        {
            /* wait until the write operation is completed */
        }

        /* Sets event flag. */
        g_iic_Event[3] = R_IIC_EV_INT_START;
    }

#ifdef CALL_ADVANCE_INTERRUPT
    R_IIC_Drv_Advance(&g_iic_Info_ch3);                         /* Calls advance function                           */
#endif  /*#ifdef CALL_ADVANCE_INTERRUPT */

#endif /* #ifdef RIIC3_ENABLE */
}


/********************************************************************************************************************
* Outline      : Interrupt TXI handler for channel 3
* Function Name: r_iic_drv_intRIIC3_TXI_isr
* Description  : Types of interrupt requests transmission data empty.
*              : Does not use.
* Arguments    : None
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_intRIIC3_TXI_isr(void)
{
#ifdef RIIC3_ENABLE

    /* Does nothing. */

#endif /* #ifdef RIIC3_ENABLE */
}


/********************************************************************************************************************
* Outline      : Interrupt RXI handler for channel 3
* Function Name: r_iic_drv_intRIIC3_RXI_isr
* Description  : Types of interrupt requests reception end.
* Arguments    : None
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_intRIIC3_RXI_isr(void)
{
#ifdef RIIC3_ENABLE

    /* Sets interrupted data receiving. */
    g_iic_Event[3] = R_IIC_EV_INT_RECEIVE;

#ifdef CALL_ADVANCE_INTERRUPT
    R_IIC_Drv_Advance(&g_iic_Info_ch3);                         /* Calls advance function                           */
#endif  /*#ifdef CALL_ADVANCE_INTERRUPT */

#endif /* #ifdef RIIC3_ENABLE */
}


/********************************************************************************************************************
* Outline      : Interrupt TEI handler for channel 3
* Function Name: r_iic_drv_intRIIC3_TEI_isr
* Description  : Types of interrupt requests transmission end.
* Arguments    : None
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_intRIIC3_TEI_isr(void)
{
#ifdef RIIC3_ENABLE

    /* Clears ICSR2.TEND. */
    RIIC3.ICSR2.BIT.TEND = 0U;
    if (0U != RIIC3.ICSR2.BIT.TEND)
    {
        /* wait until the write operation is completed */
    }

    /* Sets event. */
    switch (g_iic_InternalInfo[3].N_status)
    {
        case R_IIC_STS_SEND_SLVADR_W_WAIT:
        case R_IIC_STS_SEND_SLVADR_R_WAIT:

            /* Sets interrupted address sending. */
            g_iic_Event[3] = R_IIC_EV_INT_ADD;
        
        break;
        
        case R_IIC_STS_SEND_DATA_WAIT:

            /* Sets interrupted data sending. */
            g_iic_Event[3] = R_IIC_EV_INT_SEND;
        
        break;
        
        default:
            /* Does nothing. */
        break;
        
    }

#ifdef CALL_ADVANCE_INTERRUPT
    R_IIC_Drv_Advance(&g_iic_Info_ch3);                         /* Calls advance function                           */
#endif  /*#ifdef CALL_ADVANCE_INTERRUPT */

#endif /* #ifdef RIIC3_ENABLE */
}


/* End of File */
