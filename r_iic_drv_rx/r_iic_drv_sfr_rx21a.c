/********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2013 Renesas Electronics Corporation. All rights reserved.
*********************************************************************************************************************/
/********************************************************************************************************************
* System Name  : Serial Interface Single Master Control Software
* File Name    : r_iic_drv_sfr.c
* Version      : 1.12
* Device       : R5F521A8BDFP (RX21A Group)
* Abstract     : OS module
* Tool-Chain   : For RX21A Group
*              :  High-performance Embedded Workshop (Version 4.09.01.007),
*                 C/C++ Compiler Package for RX family (V.1.02 Release 01)
* OS           : not use
* H/W Platform : Renesas Starter Kit for RX21A
* Description  : IIC driver RIIC SFR module
* Limitation   : None
*********************************************************************************************************************/
/********************************************************************************************************************
* History      : DD.MM.YYYY Version  Description
*              : 29.03.2013 1.10     First Release
*              : 10.05.2013 1.10.R01 Corrected comments.
*              : 17.06.2013 1.11     Modified r_iic_drv_next_comm_setting().
*              : 24.06.2013 1.12     Added r_iic_drv_check_nackf_bit().
*              : 17.06.2013 1.12     Changed the setting of ICMR3.ACKBT bit and ICMR3.ACKWP bit to 1 processing.
*********************************************************************************************************************/


/********************************************************************************************************************
Includes <System Includes> , "Project Includes"
*********************************************************************************************************************/
#include <machine.h>                                            /* RX MCU's support                                 */
#include <stddef.h>                                             /* The basic macro and fixed number support         */

#include "r_iic_drv_api.h"
#include "r_iic_drv_sub.h"
#include "r_iic_drv_int.h"
#include "r_iic_drv_sfr.h"

#include "iodefine.h"

/********************************************************************************************************************
Macro definitions
*********************************************************************************************************************/


/********************************************************************************************************************
Typedef definitions
*********************************************************************************************************************/


/********************************************************************************************************************
Exported global variables (to be accessed by other files)
*********************************************************************************************************************/


/********************************************************************************************************************
Private global variables and functions
*********************************************************************************************************************/


/********************************************************************************************************************
* Outline      : Open Port Processing
* Function Name: r_iic_drv_io_open
* Description  : Sets ports to input mode.
*              : Ports input pull-up becomes "Off".
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : None
*--------------------------------------------------------------------------------------------------------------------
* Notes        : 1. Set Drive capacity control register (DSCR) for each specified channel.
*              :    As for channel 0 or 1, set the DSCR to 1 or 0.
*********************************************************************************************************************/
void r_iic_drv_io_open(r_iic_drv_info_t * pRIic_Info)
{
    volatile uint8_t uctmp = 0U;

    /* Channel number? */
    switch (pRIic_Info->ChNo)
    {
        case 0U:                                                /* Channel 0                                        */
#ifdef RIIC0_ENABLE
            /* Sets the port register of SCL pin. */
            R_IIC_DSCR_SCL0 = 1U;                               /* SCL0 drive capacity control : High-drive output  */
            R_IIC_PCR_SCL0  = 0U;                               /* SCL0 input pull-up resister : off                */
            R_IIC_PDR_SCL0  = R_IIC_IN;                         /* SCL0 input mode                                  */

            /* Sets the port register of SDA pin. */
            R_IIC_DSCR_SDA0 = 1U;                               /* SDA0 drive capacity control : High-drive output  */
            R_IIC_PCR_SDA0  = 0U;                               /* SDA0 input pull-up resister : off                */
            R_IIC_PDR_SDA0  = R_IIC_IN;                         /* SDA0 input mode                                  */
            uctmp           = R_IIC_PDR_SDA0;                   /* Reads PDR.                                       */
#endif /* #ifdef RIIC0_ENABLE */
        break;

        case 1U:                                                /* Channel 1                                        */
#ifdef RIIC1_ENABLE
            /* Sets the port register of SCL pin. */
            R_IIC_DSCR_SCL1 = 1U;                               /* SCL1 drive capacity control : High-drive output  */
            R_IIC_PCR_SCL1  = 0U;                               /* SCL1 input pull-up resister : off                */
            R_IIC_PDR_SCL1  = R_IIC_IN;                         /* SCL1 input mode                                  */

            /* Sets the port register of SDA pin. */
            R_IIC_DSCR_SDA1 = 1U;                               /* SDA1 drive capacity control : High-drive output  */
            R_IIC_PCR_SDA1  = 0U;                               /* SDA1 input pull-up resister : off                */
            R_IIC_PDR_SDA1  = R_IIC_IN;                         /* SDA1 input mode                                  */
            uctmp           = R_IIC_PDR_SDA1;                   /* Reads PDR.                                       */
#endif /* #ifdef RIIC1_ENABLE */
        break;

        default:
            /* Please add a channel as needed. */
        break;
    }
}


/********************************************************************************************************************
* Outline      : Initialize IIC Driver Processing
* Function Name: r_iic_drv_init_sfr
* Description  : Initializes RIIC register.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_init_sfr(r_iic_drv_info_t * pRIic_Info)
{
    volatile uint8_t uctmp = 0U;

    /* Creates the register pointer for the specified RIIC channel. */
    volatile uint8_t * const    pcICCR1     = ICCR1_ADR(pRIic_Info->ChNo);
    volatile uint8_t * const    pcSARL0     = SARL0_ADR(pRIic_Info->ChNo);
    volatile uint8_t * const    pcSARU0     = SARU0_ADR(pRIic_Info->ChNo);
    volatile uint8_t * const    pcSARL1     = SARL1_ADR(pRIic_Info->ChNo);
    volatile uint8_t * const    pcSARU1     = SARU1_ADR(pRIic_Info->ChNo);
    volatile uint8_t * const    pcSARL2     = SARL2_ADR(pRIic_Info->ChNo);
    volatile uint8_t * const    pcSARU2     = SARU2_ADR(pRIic_Info->ChNo);
    volatile uint8_t * const    pcICSER     = ICSER_ADR(pRIic_Info->ChNo);
    volatile uint8_t * const    pcICMR2     = ICMR2_ADR(pRIic_Info->ChNo);
    volatile uint8_t * const    pcICMR3     = ICMR3_ADR(pRIic_Info->ChNo);
    volatile uint8_t * const    pcICFER     = ICFER_ADR(pRIic_Info->ChNo);

    /* Sets SCLn and SDAn to non-driven state. */
    *pcICCR1 &= R_IIC_ICCR1_NOT_DRIVEN;                         /* Clears ICCR1.ICE bit to 0.                       */

    /* Resets all RIIC registers and the internal status. (RIIC reset) */
    *pcICCR1 |= R_IIC_ICCR1_RIIC_RESET;                         /* Sets ICCR1.IICRST bit to 1.                      */

    /* Resets ICMR.BC, ICSR1, ICSR2, ICDRS registers and the internal status. (Internal reset) */
    *pcICCR1 |= R_IIC_ICCR1_INTER_RESET;                        /* Sets ICCR1.ICE bit to 1.                         */

    /* Sets slave address format and slave address. */
    *pcSARL0 = R_IIC_SARL0_INIT;                                /* Sets SARLy and SARUy.(y=0,1,2)                   */
    *pcSARU0 = R_IIC_SARU0_INIT;                                /* Sets SARLy and SARUy.(y=0,1,2)                   */
    *pcSARL1 = R_IIC_SARL1_INIT;                                /* Sets SARLy and SARUy.(y=0,1,2)                   */
    *pcSARU1 = R_IIC_SARU1_INIT;                                /* Sets SARLy and SARUy.(y=0,1,2)                   */
    *pcSARL2 = R_IIC_SARL2_INIT;                                /* Sets SARLy and SARUy.(y=0,1,2)                   */
    *pcSARU2 = R_IIC_SARU2_INIT;                                /* Sets SARLy and SARUy.(y=0,1,2)                   */
    *pcICSER = R_IIC_ICSER_INIT;                                /* Sets ICSER register.                             */

    uctmp    = *pcICSER;                                        /* Reads ICSER.                                     */

    /* Sets a transfer clock. */
    /* Includes SFR read operation at the end of the following function. */
    r_iic_drv_set_frequency(pRIic_Info);                        /* Sets ICMR1.CKS[2:0] bit, ICBRL and ICBRH registers. */

    /* Initializes ICMR2 and ICMR3 registers. */
    *pcICMR2 = R_IIC_ICMR2_INIT;
    *pcICMR3 = R_IIC_ICMR3_INIT;

    /* Disables IIC interrupt. */
    r_iic_drv_int_disable(pRIic_Info);

    /* Initializes ICFER register. */
    *pcICFER = R_IIC_ICFER_INIT;
    uctmp    = *pcICFER;                                        /* Reads ICFER.                                     */
}


/********************************************************************************************************************
* Outline      : Disable Interrupt Processing
* Function Name: r_iic_drv_int_disable
* Description  : Disables interrupt.
*              : Sets interrupt source priority.
*              : Clears interrupt request register.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_int_disable(r_iic_drv_info_t * pRIic_Info)
{
    volatile uint8_t uctmp = 0U;

    /* Channel number? */
    switch (pRIic_Info->ChNo)
    {
        case 0U:                                                /* Channel 0                                        */
#ifdef RIIC0_ENABLE
            /* Disables interrupt. */
            R_IIC_IER_EEI0 = R_IIC_IER_DISABLE;                 /* Disables EEI0 interrupt request enable register. */
            R_IIC_IER_RXI0 = R_IIC_IER_DISABLE;                 /* Disables RXI0 interrupt request enable register. */
            R_IIC_IER_TXI0 = R_IIC_IER_DISABLE;                 /* Disables TXI0 interrupt request enable register. */
            R_IIC_IER_TEI0 = R_IIC_IER_DISABLE;                 /* Disables TEI0 interrupt request enable register. */

            /* Sets interrupt source priority. */
            R_IIC_IPR_EEI0 = R_IIC_IPR_CH0_EEI_INIT;            /* Sets EEI0 interrupt source priority register.    */
            R_IIC_IPR_RXI0 = R_IIC_IPR_CH0_RXI_INIT;            /* Sets RXI0 interrupt source priority register.    */
            R_IIC_IPR_TXI0 = R_IIC_IPR_CH0_TXI_INIT;            /* Sets TXI0 interrupt source priority register.    */
            R_IIC_IPR_TEI0 = R_IIC_IPR_CH0_TEI_INIT;            /* Sets TEI0 interrupt source priority register.    */
            uctmp          = R_IIC_IPR_TEI0;                    /* Reads IPR.                                       */
#endif /* #ifdef RIIC0_ENABLE */
        break;

        case 1U:                                                /* Channel 1                                        */
#ifdef RIIC1_ENABLE
            /* Disables interrupt. */
            R_IIC_IER_EEI1 = R_IIC_IER_DISABLE;                 /* Disables EEI1 interrupt request enable register. */
            R_IIC_IER_RXI1 = R_IIC_IER_DISABLE;                 /* Disables RXI1 interrupt request enable register. */
            R_IIC_IER_TXI1 = R_IIC_IER_DISABLE;                 /* Disables TXI1 interrupt request enable register. */
            R_IIC_IER_TEI1 = R_IIC_IER_DISABLE;                 /* Disables TEI1 interrupt request enable register. */

            /* Sets interrupt source priority. */
            R_IIC_IPR_EEI1 = R_IIC_IPR_CH1_EEI_INIT;            /* Sets EEI1 interrupt source priority register.    */
            R_IIC_IPR_RXI1 = R_IIC_IPR_CH1_RXI_INIT;            /* Sets RXI1 interrupt source priority register.    */
            R_IIC_IPR_TXI1 = R_IIC_IPR_CH1_TXI_INIT;            /* Sets TXI1 interrupt source priority register.    */
            R_IIC_IPR_TEI1 = R_IIC_IPR_CH1_TEI_INIT;            /* Sets TEI1 interrupt source priority register.    */
            uctmp          = R_IIC_IPR_TEI1;                    /* Reads IPR.                                       */
#endif /* #ifdef RIIC1_ENABLE */
        break;

        default:
            /* Please add a channel as needed. */
        break;
    }

    /* Clears the interrupt request register. */
    /* Includes SFR read operation at the end of the following function. */
    r_iic_drv_clear_ir_flag(pRIic_Info);
}


/********************************************************************************************************************
* Outline      : Enable Interrupt Processing
* Function Name: r_iic_drv_int_enable
* Description  : Clears interrupt request register.
*              : Enables interrupt.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_int_enable(r_iic_drv_info_t * pRIic_Info)
{
    volatile uint8_t uctmp = 0U;

    /* Clears the interrupt request register. */
    r_iic_drv_clear_ir_flag(pRIic_Info);

    /* Channel number? */
    switch (pRIic_Info->ChNo)
    {
        case 0U:                                                /* Channel 0                                        */
#ifdef RIIC0_ENABLE
            /* Enables interrupt. */
            R_IIC_IER_EEI0 = R_IIC_IER_ENABLE;                  /* Enables EEI0 interrupt request enable register.  */
            R_IIC_IER_RXI0 = R_IIC_IER_ENABLE;                  /* Enables RXI0 interrupt request enable register.  */
            R_IIC_IER_TXI0 = R_IIC_IER_ENABLE;                  /* Enables TXI0 interrupt request enable register.  */
            R_IIC_IER_TEI0 = R_IIC_IER_ENABLE;                  /* Enables TEI0 interrupt request enable register.  */
            uctmp          = R_IIC_IER_TEI0;                    /* Reads IER.                                       */
#endif /* #ifdef RIIC0_ENABLE */
        break;

        case 1U:                                                /* Channel 1                                        */
#ifdef RIIC1_ENABLE
            /* Enables interrupt. */
            R_IIC_IER_EEI1 = R_IIC_IER_ENABLE;                  /* Enables EEI1 interrupt request enable register.  */
            R_IIC_IER_RXI1 = R_IIC_IER_ENABLE;                  /* Enables RXI1 interrupt request enable register.  */
            R_IIC_IER_TXI1 = R_IIC_IER_ENABLE;                  /* Enables TXI1 interrupt request enable register.  */
            R_IIC_IER_TEI1 = R_IIC_IER_ENABLE;                  /* Enables TEI1 interrupt request enable register.  */
            uctmp          = R_IIC_IER_TEI1;                    /* Reads IER.                                       */
#endif /* #ifdef RIIC1_ENABLE */
        break;

        default:
            /* Please add a channel as needed. */
        break;
    }
}


/********************************************************************************************************************
* Outline      : Set ICIER Register Processing
* Function Name: r_iic_drv_int_icier_setting
* Description  : Sets an interrupt condition of RIIC.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
               : r_iic_drv_internal_status_t new_status    ;   New status
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_int_icier_setting(r_iic_drv_info_t * pRIic_Info, uint8_t New_icier)
{
    volatile uint8_t uctmp = 0U;

    /* Creates the register pointer for the specified RIIC channel. */
    volatile uint8_t * const    pcICIER     = ICIER_ADR(pRIic_Info->ChNo);

    /* Sets ICIER register. */
    *pcICIER = New_icier;
    uctmp    = *pcICIER;                                        /* Reads ICIER.                                     */
}


/********************************************************************************************************************
* Outline      : Set IIC Frequency Processing
* Function Name: r_iic_drv_set_frequency
* Description  : Sets ICMR1, ICBRL and ICBRH registers.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_set_frequency(r_iic_drv_info_t * pRIic_Info)
{
    volatile uint8_t uctmp = 0U;

    /* Creates the register pointer for the specified RIIC channel. */
    volatile uint8_t * const    pcICBRL     = ICBRL_ADR(pRIic_Info->ChNo);
    volatile uint8_t * const    pcICBRH     = ICBRH_ADR(pRIic_Info->ChNo);
    volatile uint8_t * const    pcICMR1     = ICMR1_ADR(pRIic_Info->ChNo);

    /* Channel number? */
    switch (pRIic_Info->ChNo)
    {
        case 0U:                                                /* Channel 0                                        */
#ifdef RIIC0_ENABLE
            *pcICMR1 = R_IIC_CH0_ICMR1_INIT;                    /* Sets ICMR1                                       */
            *pcICBRL = R_IIC_CH0_LCLK;                          /* Low-level width setting.                         */
            *pcICBRH = R_IIC_CH0_HCLK;                          /* High-level width setting.                        */
#endif /* #ifdef RIIC0_ENABLE */
        break;

        case 1U:                                                /* Channel 1                                        */
#ifdef RIIC1_ENABLE
            *pcICMR1 = R_IIC_CH1_ICMR1_INIT;                    /* Sets ICMR1                                       */
            *pcICBRL = R_IIC_CH1_LCLK;                          /* Low-level width setting.                         */
            *pcICBRH = R_IIC_CH1_HCLK;                          /* High-level width setting.                        */
#endif /* #ifdef RIIC1_ENABLE */
        break;

        default:
            /* Please add a channel as needed. */
        break;
    }
    
    uctmp = *pcICBRH;                                           /* Reads ICBRH.                                     */
}


/********************************************************************************************************************
* Outline      : Check SDA Level Processing
* Function Name: r_iic_drv_check_sda_level
* Description  : Checks SDA level high.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : R_IIC_TRUE                        ;   SDA = High
*              : R_IIC_FALSE                       ;   SDA = Low
*********************************************************************************************************************/
bool r_iic_drv_check_sda_level(r_iic_drv_info_t * pRIic_Info)
{
    bool              ret = R_IIC_TRUE;
    volatile uint16_t cnt = 0U;

    /* Initializes counter. */
    cnt = SDACHK_CNT;

    /* Channel number? */
    switch (pRIic_Info->ChNo)
    {
        case 0U:                                                /* Channel 0                                        */
#ifdef RIIC0_ENABLE
            /* Checks SDA level. */
            do
            {
                /* SDA = High? */
                if (R_IIC_HI == R_IIC_PIDR_SDA0)
                {
                    break;                                      /* SDA = High.                                      */
                }
                cnt--;
            }
            while (0U != cnt);
#endif /* #ifdef RIIC0_ENABLE */
        break;

        case 1U:                                                /* Channel 1                                        */
#ifdef RIIC1_ENABLE
            /* Checks SDA level. */
            do
            {
                /* SDA = High? */
                if (R_IIC_HI == R_IIC_PIDR_SDA1)
                {
                    break;                                      /* SDA = High.                                      */
                }
                cnt--;
            }
            while (0U != cnt);
#endif /* #ifdef RIIC1_ENABLE */
        break;

        default:
            /* Please add a channel as needed. */
        break;
    }

    /* Counter limit? */
    if(0U == cnt)
    {
        ret = R_IIC_FALSE;                                      /* SDA = Low.                                       */
    }

    return ret;
}


/********************************************************************************************************************
* Outline      : Check Bus Busy Processing
* Function Name: r_iic_drv_check_bus_busy
* Description  : When the bits of the RIIC registers are the following set values, the bus is released.
*              :   ICCR2.BBSY = 0 ; The I2C bus is released (bus free state).
*              :   ICCR1.SCLI = 1 ; SCLn pin input is at a high level.
*              :   ICCR1.SDAI = 1 ; SDAn pin input is at a high level.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : R_IIC_TRUE                        ;   Bus ready
*              : R_IIC_FALSE                       ;   Bus busy
*********************************************************************************************************************/
bool r_iic_drv_check_bus_busy(r_iic_drv_info_t * pRIic_Info)
{
    volatile uint32_t cnt = 0U;

    /* Creates the register pointer for the specified RIIC channel. */
    volatile uint8_t * const    pcICCR1     = ICCR1_ADR(pRIic_Info->ChNo);
    volatile uint8_t * const    pcICCR2     = ICCR2_ADR(pRIic_Info->ChNo);

    /* Checks bus busy. */
    for (cnt = BUSCHK_CNT; cnt > 0U; cnt--)
    {
        /* Bus busy? */
        if ((R_IIC_MSK_BBSY != (*pcICCR2 & R_IIC_MSK_BBSY))  &&                 /* ICCR2.BBSY = 0                   */
            (R_IIC_MSK_SCLSDA == (*pcICCR1 & R_IIC_MSK_SCLSDA)))                /* ICCR1.SCLI = 1, ICCR1.SDAI = 1   */
        {
            return R_IIC_TRUE;                                  /* Bus ready                                        */
        }
    }

    return R_IIC_FALSE;                                         /* Bus busy                                         */;
}


/********************************************************************************************************************
* Outline      : Cancel RIIC Module Stop Control Processing
* Function Name: r_iic_drv_cancel_mstp
* Description  : Cancels RIIC module stop state.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_cancel_mstp(r_iic_drv_info_t * pRIic_Info)
{
    volatile uint32_t ultmp = 0U;
    volatile uint16_t ustmp = 0U;

    /* Sets PRCR.PRC1 bit. */
    R_IIC_PRCR = R_IIC_PRCR_ENABLE;                             /* Enables writing to module stop control register. */
    ustmp = R_IIC_PRCR;                                         /* Reads PRCR.                                      */

    /* Channel number? */
    switch (pRIic_Info->ChNo)
    {
        case 0U:
#ifdef RIIC0_ENABLE
            R_IIC_MSTP0 = 0U;                                   /* RIIC0 enables input clock supply.                */
            ultmp       = R_IIC_MSTP0;                          /* Reads Module Stop Control Register.              */
#endif /* #ifdef RIIC0_ENABLE */
        break;
    
        case 1U:
#ifdef RIIC1_ENABLE
            R_IIC_MSTP1 = 0U;                                   /* RIIC1 enables input clock supply.                */
            ultmp       = R_IIC_MSTP1;                          /* Reads Module Stop Control Register.              */
#endif /* #ifdef RIIC1_ENABLE */
        break;
    
        default:
            /* Please add a channel as needed. */
        break;
    }

    /* Clears PRCR.PRC1 bit. */
    R_IIC_PRCR = R_IIC_PRCR_DISABLE;                            /* Disables writing to module stop control register.*/
    ustmp = R_IIC_PRCR;                                         /* Reads PRCR.                                      */
}


/********************************************************************************************************************
* Outline      : Generate Start Condition Processing
* Function Name: r_iic_drv_start_cond_generate
* Description  : Sets ICCR2.ST bit.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_start_cond_generate(r_iic_drv_info_t * pRIic_Info)
{
    volatile uint8_t uctmp = 0U;

    /* Creates the register pointer for the specified RIIC channel. */
    volatile uint8_t * const    pcICSR2     = ICSR2_ADR(pRIic_Info->ChNo);
    volatile uint8_t * const    pcICCR2     = ICCR2_ADR(pRIic_Info->ChNo);

    /* Clears start condition detection flag. */
    if (0U != ((*pcICSR2 & R_IIC_ICSR2_START_SET) >> 2U))
    {
        *pcICSR2 &= R_IIC_ICSR2_START_CLR;
    }

    /* Generates a start condition. */
    *pcICCR2 |= R_IIC_ICCR2_ST;                                 /* Sets ICCR2.ST bit.                               */
    uctmp    = *pcICCR2;                                        /* Reads ICCR2.                                     */
}


/********************************************************************************************************************
* Outline      : Generate Restart Condition Processing
* Function Name: r_iic_drv_re_start_cond_generate
* Description  : Sets ICCR2.RS bit.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_re_start_cond_generate(r_iic_drv_info_t * pRIic_Info)
{
    volatile uint8_t uctmp = 0U;

    /* Creates the register pointer for the specified RIIC channel. */
    volatile uint8_t * const    pcICSR2     = ICSR2_ADR(pRIic_Info->ChNo);
    volatile uint8_t * const    pcICCR2     = ICCR2_ADR(pRIic_Info->ChNo);

    /* Clears start condition detection flag. */
    if (0U != ((*pcICSR2 & R_IIC_ICSR2_START_SET) >> 2U))
    {
        *pcICSR2 &= R_IIC_ICSR2_START_CLR;
    }

    /* Generates a restart condition. */
    *pcICCR2 |= R_IIC_ICCR2_RS;                                 /* Sets ICCR2.RS bit.                               */
    uctmp    = *pcICCR2;                                        /* Reads ICCR2.                                     */
}


/********************************************************************************************************************
* Outline      : Generate Stop Condition Processing
* Function Name: r_iic_drv_stop_cond_generate
* Description  : Clears ICSR2.STOP bit and sets ICCR2.SP bit.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_stop_cond_generate(r_iic_drv_info_t * pRIic_Info)
{
    volatile uint8_t uctmp = 0U;

    /* Creates the register pointer for the specified RIIC channel. */
    volatile uint8_t * const    pcICSR2     = ICSR2_ADR(pRIic_Info->ChNo);
    volatile uint8_t * const    pcICCR2     = ICCR2_ADR(pRIic_Info->ChNo);

    /* Clears stop condition detection flag. */
    if (0U != ((*pcICSR2 & R_IIC_ICSR2_STOP_SET) >> 3U))
    {
        *pcICSR2 &= R_IIC_ICSR2_STOP_CLR;
    }
    
    /* Generates a stop condition. */
    *pcICCR2 |= R_IIC_ICCR2_SP;                                 /* Sets ICCR2.SP bit.                               */
    uctmp    = *pcICCR2;                                        /* Reads ICCR2.                                     */
}


/********************************************************************************************************************
* Outline      : Transmit Data Processing
* Function Name: r_iic_drv_set_sending_data
* Description  : Sets transmission data to ICDRT register.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
*              : uint8_t *pData                    ;   Transmitted data buffer pointer
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_set_sending_data(r_iic_drv_info_t * pRIic_Info, uint8_t *pData)
{
    volatile uint8_t uctmp = 0U;

    /* Creates the register pointer for the specified RIIC channel. */
    volatile uint8_t * const    pcICDRT     = ICDRT_ADR(pRIic_Info->ChNo);
    
    /* Sets the transmitting data. */
    *pcICDRT = *pData;                                          /* Writes data to RIIC in order to transmit.        */
    uctmp    = *pcICDRT;                                        /* Reads ICDRT.                                     */
}


/********************************************************************************************************************
* Outline      : Receive Data Prpcessing
* Function Name: r_iic_drv_get_receiving_data
* Description  : Stores received data from ICDRR register.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : Returns received data.
*********************************************************************************************************************/
uint8_t r_iic_drv_get_receiving_data(r_iic_drv_info_t * pRIic_Info)
{
    /* Creates the register pointer for the specified RIIC channel. */
    volatile uint8_t * const    pcICDRR     = ICDRR_ADR(pRIic_Info->ChNo);

    /* Stores the received data. */
    return *pcICDRR;
}


/********************************************************************************************************************
* Outline      : Receive "last byte - 2bytes" Setting Proccesing
* Function Name: r_iic_drv_receive_wait_setting
* Description  : Sets ICMR3.WAIT bit.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_receive_wait_setting(r_iic_drv_info_t * pRIic_Info)
{
    volatile uint8_t uctmp = 0U;

    /* Creates the register pointer for the specified RIIC channel. */
    volatile uint8_t * const    pcICMR3     = ICMR3_ADR(pRIic_Info->ChNo);

    /* Sets ICMR3.WAIT bit. */
    *pcICMR3 |= R_IIC_ICMR3_WAIT_SET;
    uctmp    = *pcICMR3;                                        /* Reads ICMR3.                                     */
}


/********************************************************************************************************************
* Outline      : Receive "last byte - 1byte" Setting Proccesing
* Function Name: r_iic_drv_receive_pre_end_setting
* Description  : Sets ICMR3.RDRFS bit and ACKBT bit.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_receive_pre_end_setting(r_iic_drv_info_t * pRIic_Info)
{
    volatile uint8_t uctmp = 0U;

    /* Creates the register pointer for the specified RIIC channel. */
    volatile uint8_t * const    pcICMR3     = ICMR3_ADR(pRIic_Info->ChNo);

    /* Sets ICMR3.RDRFS bit.*/
    /* Sets ICMR3.ACKBT bit. */
    *pcICMR3 |= R_IIC_ICMR3_ACKWP_SET;              /* Modification of the ACKBT bit is enabled.                    */
    *pcICMR3 = R_IIC_ICMR3_PRE_END_SET;             /* Sets ICMR3.RDRFS bit and ACKBT bit, clears ICMR3.ACKWP bit.  */
                                                    /* A 1 is sent as the acknowledge bit (NACK transmission).      */
                                                    /* Modification of the ACKBT bit is disabled.                   */
    uctmp    = *pcICMR3;                            /* Reads ICMR3.                                                 */
}


/********************************************************************************************************************
* Outline      : Receive End Setting Processing
* Function Name: r_iic_drv_receive_end_setting
* Description  : Sets ICMR3.ACKBT bit and clears ICMR3.WAIT bit.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_receive_end_setting(r_iic_drv_info_t * pRIic_Info)
{
    volatile uint8_t uctmp = 0U;

    /* Creates the register pointer for the specified RIIC channel. */
    volatile uint8_t * const    pcICMR3     = ICMR3_ADR(pRIic_Info->ChNo);

    /* Sets ICMR3.ACKBT bit. */
    *pcICMR3 |= R_IIC_ICMR3_ACKWP_SET;              /* Modification of the ACKBT bit is enabled.                    */
    *pcICMR3 = R_IIC_ICMR3_END_SET;                 /* Sets ACKBT bit, clears ICMR3.ACKWP bit.                      */
                                                    /* A 1 is sent as the acknowledge bit (NACK transmission).      */
                                                    /* Modification of the ACKBT bit is disabled.                   */

    /* Clears ICMR3.WAIT bit. */
    *pcICMR3 &= R_IIC_ICMR3_WAIT_CLR;
    uctmp    = *pcICMR3;                            /* Reads ICMR3.                                                 */
}


/********************************************************************************************************************
* Outline      : Next Communication Processing
* Function Name: r_iic_drv_next_comm_setting
* Description  : If the internal mode is "R_IIC_MODE_READ" or "R_IIC_MODE_COMBINED",
*              : Clears ICMR3.RDRFS bit and ACKBT bit, And clears ICSR2.NACKF bit.
*              : Clears ICDR2.NACKF bit and STOP bit.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_next_comm_setting(r_iic_drv_info_t * pRIic_Info)
{
    volatile uint8_t uctmp = 0U;

    /* Creates the register pointer for the specified RIIC channel. */
    volatile uint8_t * const    pcICSR2     = ICSR2_ADR(pRIic_Info->ChNo);
    volatile uint8_t * const    pcICMR3     = ICMR3_ADR(pRIic_Info->ChNo);

    /* Checks the internal mode. */
    if ((R_IIC_MODE_READ == g_iic_InternalInfo[0].Mode) ||
        (R_IIC_MODE_COMBINED == g_iic_InternalInfo[0].Mode))
    {
        /* Clears ICMR3.RDRFS bit.*/
        /* Clears ICMR3.ACKBT bit. */
        *pcICMR3 |= R_IIC_ICMR3_ACKWP_SET;          /* Modification of the ACKBT bit is enabled.                    */
        *pcICMR3 = R_IIC_ICMR3_INIT;                /* Clears ICMR3.RDRFS bit, ACKBT bit and ACKWP bit.             */
                                                    /* A 0 is sent as the acknowledge bit (ACK transmission).       */
                                                    /* Modification of the ACKBT bit is disabled.                   */
    }
    
    /* Clears ICSR2.NACKF bit and STOP bit. */
    *pcICSR2 &= R_IIC_ICSR2_DISABLE;
    uctmp    = *pcICSR2;                            /* Reads ICSR2.                                                 */
}


/********************************************************************************************************************
* Outline      : Disable IIC Function Processing
* Function Name: r_iic_drv_iic_disable
* Description  : Disables RIIC communication and disables RIIC multi-function pin controller.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_iic_disable(r_iic_drv_info_t * pRIic_Info)
{
    volatile uint8_t uctmp = 0U;

    /* Creates the register pointer for the specified RIIC channel. */
    volatile uint8_t * const    pcICCR1     = ICCR1_ADR(pRIic_Info->ChNo);

    /* Disables RIIC multi-function pin controller after setting SCL and SDA to Hi-z by Reset. */
    /* Includes SFR read operation at the end of the following function. */
    r_iic_drv_mpc_disable(pRIic_Info);

    /* Resets RIIC registers. */
    *pcICCR1 |= R_IIC_ICCR1_RIIC_RESET;                         /* Sets ICCR1.IICRST bit.                           */
    uctmp    = *pcICCR1;                                        /* Reads ICCR1.                                     */
}


/********************************************************************************************************************
* Outline      : Enable IIC Function Processing
* Function Name: r_iic_drv_iic_enable
* Description  : Enables RIIC communication and enables RIIC multi-function pin controller.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_iic_enable(r_iic_drv_info_t * pRIic_Info)
{
    volatile uint8_t uctmp = 0U;
    
    /* Creates the register pointer for the specified RIIC channel. */
    volatile uint8_t * const    pcICCR1     = ICCR1_ADR(pRIic_Info->ChNo);

    /* Enables RIIC. */
    *pcICCR1 &= R_IIC_ICCR1_ENABLE;                             /* Clears ICCR1.IICRST bit.                         */
    uctmp    = *pcICCR1;                                        /* Reads ICCR1.                                     */

    /* Enables RIIC multi-function pin controller. */
    /* Includes SFR read operation at the end of the following function. */
    r_iic_drv_mpc_enable(pRIic_Info);
}


/********************************************************************************************************************
* Outline      : Disable RIIC Multi-function Pin Controller Processing
* Function Name: r_iic_drv_mpc_disable
* Description  : Disables RIIC multi-function pin controller.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_mpc_disable(r_iic_drv_info_t * pRIic_Info)
{
    volatile uint8_t uctmp = 0U;

    /* The upper layer software should set SCL snd SDA ports to input mode using PDR. */

    /* Channel number? */
    switch (pRIic_Info->ChNo)
    {
        case 0U:                                                /* Channel 0                                        */
#ifdef RIIC0_ENABLE
            R_IIC_PMR_SCL0      = 0U;                           /* Uses as a GPIO (Input port).                     */
            R_IIC_PMR_SDA0      = 0U;                           /* Uses as a GPIO (Input port).                     */
            R_IIC_PWPR_BOWI     = 0U;                           /* Enables the PFSWE bit writing.                   */
            R_IIC_PWPR_PFSWE    = 1U;                           /* Enables the PFS register writing.                */
            R_IIC_MPC_SCL0      = R_IIC_MPC_SCL0_INIT;          /* Pin function select to Hi-Z.                     */
            R_IIC_MPC_SDA0      = R_IIC_MPC_SDA0_INIT;          /* Pin function select to Hi-Z.                     */
            R_IIC_PWPR_PFSWE    = 0U;                           /* Disables the PFS register writing.               */
            R_IIC_PWPR_BOWI     = 1U;                           /* Disables the PFSWE bit writing.                  */
#endif /* #ifdef RIIC0_ENABLE */
        break;

        case 1U:                                                /* Channel 1                                        */
#ifdef RIIC1_ENABLE
            R_IIC_PMR_SCL1      = 0U;                           /* Uses as a GPIO (Input port).                     */
            R_IIC_PMR_SDA1      = 0U;                           /* Uses as a GPIO (Input port).                     */
            R_IIC_PWPR_BOWI     = 0U;                           /* Enables the PFSWE bit writing.                   */
            R_IIC_PWPR_PFSWE    = 1U;                           /* Enables the PFS register writing.                */
            R_IIC_MPC_SCL1      = R_IIC_MPC_SCL1_INIT;          /* Pin function select to Hi-Z.                     */
            R_IIC_MPC_SDA1      = R_IIC_MPC_SDA1_INIT;          /* Pin function select to Hi-Z.                     */
            R_IIC_PWPR_PFSWE    = 0U;                           /* Disables the PFS register writing.               */
            R_IIC_PWPR_BOWI     = 1U;                           /* Disables the PFSWE bit writing.                  */
#endif /* #ifdef RIIC1_ENABLE */
        break;

        default:
            /* Please add a channel as needed. */
        break;
    }

    uctmp    = R_IIC_PWPR_BOWI;                                 /* Reads PWPR.                                       */
}


/********************************************************************************************************************
* Outline      : Enable RIIC Multi-function Pin Controller Processing
* Function Name: r_iic_drv_mpc_enable
* Description  : Enables RIIC multi-function pin controller.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_mpc_enable(r_iic_drv_info_t * pRIic_Info)
{
    volatile uint8_t uctmp = 0U;

    /* The upper layer software should call "r_iic_drv_disable()." */
    /* The upper layer software should set SCL and SDA ports to input mode using PDR. */
    /* The upper layer software should perform RIIC reset or internal reset. */

    /* Channel number? */
    switch (pRIic_Info->ChNo)
    {
        case 0U:                                                /* Channel 0                                        */
#ifdef RIIC0_ENABLE
            R_IIC_PMR_SCL0      = 0U;                           /* Uses as a GPIO (Input port).                     */
            R_IIC_PMR_SDA0      = 0U;                           /* Uses as a GPIO (Input port).                     */

            /* Specifies the assignments of input/output signals for peripheral functions to the desired pins.
               But SCL and SDA are already in a high-impedance state.
               Because the upper layer software called "r_iic_drv_disable()". */

            R_IIC_PWPR_BOWI     = 0U;                           /* Enables the PFSWE bit writing.                   */
            R_IIC_PWPR_PFSWE    = 1U;                           /* Enables the PFS register writing.                */
            R_IIC_MPC_SCL0      = R_IIC_MPC_SCL0_ENABLE;        /* Pin function select to RIIC SCL pin.             */
            R_IIC_MPC_SDA0      = R_IIC_MPC_SDA0_ENABLE;        /* Pin function select to RIIC SDA pin.             */
            R_IIC_PWPR_PFSWE    = 0U;                           /* Disables the PFS register writing.               */
            R_IIC_PWPR_BOWI     = 1U;                           /* Disables the PFSWE bit writing.                  */
            R_IIC_PMR_SCL0      = 1U;                           /* Uses as RIIC (SCL).                              */
            R_IIC_PMR_SDA0      = 1U;                           /* Uses as RIIC (SDA).                              */
            uctmp               = R_IIC_PMR_SDA0;               /* Reads PMR.                                       */
#endif /* #ifdef RIIC0_ENABLE */
        break;

        case 1U:                                                /* Channel 1                                        */
#ifdef RIIC1_ENABLE
            R_IIC_PMR_SCL1      = 0U;                           /* Uses as a GPIO (Input port).                     */
            R_IIC_PMR_SDA1      = 0U;                           /* Uses as a GPIO (Input port).                     */

            /* Specifies the assignments of input/output signals for peripheral functions to the desired pins.
               But SCL and SDA are already in a high-impedance state.
               Because the upper layer software called "r_iic_drv_disable()". */

            R_IIC_PWPR_BOWI     = 0U;                           /* Enables the PFSWE bit writing.                   */
            R_IIC_PWPR_PFSWE    = 1U;                           /* Enables the PFS register writing.                */
            R_IIC_MPC_SCL1      = R_IIC_MPC_SCL1_ENABLE;        /* Pin function select to RIIC SCL pin.             */
            R_IIC_MPC_SDA1      = R_IIC_MPC_SDA1_ENABLE;        /* Pin function select to RIIC SDA pin.             */
            R_IIC_PWPR_PFSWE    = 0U;                           /* Disables the PFS register writing.               */
            R_IIC_PWPR_BOWI     = 1U;                           /* Disables the PFSWE bit writing.                  */
            R_IIC_PMR_SCL1      = 1U;                           /* Uses as RIIC (SCL).                              */
            R_IIC_PMR_SDA1      = 1U;                           /* Uses as RIIC (SDA).                              */
            uctmp               = R_IIC_PMR_SDA1;               /* Reads PMR.                                       */
#endif /* #ifdef RIIC1_ENABLE */
        break;

        default:
            /* Please add a channel as needed. */
        break;
    }
}


/********************************************************************************************************************
* Outline      : Generate SCL Processing
* Function Name: r_iic_drv_clk
* Description  : SCL clock is generated by setting ICCR1.CLO bit.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : R_IIC_TRUE                        ;   Successful
*              : R_IIC_FALSE                       ;   Not generate SCL clock
*********************************************************************************************************************/
bool r_iic_drv_clk(r_iic_drv_info_t * pRIic_Info)
{
    bool              ret   = R_IIC_FALSE;
    volatile uint16_t cnt   = 0U;
    volatile uint8_t  uctmp = 0U;

    /* Creates the register pointer for the specified RIIC channel. */
    volatile uint8_t * const    pcICCR1     = ICCR1_ADR(pRIic_Info->ChNo);
    volatile uint8_t * const    pcICFER     = ICFER_ADR(pRIic_Info->ChNo);

    /* Sets ICCR1.CLO bit after clearing ICFER.MALE bit to disable the arbitration lost detection. */
    /* Clears ICFER.MALE bit. */
    *pcICFER = R_IIC_ICFER_MALE_CLR;
    
    /* Sets ICCR1.CLO bit. Generates the SCL clock. */
    *pcICCR1 |= R_IIC_ICCR1_CLO_SET;

    uctmp    = *pcICCR1;                                        /* Reads ICCR1.                                     */

    /* Waits the SCL clock generation. */
    for (cnt = GEN_SCLCLK_WAIT; cnt > 0U; cnt--)
    {
        /* Generates SCL clock? */
        if (R_IIC_ICCR1_CLO_SET != (*pcICCR1 & R_IIC_ICCR1_CLO_SET))
        {
            ret = R_IIC_TRUE;                                   /* Successful                                       */
            break;
        }
    }
    
    /* Sets ICFER.MALE bit. */
    *pcICFER = R_IIC_ICFER_INIT;
    uctmp    = *pcICFER;                                        /* Reads ICFER.                                     */

    return ret;
}


/********************************************************************************************************************
* Outline      : Clears Interrupt Request Flag Processing
* Function Name: r_iic_drv_clear_ir_flag
* Description  : Clears interrupt request register.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_clear_ir_flag(r_iic_drv_info_t * pRIic_Info)
{
    uint8_t          internal_flag = 0U;                    /* Determines whether reinitialization is necessary.    */
    volatile uint8_t uctmp         = 0U;

    /* Creates the register pointer for the specified RIIC channel. */
    volatile uint8_t * const    pcICCR1     = ICCR1_ADR(pRIic_Info->ChNo);
    volatile uint8_t * const    pcICIER     = ICIER_ADR(pRIic_Info->ChNo);
    volatile uint8_t * const    pcIR_RXI    = IR_RXI_ADR(pRIic_Info->ChNo);
    volatile uint8_t * const    pcIR_TXI    = IR_TXI_ADR(pRIic_Info->ChNo);

    /* Checks IR flag. */
    /* If IR flag is set, clears IR flag. */
    if ((R_IIC_IR_SET == *pcIR_TXI) || (R_IIC_IR_SET == *pcIR_RXI))
    {
        /* Checks ICCR1.ICE bit. */
        if (R_IIC_ICCR1_INTER_RESET == (*pcICCR1 & R_IIC_ICCR1_INTER_RESET))    /* ICCR1.ICE = 1                    */
        {
            /* Sets internal flag. */
            internal_flag = 1U;
            
            /* Clears ICCR1.ICE bit and sets ICCR1.IICRST bit. */
            *pcICCR1 &= R_IIC_ICCR1_NOT_DRIVEN;
            if (R_IIC_ICCR1_NOT_DRIVEN != (*pcICCR1 & R_IIC_ICCR1_NOT_DRIVEN))
            {
                /* wait until the write operation is completed */
            }
        }

        /* Initializes ICIER register. */
        *pcICIER = R_IIC_ICIER_INIT;
        if (R_IIC_ICIER_INIT != *pcICIER)
        {
            /* wait until the write operation is completed */
        }

        /* Channel number? */
        switch (pRIic_Info->ChNo)
        {
            case 0U:
#ifdef RIIC0_ENABLE
                R_IIC_IR_RXI0 = R_IIC_IR_CLR;                   /* Clears RXI0 interrupt request register.          */
                R_IIC_IR_TXI0 = R_IIC_IR_CLR;                   /* Clears TXI0 interrupt request register.          */
                uctmp         = R_IIC_IR_TXI0;                  /* Reads IR.                                        */
#endif /* #ifdef RIIC0_ENABLE */
            break;
            
            case 1U:
#ifdef RIIC1_ENABLE
                R_IIC_IR_RXI1 = R_IIC_IR_CLR;                   /* Clears RXI1 interrupt request register.          */
                R_IIC_IR_TXI1 = R_IIC_IR_CLR;                   /* Clears TXI1 interrupt request register.          */
                uctmp         = R_IIC_IR_TXI1;                  /* Reads IR.                                        */
#endif /* #ifdef RIIC1_ENABLE */
            break;

            default:
                /* Please add a channel as needed. */
            break;
            
        }

        /* Checks the need of the reinitialization. */
        if (1U == internal_flag)
        {
            internal_flag = 0U;
            /* Reinitializes RIIC register because cleared ICCR1.ICE bit. */
            /* Includes SFR read operation at the end of the following function. */
            r_iic_drv_init_sfr(pRIic_Info);
        }
    }
}


/********************************************************************************************************************
* Outline      : Sets Master/Slave Mode bit Processing
* Function Name: r_iic_drv_set_mst_bit()
* Description  : Sets ICCR2.MST bit.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_set_mst_bit(r_iic_drv_info_t * pRIic_Info)
{
    volatile uint8_t uctmp = 0U;

    /* Creates the register pointer for the specified RIIC channel. */
    volatile uint8_t * const    pcICCR2     = ICCR2_ADR(pRIic_Info->ChNo);
    volatile uint8_t * const    pcICMR1     = ICMR1_ADR(pRIic_Info->ChNo);

    /* Sets ICMR1.MTWP bit. */
    *pcICMR1 |= R_IIC_ICMR1_MTWP_SET;
    
    /* Sets ICCR2.MST bit. */
    *pcICCR2 |= R_IIC_ICCR2_MST_SET;

    /* Clears ICMR1.MTWP bit. */
    *pcICMR1 &= R_IIC_ICMR1_MTWP_CLR;
    uctmp = *pcICMR1;                                           /* Reads ICMR1.                                     */
}


/********************************************************************************************************************
* Outline      : Checks ICSR2.NACKF bit Processing
* Function Name: r_iic_drv_check_nackf_bit()
* Description  : Checks the stop condition generation after the NACK detection.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : R_IIC_TRUE                        ;   NACKF bit 0: NACK is not detected.
*              : R_IIC_FALSE                       ;   NACKF bit 1: NACK is detected.
*********************************************************************************************************************/
bool r_iic_drv_check_nackf_bit(r_iic_drv_info_t * pRIic_Info)
{
    bool              ret   = R_IIC_TRUE;

    /* Creates the register pointer for the specified RIIC channel. */
    volatile uint8_t * const    pcICSR2     = ICSR2_ADR(pRIic_Info->ChNo);

    if (R_IIC_ICSR2_NACKF_SET == (*pcICSR2 & R_IIC_ICSR2_NACKF_SET))
    {
        ret = R_IIC_FALSE;
    }

    return ret;
}


/* End of File */
