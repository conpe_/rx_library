/********************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2013 Renesas Electronics Corporation. All rights reserved.
*********************************************************************************************************/
/********************************************************************************************************
* File Name    : r_iic_drv_int.h
* Version      : 1.12
* Description  : IIC driver RIIC interrupt module definitions
*********************************************************************************************************/
/********************************************************************************************************
* History      : DD.MM.YYYY Version  Description
*              : 29.03.2013 1.10     First Release
*              : 10.05.2013 1.10.R01 Corrected comments.
*              : 17.06.2013 1.11     Corrected comments.
*              : 24.06.2013 1.12     Corrected comments.
*********************************************************************************************************/
#ifndef __R_IIC_DRV_INT_H__
#define __R_IIC_DRV_INT_H__


/********************************************************************************************************
Macro definitions
*********************************************************************************************************/


/********************************************************************************************************
Typedef definitions
*********************************************************************************************************/


/********************************************************************************************************
Exported global variables
*********************************************************************************************************/
#ifdef CALL_ADVANCE_INTERRUPT
    #ifdef RIIC0_ENABLE
extern r_iic_drv_info_t         g_iic_Info_ch0;         /* Channel 0 IIC driver information             */
    #endif /* #ifdef RIIC0_ENABLE */
    #ifdef RIIC1_ENABLE
extern r_iic_drv_info_t         g_iic_Info_ch1;         /* Channel 1 IIC driver information             */
    #endif /* #ifdef RIIC1_ENABLE */
    #ifdef RIIC2_ENABLE
extern r_iic_drv_info_t         g_iic_Info_ch2;         /* Channel 2 IIC driver information             */
    #endif /* #ifdef RIIC2_ENABLE */
    #ifdef RIIC3_ENABLE
extern r_iic_drv_info_t         g_iic_Info_ch3;         /* Channel 3 IIC driver information             */
    #endif /* #ifdef RIIC3_ENABLE */
#endif  /*#ifdef CALL_ADVANCE_INTERRUPT*/


/********************************************************************************************************
Exported global functions (to be accessed by other files)
*********************************************************************************************************/
/* Driver interrupt functions */
void r_iic_drv_intRIIC0_EEI_isr(void);
void r_iic_drv_intRIIC0_TXI_isr(void);
void r_iic_drv_intRIIC0_RXI_isr(void);
void r_iic_drv_intRIIC0_TEI_isr(void);
void r_iic_drv_intRIIC1_EEI_isr(void);
void r_iic_drv_intRIIC1_TXI_isr(void);
void r_iic_drv_intRIIC1_RXI_isr(void);
void r_iic_drv_intRIIC1_TEI_isr(void);
void r_iic_drv_intRIIC2_EEI_isr(void);
void r_iic_drv_intRIIC2_TXI_isr(void);
void r_iic_drv_intRIIC2_RXI_isr(void);
void r_iic_drv_intRIIC2_TEI_isr(void);
void r_iic_drv_intRIIC3_EEI_isr(void);
void r_iic_drv_intRIIC3_TXI_isr(void);
void r_iic_drv_intRIIC3_RXI_isr(void);
void r_iic_drv_intRIIC3_TEI_isr(void);


#endif /* __R_IIC_DRV_INT_H__ */


/* End of File */
