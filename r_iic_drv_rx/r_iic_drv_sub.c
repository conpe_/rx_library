/********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2013 Renesas Electronics Corporation. All rights reserved.
*********************************************************************************************************************/
/********************************************************************************************************************
* System Name  : Serial Interface Single Master Control Software
* File Name    : r_iic_drv_sub.c
* Version      : 1.12
* Device       : R5F563NBDDFC (RX63N Group)
* Abstract     : SUB module
* Tool-Chain   : For RX63N Group
*              :  High-performance Embedded Workshop (Version 4.09.01.007),
*                 C/C++ Compiler Package for RX family (V.1.02 Release 01)
* OS           : not use
* H/W Platform : Renesas Starter Kit for RX63N
* Description  : IIC driver RIIC SUB module
* Limitation   : None
*********************************************************************************************************************/
/********************************************************************************************************************
* History      : DD.MM.YYYY Version  Description
*              : 29.03.2013 1.10     First Release
*              : 10.05.2013 1.10.R01 Corrected comments.
*              : 17.06.2013 1.11     Added r_iic_drv_next_comm_setting() for r_iic_drv_release().
*              : 24.06.2013 1.12     Added r_iic_drv_check_nackf_bit() for r_iic_drv_release().
*              : 24.06.2013 1.12     Changed return value of r_iic_drv_nack().
*              : 24.06.2013 1.12     Deleted r_iic_drv_stop_cond_wait().
*********************************************************************************************************************/


/********************************************************************************************************************
Includes <System Includes> , "Project Includes"
*********************************************************************************************************************/
#include <machine.h>
#include <stddef.h>                                             /* The basic macro and fixed number support         */

#include "r_iic_drv_api.h"
#include "r_iic_drv_sub.h"
#include "r_iic_drv_int.h"
#include "r_iic_drv_sfr.h"


/********************************************************************************************************************
Macro definitions
*********************************************************************************************************************/


/********************************************************************************************************************
Typedef definitions
*********************************************************************************************************************/


/********************************************************************************************************************
Exported global variables (to be accessed by other files)
*********************************************************************************************************************/


/********************************************************************************************************************
Private global variables and functions
*********************************************************************************************************************/
/* function table */
static const r_iic_drv_mtx_t gc_iic_mtx_tbl[R_IIC_STS_MAX][R_IIC_EV_MAX] =
{
    /* Uninitialized state */
    {
        { R_IIC_EV_INIT                 ,   r_iic_drv_init_driver           } , /* IIC Driver Initialization            */
        { R_IIC_EV_GEN_START_COND       ,   NULL                            } , /* Start Condition Generation           */
        { R_IIC_EV_INT_START            ,   NULL                            } , /* Start Condition Generation Interrupt */
        { R_IIC_EV_INT_ADD              ,   NULL                            } , /* Address Sending Interrupt            */
        { R_IIC_EV_INT_SEND             ,   NULL                            } , /* Data Sending Interrupt               */
        { R_IIC_EV_INT_RECEIVE          ,   NULL                            } , /* Data Receiving Interrupt             */
        { R_IIC_EV_INT_STOP             ,   NULL                            } , /* Stop Condition Generation Interrupt  */
        { R_IIC_EV_INT_AL               ,   NULL                            } , /* Arbitration-Lost Interrupt           */
        { R_IIC_EV_INT_NACK             ,   NULL                            } , /* No Acknowledge Interrupt             */
    },
    
    /* Idle state */
    {
        { R_IIC_EV_INIT                 ,   NULL                            } , /* IIC Driver Initialization            */
        { R_IIC_EV_GEN_START_COND       ,   r_iic_drv_generate_start_cond   } , /* Start Condition Generation           */
        { R_IIC_EV_INT_START            ,   NULL                            } , /* Start Condition Generation Interrupt */
        { R_IIC_EV_INT_ADD              ,   NULL                            } , /* Address Sending Interrupt            */
        { R_IIC_EV_INT_SEND             ,   NULL                            } , /* Data Sending Interrupt               */
        { R_IIC_EV_INT_RECEIVE          ,   NULL                            } , /* Data Receiving Interrupt             */
        { R_IIC_EV_INT_STOP             ,   NULL                            } , /* Stop Condition Generation Interrupt  */
        { R_IIC_EV_INT_AL               ,   NULL                            } , /* Arbitration-Lost Interrupt           */
        { R_IIC_EV_INT_NACK             ,   NULL                            } , /* No Acknowledge Interrupt             */
    },                                                                          
    
    /* Start condition generation completion wait state */
    {
        { R_IIC_EV_INIT                 ,   NULL                            } , /* IIC Driver Initialization            */
        { R_IIC_EV_GEN_START_COND       ,   NULL                            } , /* Start Condition Generation           */
        { R_IIC_EV_INT_START            ,   r_iic_drv_after_gen_start_cond  } , /* Start Condition Generation Interrupt */
        { R_IIC_EV_INT_ADD              ,   NULL                            } , /* Address Sending Interrupt            */
        { R_IIC_EV_INT_SEND             ,   NULL                            } , /* Data Sending Interrupt               */
        { R_IIC_EV_INT_RECEIVE          ,   NULL                            } , /* Data Receiving Interrupt             */
        { R_IIC_EV_INT_STOP             ,   NULL                            } , /* Stop Condition Generation Interrupt  */
        { R_IIC_EV_INT_AL               ,   r_iic_drv_arbitration_lost      } , /* Arbitration-Lost Interrupt           */
        { R_IIC_EV_INT_NACK             ,   r_iic_drv_nack                  } , /* No Acknowledge Interrupt             */
    },                                                                          
    
    /* Slave address [Write] transmission completion wait state */
    {
        { R_IIC_EV_INIT                 ,   NULL                            } , /* IIC Driver Initialization            */
        { R_IIC_EV_GEN_START_COND       ,   NULL                            } , /* Start Condition Generation           */
        { R_IIC_EV_INT_START            ,   NULL                            } , /* Start Condition Generation Interrupt */
        { R_IIC_EV_INT_ADD              ,   r_iic_drv_after_send_slvadr     } , /* Address Sending Interrupt            */
        { R_IIC_EV_INT_SEND             ,   NULL                            } , /* Data Sending Interrupt               */
        { R_IIC_EV_INT_RECEIVE          ,   NULL                            } , /* Data Receiving Interrupt             */
        { R_IIC_EV_INT_STOP             ,   NULL                            } , /* Stop Condition Generation Interrupt  */
        { R_IIC_EV_INT_AL               ,   r_iic_drv_arbitration_lost      } , /* Arbitration-Lost Interrupt           */
        { R_IIC_EV_INT_NACK             ,   r_iic_drv_nack                  } , /* No Acknowledge Interrupt             */
    },                                                                          
    
    /* Slave address [Read] transmission completion wait state */
    {
        { R_IIC_EV_INIT                 ,   NULL                            } , /* IIC Driver Initialization            */
        { R_IIC_EV_GEN_START_COND       ,   NULL                            } , /* Start Condition Generation           */
        { R_IIC_EV_INT_START            ,   NULL                            } , /* Start Condition Generation Interrupt */
        { R_IIC_EV_INT_ADD              ,   NULL                            } , /* Address Sending Interrupt            */
        { R_IIC_EV_INT_SEND             ,   NULL                            } , /* Data Sending Interrupt               */
        { R_IIC_EV_INT_RECEIVE          ,   r_iic_drv_after_send_slvadr     } , /* Data Receiving Interrupt             */
        { R_IIC_EV_INT_STOP             ,   NULL                            } , /* Stop Condition Generation Interrupt  */
        { R_IIC_EV_INT_AL               ,   r_iic_drv_arbitration_lost      } , /* Arbitration-Lost Interrupt           */
        { R_IIC_EV_INT_NACK             ,   r_iic_drv_nack                  } , /* No Acknowledge Interrupt             */
    },                                                                          
    
    /* Data transmission completion wait state */
    {
        { R_IIC_EV_INIT                 ,   NULL                            } , /* IIC Driver Initialization            */
        { R_IIC_EV_GEN_START_COND       ,   NULL                            } , /* Start Condition Generation           */
        { R_IIC_EV_INT_START            ,   NULL                            } , /* Start Condition Generation Interrupt */
        { R_IIC_EV_INT_ADD              ,   NULL                            } , /* Address Sending Interrupt            */
        { R_IIC_EV_INT_SEND             ,   r_iic_drv_write_data_sending    } , /* Data Sending Interrupt               */
        { R_IIC_EV_INT_RECEIVE          ,   NULL                            } , /* Data Receiving Interrupt             */
        { R_IIC_EV_INT_STOP             ,   NULL                            } , /* Stop Condition Generation Interrupt  */
        { R_IIC_EV_INT_AL               ,   r_iic_drv_arbitration_lost      } , /* Arbitration-Lost Interrupt           */
        { R_IIC_EV_INT_NACK             ,   r_iic_drv_nack                  } , /* No Acknowledge Interrupt             */
    },                                                                          
    
    /* Data reception completion wait state */
    {
        { R_IIC_EV_INIT                 ,   NULL                            } , /* IIC Driver Initialization            */
        { R_IIC_EV_GEN_START_COND       ,   NULL                            } , /* Start Condition Generation           */
        { R_IIC_EV_INT_START            ,   NULL                            } , /* Start Condition Generation Interrupt */
        { R_IIC_EV_INT_ADD              ,   NULL                            } , /* Address Sending Interrupt            */
        { R_IIC_EV_INT_SEND             ,   NULL                            } , /* Data Sending Interrupt               */
        { R_IIC_EV_INT_RECEIVE          ,   r_iic_drv_read_data_receiving   } , /* Data Receiving Interrupt             */
        { R_IIC_EV_INT_STOP             ,   NULL                            } , /* Stop Condition Generation Interrupt  */
        { R_IIC_EV_INT_AL               ,   r_iic_drv_arbitration_lost      } , /* Arbitration-Lost Interrupt           */
        { R_IIC_EV_INT_NACK             ,   r_iic_drv_nack                  } , /* No Acknowledge Interrupt             */
    },                                                                          
    
    /* Stop condition generation completion wait state */
    {
        { R_IIC_EV_INIT                 ,   NULL                            } , /* IIC Driver Initialization            */
        { R_IIC_EV_GEN_START_COND       ,   NULL                            } , /* Start Condition Generation           */
        { R_IIC_EV_INT_START            ,   NULL                            } , /* Start Condition Generation Interrupt */
        { R_IIC_EV_INT_ADD              ,   NULL                            } , /* Address Sending Interrupt            */
        { R_IIC_EV_INT_SEND             ,   NULL                            } , /* Data Sending Interrupt               */
        { R_IIC_EV_INT_RECEIVE          ,   NULL                            } , /* Data Receiving Interrupt             */
        { R_IIC_EV_INT_STOP             ,   r_iic_drv_release               } , /* Stop Condition Generation Interrupt  */
        { R_IIC_EV_INT_AL               ,   r_iic_drv_arbitration_lost      } , /* Arbitration-Lost Interrupt           */
        { R_IIC_EV_INT_NACK             ,   r_iic_drv_nack                  } , /* No Acknowledge Interrupt             */
    },                                                                          

};


/********************************************************************************************************************
* Outline      : IIC Protocol Execution Processing
* Function Name: r_iic_drv_func_table
* Description  : Executes the function which set in "pfunc".
* Arguments    : r_iic_drv_internal_event_t event  ;   Event
*              : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : Refer to the each calling function.
*********************************************************************************************************************/
error_t r_iic_drv_func_table(
    r_iic_drv_internal_event_t event,
    r_iic_drv_info_t * pRIic_Info
                                            )
{
    error_t ret = R_IIC_ERR_OTHER;
    error_t (* pFunc)( r_iic_drv_info_t *);
    r_iic_drv_internal_status_t     n_status;

    /* Acquires a now state. */
    n_status = g_iic_InternalInfo[pRIic_Info->ChNo].N_status;

    /* Checks the parameter. */
    if ((R_IIC_STS_MAX > n_status) && (R_IIC_EV_MAX > event))
    {
        /* Checks the appointed function. */
        if (NULL != gc_iic_mtx_tbl[n_status][event].proc)
        {
            /* Sets the function. */
            pFunc = gc_iic_mtx_tbl[n_status][event].proc;
            
            /* Event flag initialization. */
            g_iic_Event[pRIic_Info->ChNo] = R_IIC_EV_INIT;
            
            /* Calls the status transition function. */
            ret = (* pFunc)(pRIic_Info);
        }
        else
        {
            /* Appointed function error */
            ret = R_IIC_ERR_OTHER;
        }
    }
    else
    {
        /* Appointed function error */
        ret = R_IIC_ERR_OTHER;
    }

    return ret;
}


/********************************************************************************************************************
* Outline      : Initialize IIC Driver Processing
* Function Name: r_iic_drv_init_driver
* Description  : Initializes the IIC registers.
*              : Sets the internal status.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : R_IIC_IDLE                        ;   Successful operation, idle state
*********************************************************************************************************************/
error_t r_iic_drv_init_driver(r_iic_drv_info_t * pRIic_Info)
{
    error_t ret = R_IIC_IDLE;

    /* Initializes the IIC registers. */
    /* Includes SFR read operation at the end of the following function. */
    r_iic_drv_init_sfr(pRIic_Info);

    /* Sets the internal status. */
    r_iic_drv_set_internal_status(pRIic_Info, R_IIC_STS_IDLE);
    
    return ret;
}


/********************************************************************************************************************
* Outline      : Generate Start Condition Processing
* Function Name: r_iic_drv_generate_start_cond
* Description  : Checks bus busy.
*              : Sets the internal status. 
*              : Generates the start condition.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : R_IIC_COMMUNICATION               ;   Successful operation, communication state
*              :
*              : R_IIC_ERR_NON_REPLY               ;   None reply error
*********************************************************************************************************************/
error_t r_iic_drv_generate_start_cond(r_iic_drv_info_t * pRIic_Info)
{
    error_t ret = R_IIC_COMMUNICATION;

    /* Checks bus busy. */
    if (R_IIC_FALSE == r_iic_drv_check_bus_busy(pRIic_Info))
    {
        ret = R_IIC_ERR_NON_REPLY;                                  /* Bus busy                                     */
        return ret;
    }

    /* Sets the internal status. */
    r_iic_drv_set_internal_status(pRIic_Info, R_IIC_STS_ST_COND_WAIT);

    /* Generates the start condition. */
    r_iic_drv_start_cond_generate(pRIic_Info);

    return ret;
}


/********************************************************************************************************************
* Outline      : After Start Condition Generation Processing
* Function Name: r_iic_drv_after_gen_start_cond
* Description  : Performs one of the following processing according to the state.
*              :   Transmits the slave address for writing.
*              :   Transmits the slave address for reading.
*              :   Generates the stop condition.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : R_IIC_COMMUNICATION               ;   Successful operation, communication state
*              :
*              : R_IIC_ERR_OTHER                   ;   Other error
*********************************************************************************************************************/
error_t r_iic_drv_after_gen_start_cond(r_iic_drv_info_t * pRIic_Info)
{
    error_t ret = R_IIC_COMMUNICATION;

    /* IIC mode? */
    switch (g_iic_InternalInfo[pRIic_Info->ChNo].Mode)
    {
        case R_IIC_MODE_WRITE:
        case R_IIC_MODE_COMBINED:
        
            /* Checks the previous status. */
            switch (g_iic_InternalInfo[pRIic_Info->ChNo].B_status)
            {
                /* The previous status is idle. */
                case R_IIC_STS_IDLE:

                    /* Is the slave address pointer set? */
                    if (NULL == pRIic_Info->pSlvAdr)            /* Pattern 4 of Master Write                        */
                    {
                        /* Sets the internal status. */
                        r_iic_drv_set_internal_status(pRIic_Info, R_IIC_STS_SP_COND_WAIT);

                        /* Enables the IIC bus interrupt. */
                        r_iic_drv_int_icier_setting(pRIic_Info, R_IIC_ICIER_SPIE_NAKIE);

                        /* Generates the stop condition. */
                        r_iic_drv_stop_cond_generate(pRIic_Info);

                        return ret;
                    }

                    /* Sets a write code. */
                    *(pRIic_Info->pSlvAdr) &= ~R_CODE;

                    /* Sets the internal status. */
                    r_iic_drv_set_internal_status(pRIic_Info, R_IIC_STS_SEND_SLVADR_W_WAIT);

                    /* Enables the IIC bus interrupt. */
                    /* Transmit data empty interrupt request is enabled. */
                    r_iic_drv_int_icier_setting(pRIic_Info, R_IIC_ICIER_TEIE);

                    /* Transmits the slave address. */
                    r_iic_drv_set_sending_data(pRIic_Info, pRIic_Info->pSlvAdr);

                    return ret;

                break;

                /* Previous status is data transfer completion waiting status. */
                case R_IIC_STS_SEND_DATA_WAIT:

                    /* Sets a read code. */
                    *(pRIic_Info->pSlvAdr) |= R_CODE;

                    /* Sets the internal status. */
                    r_iic_drv_set_internal_status(pRIic_Info, R_IIC_STS_SEND_SLVADR_R_WAIT);

                    /* Enables the IIC bus interrupt. */
                    r_iic_drv_int_icier_setting(pRIic_Info, R_IIC_ICIER_RIE);

                    /* Transmits the slave address. */
                    r_iic_drv_set_sending_data(pRIic_Info, pRIic_Info->pSlvAdr);

                    return ret;
                    
                break;
                
                default:
                    /* None status. */
                    return R_IIC_ERR_OTHER;
                break;
            }
        break;
        
        case R_IIC_MODE_READ:

            /* Sets a read code. */
            *(pRIic_Info->pSlvAdr) |= R_CODE;

            /* Sets the internal status. */
            r_iic_drv_set_internal_status(pRIic_Info, R_IIC_STS_SEND_SLVADR_R_WAIT);

            /* Enables the IIC bus interrupt. */
            r_iic_drv_int_icier_setting(pRIic_Info, R_IIC_ICIER_RIE);

            /* Transmits the slave address. */
            r_iic_drv_set_sending_data(pRIic_Info, pRIic_Info->pSlvAdr);

            return ret;
            
        break;
        
        default:
            return R_IIC_ERR_OTHER;
        break;
    }
}


/********************************************************************************************************************
* Outline      : After Slave Address Transmission Processing
* Function Name: r_iic_drv_after_send_slvadr
* Description  : Performs one of the following processing according to the state.
*              :   Transmits a data.
*              :   Generates the stop condition.
*              :   Receives a data.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : R_IIC_COMMUNICATION               ;   Successful operation, communication state
*              :
*              : R_IIC_ERR_OTHER                   ;   Other error
*********************************************************************************************************************/
error_t r_iic_drv_after_send_slvadr(r_iic_drv_info_t * pRIic_Info)
{
    error_t          ret   = R_IIC_COMMUNICATION;
    volatile uint8_t uctmp = 0U;

    /* IIC mode? */
    switch (g_iic_InternalInfo[pRIic_Info->ChNo].Mode)
    {
        case R_IIC_MODE_WRITE:
            
            /* Are 1st data and 2nd data pointer set? */
            if ((NULL == pRIic_Info->pData1st) && (NULL == pRIic_Info->pData2nd))   /* Pattern 3 of Master Write    */
            {
                /* Sets the internal status. */
                r_iic_drv_set_internal_status(pRIic_Info, R_IIC_STS_SP_COND_WAIT);

                /* Enables the IIC bus interrupt. */
                r_iic_drv_int_icier_setting(pRIic_Info, R_IIC_ICIER_SPIE_NAKIE);

                /* Generates the stop condition. */
                r_iic_drv_stop_cond_generate(pRIic_Info);
                
                return ret;
            }
            
            /* Is 1st data pointer set? */
            if ((NULL != pRIic_Info->pData1st) && (NULL != pRIic_Info->pData2nd))   /* Pattern 1 of Master Write    */
            {
                /* Sets the internal status. */
                r_iic_drv_set_internal_status(pRIic_Info, R_IIC_STS_SEND_DATA_WAIT);

                /* Enables the IIC bus interrupt. */
                r_iic_drv_int_icier_setting(pRIic_Info, R_IIC_ICIER_TEIE);

                /* 1st data counter = 0?  */
                if (0U != pRIic_Info->Cnt1st)
                {
                    /* Transmits a 1st data. */
                    r_iic_drv_set_sending_data(pRIic_Info, pRIic_Info->pData1st);
                    
                    /* Decreases the 1st data counter. */
                    pRIic_Info->Cnt1st--;
                    
                    /* Increases the 1st data buffer pointer. */
                    pRIic_Info->pData1st++;
                }
                else
                {
                    return R_IIC_ERR_OTHER;
                }
            }
            else if ((NULL == pRIic_Info->pData1st) && (NULL != pRIic_Info->pData2nd))  /* Pattern 2 of Master Write*/
            {
                /* Sets the internal status. */
                r_iic_drv_set_internal_status(pRIic_Info, R_IIC_STS_SEND_DATA_WAIT);

                /* Enables the IIC bus interrupt. */
                r_iic_drv_int_icier_setting(pRIic_Info, R_IIC_ICIER_TEIE);

                /* 2nd data counter = 0?  */
                if (0U != pRIic_Info->Cnt2nd)
                {
                    /* Transmits a 2nd data. */
                    r_iic_drv_set_sending_data(pRIic_Info, pRIic_Info->pData2nd);
                    
                    /* Decreases the 2nd data counter. */
                    pRIic_Info->Cnt2nd--;

                    /* Increases the 2nd data buffer pointer. */
                    pRIic_Info->pData2nd++;
                    
                }
                else
                {
                    return R_IIC_ERR_OTHER;
                }
            }
            else
            {
                return R_IIC_ERR_OTHER;
            }
            
        break;

        case R_IIC_MODE_READ:

            /* Sets the internal status. */
            r_iic_drv_set_internal_status(pRIic_Info, R_IIC_STS_RECEIVE_DATA_WAIT);

            /* Enables the IIC bus interrupt. */
            r_iic_drv_int_icier_setting(pRIic_Info, R_IIC_ICIER_RIE);

            if (2U >= pRIic_Info->Cnt2nd)
            {
                /* The period between the ninth clock cycle and the first clock cycle is held low. */
                r_iic_drv_receive_wait_setting(pRIic_Info);
            }
            
            if (1U >= pRIic_Info->Cnt2nd)
            {
                /* Processing before receiving the "last byte - 1byte" in RX. */
                r_iic_drv_receive_pre_end_setting(pRIic_Info);
            }

            /* Performs dummy read. */
            uctmp = r_iic_drv_get_receiving_data(pRIic_Info);
        
        break;
        
        case R_IIC_MODE_COMBINED:

            /* Now status? */
            switch (g_iic_InternalInfo[pRIic_Info->ChNo].N_status)
            {
                case R_IIC_STS_SEND_SLVADR_W_WAIT:

                    /* Sets the internal status. */
                    r_iic_drv_set_internal_status(pRIic_Info, R_IIC_STS_SEND_DATA_WAIT);

                    /* Enables the IIC bus interrupt. */
                    r_iic_drv_int_icier_setting(pRIic_Info, R_IIC_ICIER_TEIE);

                    /* Transmits a 1st data. */
                    r_iic_drv_set_sending_data(pRIic_Info, pRIic_Info->pData1st);
                    
                    /* Decreases the 1st data counter. */
                    pRIic_Info->Cnt1st--;

                    /* Increases the 1st Data buffer pointer. */
                    pRIic_Info->pData1st++;
                    
                break;
                
                case R_IIC_STS_SEND_SLVADR_R_WAIT:
                
                    /* Sets the internal status. */
                    r_iic_drv_set_internal_status(pRIic_Info, R_IIC_STS_RECEIVE_DATA_WAIT);

                    /* Enables the IIC bus interrupt. */
                    r_iic_drv_int_icier_setting(pRIic_Info, R_IIC_ICIER_RIE);

                    if (2U >= pRIic_Info->Cnt2nd)
                    {
                        /* The period between the ninth clock cycle and the first clock cycle is held low. */
                        r_iic_drv_receive_wait_setting(pRIic_Info);
                    }
                    
                    if (1U >= pRIic_Info->Cnt2nd)
                    {
                        /* Processing before receiving the "last byte - 1byte" in RX. */
                        r_iic_drv_receive_pre_end_setting(pRIic_Info);
                    }

                    /* Performs dummy read. */
                    uctmp = r_iic_drv_get_receiving_data(pRIic_Info);

                break;
                
                default:
                    return R_IIC_ERR_OTHER;
                break;
            }
        break;
        
        default:
            return R_IIC_ERR_OTHER;
        break;
        
    }

    return ret;

}


/********************************************************************************************************************
* Outline      : After Transmitting Data Processing
* Function Name: r_iic_drv_write_data_sending
* Description  : Performs one of the following processing  according to the state.
*              :   Transmits a data.
*              :   Generates stop condition.
*              :   Generates restart condition.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : R_IIC_COMMUNICATION               ;   Successful operation, communication state
*              :
*              : R_IIC_ERR_NON_REPLY               ;   None replay error
*              : R_IIC_ERR_OTHER                   ;   Other error
*********************************************************************************************************************/
error_t r_iic_drv_write_data_sending(r_iic_drv_info_t * pRIic_Info)
{
    error_t ret = R_IIC_COMMUNICATION;

    /* IIC mode? */
    switch (g_iic_InternalInfo[pRIic_Info->ChNo].Mode)
    {
        case R_IIC_MODE_WRITE:

            /* Is 1st data pointer set? */
            if (NULL != pRIic_Info->pData1st)
            {
                /* 1st data counter = 0?  */
                if (0U != pRIic_Info->Cnt1st)                               /* Pattern 1 of Master Write            */
                {
                    /* Transmits a 1st data. */
                    r_iic_drv_set_sending_data(pRIic_Info, pRIic_Info->pData1st);
                    
                    /* Decreases the 1st data counter. */
                    pRIic_Info->Cnt1st--;

                    /* Increases the 1st data buffer pointer. */
                    pRIic_Info->pData1st++;

                    return ret;
                }
            }
            
            /* Is 2nd data pointer set? */
            if (NULL != pRIic_Info->pData2nd)
            {
                /* 2nd data counter = 0? */
                if (0U != pRIic_Info->Cnt2nd)                               /* Pattern 2 of Master Write            */
                {
                    /* Transmits a 2nd data. */
                    r_iic_drv_set_sending_data(pRIic_Info, pRIic_Info->pData2nd);
                    
                    /* Decreases the 2nd data counter. */
                    pRIic_Info->Cnt2nd--;

                    /* Increases the 2nd data buffer pointer. */
                    pRIic_Info->pData2nd++;

                    return ret;
                }
            }

            /* Sets the internal status. */
            r_iic_drv_set_internal_status(pRIic_Info, R_IIC_STS_SP_COND_WAIT);

            /* Enables the IIC bus interrupt. */
            r_iic_drv_int_icier_setting(pRIic_Info, R_IIC_ICIER_SPIE_NAKIE);

            /* Generates the stop condition. */
            r_iic_drv_stop_cond_generate(pRIic_Info);
            
            return ret;

        break;
        
        case R_IIC_MODE_COMBINED:

            /* Is 1st data pointer set? */
            if (0U != pRIic_Info->Cnt1st)
            {
                /* Transmits a 1st data. */
                r_iic_drv_set_sending_data(pRIic_Info, pRIic_Info->pData1st);
                
                /* Decreases the 1st data counter. */
                pRIic_Info->Cnt1st--;

                /* Increases the 1st data buffer pointer. */
                pRIic_Info->pData1st++;

                return ret;
            }

            /* Sets the internal status. */
            r_iic_drv_set_internal_status(pRIic_Info, R_IIC_STS_ST_COND_WAIT);

            /* Enables the IIC bus interrupt. */
            r_iic_drv_int_icier_setting(pRIic_Info, R_IIC_ICIER_STIE);

            /* Restarts the condition generation */
            r_iic_drv_re_start_cond_generate(pRIic_Info);

        break;
        
        default:
            return R_IIC_ERR_OTHER;
        break;
    }
    
    return ret;
}


/********************************************************************************************************************
* Outline      : After Receiving Data Processing
* Function Name: r_iic_drv_read_data_receiving
* Description  : Performs one of the following processing  according to the state.
*              :   Receives a data.
*              :   Generates stop condition.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : R_IIC_COMMUNICATION               ;   Successful operation, communication state
*********************************************************************************************************************/
error_t r_iic_drv_read_data_receiving(r_iic_drv_info_t * pRIic_Info)
{
    error_t          ret   = R_IIC_COMMUNICATION;
    volatile uint8_t uctmp = 0U;

    if (3U >= pRIic_Info->Cnt2nd)
    {
        /* The period between the ninth clock cycle and the first clock cycle is held low. */
        r_iic_drv_receive_wait_setting(pRIic_Info);
    }
    
    if (2U >= pRIic_Info->Cnt2nd)
    {
        /* Processing before receiving the "last byte - 1byte" in RX. */
        r_iic_drv_receive_pre_end_setting(pRIic_Info);
    }

    if (1U >= pRIic_Info->Cnt2nd)
    {
        /* Sets the internal status. */
        r_iic_drv_set_internal_status(pRIic_Info, R_IIC_STS_SP_COND_WAIT);

        /* Enables the IIC bus interrupt. */
        r_iic_drv_int_icier_setting(pRIic_Info, R_IIC_ICIER_SPIE_NAKIE);

        /* Generates the stop condition. */
        r_iic_drv_stop_cond_generate(pRIic_Info);
        
        /* Stores the received data. */
        *pRIic_Info->pData2nd = r_iic_drv_get_receiving_data(pRIic_Info);
        pRIic_Info->Cnt2nd--;                               /* Decreases the receiving data counter.                */
        pRIic_Info->pData2nd++;                             /* Increases the receiving data buffer pointer.         */

        /* Reception end setting */
        r_iic_drv_receive_end_setting(pRIic_Info);
    }
    else
    {
        /* Stores the received data. */
        *pRIic_Info->pData2nd = r_iic_drv_get_receiving_data(pRIic_Info);
        pRIic_Info->Cnt2nd--;                               /* Decreases the receiving data counter.                */
        pRIic_Info->pData2nd++;                             /* Increases the receiving data buffer pointer.         */
    }
    
    return ret;
}


/********************************************************************************************************************
* Outline      : After Generating Stop Condition Processing
* Function Name: r_iic_drv_release
* Description  : Checks bus busy.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : R_IIC_FINISH                      ;   Successful operation, finished communication and idle state
*              :
*              : R_IIC_ERR_OTHER                   ;   Other error
*********************************************************************************************************************/
error_t r_iic_drv_release(r_iic_drv_info_t * pRIic_Info)
{
    error_t ret     = R_IIC_FINISH;
    bool    boolret = R_IIC_FALSE;

    /* Checks the stop condition generation after the NACK detection. */
    if (R_IIC_FALSE == r_iic_drv_check_nackf_bit(pRIic_Info))
    {
        ret = R_IIC_NACK;
    }

    /* If the internal mode is "R_IIC_MODE_READ" or "R_IIC_MODE_COMBINED",
       Clears ICMR3.RDRFS bit and ACKBT bit. */
    /* Clears ICDR2.NACKF bit and STOP bit. */
    r_iic_drv_next_comm_setting(pRIic_Info);

    /* Waits from "bus busy" to "bus ready". */
    boolret = r_iic_drv_check_bus_busy(pRIic_Info);
    if (R_IIC_FALSE == boolret)
    {
        return R_IIC_ERR_OTHER;
    }

    return ret;
}


/********************************************************************************************************************
* Outline      : Arbitration Lost Error Processing
* Function Name: r_iic_drv_arbitration_lost
* Description  : Returns R_IIC_ERR_AL.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : R_IIC_ERR_AL                      ;   Arbitration lost error
*********************************************************************************************************************/
error_t r_iic_drv_arbitration_lost(r_iic_drv_info_t * pRIic_Info)
{
    error_t ret = R_IIC_ERR_AL;
    
    return ret;
}


/********************************************************************************************************************
* Outline      : Nack Error Processing
* Function Name: r_iic_drv_nack
* Description  : Generates the stop condition and returns R_IIC_NACK.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : R_IIC_NACK                        ;   Detected NACK, finished communication and idle state
*********************************************************************************************************************/
error_t r_iic_drv_nack(r_iic_drv_info_t * pRIic_Info)
{
    error_t          ret   = R_IIC_COMMUNICATION;
    volatile uint8_t uctmp = 0U;

    /* Sets the internal status. */
    r_iic_drv_set_internal_status(pRIic_Info, R_IIC_STS_SP_COND_WAIT);

    /* Enables the IIC bus interrupt. */
    r_iic_drv_int_icier_setting(pRIic_Info, R_IIC_ICIER_SPIE);

    /* Generates the stop condition. */
    r_iic_drv_stop_cond_generate(pRIic_Info);

    /* Checks the internal mode. */
    if ((R_IIC_MODE_READ == g_iic_InternalInfo[pRIic_Info->ChNo].Mode) ||
        (R_IIC_MODE_COMBINED == g_iic_InternalInfo[pRIic_Info->ChNo].Mode))
    {
        /* Dummy Read */
        /* Refer to the RX User's Manual. */
        uctmp = r_iic_drv_get_receiving_data(pRIic_Info);
    }
    
    return ret;
}


/********************************************************************************************************************
* Outline      : RAM Initialization Processing
* Function Name: r_iic_drv_ram_init
* Description  : Initializes RAM.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_ram_init(r_iic_drv_info_t * pRIic_Info)
{
    /* Clears the event flag. */
    g_iic_Event[pRIic_Info->ChNo]               = R_IIC_EV_INIT;

    /* Sets the mode to R_IIC_MODE_NONE. */
    g_iic_InternalInfo[pRIic_Info->ChNo].Mode   = R_IIC_MODE_NONE;
}


/********************************************************************************************************************
* Outline      : Update Internal Status Processing
* Function Name: r_iic_drv_set_internal_status
* Description  : Updates the now status and the previous status.
* Arguments    : r_iic_drv_info_t * pRIic_Info             ;   IIC Information
*              : r_iic_drv_internal_status_t new_status    ;   New status
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_set_internal_status(r_iic_drv_info_t * pRIic_Info, r_iic_drv_internal_status_t new_status)
{
    /* Sets the previous status. */
    g_iic_InternalInfo[pRIic_Info->ChNo].B_status = g_iic_InternalInfo[pRIic_Info->ChNo].N_status;

    /* Sets the now status. */
    g_iic_InternalInfo[pRIic_Info->ChNo].N_status = new_status;
}


/********************************************************************************************************************
* Outline      : Update Channel Status Processing
* Function Name: r_iic_drv_set_ch_status
* Description  : Updates the channel status.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_set_ch_status(r_iic_drv_info_t * pRIic_Info, error_t status)
{
    /* Sets the channel status. */
    g_iic_ChStatus[pRIic_Info->ChNo] = status;
    
    /* Sets the device status. */
    *(pRIic_Info->pDevStatus) = status;
}


/********************************************************************************************************************
* Outline      : Check Channel Status Processing (at initialization)
* Function Name: r_iic_drv_check_chstatus_init
* Description  : When calls the initialization function, checks the channel status.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : R_IIC_NO_INIT                     ;   Successful operation
*              : R_IIC_IDLE / R_IIC_FINISH / R_IIC_NACK    ;   Already idle state
*              : R_IIC_BUS_BUSY                    ;   Bus busy
*              : 
*              : R_IIC_ERR_AL                      ;   Arbitration lost error
*              : R_IIC_ERR_NON_REPLY               ;   None reply error
*              : R_IIC_ERR_SDA_LOW_HOLD            ;   SDA Low hold error
*              ; R_IIC_ERR_OTHER                   ;   Other error
*********************************************************************************************************************/
error_t r_iic_drv_check_chstatus_init(r_iic_drv_info_t * pRIic_Info)
{
    error_t ret = R_IIC_ERR_OTHER;

    /* Checks the channel status. */
    switch (g_iic_ChStatus[pRIic_Info->ChNo])
    {
        case R_IIC_NO_INIT:
            /* Checks the device status. */
            if ((R_IIC_IDLE   == *(pRIic_Info->pDevStatus)) ||
                (R_IIC_FINISH == *(pRIic_Info->pDevStatus)) ||
                (R_IIC_NACK   == *(pRIic_Info->pDevStatus)) ||
                (R_IIC_COMMUNICATION == *(pRIic_Info->pDevStatus)))
            {
                /* Another device performed a driver reset processing
                   when this device is in idle state or communicating state. */
                /* It is necessary to reinitialize. */
                /* Sets the return value to uninitialized state. */
                ret = R_IIC_NO_INIT;
            }
            else
            {
                /* The device is in uninitialized state or error state. */
                ret = *(pRIic_Info->pDevStatus);
            }
        break;

        case R_IIC_IDLE:
        case R_IIC_FINISH:
        case R_IIC_NACK:
        case R_IIC_COMMUNICATION:
            /* Checks the device status. */
            if (R_IIC_NO_INIT == *(pRIic_Info->pDevStatus))
            {
                /* Already, the channel has been initialized by another device's function call. */
                /* Sets the device status to idle state and the return value. */
                *(pRIic_Info->pDevStatus) = R_IIC_IDLE;
                ret = *(pRIic_Info->pDevStatus);
            }
            else if (R_IIC_COMMUNICATION == *(pRIic_Info->pDevStatus))
            {
                /* The channel is on communication. Sets the return value to bus busy state. */
                ret = R_IIC_BUS_BUSY;
            }
            else
            {
                /* The device is in idle state or error state. */
                ret = *(pRIic_Info->pDevStatus);
            }
        break;

        case R_IIC_ERR_AL:
        case R_IIC_ERR_NON_REPLY:
        case R_IIC_ERR_SDA_LOW_HOLD:
        case R_IIC_ERR_OTHER:
            /* The device is in error state. */
            /* Sets the return value to error state. */
            ret = g_iic_ChStatus[pRIic_Info->ChNo];
        break;

        default:
            /* Does nothing. */
        break;
    }

    return ret;
}


/********************************************************************************************************************
* Outline      : Check Channel Status Processing (at Start Function)
* Function Name: r_iic_drv_check_chstatus_start
* Description  : When calls the starting function, checks the channel status.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : R_IIC_IDLE / R_IIC_FINISH / R_IIC_NACK    ;   Successful operation
*              : R_IIC_NO_INIT                     ;   Uninitialized state
*              : R_IIC_BUS_BUSY                    ;   Bus busy
*              : 
*              : R_IIC_ERR_AL                      ;   Arbitration lost error
*              : R_IIC_ERR_NON_REPLY               ;   None reply error
*              : R_IIC_ERR_SDA_LOW_HOLD            ;   SDA Low hold error
*              ; R_IIC_ERR_OTHER                   ;   Other error
*********************************************************************************************************************/
error_t r_iic_drv_check_chstatus_start(r_iic_drv_info_t * pRIic_Info)
{
    error_t ret = R_IIC_ERR_OTHER;

    /* Checks the channel status. */
    switch (g_iic_ChStatus[pRIic_Info->ChNo])
    {
        case R_IIC_NO_INIT:
            if ((R_IIC_IDLE   == *(pRIic_Info->pDevStatus)) ||
                (R_IIC_FINISH == *(pRIic_Info->pDevStatus)) ||
                (R_IIC_NACK   == *(pRIic_Info->pDevStatus)) ||
                (R_IIC_COMMUNICATION == *(pRIic_Info->pDevStatus)))
            {
                /* Another device performed a driver reset processing
                   when this device is in idle state or communicating state. */
                /* It is necessary to reinitialize. */
                /* Sets the return value to uninitialized state. */
                ret = R_IIC_NO_INIT;
            }
            else
            {
                /* The device is in uninitialized state or error state. */
                ret = *(pRIic_Info->pDevStatus);
            }
        break;

        case R_IIC_IDLE:
        case R_IIC_FINISH:
        case R_IIC_NACK:
            /* Checks the device status. */
            if (R_IIC_COMMUNICATION == *(pRIic_Info->pDevStatus))
            {
                /* Another device performed a driver reset processing 
                   when this device is in idle state or communicating state. */
                /* It is necessary to reinitialize. */
                /* Sets the return value to uninitialized state. */
                ret = R_IIC_NO_INIT;
            }
            else
            {
                /* The device is in uninitialized state, idle state or error state. */
                ret = *(pRIic_Info->pDevStatus);
            }
        break;

        case R_IIC_COMMUNICATION:
            /* Checks the device status. */
            if ((R_IIC_IDLE   == *(pRIic_Info->pDevStatus)) ||
                (R_IIC_FINISH == *(pRIic_Info->pDevStatus)) ||
                (R_IIC_NACK   == *(pRIic_Info->pDevStatus)) ||
                (R_IIC_COMMUNICATION == *(pRIic_Info->pDevStatus)))
            {
                /* The device or another device is on communication. */
                /* Sets the return value to bus busy state. */
                ret = R_IIC_BUS_BUSY;
            }
            else
            {
                /* The device is in uninitialized state or error state. */
                ret = *(pRIic_Info->pDevStatus);
            }
        break;

        case R_IIC_ERR_AL:
        case R_IIC_ERR_NON_REPLY:
        case R_IIC_ERR_SDA_LOW_HOLD:
        case R_IIC_ERR_OTHER:
            /* The device is in error state. */
            /* Sets the return value to error state. */
            ret = g_iic_ChStatus[pRIic_Info->ChNo];
        break;

        default:
            /* Does nothing. */
        break;
    }
    
    return ret;
}


/********************************************************************************************************************
* Outline      : Check Channel Status Processing (at Advance Function)
* Function Name: r_iic_drv_check_chstatus_advance
* Description  : When calls the advance function, checks channel status.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : R_IIC_COMMUNICATION               ;   Successful operation
*              : R_IIC_NO_INIT                     ;   Uninitialized state
*              : R_IIC_IDLE / R_IIC_FINISH / R_IIC_NACK    ;   Not started state
*              : R_IIC_BUS_BUSY                    ;   Bus busy
*              : 
*              : R_IIC_ERR_AL                      ;   Arbitration lost error
*              : R_IIC_ERR_NON_REPLY               ;   None reply error
*              : R_IIC_ERR_SDA_LOW_HOLD            ;   SDA Low hold error
*              ; R_IIC_ERR_OTHER                   ;   Other error
*********************************************************************************************************************/
error_t r_iic_drv_check_chstatus_advance(r_iic_drv_info_t * pRIic_Info)
{
    error_t ret = R_IIC_ERR_OTHER;

    /* Checks the channel status. */
    switch (g_iic_ChStatus[pRIic_Info->ChNo])
    {
        case R_IIC_NO_INIT:
            if ((R_IIC_IDLE   == *(pRIic_Info->pDevStatus)) ||
                (R_IIC_FINISH == *(pRIic_Info->pDevStatus)) ||
                (R_IIC_NACK   == *(pRIic_Info->pDevStatus)) ||
                (R_IIC_COMMUNICATION == *(pRIic_Info->pDevStatus)))
            {
                /* Another device performed a driver reset processing
                   when this device is in idle state or communicating state. */
                /* It is necessary to reinitialize. */
                /* Sets the return value to uninitialized state. */
                ret = R_IIC_NO_INIT;
            }
            else
            {
                /* The device is in uninitialized state or error state. */
                ret = *(pRIic_Info->pDevStatus);
            }
        break;

        case R_IIC_IDLE:
        case R_IIC_FINISH:
        case R_IIC_NACK:
            /* Checks the device status. */
            if (R_IIC_COMMUNICATION == *(pRIic_Info->pDevStatus))
            {
                /* Another device performed a driver reset processing
                   when this device is in idle state or communicating state. */
                /* It is necessary to reinitialize. */
                /* Sets the return value to uninitialized state. */
                ret = R_IIC_NO_INIT;
            }
            else
            {
                /* The device is in uninitialized state, idle state or error state. */
                ret = *(pRIic_Info->pDevStatus);
            }
        break;

        case R_IIC_COMMUNICATION:
            /* Checks the device status. */
            if ((R_IIC_IDLE   == *(pRIic_Info->pDevStatus)) ||
                (R_IIC_FINISH == *(pRIic_Info->pDevStatus)) ||
                (R_IIC_NACK   == *(pRIic_Info->pDevStatus))    )
            {
                /* The device or another device is on communication. */
                /* Sets the return value to bus busy state. */
                ret = R_IIC_BUS_BUSY;
            }
            else
            {
                /* The device is in uninitialized state, communicating state or error state. */
                ret = *(pRIic_Info->pDevStatus);
            }
        break;

        case R_IIC_ERR_AL:
        case R_IIC_ERR_NON_REPLY:
        case R_IIC_ERR_SDA_LOW_HOLD:
        case R_IIC_ERR_OTHER:
            /* The device is in error state. */
            /* Sets the return value to error state. */
            ret = g_iic_ChStatus[pRIic_Info->ChNo];
        break;

        default:
            /* Does nothing. */
        break;
    }
    
    return ret;
}


/********************************************************************************************************************
* Outline      : Disable IIC Processing
* Function Name: r_iic_drv_disable
* Description  : Disables IIC.
*              : Disables interrupt.
*              : Initializes IIC bus interrupt enable register.
*              : Sets internal status.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_disable(r_iic_drv_info_t * pRIic_Info)
{
    /* Disables IIC. */
    r_iic_drv_iic_disable(pRIic_Info);

    /* Disables the interrupt. */
    r_iic_drv_int_disable(pRIic_Info);

    /* Initializes the IIC bus interrupt enable register. */
    r_iic_drv_int_icier_setting(pRIic_Info, R_IIC_ICIER_INIT);
    
    /* Sets the internal status. */
    r_iic_drv_set_internal_status(pRIic_Info, R_IIC_STS_IDLE);
}


/********************************************************************************************************************
* Outline      : Enable IIC Processing
* Function Name: r_iic_drv_enable
* Description  : Enables IIC bus interrupt enable register.
*              : Enables interrupt.
*              : Enables IIC.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : None
*********************************************************************************************************************/
void r_iic_drv_enable(r_iic_drv_info_t * pRIic_Info)
{
    /* Enables IIC bus interrupt enable register. */
    r_iic_drv_int_icier_setting(pRIic_Info, R_IIC_ICIER_STIE);

    /* Enables the interrupt. */
    r_iic_drv_int_enable(pRIic_Info);

    /* Enables IIC. */
    r_iic_drv_iic_enable(pRIic_Info);
}


/********************************************************************************************************************
* Outline      : Lock API Flag Processing
* Function Name: r_iic_drv_lock_api
* Description  : Confirms whether API is active and sets the API flag if it is not active.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : R_IIC_TRUE                        ;   This has locked.
*              : R_IIC_FALSE                       ;   Another task already has locked.
*********************************************************************************************************************/
bool r_iic_drv_lock_api(r_iic_drv_info_t * pRIic_Info)
{
    bool    ret       = R_IIC_FALSE;
    int32_t is_locked = R_IIC_TRUE;                             /* Variable used in trying to acquire lock.         */
    
    xchg(&is_locked, &g_iic_Api[pRIic_Info->ChNo]);
    if ((int32_t)R_IIC_FALSE == is_locked)
    {
        ret = R_IIC_TRUE;                                       /* Locks                                            */
    }
    else
    {
        /* Lock was not obtained, another task already has it. */
    }
    return ret;
}


/********************************************************************************************************************
* Outline      : Unlock API Flag Processing
* Function Name: r_iic_drv_unlock_api
* Description  : Clears the API flag.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : R_IIC_TRUE                        ;   Unclocks.
*********************************************************************************************************************/
bool r_iic_drv_unlock_api(r_iic_drv_info_t * pRIic_Info)
{
    int32_t ret = R_IIC_TRUE;

    /* Clears the API flag. */
    g_iic_Api[pRIic_Info->ChNo] = (int32_t)R_IIC_FALSE;
    
    return ret;
    
}


/********************************************************************************************************************
* Outline      : Check Event Flag Processing (at Stop Condition Generation)
* Function Name: r_iic_drv_check_stop_event
* Description  : Checks the event flag.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : R_IIC_TRUE                        ;   Sets the event flag to R_IIC_EV_INT_STOP.
*              : R_IIC_FALSE                       ;   Does not set the event flag to R_IIC_EV_INT_STOP.
*********************************************************************************************************************/
bool r_iic_drv_check_stop_event(r_iic_drv_info_t * pRIic_Info)
{
    
    if (R_IIC_EV_INT_STOP == g_iic_Event[pRIic_Info->ChNo])
    {
        return R_IIC_TRUE;
    }
    else
    {
        return R_IIC_FALSE;
    }
}


/* End of File */
