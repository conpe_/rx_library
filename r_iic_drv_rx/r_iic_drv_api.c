/********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2013 Renesas Electronics Corporation. All rights reserved.
*********************************************************************************************************************/
/********************************************************************************************************************
* System Name  : Serial Interface Single Master Control Software
* File Name    : r_iic_drv_api.c
* Version      : 1.12
* Device       : R5F563NBDDFC (RX63N Group)
* Abstract     : User I/F file
* Tool-Chain   : For RX63N Group
*              :  High-performance Embedded Workshop (Version 4.09.01.007),
*                 C/C++ Compiler Package for RX family (V.1.02 Release 01)
* OS           : not use
* H/W Platform : Renesas Starter Kit for RX63N
* Description  : IIC driver RIIC I/F module
* Limitation   : None
*********************************************************************************************************************/
/********************************************************************************************************************
* History      : DD.MM.YYYY Version  Description
*              : 29.03.2013 1.10     First Release
*              : 10.05.2013 1.10.R01 Corrected comments.
*              : 17.06.2013 1.11     Corrected comments.
*              : 24.06.2013.1.12     Changed NACK processing of R_IIC_Drv_Advance().
*********************************************************************************************************************/


/********************************************************************************************************************
Includes <System Includes> , "Project Includes"
*********************************************************************************************************************/
#include <stddef.h>                                             /* The basic macro and fixed number support         */

#include "r_iic_drv_api.h"
#include "r_iic_drv_sub.h"
#include "r_iic_drv_int.h"
#include "r_iic_drv_sfr.h"


/********************************************************************************************************************
Macro definitions
*********************************************************************************************************************/


/********************************************************************************************************************
Typedef definitions
*********************************************************************************************************************/


/********************************************************************************************************************
Exported global variables (to be accessed by other files)
*********************************************************************************************************************/
volatile error_t                         g_iic_ChStatus[MAX_IIC_CH_NUM];     /* Channel status                      */
volatile r_iic_drv_internal_event_t      g_iic_Event[MAX_IIC_CH_NUM];        /* Event flag                          */
volatile r_iic_drv_internal_info_t       g_iic_InternalInfo[MAX_IIC_CH_NUM]; /* Internal status management          */
volatile uint32_t                        g_iic_ReplyCnt[MAX_IIC_CH_NUM];     /* Counter of calling
                                                                                "R_IIC_Drv_Advance()" upper limit   */
int32_t                                  g_iic_Api[MAX_IIC_CH_NUM];          /* API flag                            */

/********************************************************************************************************************
Private global variables and functions
*********************************************************************************************************************/
/* Callback function */
static r_iic_callback R_IIC_Drv_CallBackFunc[MAX_IIC_CH_NUM];


/********************************************************************************************************************
* Outline      : Initialization processing
* Function Name: R_IIC_Drv_Init
* Description  : Initializes the RIIC driver.
*              : Initializes the SFR for RIIC control.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : R_IIC_IDLE                        ;   Successful operation
*              : R_IIC_FINISH / R_IIC_NACK         ;   Already idle state
*              : R_IIC_LOCK_FUNC                   ;   Another task is handling API function
*              : R_IIC_BUS_BUSY                    ;   Bus busy
*              : 
*              : R_IIC_ERR_PARAM                   ;   Parameter error
*              : R_IIC_ERR_AL                      ;   Arbitration lost error
*              : R_IIC_ERR_NON_REPLY               ;   None reply error
*              : R_IIC_ERR_SDA_LOW_HOLD            ;   SDA Low hold error
*              ; R_IIC_ERR_OTHER                   ;   Other error
*********************************************************************************************************************/
error_t R_IIC_Drv_Init(r_iic_drv_info_t * pRIic_Info)
{
    bool    chk = R_IIC_FALSE;
    error_t ret = R_IIC_ERR_OTHER;                              /* Sets R_IIC_ERR_OTHER as initialization of ret.   */

    /* Parameter check */
    if ((NULL == pRIic_Info) ||
        (NULL == pRIic_Info->pDevStatus))
    {
        return R_IIC_ERR_PARAM;
    }

    /* Checks the API flag. */
    chk = r_iic_drv_lock_api(pRIic_Info);

    if (R_IIC_FALSE == chk)
    {
        /* Lock has already been acquired by another task. Needs to try again later.   */
        return R_IIC_LOCK_FUNC;
    }

    /* Calls the API function. */
    ret = r_iic_drv_init(pRIic_Info);

    /* Clears the API flag. */
    r_iic_drv_unlock_api(pRIic_Info);

    return ret;

}

/*------------------------------------------------------------------------------------------------------------------*/
error_t r_iic_drv_init(r_iic_drv_info_t * pRIic_Info)
{
    error_t ret = R_IIC_ERR_OTHER;                              /* Sets R_IIC_ERR_OTHER as initialization of ret.   */

    /* Checks the channel status. */
    ret = r_iic_drv_check_chstatus_init(pRIic_Info);
    if (R_IIC_NO_INIT != ret)
    {
        return ret;
    }

    /* Updates the channel status. */
    r_iic_drv_set_ch_status(pRIic_Info, R_IIC_IDLE);

    /* Ports open setting */
    /* Includes SFR read operation at the end of the following function. */
    r_iic_drv_io_open(pRIic_Info);

    /* Disables RIIC multi-function pin controller after setting SCL and SDA to Hi-z. */
    /* Includes SFR read operation at the end of the following function. */
    r_iic_drv_mpc_disable(pRIic_Info);

    /* Enables the IIC peripheral registers. */
    /* Includes SFR read operation at the end of the following function. */
    r_iic_drv_cancel_mstp(pRIic_Info);

    /* RAM initialization */
    r_iic_drv_ram_init(pRIic_Info);
    
    /* Sets the internal status. (Internal information initialization) */
    r_iic_drv_set_internal_status(pRIic_Info, R_IIC_STS_NO_INIT);

    /* Initializes the IIC driver. */
    ret = r_iic_drv_func_table(R_IIC_EV_INIT, pRIic_Info);
    if (R_IIC_IDLE != ret)
    {
        /* Updates the channel status. */
        r_iic_drv_set_ch_status(pRIic_Info, R_IIC_ERR_OTHER);

        return R_IIC_ERR_OTHER;
    }

    /* Disables IIC. */
    r_iic_drv_disable(pRIic_Info);

    return R_IIC_IDLE;
}


/********************************************************************************************************************
* Outline      : Master transmission start processing
* Function Name: R_IIC_Drv_MasterTx
* Description  : Generates the start condition.
*              : Starts the master transmission.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : R_IIC_COMMUNICATION               ;   Successful operation
*              : R_IIC_NO_INIT                     ;   Uninitialized state
*              : R_IIC_LOCK_FUNC                   ;   Another task is handling API function
*              : R_IIC_BUS_BUSY                    ;   Bus busy
*              : 
*              : R_IIC_ERR_PARAM                   ;   Parameter error
*              : R_IIC_ERR_AL                      ;   Arbitration lost error
*              : R_IIC_ERR_NON_REPLY               ;   None reply error
*              : R_IIC_ERR_SDA_LOW_HOLD            ;   SDA Low hold error
*              ; R_IIC_ERR_OTHER                   ;   Other error
*********************************************************************************************************************/
error_t R_IIC_Drv_MasterTx(r_iic_drv_info_t * pRIic_Info)
{
    bool    chk  = R_IIC_FALSE;
    error_t ret  = R_IIC_ERR_OTHER;                             /* Sets R_IIC_ERR_OTHER as initialization of ret.   */

    /* Parameter check */
    if ((NULL == pRIic_Info) ||
        (NULL == pRIic_Info->pDevStatus))
    {
        return R_IIC_ERR_PARAM;
    }
    
    /* Checks the API flag. */
    chk = r_iic_drv_lock_api(pRIic_Info);

    if (R_IIC_FALSE == chk)
    {
        /* Lock has already been acquired by another task. Needs to try again later.   */
        return R_IIC_LOCK_FUNC;
    }

    /* Calls the API function. */
    ret = r_iic_drv_mastertx(pRIic_Info);

    /* Clears the API flag. */
    r_iic_drv_unlock_api(pRIic_Info);

    return ret;

}

/*------------------------------------------------------------------------------------------------------------------*/
error_t r_iic_drv_mastertx(r_iic_drv_info_t * pRIic_Info)
{
    error_t ret = R_IIC_ERR_OTHER;                              /* Sets R_IIC_ERR_OTHER as initialization of ret.   */

    /* Checks the channel status. */
    ret = r_iic_drv_check_chstatus_start(pRIic_Info);
    if ((R_IIC_IDLE != ret) &&
        (R_IIC_FINISH != ret) &&
        (R_IIC_NACK != ret)         )
    {
        return ret;
    }

    /* Updates the channel status. */
    r_iic_drv_set_ch_status(pRIic_Info, R_IIC_COMMUNICATION);

    if (NULL != pRIic_Info->CallBackFunc)
    {
        /* Sets the callback function. */
        R_IIC_Drv_CallBackFunc[pRIic_Info->ChNo] = pRIic_Info->CallBackFunc;
    }

    /* RAM initialization */
    r_iic_drv_ram_init(pRIic_Info);

    /* Counter initialization for calling "R_IIC_Drv_Advance()" upper limit error */
    g_iic_ReplyCnt[pRIic_Info->ChNo] = REPLY_CNT;

    /* Sets the internal mode. */
    g_iic_InternalInfo[pRIic_Info->ChNo].Mode = R_IIC_MODE_WRITE;

    /* Enables IIC. */
    r_iic_drv_enable(pRIic_Info);

    /* Generates the start condition.  */
    ret = r_iic_drv_func_table(R_IIC_EV_GEN_START_COND, pRIic_Info);
    if (R_IIC_COMMUNICATION != ret)
    {
        /* Disables IIC. */
        r_iic_drv_disable(pRIic_Info);

        /* Updates the channel status. */
        r_iic_drv_set_ch_status(pRIic_Info, R_IIC_ERR_NON_REPLY);

        return R_IIC_ERR_NON_REPLY;
    }

    return R_IIC_COMMUNICATION;
}

/********************************************************************************************************************
* Outline      : Master reception start processing
* Function Name: R_IIC_Drv_MasterRx
* Description  : Generates the start condition.
*              : Starts the master reception.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : R_IIC_COMMUNICATION               ;   Successful operation
*              : R_IIC_NO_INIT                     ;   Uninitialized state
*              : R_IIC_LOCK_FUNC                   ;   Another task is handling API function
*              : R_IIC_BUS_BUSY                    ;   Bus busy
*              : 
*              : R_IIC_ERR_PARAM                   ;   Parameter error
*              : R_IIC_ERR_AL                      ;   Arbitration lost error
*              : R_IIC_ERR_NON_REPLY               ;   None reply error
*              : R_IIC_ERR_SDA_LOW_HOLD            ;   SDA Low hold error
*              ; R_IIC_ERR_OTHER                   ;   Other error
*********************************************************************************************************************/
error_t R_IIC_Drv_MasterRx(r_iic_drv_info_t * pRIic_Info)
{
    bool    chk  = R_IIC_FALSE;
    error_t ret  = R_IIC_ERR_OTHER;                             /* Sets R_IIC_ERR_OTHER as initialization of ret.   */

    /* Parameter check */
    if ((NULL == pRIic_Info) ||
        (NULL == pRIic_Info->pDevStatus))
    {
        return R_IIC_ERR_PARAM;
    }

    if ((NULL == pRIic_Info->pSlvAdr) ||
        (NULL == pRIic_Info->pData2nd) ||
        (0 == pRIic_Info->Cnt2nd))
    {
        return R_IIC_ERR_PARAM;
    }

    /* Checks the API flag. */
    chk = r_iic_drv_lock_api(pRIic_Info);

    if (R_IIC_FALSE == chk)
    {
        /* Lock has already been acquired by another task. Needs to try again later.   */
        return R_IIC_LOCK_FUNC;
    }

    /* Calls the API function. */
    ret = r_iic_drv_masterrx(pRIic_Info);

    /* Clears the API flag. */
    r_iic_drv_unlock_api(pRIic_Info);

    return ret;

}

/*------------------------------------------------------------------------------------------------------------------*/
error_t r_iic_drv_masterrx(r_iic_drv_info_t * pRIic_Info)
{
    error_t ret = R_IIC_ERR_OTHER;                              /* Sets R_IIC_ERR_OTHER as initialization of ret.   */

    /* Checks the channel status. */
    ret = r_iic_drv_check_chstatus_start(pRIic_Info);
    if ((R_IIC_IDLE != ret) &&
        (R_IIC_FINISH != ret) &&
        (R_IIC_NACK != ret)         )
    {
        return ret;
    }

    /* Updates the channel status. */
    r_iic_drv_set_ch_status(pRIic_Info, R_IIC_COMMUNICATION);

    if (NULL != pRIic_Info->CallBackFunc)
    {
        /* Sets the callback function. */
        R_IIC_Drv_CallBackFunc[pRIic_Info->ChNo] = pRIic_Info->CallBackFunc;
    }

    /* RAM initialization */
    r_iic_drv_ram_init(pRIic_Info);

    /* Counter initialization for calling "R_IIC_Drv_Advance()" upper limit error */
    g_iic_ReplyCnt[pRIic_Info->ChNo] = REPLY_CNT;
    
    /* Sets the internal mode. */
    g_iic_InternalInfo[pRIic_Info->ChNo].Mode = R_IIC_MODE_READ;

    /* Enables IIC. */
    r_iic_drv_enable(pRIic_Info);

    /* Generates the start condition. */
    ret = r_iic_drv_func_table(R_IIC_EV_GEN_START_COND, pRIic_Info);
    if (R_IIC_COMMUNICATION != ret)
    {
        /* Disables IIC. */
        r_iic_drv_disable(pRIic_Info);

        /* Updates the channel status. */
        r_iic_drv_set_ch_status(pRIic_Info, R_IIC_ERR_NON_REPLY);

        return R_IIC_ERR_NON_REPLY;
    }
    
    return R_IIC_COMMUNICATION;
}


/********************************************************************************************************************
* Outline      : Master combination start processing
* Function Name: R_IIC_Drv_MasterTRx
* Description  : Generates the start condition.
*              : Starts the master combination.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : R_IIC_COMMUNICATION               ;   Successful operation
*              : R_IIC_NO_INIT                     ;   Uninitialized state
*              : R_IIC_LOCK_FUNC                   ;   Another task is handling API function
*              : R_IIC_BUS_BUSY                    ;   Bus busy
*              : 
*              : R_IIC_ERR_PARAM                   ;   Parameter error
*              : R_IIC_ERR_AL                      ;   Arbitration lost error
*              : R_IIC_ERR_NON_REPLY               ;   None reply error
*              : R_IIC_ERR_SDA_LOW_HOLD            ;   SDA Low hold error
*              ; R_IIC_ERR_OTHER                   ;   Other error
*********************************************************************************************************************/
error_t R_IIC_Drv_MasterTRx(r_iic_drv_info_t * pRIic_Info)
{
    bool    chk  = R_IIC_FALSE;
    error_t ret  = R_IIC_ERR_OTHER;                             /* Sets R_IIC_ERR_OTHER as initialization of ret.   */

    /* Parameter check */
    if ((NULL == pRIic_Info) ||
        (NULL == pRIic_Info->pDevStatus))
    {
        return R_IIC_ERR_PARAM;
    }

    if ((NULL == pRIic_Info->pSlvAdr) ||
        (NULL == pRIic_Info->pData1st) ||
        (NULL == pRIic_Info->pData2nd))
    {
        return R_IIC_ERR_PARAM;
    }

    /* Checks the API flag. */
    chk = r_iic_drv_lock_api(pRIic_Info);

    if (R_IIC_FALSE == chk)
    {
        /* Lock has already been acquired by another task. Needs to try again later.   */
        return R_IIC_LOCK_FUNC;
    }

    /* Calls the API function. */
    ret = r_iic_drv_mastertrx(pRIic_Info);

    /* Clears the API flag. */
    r_iic_drv_unlock_api(pRIic_Info);
    
    return ret;

}

/*------------------------------------------------------------------------------------------------------------------*/
error_t r_iic_drv_mastertrx(r_iic_drv_info_t * pRIic_Info)
{
    error_t ret = R_IIC_ERR_OTHER;                              /* Sets R_IIC_ERR_OTHER as initialization of ret.   */
    
    /* Checks the channel status. */
    ret = r_iic_drv_check_chstatus_start(pRIic_Info);
    if ((R_IIC_IDLE != ret) &&
        (R_IIC_FINISH != ret) &&
        (R_IIC_NACK != ret)         )
    {
        return ret;
    }

    /* Updates the channel status. */
    r_iic_drv_set_ch_status(pRIic_Info, R_IIC_COMMUNICATION);

    if (NULL != pRIic_Info->CallBackFunc)
    {
        /* Sets the callback function. */
        R_IIC_Drv_CallBackFunc[pRIic_Info->ChNo] = pRIic_Info->CallBackFunc;
    }

    /* RAM initialization */
    r_iic_drv_ram_init(pRIic_Info);

    /* Counter initialization for calling "R_IIC_Drv_Advance()" upper limit error */
    g_iic_ReplyCnt[pRIic_Info->ChNo] = REPLY_CNT;
    
    /* Sets the internal mode. */
    g_iic_InternalInfo[pRIic_Info->ChNo].Mode = R_IIC_MODE_COMBINED;

    /* Enables IIC. */
    r_iic_drv_enable(pRIic_Info);

    /* Generates the start condition. */
    ret = r_iic_drv_func_table(R_IIC_EV_GEN_START_COND, pRIic_Info);
    if (R_IIC_COMMUNICATION != ret)
    {
        /* Disables IIC. */
        r_iic_drv_disable(pRIic_Info);

        /* Updates the channel status. */
        r_iic_drv_set_ch_status(pRIic_Info, R_IIC_ERR_NON_REPLY);

        return R_IIC_ERR_NON_REPLY;
    }

    return R_IIC_COMMUNICATION;
}


/********************************************************************************************************************
* Outline      : Advance processing
* Function Name: R_IIC_Drv_Advance
* Description  : Advances the IIC communication.
*              : The return value shows the communication result. Refer to the return value.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
* Return Value : R_IIC_COMMUNICATION               ;   Successful operation ,not finished communication
*              : R_IIC_FINISH                      ;   Successful operation ,finished communication
*              : R_IIC_NACK                        ;   Detected NACK and finished communication
*              : R_IIC_NO_INIT                     ;   Uninitialized state
*              : R_IIC_IDLE                        ;   Not started state
*              : R_IIC_LOCK_FUNC                   ;   Another task is handling API function
*              : R_IIC_BUS_BUSY                    ;   Bus busy
*              : 
*              : R_IIC_ERR_PARAM                   ;   Parameter error
*              : R_IIC_ERR_AL                      ;   Arbitration lost error
*              : R_IIC_ERR_NON_REPLY               ;   None reply error
*              : R_IIC_ERR_SDA_LOW_HOLD            ;   SDA Low hold error
*              ; R_IIC_ERR_OTHER                   ;   Other error
*********************************************************************************************************************/
error_t R_IIC_Drv_Advance(r_iic_drv_info_t * pRIic_Info)
{
    bool    chk = R_IIC_FALSE;
    error_t ret = R_IIC_ERR_OTHER;                              /* Sets R_IIC_ERR_OTHER as initialization of ret.   */

    /* Parameter check */
    if ((NULL == pRIic_Info) ||
        (NULL == pRIic_Info->pDevStatus))
    {
        return R_IIC_ERR_PARAM;
    }
    
    /* Checks the API flag. */
    chk = r_iic_drv_lock_api(pRIic_Info);

    if (R_IIC_FALSE == chk)
    {
        /* Lock has already been acquired by another task. Needs to try again later.   */
        return R_IIC_LOCK_FUNC;
    }

    /* Calls the API function. */
    ret = r_iic_drv_advance(pRIic_Info);

    /* Clears the API flag. */
    r_iic_drv_unlock_api(pRIic_Info);

    return ret;

}

/*------------------------------------------------------------------------------------------------------------------*/
error_t r_iic_drv_advance(r_iic_drv_info_t * pRIic_Info)
{
    error_t ret     = R_IIC_ERR_OTHER;                          /* Sets R_IIC_ERR_OTHER as initialization of ret.   */

    /* Checks the channel status. */
    ret = r_iic_drv_check_chstatus_advance(pRIic_Info);
    if (R_IIC_COMMUNICATION != ret)
    {
        return ret;
    }
    
    /* Event happened? */
    if (R_IIC_EV_INIT != g_iic_Event[pRIic_Info->ChNo])
    {
        /* Counter initialization for calling "R_IIC_Drv_Advance()" upper limit error */
        g_iic_ReplyCnt[pRIic_Info->ChNo] = REPLY_CNT;
        
        /* IIC communication processing */
        ret = r_iic_drv_func_table(g_iic_Event[pRIic_Info->ChNo], pRIic_Info);
        
        /* return value? */
        switch (ret)
        {
            /* Advances communication. (Not finished) */
            case R_IIC_COMMUNICATION:
                return ret;
            break;
            
            /* Finished communication. */
            case R_IIC_FINISH:

                /* Disables IIC. */
                r_iic_drv_disable(pRIic_Info);
                
                /* Updates the channel status. */
                r_iic_drv_set_ch_status(pRIic_Info, R_IIC_FINISH);

                /* Checks the callback function. */
                if (NULL != R_IIC_Drv_CallBackFunc[pRIic_Info->ChNo])
                {
                    /* Calls the callback function. */
                    R_IIC_Drv_CallBackFunc[pRIic_Info->ChNo]();
                }

                /* IIC finish. */
                return ret;
                
            break;
            
            /* NACK is occurred. */
            case R_IIC_NACK:

                /* Disables IIC. */
                r_iic_drv_disable(pRIic_Info);

                /* Updates the channel status. */
                r_iic_drv_set_ch_status(pRIic_Info, R_IIC_NACK);

                /* Checks callback function. */
                if (NULL != R_IIC_Drv_CallBackFunc[pRIic_Info->ChNo])
                {
                    /* Calls the callback function. */
                    R_IIC_Drv_CallBackFunc[pRIic_Info->ChNo]();
                }

                return ret;

            break;

            default:
            
                /* Checks the return value. */
                if (R_IIC_ERR_AL == ret)
                {
                    /* Disables IIC. */
                    r_iic_drv_disable(pRIic_Info);

                    /* Updates the channel status. */
                    r_iic_drv_set_ch_status(pRIic_Info, R_IIC_ERR_AL);
                }
                else if (R_IIC_ERR_OTHER == ret)
                {
                    /* Disables IIC. */
                    r_iic_drv_disable(pRIic_Info);

                    /* Updates the channel status. */
                    r_iic_drv_set_ch_status(pRIic_Info, R_IIC_ERR_OTHER);
                }
                else
                {
                    /* Does nothing. */
                }

                /* Checks the callback function. */
                if (NULL != R_IIC_Drv_CallBackFunc[pRIic_Info->ChNo])
                {
                    /* Calls the callback function. */
                    R_IIC_Drv_CallBackFunc[pRIic_Info->ChNo]();
                }

                return ret;
                
            break;
        }
    }
    else    /* Event nothing. */
    {
        /* Decreases counter for calling "R_IIC_Drv_Advance()" upper limit. */
        g_iic_ReplyCnt[pRIic_Info->ChNo]--;
        
        /* Counter limitation? */
        if (0U == g_iic_ReplyCnt[pRIic_Info->ChNo])
        {
            /* Disables IIC. */
            r_iic_drv_disable(pRIic_Info);

            /* Updates the channel status. */
            r_iic_drv_set_ch_status(pRIic_Info, R_IIC_ERR_NON_REPLY);

            /* Checks the callback function. */
            if (NULL != R_IIC_Drv_CallBackFunc[pRIic_Info->ChNo])
            {
                /* Calls the callback function. */
                R_IIC_Drv_CallBackFunc[pRIic_Info->ChNo]();
            }

            /* Non-reply error and stop condition generation failure */
            return R_IIC_ERR_NON_REPLY;
        }
        
        /* During communication */
        return R_IIC_COMMUNICATION;
    }
}


/********************************************************************************************************************
* Outline      : SCL clock generation processing
* Function Name: R_IIC_Drv_GenClk
* Description  : Generates the SCL clock. The clock counts can decide to set "ClkCnt".
*              : This processing is intended to cancel SDA Low hold.
*              : Normally, must not use this processing.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
*              : uint8_t ClkCnt                    ;   SCL clock counter
* Return Value : R_IIC_NO_INIT                     ;   Successful operation
*              : R_IIC_LOCK_FUNC                   ;   Another task is handling API function
*              : 
*              : R_IIC_ERR_PARAM                   ;   Parameter error
*              : R_IIC_ERR_SDA_LOW_HOLD            ;   SDA Low hold error
*              ; R_IIC_ERR_OTHER                   ;   Other error
*********************************************************************************************************************/
error_t R_IIC_Drv_GenClk(r_iic_drv_info_t * pRIic_Info, uint8_t ClkCnt)
{
    bool    chk = R_IIC_FALSE;
    error_t ret = R_IIC_ERR_OTHER;                              /* Sets R_IIC_ERR_OTHER as initialization of ret.   */

    /* Parameter check */
    if ((NULL == pRIic_Info) ||
        (NULL == pRIic_Info->pDevStatus))
    {
        return R_IIC_ERR_PARAM;
    }
    
    if (0 == ClkCnt)
    {
        return R_IIC_ERR_PARAM;
    }

    /* Checks the API flag. */
    chk = r_iic_drv_lock_api(pRIic_Info);

    if (R_IIC_FALSE == chk)
    {
        /* Lock has already been acquired by another task. Needs to try again later.   */
        return R_IIC_LOCK_FUNC;
    }

    /* Calls the API function. */
    ret = r_iic_drv_genclk(pRIic_Info, ClkCnt);

    /* Clears the API flag. */
    r_iic_drv_unlock_api(pRIic_Info);

    return ret;

}

/*------------------------------------------------------------------------------------------------------------------*/
error_t r_iic_drv_genclk(r_iic_drv_info_t * pRIic_Info, uint8_t ClkCnt)
{
    error_t ret = R_IIC_ERR_OTHER;                              /* Sets R_IIC_ERR_OTHER as initialization of ret.   */

    /* Enables IIC. */
    r_iic_drv_enable(pRIic_Info);

    /* Sets to Master mode. */
    r_iic_drv_set_mst_bit(pRIic_Info);

    /* Checks the SDA level. */
    do
    {
        /* SDA = High? */
        if (R_IIC_TRUE == r_iic_drv_check_sda_level(pRIic_Info))
        {
            /* SDA = High */
            break;
        }
        else
        {
            ret = r_iic_drv_clk(pRIic_Info);                    /* Generates the SCL clock from high to low.        */
            if (R_IIC_FALSE == ret)
            {
                /* Updates the channel status. */
                r_iic_drv_set_ch_status(pRIic_Info, R_IIC_ERR_OTHER);
                return R_IIC_ERR_OTHER;                         /* It cannot generate SCL clock.                    */
            }
        }
        ClkCnt--;
    }
    while (0U != ClkCnt);

    /* Disables IIC. */
    r_iic_drv_disable(pRIic_Info);

    /* Counter = 0? */
    if (0U != ClkCnt)
    {
        /* Updates the channel status. */
        r_iic_drv_set_ch_status(pRIic_Info, R_IIC_NO_INIT);
        return R_IIC_NO_INIT;                                   /* Successful operation.                            */
    }
    else
    {
        /* Updates the channel status. */
        r_iic_drv_set_ch_status(pRIic_Info, R_IIC_ERR_SDA_LOW_HOLD);
        return R_IIC_ERR_SDA_LOW_HOLD;                          /* Failed operation.                                */
    }
}


/********************************************************************************************************************
* Outline      : Driver reset processing
* Function Name: R_IIC_Drv_Reset
* Description  : Resets the RIIC driver.
*              : The processing performs internal reset by setting to ICCR1.IICRST bit.
*              : Forcibly, stops the IIC communication.
*              : After called the processing, channel status becomes "R_IIC_NO_INIT".
*              : When starts the communication again, please call an initialization processing.
* Arguments    : r_iic_drv_info_t * pRIic_Info     ;   IIC Information
*              : uint8_t ClkCnt                    ;   SCL clock counter
* Return Value : R_IIC_NO_INIT                     ;   Successful operation
*              : R_IIC_LOCK_FUNC                   ;   Another task is handling API function
*              : 
*              : R_IIC_ERR_PARAM                   ;   Parameter error
*              ; R_IIC_ERR_OTHER                   ;   Other error
*********************************************************************************************************************/
error_t R_IIC_Drv_Reset(r_iic_drv_info_t * pRIic_Info)
{
    bool    chk = R_IIC_FALSE;
    error_t ret = R_IIC_ERR_OTHER;                              /* Sets R_IIC_ERR_OTHER as initialization of ret.   */

    /* Parameter check */
    if ((NULL == pRIic_Info) ||
        (NULL == pRIic_Info->pDevStatus))
    {
        return R_IIC_ERR_PARAM;
    }

    /* Checks the API flag. */
    chk = r_iic_drv_lock_api(pRIic_Info);

    if (R_IIC_FALSE == chk)
    {
        /* Lock has already been acquired by another task. Needs to try again later.   */
        return R_IIC_LOCK_FUNC;
    }

    /* Calls the API function. */
    ret = r_iic_drv_reset(pRIic_Info);

    /* Clears the API flag. */
    r_iic_drv_unlock_api(pRIic_Info);

    return ret;

}

/*------------------------------------------------------------------------------------------------------------------*/
error_t r_iic_drv_reset(r_iic_drv_info_t * pRIic_Info)
{
    /* Disables IIC. */
    r_iic_drv_disable(pRIic_Info);

    /* Updates the channel status. */
    r_iic_drv_set_ch_status(pRIic_Info, R_IIC_NO_INIT);

    return R_IIC_NO_INIT;

}


/* End of File */
