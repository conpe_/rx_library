/****************************************/
/*	Font				*/
/*		for RX63n @ CS+		*/
/*		Wrote by conpe_		*/
/*			2017/02/06	*/
/****************************************/

//yg’ϋz
// tHgf[^πn΅Δnew

//yXVπz
// 2017.02.06 VKμ¬


#ifndef __FONT_H__
#define __FONT_H__

#include "CommonDataType.h"



class font_t{
public:
	font_t(const uint8_t* SetFontData);
	
	int8_t getFontData(const uint8_t** FontData, char* strchar);
	int8_t getFontData(const uint8_t** FontData, uint16_t sjis);
	
	uint8_t getFontHeight(void){return FontHeight;};
	uint8_t getFontWidth(void){return FontWidth;};
	void getFontSize(uint8_t *Height, uint8_t *Width){*Height = FontHeight; *Width = FontWidth;};
	
protected:
	
	
	
private:
	const uint8_t *FontDataFNT;
	uint8_t FontWidth;
	uint8_t FontHeight;
	uint8_t BlockTableNum;
	bool_t fHalfFont;	// Όp
	uint16_t HeaderSize;
	uint16_t FontSize;
};


#endif
