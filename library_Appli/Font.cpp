/****************************************/
/*	Font				*/
/*		for RX63n @ CS+		*/
/*		Wrote by conpe_		*/
/*			2017/02/06	*/
/****************************************/

#include "Font.h"


font_t::font_t(const uint8_t* SetFontData){
	
	FontDataFNT = SetFontData;
	
	FontWidth = FontDataFNT[14];
	FontHeight = FontDataFNT[15];
	if(0x01 == FontDataFNT[16]){
		fHalfFont = false;
		BlockTableNum = FontDataFNT[17];
	}else{
		fHalfFont = true;
		BlockTableNum = 0;
	}
	HeaderSize = 18;
	FontSize = uint8_t((FontWidth+7)/8) * FontHeight;
}


int8_t font_t::getFontData(const uint8_t** FontData, char* strchar){
	return getFontData(FontData, ((uint16_t)strchar[0]<<8 | (uint16_t)strchar[1]));
}

int8_t font_t::getFontData(const uint8_t** FontData, uint16_t sjis){
	uint32_t adrs;
	uint16_t BlockStart;
	uint16_t BlockEnd;
	uint16_t CharNum;
	uint16_t i;
	
	// 半角だったら全角の文字コードにする処理入れたい
	
	
	if(false != fHalfFont){
		adrs = HeaderSize + sjis * FontSize;
		*FontData = &FontDataFNT[adrs];
		return 0;
	}else{
		CharNum = 0;
		for(i=0; i<BlockTableNum; ++i){
			BlockStart = (uint16_t)FontDataFNT[HeaderSize + i*4] | ((uint16_t)FontDataFNT[HeaderSize + i*4 + 1]<<8);
			BlockEnd = (uint16_t)FontDataFNT[HeaderSize + i*4 + 2] | ((uint16_t)FontDataFNT[HeaderSize + i*4 + 3]<<8);
			if((BlockStart<=sjis) && (BlockEnd>=sjis) ){	// find in this block 
				CharNum += (sjis - BlockStart);
				
				adrs = HeaderSize + 4*BlockTableNum + CharNum * FontSize;
				
				*FontData = &FontDataFNT[adrs];
				return 0;
			}else{
				CharNum += (BlockEnd - BlockStart) + 1;
			}
		}
	}
	
	return -1;
}

