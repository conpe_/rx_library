/*
 * aplFilter.h
 *
 *  Created on: 2018/12/13
 *      Author: khashimoto
 */

#ifndef INROF2019_MAIN_APLFILTER_H_
#define INROF2019_MAIN_APLFILTER_H_


#include "CommonDataType.h"



class BiQuadFilter_t{
public:
	float calc(float In);
	void setCoef(float a0, float a1, float a2, float b0, float b1, float b2);
	void getParam(float param[]);
private:
	float a0,a1,a2;		/* フィルタ係数 */
	float b0,b1,b2;
	float in[2];		/* 前々回までの入力値 */
	float out[2];		/* 前々回までの出力値 */
};

/* ローパスフィルタ */
class LowPassFilter_t:public BiQuadFilter_t{
public:
	void setParam(float SampleFreq, float CutOffFreq, float Q);
};

/* ハイパスフィルタ */
class HighPassFilter_t:public BiQuadFilter_t{
public:
	void setParam(float SampleFreq, float CutOffFreq, float Q);
};

/* バンドパスフィルタ */
class BandPassFilter_t:public BiQuadFilter_t{
public:
	void setParam(float SampleFreq, float CutOffFreq, float Bw);
};






class MovingAverage_t{
public:
	MovingAverage_t(uint8_t AverageNum = 2);
	~MovingAverage_t(void);
	float calc(float);

private:
	uint8_t AverageNum;
	float *buff;
	uint8_t BuffCounter;
};



/* DownRateは負で指定 */

class RateLimiter_t{
public:
	RateLimiter_t(void){setRate(0.0); Out = 0.0f; };
	RateLimiter_t(float Rate){setRate(Rate); Out = 0.0f; };
	RateLimiter_t(float UpRate, float DownRate){setRate(UpRate,DownRate); Out = 0.0f;};

	virtual ~RateLimiter_t(void){};

	virtual void setRate(float Rate);
	virtual void setRate(float UpRate, float DownRate);	/* DownRateは負 */

	virtual float calc(float In);
	virtual bool_t calc(float* Val);
	virtual void reset(float IniVal = 0.0f);

protected:
	float Out;
	float UpRate;
	float DownRate;
};



/* 負の値も絶対値で計算するレートリミッタ */
class RateLimiterAbs_t: public RateLimiter_t{
public:
	RateLimiterAbs_t(void):RateLimiter_t(){};
	RateLimiterAbs_t(float Rate):RateLimiter_t(Rate, Rate){};
	RateLimiterAbs_t(float UpRate, float DownRate):RateLimiter_t(UpRate,DownRate){};	/* DownRateも正の値で指定する */

	void setRate(float Rate);
	void setRate(float UpRate, float DownRate);	/* DownRateも正 */

	float calc(float In);
	bool_t calc(float* Val);

};



#endif /* INROF2019_MAIN_APLFILTER_H_ */
