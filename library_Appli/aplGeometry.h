/*
 * aplGeometry.h
 *
 *  Created on: 2018/12/07
 *      Author: khashimoto
 */

#ifndef APLGEOMETRY_H_
#define APLGEOMETRY_H_

#include "CommonDataType.h"
#include <mathf.h>


class Point{
public:
	float x;	/* 位置x[mm] */
	float y;	/* 位置y[mm] */

	/* 点と点の距離 */
	float operator- (Point base){
		return sqrtf(powf(x - base.x, 2.0f)+powf(y - base.y, 2));
	}

	/* 点と点を結んだ角度 */
	static float calcPointAng(Point *tgt, Point *base){
		return atan2f(tgt->y - base->y, tgt->x - base->x);
	}
};

class Angle{
public:
	Angle(float dir = 0.0){this->dir = dir;};
	float dir;

	/* 角度を±piの角度にする */
	float normalizeDir(float base = 0.0f){return this->dir = normalize(this->dir, base);};
	/* ±piの角度にした角度を取得する */
	float getNormalizedDir(float base = 0.0f){return normalize(this->dir, base);};

	/* 角度同士の引き算 */
	float operator- (Angle base_ang){
		return normalize(this->dir, base_ang.dir) - base_ang.dir;
	}

	/* 角度同士の足し算 */
	float operator+ (Angle add_ang){
		return (this->dir + add_ang.dir);
	}

	/* 角度正規化 */
	/* dirをbaseの±piの範囲にして返す */
	static float normalize(float dir, float base){
		while(dir > (base + M_PI)){
			dir -= (2*M_PI);
		}
		while(dir < (base - M_PI)){
			dir += (2*M_PI);
		}
		return dir;
	}
};

class Position{
public:
	Point pt;	/* 位置[mm] */
	Angle dir;	/* 向き[rad] */
};

class Velocity{
public:
	float vel;		/* 速度[mm/s] */
	float ang_vel;	/* 角速度[rad/s] */
};




#endif /* APLGEOMETRY_H_ */
