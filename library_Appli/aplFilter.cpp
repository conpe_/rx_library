/*
 * aplFilter.cpp
 *
 *  Created on: 2018/12/13
 *      Author: khashimoto
 */


#include <math.h>
#include "aplFilter.h"




void BiQuadFilter_t::getParam(float param[]){
	param[0]=this->a0;
	param[1]=this->a1;
	param[2]=this->a2;
	param[3]=this->b0;
	param[4]=this->b1;
	param[5]=this->b2;
}


float BiQuadFilter_t::calc(float In){
	float Out;
	// 入力信号にフィルタを適用し、出力信号として書き出す。
	Out = b0/a0 * In + b1/a0 * in[0]  + b2/a0 * in[1] - a1/a0 * out[0] - a2/a0 * out[1];

	in[1]  = in[0];		// 2つ前の入力信号を更新
	in[0]  = In;		// 1つ前の入力信号を更新

	out[1] = out[0];	// 2つ前の出力信号を更新
	out[0] = Out;		// 1つ前の出力信号を更新

	return Out;
}

void BiQuadFilter_t::setCoef(float a0, float a1, float a2, float b0, float b1, float b2){
	this->a0 = a0;
	this->a1 = a1;
	this->a2 = a2;
	this->b0 = b0;
	this->b1 = b1;
	this->b2 = b2;
}


void LowPassFilter_t::setParam(float SampleFreq, float CutOffFreq, float Q){
	float omega = 2.0f * M_PI *  CutOffFreq / SampleFreq;
	float alpha = sinf(omega) / (2.0f * Q);

	setCoef(
			1.0f + alpha,
			-2.0f * cosf(omega),
			1.0f - alpha,
			(1.0f - cosf(omega)) / 2.0f,
			1.0f - cosf(omega),
			(1.0f - cosf(omega)) / 2.0f
		);

}


void BandPassFilter_t::setParam(float SampleFreq, float CutOffFreq, float Bw){
	float omega = 2.0f * M_PI *  CutOffFreq / SampleFreq;
	float alpha = sinf(omega) * sinhf(logf(2.0f) / 2.0 * Bw * omega / sin(omega));

	setCoef(
			1.0f + alpha,
			-2.0f * cosf(omega),
			1.0f - alpha,
			alpha,
			0.0f,
			-alpha
		);

}


void HighPassFilter_t::setParam(float SampleFreq, float CutOffFreq, float Q){
	float omega = 2.0f * M_PI *  CutOffFreq / SampleFreq;
	float alpha = sinf(omega) / (2.0f * Q);

	setCoef(
			1.0f + alpha,
			-2.0f * cosf(omega),
			1.0f - alpha,
			(1.0f + cosf(omega)) / 2.0f,
			-(1.0f + cosf(omega)),
			(1.0f + cosf(omega)) / 2.0f
		);

}




MovingAverage_t::MovingAverage_t(uint8_t AverageNum){
	this->AverageNum = AverageNum;
	buff = new float[AverageNum];
	BuffCounter = 0;
}

MovingAverage_t::~MovingAverage_t(void){
	if(NULL != buff){
		delete[] buff;
	}
}

float MovingAverage_t::calc(float In){
	float Out;

	buff[BuffCounter] = In;

	++BuffCounter;
	if(AverageNum <= BuffCounter){
		BuffCounter = 0;
	}

	Out = 0.0f;
	for(uint8_t i=0; i<AverageNum; ++i){
		Out += buff[i];
	}

	return (Out / (float)AverageNum);
}





void RateLimiter_t::setRate(float Rate){
	setRate(Rate, -Rate);
}

void RateLimiter_t::setRate(float UpRate, float DownRate){
	this->UpRate = UpRate;
	this->DownRate = DownRate;
}

float RateLimiter_t::calc(float In){
	float tmp = In;

	calc(&tmp);

	return tmp;
}

bool_t RateLimiter_t::calc(float* Val){
	bool_t ret = false;

	if(*Val > Out + UpRate){
		Out += UpRate;
	}else if(*Val < Out + DownRate){
		Out += DownRate;
	}else{		/* 一致 */
		Out = *Val;
		ret = true;
	}

	*Val = Out;

	return ret;
}

void RateLimiter_t::reset(float IniVal){
	Out = IniVal;
}

void RateLimiterAbs_t::setRate(float Rate){
	setRate(Rate, Rate);
}
void RateLimiterAbs_t::setRate(float UpRate, float DownRate){
	this->UpRate = UpRate;
	this->DownRate = DownRate;
}


float RateLimiterAbs_t::calc(float In){
	float tmp = In;

	calc(&tmp);

	return tmp;
}

bool_t RateLimiterAbs_t::calc(float* Val){
	bool_t ret = false;
	bool_t minus;
	bool_t sign_change;
	float ValAbs;
	float OutAbs;

	OutAbs = fabs(Out);
	ValAbs = fabs(*Val);
	minus = (*Val < 0.0f);

	sign_change = ((Out<0.0f) ^ (*Val<0.0f));		/* 今の値と符号が異なる */

	if((!sign_change && (ValAbs > OutAbs + UpRate))){	/* 符号一致しているときはアップレート */
		OutAbs += UpRate;
	}else if((!sign_change && (ValAbs < OutAbs - DownRate)) || (sign_change && (fabs(Out-(*Val))>DownRate))){	/* 符号変わっている間はダウンレート */
		OutAbs -= DownRate;
	}else{		/* 一致 */
		OutAbs = ValAbs;
		ret = true;
	}

	if(minus){
		Out = -OutAbs;
	}else{
		Out = OutAbs;
	}

	*Val = Out;

	return ret;
}




