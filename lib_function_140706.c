/***********************************************************************/
/*                                                                     */
/*  ちょっと便利な関数群                                               */
/*  DATE        :2011/10/17		                                     */
/*  CPU TYPE    :SH7125                                                */
/*                                                                     */
/***********************************************************************/

//#ifndef _STDARG
//#define _STDARG
	#include <stdarg.h>	//可変長引数
//#endif

#include "iodefine.h"
#include "lib_function_140706.h"

#include <HardwareSetting.h>

/*
HardWareSetting.h内で指定するもの

// -------- lib_SCI --------

#define PCLK	48	// モジュールへの供給クロック([MHz])



// -------- lib_SCI --------
*/


/********* グローバル変数 *********/

// レジスタ
volatile __evenaccess struct st_cmt0 *CMTreg[4] = {&CMT0, &CMT1, &CMT2, &CMT3};

static uint16_t CmtNumForDelayFnc;


/*==========================================================================
待ち時間(CMT1使用)
	引数:ms単位
==========================================================================*/
void delayMs(uint16_t ms){
	/* 25MHz */
	uint16_t CntStart;
	//uint16_t CntLast, CntNow;
	uint16_t CntMs;
	
	CntStart = PCLK/8/1000;
	
	for(CntMs = 0; CntMs<ms; CntMs++){
		CMTreg[CmtNumForDelayFnc]->CMCNT = 0;					// タイマリセット
		while(CMTreg[CmtNumForDelayFnc]->CMCNT < CntStart);		// 1ms経つまで待つ
	}
	
	/*
	CntStart = CMTreg[CmtNumForDelayFnc]->CMCNT;
	
	
	CntNow = CntStart;
	CntLast = CntStart;
	
	for(CntMs = 0; CntMs<ms; CntMs++){
		while(!((CntLast<CntNow || ((CntStart==0)&&(CntLast==(CMTreg[CmtNumForDelayFnc]->CMCOR-1)))) && CntNow==CntStart)){
				// さっきは小さい値だった時、 スタート時と同じ値になった(1ms経ってる)
				// CntStartが0の時は、さっきが最大値だった時。
			CntLast = CntNow;
			CntNow = CMTreg[CmtNumForDelayFnc]->CMCNT;
		}
	}
	*/
}


/*==========================================================================
待ち時間 初期設定
	引数:Delay関数処理用に用いるCMTモジュールNo. (0〜3)
==========================================================================*/
void setupDelay(uint16_t CmtNum){	// ←引数にCMT番号入れるようにしよう
	/* 16bit x 2ch */
	/* クロックはPCLKで選択 */
	
	
	//delay関数用
	switch(CmtNum){
		case 0:
			MSTP(CMT0) = 0;				// 0:スタンバイ解除 
			CMT.CMSTR0.BIT.STR0 = 0;	// カウントストップ
			CmtNumForDelayFnc = 0;
			break;
		case 1:
			MSTP(CMT1) = 0;
			CMT.CMSTR0.BIT.STR1 = 0;	// カウントストップ
			CmtNumForDelayFnc = 1;
			break;
		case 2:
			MSTP(CMT2) = 0;
			CMT.CMSTR1.BIT.STR2 = 0;	// カウントストップ
			CmtNumForDelayFnc = 2;
			break;
		case 3:
			MSTP(CMT3) = 0;
			CMT.CMSTR1.BIT.STR3 = 0;	// カウントストップ
			CmtNumForDelayFnc = 3;
			break;
		default:
			CmtNumForDelayFnc = 4;
			break;
	}
	
	CMTreg[CmtNum]->CMCR.BIT.CMIE = 0;		// 割り込みoff
	CMTreg[CmtNum]->CMCR.BIT.CKS = 0;		/* 0:PCLK/8 1:PCLK/32 2:PCLK/128 3:PCLK/512 */
	//1ms = 3124*8/48000000 -> 12000 = 96000000/8/1000 
	//CMTreg[CmtNum]->CMCOR = PCLK/8/1000;	// コンペアマッチまでの期間 1msec
	//CMTreg[CmtNum]->CMCOR = 12000;	// コンペアマッチまでの期間 1msec
	CMTreg[CmtNum]->CMCOR = 0xffff;	// コンペアマッチまでの期間 たくさんmsec
	//↑けいさんしましょう。PCLKつかって
	
	CMTreg[CmtNum]->CMCNT = 0;				/* カウンタ */
	
	switch(CmtNum){
		case 0:
			CMT.CMSTR0.BIT.STR0 = 1;	// カウントスタート
			break;
		case 1:
			CMT.CMSTR0.BIT.STR1 = 1;
			break;
		case 2:
			CMT.CMSTR1.BIT.STR2 = 1;
			break;
		case 3:
			CMT.CMSTR1.BIT.STR3 = 1;
			break;
		default:
			break;
	}
	//INTC.IPRJ.BIT._CMT0 = 10;
	
}


/*==========================================================================
ビットテスト
	numのbit番のビットが立ってるか。
==========================================================================*/
unsigned short int checkBit(uint16_t num, uint16_t bit){
	short b;
	
	b = (num>>bit)&0x01;
	
	return b;
}

/*==========================================================================
ビットセット
	numのbit番のビットを立てる。
==========================================================================*/
uint16_t setBit(uint16_t num, uint16_t bit){
	short b;
	
	b = num|(0x0001<<bit);
	
	return b;
}

/*==========================================================================
ビットクリア
	numのbit番のビットをクリア。
	intまで。
==========================================================================*/
uint16_t clearBit(uint16_t num, uint16_t bit){
	
	num = num&((0x0001<<bit)^0xFFFF);
//	num = num^((uint16_t)1<<bit);	//←なんでだめかな？
	
	return num;
}

/*==========================================================================
ビット書き込み
	numのbit番のビットに書く。
	intまで。
==========================================================================*/
uint16_t writeBit(uint16_t data, uint8_t writedata, uint16_t bit){
	
	if(writedata){
		return setBit(data, bit);
	}else{
		return clearBit(data, bit);
	}
}



/*==========================================================================
乗数
	
==========================================================================*/
double myPow(int num, signed int pow){
	uint16_t i;
	double ret = 1;
	
	if(pow>0){
		for(i=0;i<pow;i++){
			ret = ret*(double)num;
		}
	}else if(pow<0){
		pow = - pow;
		for(i=0;i<pow;i++){
			ret = ret/(double)num;
		}
	}else{
		ret = 1;
	}
	
	return ret;
}


/*==========================================================================
最大値制限
	valueをmaxで制限
==========================================================================*/
signed int saturateValue(signed int value, int max){

	if(max<0){
		max = -max;
	}
	
	if(value < (-max)){
		value = (-max);
	}else if(value > max){
		value = max;
	}
	
	return value;
}
/*==========================================================================
最大値制限
	valueをmaxで制限
==========================================================================*/
float saturateValuef(float value, float max){
	
	if(max<0){
		max = -max;
	}
	
	if(value < (-max)){
		value = (-max);
	}else if(value > max){
		value = max;
	}
	
	return value;
}




/*********************
myぷりんとえふ
関数出力Ver.
引数：
	出力関数名 pfunc
	出力フォーマット format
		%% : %
		%a__ : 文字コード指定(16進2ケタ)
		%c : 文字(char)
		%s : 文字列(char*)
		%ld : 10進(long)
		%lx : 16進小文字(long)
		%lX : 16進大文字(long)
		%d : 10進(int)
		%x : 16進小文字(int)
		%X : 16進大文字(int)
		
		2
		%3d  →   2
		%03d → 002		
		桁数指定は9ケタまで
		
	変数たち ...
返値:
	文字数
**********************/	
int myPrintf(void(*pfunc)(char), char *format, va_list ap)
{
	signed int i;
	int moji_cnt = 0;	//文字数カウント
	int moji_cnt_tmp = 0;
	char outtmp[32];
	char *str;	//文字列受取り用
	char code;
	signed int digit;	// 整形用 桁数
	char zero;	// 整形用 頭にゼロつけるフラグ
	signed int digit_cnt;	// 整形用 通常時の桁数カウント
	char headchar;	// '0' or ' '
	
		
	while(*format)	//文字列続く限り
	{
		digit = 0;
		digit_cnt = 0;
		zero = 0;
		switch(*format)
		{
		case '%':
			format++;
			switch(*format)
			{
			case 'a':
				format++;
				code = (*format&0x0F)<<4;
				format++;
				outtmp[0] = code|(*format&0x0F);
				moji_cnt_tmp = 1;
				break;
			case '%':	//%%
				outtmp[0] = '%';
				moji_cnt_tmp = 1;
				break;
			case 'c':	//文字
				outtmp[0] = (char)va_arg(ap, int);
				moji_cnt_tmp = 1;
				break;
			case 's':	//文字列
				str = va_arg(ap, char*);
				myPrintf(pfunc, str, ap);
				moji_cnt_tmp = 0;
				/*
				moji_cnt_tmp = 0;
				while(*(str+moji_cnt_tmp) != '\0')
				{
					outtmp[moji_cnt_tmp] = *(str+moji_cnt_tmp);
					moji_cnt_tmp++;
				}*/
				break;
			//整形 %03dとか
			case '0':
				zero = 1;	// 頭にゼロつけるフラグ
				format++;
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				digit = (*format)-'0';
				format++;
			case 'l':
			case 'd':
			case 'x':
			case 'X':
				switch(*format)
				{
				case 'l':	//数値(long)
					format++;
					switch(*format)
					{
					case 'd':	//10進数字
						digit_cnt = myItoa_d(va_arg(ap, long), outtmp);
						break;
					case 'x':	//16進小文字
						digit_cnt = myItoa_x(va_arg(ap, long), outtmp);
						break;
					case 'X':	//16進大文字
						digit_cnt = myItoa_X(va_arg(ap, long), outtmp);
						break;
					default:
						outtmp[0] = '%';
						outtmp[1] = 'l';
						outtmp[2] = *format;
						moji_cnt_tmp = 3;
						break;
					}
					break;
				//数値(int)
				case 'd':	//10進数字(int)
					digit_cnt = myItoa_d(va_arg(ap, int), outtmp);
					break;
				case 'x':	//16進小文字
					digit_cnt = myItoa_x(va_arg(ap, int), outtmp);
					break;
				case 'X':	//16進大文字
					digit_cnt = myItoa_X(va_arg(ap, int), outtmp);
					break;
				default:
					outtmp[0] = '%';
					outtmp[1] = *format;
					moji_cnt_tmp = 2;
					break;
				}
				break;
			default:
				outtmp[0] = '%';
				outtmp[1] = *format;
				moji_cnt_tmp = 2;
				break;
			}
			
			// 整形
			if(digit_cnt!=0)	//これは数字
			{
				if((digit!=digit_cnt)&&(digit!=0))	//指定digitとItoaしたdigitがあってない&&指定digitがゼロじゃなければ整形
				{
					moji_cnt_tmp = digit;
					if(zero)
					{
						headchar = '0';
					}
					else
					{
						headchar = ' ';
					}
					
					if(digit>digit_cnt)		//digitの方が大きいときは右のケタからずらしていく
					{
						for(i=0;i<digit_cnt;i++)	//ずらす
						{
						//	if(((digit-i) > 0)&&(digit_cnt-i)>0)
						//	{
								outtmp[digit-1-i] = outtmp[digit_cnt-1-i];
						//	}
						}
					}
					else					//digit_cntの方が大きいときは左のケタからずらしていく
					{
						for(i=0;i<digit;i++)	//ずらす
						{
						//	if(((digit-i) > 0)&&(digit_cnt-i)>0)
						//	{
								outtmp[i] = outtmp[digit_cnt-digit+i];
						//	}
						}
					}
					i = digit-digit_cnt;
					while(i>0)					//頭埋める
					{
						outtmp[i-1] = headchar;
						i--;
					}
				}
				else	//数字だけど整形なし
				{
					moji_cnt_tmp = digit_cnt;
				}
			}
			break;	// end of case '%'
			
		default :	//ただの文字
			outtmp[0] = *format;
			moji_cnt_tmp = 1;
			
			break;
		}
		
		i=0;
		while(i<moji_cnt_tmp)
		{
			(*pfunc)(outtmp[i]);	//目的の関数に1文字ずつ出力
	//	SCI1_tx_char_break(outtmp[i]);
			i++;
		//	LEDR=1;
		//	LEDG=1;
		}
		
		
		format++;
		moji_cnt = moji_cnt + moji_cnt_tmp;
	}
	
	return moji_cnt;
}


/*********************
my絶対値
引数：
	数値 a
返値:
	絶対値とった値
**********************/	
unsigned long myAbs(signed long a)
{
	
	if(a<0){
		a = -a;
	}
	
	return (unsigned long)a;
}


/*********************
数字を文字へ_d
引数：
	数字 integer
	格納配列 asci 10進
返値:
	文字数
**********************/	
int myItoa_d(long integer, char *asci)
{
	int i;
	int num = 0;	//桁ポインタ
	char buf[20];	//20文字まで
	
	if(integer<0)	
	{
		integer = myAbs(integer);
		*asci = '-';
		asci++;
		num++;
	}
	
	do{		//とりあえず下のケタからbufに埋めていく
		buf[num]=(char)( (integer%10) + '0');
		num++;
		if(num>20)	integer = 0;
		integer = integer/10;	//次の桁を1の位に持ってくる
	}while(integer);			//割り切ったら終わり
	
	i = num;
	
	while(i>0){			//bufの中身を後ろから出力
		i--;
		*asci = buf[i];
		asci++;
	}
	*asci = '\0';
	
	return num;
}


/*********************
数字を文字へ_x
引数：
	数字 integer
	格納配列 asci 16進小文字
返値:
	文字数
**********************/	
int myItoa_x(long integer, char *asci)
{
	int i;
	int num = 0;	//桁ポインタ
	char buf[20];	//20文字まで
	char tmp;
	
	if(integer<0)	
	{
		integer = myAbs(integer);
		*asci = '-';
		asci++;
		num++;
	}
	
	
	do{		//とりあえず下のケタから埋めていく
		tmp = (integer & 0xf); 	//下のケタを抽出
		if(tmp<10){			//0〜9
			buf[num]=(char)( tmp + '0');
		}else{				//A〜F
			buf[num]=(char)( (tmp-10) + 'a');
		}
		num++;
		if(num>20)	integer = 0;
		integer = integer>>4;	//次の桁を1の位に持ってくる
	}while(integer);			//割り切ったら終わり
	
	i = num;
	
	while(i>0){			//bufの中身を後ろから出力
		i--;
		*asci = buf[i];
		asci++;
	}
	*asci = '\0';
	
	return num;
}


/*********************
数字を文字へ_X
引数：
	数字 integer
	格納配列 asci 10進大文字
返値:
	文字数
**********************/	
int myItoa_X(long integer, char *asci)
{
	int i;
	int num = 0;	//桁ポインタ
	char buf[20];	//20文字まで
	char tmp;
	
	if(integer<0)	
	{
		integer = myAbs(integer);
		*asci = '-';
		asci++;
		num++;
	}
	
	
	do{		//とりあえず下のケタから埋めていく
		tmp = (integer & 0xf); 	//下のケタを抽出
		if(tmp<10){			//0〜9
			buf[num]=(char)( tmp + '0');
		}else{				//A〜F
			buf[num]=(char)( (tmp-10) + 'A');
		}
		num++;
		if(num>20)	integer = 0;
		integer = integer>>4;	//次の桁を1の位に持ってくる
	}while(integer);			//割り切ったら終わり
	
	i = num;
	
	while(i>0){			//bufの中身を後ろから出力
		i--;
		*asci = buf[i];
		asci++;
	}
	*asci = '\0';
	
	return num;
}


uint16_t my_abs_int16_t(int16_t num){
	if(num<0){
		return (uint16_t)-num;
	}
	return (uint16_t)num;
}