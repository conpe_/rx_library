/********************************************/
/*	GPSモジュール							*/
/*		for RX63n @ CS+					*/
/*		Wrote by conpe_					*/
/*							2018/02/09	*/
/********************************************/

// シリアルでNMEAのメッセージが渡されるGPSモジュール
// 


// NMEAメッセージをほかアプリでまるまる使いたい場合，
// setNmeaBuff，getNmeaBuff あたりを使う


#if !defined(__CANMOREGPS)
#define __CANMOREGPS

#include "SCI.h"
#include "CommonDataType.h"

class calendar_t{
public:
    uint16_t Year;
    uint8_t Month;
    uint8_t Day;
    uint8_t Hour;
    uint8_t Minute;
    uint8_t Sec;
    void setUnixTime(uint64_t UnixTime){
        Year = 1970 + UnixTime/(60*60*24*365);
    };
    uint64_t getUnixTime(void);
};

enum Orient_e{
    N = 'N',
    S = 'S',
    E = 'E',
    W = 'W'
};

// DDD MM.MMMM
typedef struct{
    uint8_t d;
    uint8_t mm;
    uint16_t mmmm;
} DM_t;

class worldpos_t{
public:
    DM_t Latitude;    
    Orient_e LatitudeHemisphere;
    DM_t Longitude;   
    Orient_e LongitudeHemisphere;
};

enum CmGps_err_e{
    CMGPS_E_OK = 0,
    CMGPS_E_NG = 1,
    CMGPS_E_COMU_NG = 2,    
    CMGPS_E_TESTMODE = 3,
    CMGPS_E_POS_FIX_UNAVAILABLE = 4,
    CMGPS_E_VALID_POS = 0
};

class CanMoreGt723F_t{
public:
    CanMoreGt723F_t(void);
    CanMoreGt723F_t(uint8_t TestNum);
    virtual ~CanMoreGt723F_t(void);
    virtual void begin(Sci_t* Ser);

    virtual void setTaskFreq(uint16_t Freq){TaskFreq = Freq;};
    virtual void task(void);
    CmGps_err_e getStatus(void){return NowErr;};
    CmGps_err_e getPos(worldpos_t* pos){*pos = Position[0]; return NowErr;};
    uint8_t getUtc(calendar_t* Utc){*Utc = UtcTime[0]; return IsTimeValid;};
    uint8_t numSatellites(void){return NumSatellites[0];};
    float getHdop(void){return HDOP[0];};
    uint8_t getPositionFixQuality(void){return PositionFixQuality[0];};
    //uint8_t isEnable(void){return (0<PositionFixQuality[0]) && (IsPosValid) && (IsTimeValid);};
    uint8_t isEnable(void){return ((IsPosValid) && (IsTimeValid));};
    uint8_t isPosValid(void){return IsPosValid;};
    uint8_t isTimeValid(void){return IsTimeValid;};
    
    //void enableDebugPrint(HardwareSerial* Ser){fDebugOut = true; SerDebug = Ser; SerDebug->println("Gps Print Enable");};
    //void disableDebugPrint(void){fDebugOut = false; SerDebug->println("Gps Print Disable");};
    //uint8_t isDebugPrint(void){return fDebugOut;};
    void enableDebugPrint(Sci_t* Ser){fDebugOut = true;};
    void disableDebugPrint(void){fDebugOut = false;};
    uint8_t isDebugPrint(void){return fDebugOut;};
    
    void setNmeaBuff(uint8_t *Buff, uint16_t Num){NmeaMsgBuff = Buff; NmeaMsgBuffNum = Num;};	// set the Msg buffer to write.
    void detatchNmeaBuff(void){NmeaMsgBuff = NULL; NmeaMsgBuffNum = 0;};	// set the Msg buffer to write.
    uint16_t getNmeaBuff(uint8_t **NmeaMsgBuff);	// set the Msg buffer to write. (clear buff index)
    uint16_t getNmeaBuffNum(void){return NmeaMsgBuffWriteIdx;};
    
    // debug
    uint8_t getSerStatus(void){return SerialRcvStatus;};
    uint8_t getSerialBuffCnt(void){return SerialBuffCnt;};
    
private:
    enum nmea_message_e{
        NMEA_GPGGA = 0,  // GPS FIX DATA 
        NMEA_GPGLL = 1,  // 
        NMEA_GPGSA = 2,  // 
        NMEA_GPGSV = 3,  // 
        NMEA_GPRMC = 4,  // 
        NMEA_GPVTG = 5,  // 
        NMEA_GPZDA = 6,  // ZDA TIME AND DATE
    };


    //HardwareSerial* Ser;
    //HardwareSerial* SerDebug;
    Sci_t* Ser;
    uint8_t fDebugOut;
    
    uint16_t TaskFreq;		// タスク実行周期

    calendar_t UtcTime[2];
    worldpos_t Position[2];
    CmGps_err_e NowErr;
    uint8_t PositionFixQuality[2];
    uint8_t NumSatellites[2];
    float HDOP[2];
    bool_t rmc_validdata;	// RMCのステータスがAなら1
    
    
    uint8_t* SerialBuff;
    uint8_t SerialRcvStatus;
    uint8_t SerialRcvStatusSub;
    uint8_t SerialBuffCnt;
    uint8_t SerialBuffCntLast;
    
    uint8_t LastRcvCheckSum;
    uint8_t CheckSum;
    uint8_t fSumming;
    
    bool_t IsTimeValid;
    bool_t IsTimeValidTmp;
    uint16_t ValidTimeCnt;
    bool_t IsPosValid;
    bool_t IsPosValidTmp;
    uint16_t ValidPosCnt;
    
    bool_t IsDataValid;    // checksum ok
    
    uint8_t* NmeaMsgBuff;		// NMEA message
    uint16_t NmeaMsgBuffNum;    // buff num
    uint16_t NmeaMsgBuffWriteIdx;    // NMEA message nextwrite idx
    
    void debugprint(uint8_t *buff, uint8_t Num);
    void testDefaultVal(void);
    
};


#endif
