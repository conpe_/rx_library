/********************************************/
/*	さくらのIoTモジュール						*/
/*		for RX63n @ CS+					*/
/*		Wrote by conpe_					*/
/*							2018/02/08	*/
/********************************************/

// 
// I2C接続
// はじめにbegin()して，20msごとにtask_20ms()を実行

// update
// 2018.02.08 new

#ifndef __SAKURA_IOT_H__
#define __SAKURA_IOT_H__

#include "RIIC.h"
#include "CommonDataType.h"
//extern uint32_t getTime_ms(void);	// 時間計測


// I2Cアドレス
#define SAKURAIOT_I2C_ADDRESS_DEFAULT 0x4F

enum sakuraiot_comu_content{
	SAKURAIOT_GET_STATUS = 0x01,
	SAKURAIOT_GET_SIGNAL_QUALITY = 0x02,
	SAKURAIOT_TX_ENQUEUE = 0x20,
	SAKURAIOT_TX_QUEUE_FLUSH = 0x22,
	SAKURAIOT_TX_QUEUE_SEND = 0x24,
	SAKURAIOT_GET_TX_STATUS = 0x25,
	SAKURAIOT_GET_FIRMWARE_VERSION = 0xA2,
	SAKURAIOT_SEND_UNLOCK = 0xA8,
	SAKURAIOT_UPDATE_FIRMWARE = 0xA9,
	SAKURAIOT_GET_FIRMWARE_UPDATE_STATUS = 0xAA,
	SAKURAIOT_REQ_SOFTWARE_RESET = 0xAF,
	SAKURAIOT_SET_POWER_SAVE_MODE = 0xB0,
	SAKURAIOT_GET_POWER_SAVE_MODE = 0xB1
};

enum sakuraiot_tx_status{
	SAKURAIOT_TXSTA_IDLE = 0x00,
	SAKURAIOT_TXSTA_TRANSMITTING = 0x01,
	SAKURAIOT_TXSTA_FAILED = 0x02,	
};


class sakuraiot_comus_t;

class sakuraiot_t{
public:
	enum cmd_ret_e{
		RET_NONE			= 0x00,
		RET_SUCCESS		= 0x01,
		RET_PARITY_ERR	= 0x02,
		RET_TYPE_ERR		= 0x03,
		RET_ERR			= 0x04,
		RET_EXEC_ERR		= 0x05,
		RET_UNLOCK_ERR	= 0x06,
		RET_DOING		= 0x07,
	};
	
	enum connection_status_e{
		CONSTA_TRYING_CONNECT		= 0x00,
		CONSTA_OUT_OF_SERVICE	 	= 0x01,
		CONSTA_CONNECT_ERR		= 0x02,
		CONSTA_DISCONNECT 		= 0x03,
		CONSTA_CONNECT			= 0x80,
	};
	
	
public:
// コンストラクタ
	sakuraiot_t(void);
	~sakuraiot_t(void);
	
// モジュールセットアップ
	int8_t begin(I2c_t* I2Cn, uint8_t I2cAddress = SAKURAIOT_I2C_ADDRESS_DEFAULT);
	
// モジュール再起動時呼ぶ
	void reset(void){ ConnectStatus = CONSTA_TRYING_CONNECT; };

/* タスク */
public:
	int8_t task_20ms(void);
private:
	uint8_t TaskCnt;
	
/* get connection status */
public:
	connection_status_e getConnectionStatus(void){return ConnectStatus; };
private:
	connection_status_e ConnectStatus;
	uint8_t updateConnectionStatus(void);
	
/* get signal quality */
public:
	uint8_t getSignalQuality(void){return SignalQuality;};
private:
	uint8_t SignalQuality;
	uint8_t updateSignalQuality(void);
	
/* Tx enqueue */
public:
	uint8_t enqueueTxRaw(uint8_t ch, uint8_t type, uint8_t length, uint8_t *data, uint64_t offset = 0);
	uint8_t enqueueTx(uint8_t ch, int32_t value, uint64_t offset = 0);
	uint8_t enqueueTx(uint8_t ch, uint32_t value, uint64_t offset = 0);
	uint8_t enqueueTx(uint8_t ch, int64_t value, uint64_t offset = 0);
	uint8_t enqueueTx(uint8_t ch, uint64_t value, uint64_t offset = 0);
	uint8_t enqueueTx(uint8_t ch, float value, uint64_t offset = 0);
	uint8_t enqueueTx(uint8_t ch, double value, uint64_t offset = 0);
	uint8_t enqueueTx(uint8_t ch, uint8_t value[8], uint64_t offset = 0);
	
	
/* Tx queue flush */
public:
	uint8_t flushTxQueue(void);
	
/* Tx queue send */
public:
	uint8_t sendTxQueue(void);
	
	
/* get Tx status */
public:
	sakuraiot_tx_status getTxStatus(void){return TxStatus;};
	sakuraiot_tx_status getTxStatusImm(void){return TxStatusImm;};
private:
	sakuraiot_tx_status TxStatus;
	sakuraiot_tx_status TxStatusImm;
	uint8_t updateTxStatus(void);

/* send Unlock */
private:
	uint8_t sendUnlock(void);
	uint8_t SendUnlockResult;
public:
	uint8_t getSendUnlockRes(void){return SendUnlockResult;};

/* Firmware */
public:
	uint8_t getFirmwareVersion(uint8_t** VerStr, uint8_t* VerStrLen);	/* ASCII文字列 */
	uint8_t updateFirmware(void);
	uint8_t getUpdateFirmWareRes(void){return FirmwareUpdateCmdResult;};
	uint8_t getFirmwareUpdateStatusRes(void){return FirmwareUpdateStatusCmdResult;};
	uint8_t getFirmwareUpdateStatus(void){return FirmwareUpdateStatus;};
private:
	uint8_t FirmwareUpdateStatus;
	uint8_t FirmwareUpdateCmdResult;
	uint8_t FirmwareUpdateStatusCmdResult;
	uint8_t updateGetFirmwareUpdateStatus(void);	/* ファームウェア更新状況の取得 */
	uint8_t updateGetFirmwareVersion(void);		/* ファームウェアバージョンの取得 */
	uint8_t FirmWareVersionStr[33];
	uint8_t FirmWareVersionStrLen;

/* Software reset */
public:
	uint8_t reqSoftwareReset(void);
	
/* Power save mode */
public:
	uint8_t setPowerSaveMode(uint8_t mode);
	uint8_t getPowerSaveMode(void){return PowerSaveMode;};
private:
	uint8_t PowerSaveMode;
	uint8_t updateGetPowerSaveMode(void);
	
/* マイコンとの通信関係 */
	// I2C受信データを取得して通信内容ごとに処理する
public:
	int8_t fetchI2cRcvData(const sakuraiot_comus_t* Comu);
	bool_t isI2cIdle(void){return fLastAttachComuFin;};
private:
	I2c_t *I2Cn;
	uint8_t I2cAddress;		// I2Cアドレス
	
	
	// I2Cクラスに通信内容をアタッチ
	int8_t attachI2cComu(sakuraiot_comu_content ComuType, uint8_t* TxData = NULL, uint16_t TxNum = 0xFF, uint16_t RxNum = 0xFF);
	sakuraiot_comus_t* LastAttachComu;		// 最後に通信要求した通信
	bool_t fLastAttachComuFin;		// 最後に通信要求した通信が終わったかフラグ
};

class sakuraiot_comus_t:public I2c_comu_t{
public:
	sakuraiot_comus_t(sakuraiot_t* Parent, uint8_t DestAddress, sakuraiot_comu_content ComuType, uint8_t* TxData, uint16_t TxNum, uint16_t RxNum);	// 送受信
	
	sakuraiot_t* Parent;
	sakuraiot_comu_content ComuType;
	
	void callBack(void){Parent->fetchI2cRcvData(this);};
	
};

#endif
