/********************************************/
/*	GPSモジュール							*/
/*		for RX63n @ CS+					*/
/*		Wrote by conpe_					*/
/*							2018/02/09	*/
/********************************************/

#include "CanMoreGt723F.h"

#define CM_GPS_BAUD_DEFAULT 9600
#define CM_GPS_SERIALBUFF_NUM 511

#define CM_GPS_NMEA_NUM 7
#define CM_GPS_VALID_TIME_CNT (5*TaskFreq)		/* 時刻データが正しく取得できているかのカウント 5sec以内に次のデータくるべし */
#define CM_GPS_VALID_POS_CNT (5*TaskFreq)

CanMoreGt723F_t::CanMoreGt723F_t(void){
	Ser = NULL;
	TaskFreq = 1000;
	fDebugOut = false;
	//UtcTime[0] = 0;
	//Position[0] = 0;
	NowErr = CMGPS_E_OK;
	for(int i = 0; i<2; ++i){
		PositionFixQuality[i] = 0;
		NumSatellites[i] = 0;
		HDOP[i] = 0.0f;

	    UtcTime[i].Year = 1970;
	    UtcTime[i].Month = 0;
	    UtcTime[i].Day = 0;
	    UtcTime[i].Hour = 0;
	    UtcTime[i].Day = 0;
	    UtcTime[i].Minute = 0;

	    Position[i].Latitude.d = 0;
	    Position[i].Latitude.mm = 0;
	    Position[i].Latitude.mmmm = 0;
	    Position[i].LatitudeHemisphere = N;
	    Position[i].Longitude.d = 0;
	    Position[i].Longitude.mm = 0;
	    Position[i].Longitude.mmmm = 0;
	    Position[i].LongitudeHemisphere = N;
	}
	SerialBuff = NULL;
	SerialRcvStatus = 0;
	SerialRcvStatusSub = 0;
	SerialBuffCnt = 0;
	SerialBuffCntLast = 0;
	LastRcvCheckSum = 0;
	CheckSum = 0;
	fSumming = 0;
	IsTimeValid = false;
	IsTimeValidTmp = false;
	ValidTimeCnt = 0;
	IsPosValid = false;
	IsPosValidTmp = false;
	ValidPosCnt = 0;
	IsDataValid = false;
	NmeaMsgBuff = NULL;
	NmeaMsgBuffNum = 0;
	NmeaMsgBuffWriteIdx = 0;
	rmc_validdata = false;
}

CanMoreGt723F_t::CanMoreGt723F_t(uint8_t TestNum){
    switch(TestNum){
    case 1:
        testDefaultVal();
        break;
    default:
        break;
    }
}
CanMoreGt723F_t::~CanMoreGt723F_t(void){
	if(NULL != SerialBuff){
		delete[] SerialBuff;
	}
	if(NULL != NmeaMsgBuff){
		delete[] NmeaMsgBuff;
	}
}

void CanMoreGt723F_t::begin(Sci_t* Ser){
    this->Ser = Ser;
    Ser->begin(9600, true, true, 511);
    
    SerialBuff = new uint8_t[CM_GPS_SERIALBUFF_NUM];

    NmeaMsgBuff = NULL;
    NmeaMsgBuffWriteIdx = 0;
    
    SerialRcvStatus = 0;
    SerialBuffCnt = 0;
    HDOP[0] = 99.9;
    PositionFixQuality[0] = 0;
    NumSatellites[0] = 0;
    IsTimeValid = 0;
    ValidTimeCnt = 0;
    UtcTime[0].Year = 1970;
    UtcTime[0].Month = 0;
    UtcTime[0].Day = 0;
    UtcTime[0].Hour = 0;
    UtcTime[0].Day = 0;
    UtcTime[0].Minute = 0;
    
    NowErr = CMGPS_E_NG;
    
    fDebugOut = false;
}


// SerialMessage decode
void CanMoreGt723F_t::task(void){
    uint8_t SerTmp;
    uint8_t findnmea;
    const uint8_t STR_GPGGA[] = "GPGGA";
    const uint8_t STR_GPGLL[] = "GPGLL";
    const uint8_t STR_GPGSA[] = "GPGSA";
    const uint8_t STR_GPGSV[] = "GPGSV";
    const uint8_t STR_GPRMC[] = "GPRMC";
    const uint8_t STR_GPVTG[] = "GPVTG";
    const uint8_t STR_GPZDA[] = "GPZDA";
    typedef struct {
      const uint8_t* str_nmea;
      nmea_message_e e_nmea;
    } nmea_st;
    const nmea_st ST_NMEA[CM_GPS_NMEA_NUM] = {
        {STR_GPGGA, NMEA_GPGGA},
        {STR_GPGLL, NMEA_GPGLL}, 
        {STR_GPGSA, NMEA_GPGSA}, 
        {STR_GPGSV, NMEA_GPGSV}, 
        {STR_GPRMC, NMEA_GPRMC}, 
        {STR_GPVTG, NMEA_GPVTG}, 
        {STR_GPZDA, NMEA_GPZDA}
    };
    
    static nmea_message_e RcvMessage;
    
    while(0 < Ser->available()){
        SerTmp = Ser->read();
        
        if((NULL!=NmeaMsgBuff)&&(0!=NmeaMsgBuffNum)){
        	if(NmeaMsgBuffWriteIdx < NmeaMsgBuffNum){
        		NmeaMsgBuff[NmeaMsgBuffWriteIdx] = SerTmp;
	        	NmeaMsgBuffWriteIdx++;
        	}
        }
        if(CM_GPS_SERIALBUFF_NUM > SerialBuffCnt){
            SerialBuff[SerialBuffCnt] = SerTmp;
            ++SerialBuffCnt;
        }else{
            SerialBuffCnt = 0;
            SerialRcvStatus = 0;
            SerialRcvStatusSub = 0;
        }
                
		if('$' == SerTmp){  // header
			SerialRcvStatus = 0;
		}
            
        switch(SerialRcvStatus){
        case 0: // rcv header
            if('$' == SerTmp){  // header
                SerialRcvStatus = 1;
                SerialBuffCntLast = 1;
                
                // not include '$'
	            fSumming = false;
                CheckSum = 0x00;
            }else{
                SerialBuffCnt = 0;
            }
            break;
        case 1: // rcv header
            fSumming = true;
            if(',' == SerTmp){
                
            	//	debugprint(SerialBuff, SerialBuffCnt-1);
            		
	            SerialRcvStatus = 0;    // header NG
                
                for(findnmea = 0; findnmea<CM_GPS_NMEA_NUM; findnmea++){
                    if( 0 == memcmp(ST_NMEA[findnmea].str_nmea+2, &SerialBuff[SerialBuffCntLast+2], 3)){	/* 下3文字を比較 */
                        RcvMessage = ST_NMEA[findnmea].e_nmea;
                        SerialRcvStatus = 2;
                        SerialBuffCntLast = SerialBuffCnt;
                        break;
                    }
                }
                
            }
            break;
            
        case 255: // checksum
		if('*' == SerTmp){
			fSumming = false;
			SerialBuffCntLast = SerialBuffCnt;
		}
		//if(0x0D == SerTmp){	// return code <CR>
		if((fSumming == false) && (SerialBuffCntLast+2 < SerialBuffCnt)){	// return code <CR>
            	
			if((SerialBuff[SerialBuffCnt-3]>='0') && (SerialBuff[SerialBuffCnt-3]<='9')){
				LastRcvCheckSum = 16*(SerialBuff[SerialBuffCnt-3] - '0');
	            	}else{
	            		LastRcvCheckSum = 16*(SerialBuff[SerialBuffCnt-3] - 'A' + 10);
	            	}
	            	if((SerialBuff[SerialBuffCnt-2]>='0') && (SerialBuff[SerialBuffCnt-2]<='9')){
	            		LastRcvCheckSum += (SerialBuff[SerialBuffCnt-2] - '0');
	            	}else{
	            		LastRcvCheckSum += (SerialBuff[SerialBuffCnt-2] - 'A' + 10);
	            	}
            	
            	
            	// update status
            	if(CheckSum == LastRcvCheckSum){
	            	// fixed data copy
	            	switch(RcvMessage){
	            	case NMEA_GPGGA:
	            		UtcTime[0].Hour = UtcTime[1].Hour;
	            		UtcTime[0].Minute = UtcTime[1].Minute;
	            		UtcTime[0].Sec = UtcTime[1].Sec;
	            		Position[0] = Position[1];
	            		IsPosValidTmp = 1;
	            		PositionFixQuality[0] = PositionFixQuality[1];
	            		NumSatellites[0] = NumSatellites[1];
	            		HDOP[0] = HDOP[1];
	            		break;
	            	case NMEA_GPRMC:
				if(rmc_validdata){
		            		UtcTime[0] = UtcTime[1];
		            		IsTimeValidTmp = 1;
		            		Position[0] = Position[1];
		            		IsPosValidTmp = 1;
				}else{
		            		IsPosValidTmp = 0;
				}
	            		break;
	            	case NMEA_GPZDA:
	            		UtcTime[0] = UtcTime[1];
	            		IsTimeValidTmp = 1;
	            		break;
	            	}
	            	
            		if(0 < PositionFixQuality[0]){
	            		NowErr = CMGPS_E_VALID_POS;
            		}else{
	            		NowErr = CMGPS_E_POS_FIX_UNAVAILABLE;
            		}
            		
            		//debugprint(SerialBuff, SerialBuffCnt-1);
	            	
	            	
            	}else{
            		if(fDebugOut){
            		//	SerDebug->println("Gps Check sum err");
            			debugprint(SerialBuff, SerialBuffCnt);
            		}
	            	NowErr = CMGPS_E_COMU_NG;
            	}
            	
            	/*
            		if(fDebugOut){
            			SerDebug->print("GPS Err");
            			SerDebug->print((uint8_t)NowErr, DEC);
            			SerDebug->print(" TimeCnt");
            			SerDebug->println(ValidTimeCnt, DEC);
            			SerDebug->print(UtcTime[0].Year, DEC);
            			SerDebug->print(UtcTime[0].Month, DEC);
            			SerDebug->print(UtcTime[0].Day, DEC);
            			SerDebug->printn(UtcTime[0].Hour, DEC);
            		}
            	*/
            	
                SerialBuffCntLast = 0;
            	SerialRcvStatus = 0;
            	SerialBuffCnt = 0;
            }
            break;
            
            
        default:    // 2~254
        
            if(NMEA_GPGGA == RcvMessage){
                switch(SerialRcvStatus){
                case 2: // Time
                    if(',' == SerTmp){
                        UtcTime[1].Hour = 10 * (SerialBuff[SerialBuffCntLast] - '0');
                        UtcTime[1].Hour += 1 * (SerialBuff[SerialBuffCntLast+1] - '0');
                        UtcTime[1].Minute = 10 * (SerialBuff[SerialBuffCntLast+2] - '0');
                        UtcTime[1].Minute += 1 * (SerialBuff[SerialBuffCntLast+3] - '0');
                        UtcTime[1].Sec = 10 * (SerialBuff[SerialBuffCntLast+4] - '0');
                        UtcTime[1].Sec += 1 * (SerialBuff[SerialBuffCntLast+5] - '0');
                        
                        SerialBuffCntLast = SerialBuffCnt;
                        SerialRcvStatus = 3;
                    }
                    break;
                case 3: // Latitude
                    if(',' == SerTmp){
                        Position[1].Latitude.d = 10 * (SerialBuff[SerialBuffCntLast] - '0');
                        Position[1].Latitude.d += 1 * (SerialBuff[SerialBuffCntLast+1] - '0');
                        Position[1].Latitude.mm = 10 * (SerialBuff[SerialBuffCntLast+2] - '0');
                        Position[1].Latitude.mm += 1 * (SerialBuff[SerialBuffCntLast+3] - '0');
                        Position[1].Latitude.mmmm = 1000 * (uint16_t)(SerialBuff[SerialBuffCntLast+5] - '0');
                        Position[1].Latitude.mmmm += 100 * (uint16_t)(SerialBuff[SerialBuffCntLast+6] - '0');
                        Position[1].Latitude.mmmm += 10 * (uint16_t)(SerialBuff[SerialBuffCntLast+7] - '0');
                        Position[1].Latitude.mmmm += 1 * (uint16_t)(SerialBuff[SerialBuffCntLast+8] - '0');
                        
                        SerialBuffCntLast = SerialBuffCnt;
                        SerialRcvStatus = 4;
                    }
                    break;
                case 4: // LatitudeHemisphere
                    if(',' == SerTmp){
                        Position[1].LatitudeHemisphere = (Orient_e)SerialBuff[SerialBuffCntLast];
                        
                        SerialBuffCntLast = SerialBuffCnt;
                        SerialRcvStatus = 5;
                    }
                    break;
                case 5: // Longitude
                    if(',' == SerTmp){
                        Position[1].Longitude.d = 100 * (SerialBuff[SerialBuffCntLast] - '0');
                        Position[1].Longitude.d += 10 * (SerialBuff[SerialBuffCntLast+1] - '0');
                        Position[1].Longitude.d += 1 * (SerialBuff[SerialBuffCntLast+2] - '0');
                        Position[1].Longitude.mm = 10 * (SerialBuff[SerialBuffCntLast+3] - '0');
                        Position[1].Longitude.mm += 1 * (SerialBuff[SerialBuffCntLast+4] - '0');
                        Position[1].Longitude.mmmm = 1000 * (uint16_t)(SerialBuff[SerialBuffCntLast+6] - '0');
                        Position[1].Longitude.mmmm += 100 * (uint16_t)(SerialBuff[SerialBuffCntLast+7] - '0');
                        Position[1].Longitude.mmmm += 10 * (uint16_t)(SerialBuff[SerialBuffCntLast+8] - '0');
                        Position[1].Longitude.mmmm += 1 * (uint16_t)(SerialBuff[SerialBuffCntLast+9] - '0');
                        
                        SerialBuffCntLast = SerialBuffCnt;
                        SerialRcvStatus = 6;
                    }
                    break;
                case 6: // LongitudeHemisphere
                    if(',' == SerTmp){
                        Position[1].LongitudeHemisphere = (Orient_e)SerialBuff[SerialBuffCntLast];
                        
                        SerialBuffCntLast = SerialBuffCnt;
                        SerialRcvStatus = 7;
                    }
                    break;
                case 7: // Position fix quality indicator
                    if(',' == SerTmp){
                        
                        PositionFixQuality[1] = SerialBuff[SerialBuffCntLast] - '0';
                        
                        SerialBuffCntLast = SerialBuffCnt;
                        SerialRcvStatus = 8;
                    }
                    break;
                case 8: // Number of satellites in use
                    if(',' == SerTmp){
                        NumSatellites[1] = 10 * (SerialBuff[SerialBuffCntLast] - '0');
                        NumSatellites[1] += 1 * (SerialBuff[SerialBuffCntLast+1] - '0');
                        
                        SerialBuffCntLast = SerialBuffCnt;
                        SerialRcvStatus = 9;
                    }
                    break;
                case 9: // Horizontal dilution of precision
                    if(',' == SerTmp){
                        HDOP[1] = 10 * (SerialBuff[SerialBuffCntLast] - '0');
                        HDOP[1] += 1 * (SerialBuff[SerialBuffCntLast+1] - '0');
                        HDOP[1] += 0.1 * (SerialBuff[SerialBuffCntLast+3] - '0');
                        
                        SerialBuffCntLast = SerialBuffCnt;
                        SerialRcvStatus = 255;
                    }
                    break;
                    
                
                default:
                    SerialRcvStatus = 0;
                    SerialBuffCnt = 0;
                    break;
                }
                
            }else if(NMEA_GPGSV == RcvMessage){
                switch(SerialRcvStatus){
                case 2: // Total number of GSV messages
                    if(',' == SerTmp){
                        
                        SerialBuffCntLast = SerialBuffCnt;
                        SerialRcvStatus ++;
                    }
                    break;
                case 3: // Number of current GSV message
                    if(',' == SerTmp){
                        
                        SerialBuffCntLast = SerialBuffCnt;
                        SerialRcvStatus ++;
                    }
                    break;
                case 4: // Satellite PRN number
                    if(',' == SerTmp){
                        
                        SerialBuffCntLast = SerialBuffCnt;
                        SerialRcvStatus ++;
                    }
                    break;
                case 5: // Satellite elevation number
                    if(',' == SerTmp){
                        
                        SerialBuffCntLast = SerialBuffCnt;
                        SerialRcvStatus ++;
                    }
                    break;
                case 6: // Satellite azimuth angle
                    if(',' == SerTmp){
                        
                        SerialBuffCntLast = SerialBuffCnt;
                        SerialRcvStatus = 255;  //  last 1 sector not .. 
                    }
                    break;
                case 7: // C/No 00~99dB
                    if(',' == SerTmp){
                        
                        
                        SerialBuffCntLast = SerialBuffCnt;
                        SerialRcvStatus = 255;
                    }
                    break;
                    
                  
                    
                
                default:
                    SerialRcvStatus = 0;
                    SerialBuffCnt = 0;
                    break;
                }
                
            }else if(NMEA_GPRMC == RcvMessage){
                switch(SerialRcvStatus){
                case 2: // UTC time in hhmmss.ss
                    if(',' == SerTmp){
                        
                        UtcTime[1].Hour = 10 * (SerialBuff[SerialBuffCntLast] - '0');
                        UtcTime[1].Hour += 1 * (SerialBuff[SerialBuffCntLast+1] - '0');
                        UtcTime[1].Minute = 10 * (SerialBuff[SerialBuffCntLast+2] - '0');
                        UtcTime[1].Minute += 1 * (SerialBuff[SerialBuffCntLast+3] - '0');
                        UtcTime[1].Sec = 10 * (SerialBuff[SerialBuffCntLast+4] - '0');
                        UtcTime[1].Sec += 1 * (SerialBuff[SerialBuffCntLast+5] - '0');
                        
                        SerialBuffCntLast = SerialBuffCnt;
                        SerialRcvStatus ++;
                    }
                    break;
                case 3: // Status, V = navigation receiver warning, A = valid position
			if(',' == SerTmp){
				rmc_validdata = ('A' == SerialBuff[SerialBuffCntLast]);
				SerialRcvStatus ++;
			}
                    break;
                case 4: // Latitude in dddmm.mmmm
                    if(',' == SerTmp){
                        Position[1].Latitude.d = 10 * (SerialBuff[SerialBuffCntLast] - '0');
                        Position[1].Latitude.d += 1 * (SerialBuff[SerialBuffCntLast+1] - '0');
                        Position[1].Latitude.mm = 10 * (SerialBuff[SerialBuffCntLast+2] - '0');
                        Position[1].Latitude.mm += 1 * (SerialBuff[SerialBuffCntLast+3] - '0');
                        Position[1].Latitude.mmmm = 1000 * (uint16_t)(SerialBuff[SerialBuffCntLast+5] - '0');
                        Position[1].Latitude.mmmm += 100 * (uint16_t)(SerialBuff[SerialBuffCntLast+6] - '0');
                        Position[1].Latitude.mmmm += 10 * (uint16_t)(SerialBuff[SerialBuffCntLast+7] - '0');
                        Position[1].Latitude.mmmm += 1 * (uint16_t)(SerialBuff[SerialBuffCntLast+8] - '0');
                        
                        SerialBuffCntLast = SerialBuffCnt;
                        SerialRcvStatus ++;
                    }
                    break;
                case 5: // Latitude hemisphere indicator
                    if(',' == SerTmp){
                        
                        Position[1].LatitudeHemisphere = (Orient_e)SerialBuff[SerialBuffCntLast];
                        
                        SerialBuffCntLast = SerialBuffCnt;
                        SerialRcvStatus ++;
                    }
                    break;
                case 6: // Longitude in dddmm.mmmm
                    if(',' == SerTmp){
                        Position[1].Longitude.d = 100 * (SerialBuff[SerialBuffCntLast] - '0');
                        Position[1].Longitude.d += 10 * (SerialBuff[SerialBuffCntLast+1] - '0');
                        Position[1].Longitude.d += 1 * (SerialBuff[SerialBuffCntLast+2] - '0');
                        Position[1].Longitude.mm = 10 * (SerialBuff[SerialBuffCntLast+3] - '0');
                        Position[1].Longitude.mm += 1 * (SerialBuff[SerialBuffCntLast+4] - '0');
                        Position[1].Longitude.mmmm = 1000 * (uint16_t)(SerialBuff[SerialBuffCntLast+6] - '0');
                        Position[1].Longitude.mmmm += 100 * (uint16_t)(SerialBuff[SerialBuffCntLast+7] - '0');
                        Position[1].Longitude.mmmm += 10 * (uint16_t)(SerialBuff[SerialBuffCntLast+8] - '0');
                        Position[1].Longitude.mmmm += 1 * (uint16_t)(SerialBuff[SerialBuffCntLast+9] - '0');
                        
                        SerialBuffCntLast = SerialBuffCnt;
                        SerialRcvStatus ++;
                    }
                    break;
                case 7: // Longitude hemisphere indicator
                    if(',' == SerTmp){
                        
                        Position[1].LongitudeHemisphere = (Orient_e)SerialBuff[SerialBuffCntLast];
                        
                        SerialBuffCntLast = SerialBuffCnt;
                        SerialRcvStatus ++;
                    }
                    break;
                case 8: // Speed over ground
                    if(',' == SerTmp){
                        
                        
                        SerialBuffCntLast = SerialBuffCnt;
                        SerialRcvStatus ++;
                    }
                    break;
                case 9: // Course over ground
                    if(',' == SerTmp){
                        
                        
                        SerialBuffCntLast = SerialBuffCnt;
                        SerialRcvStatus ++;
                    }
                    break;
                case 10: // UTC date of position fix
                    if(',' == SerTmp){
                        UtcTime[1].Day = 10 * (SerialBuff[SerialBuffCntLast] - '0');
                        UtcTime[1].Day += 1 * (SerialBuff[SerialBuffCntLast+1] - '0');
                        UtcTime[1].Month = 10 * (SerialBuff[SerialBuffCntLast+2] - '0');
                        UtcTime[1].Month += 1 * (SerialBuff[SerialBuffCntLast+3] - '0');
                        UtcTime[1].Year = 10 * (SerialBuff[SerialBuffCntLast+4] - '0');
                        UtcTime[1].Year += 1 * (SerialBuff[SerialBuffCntLast+5] - '0');
                        UtcTime[1].Year += 2000;
                        
                        SerialBuffCntLast = SerialBuffCnt;
                        SerialRcvStatus ++;
                    }
                    break;
                case 11: // Magnetic variation
                    if(',' == SerTmp){
                        
                        
                        SerialBuffCntLast = SerialBuffCnt;
                        SerialRcvStatus ++;
                    }
                    break;
                case 12: // Magnetic variation direction
                    if(',' == SerTmp){
                        
                        
                        SerialBuffCntLast = SerialBuffCnt;
                        SerialRcvStatus = 255;  //  last 1 sector not .. 
                    }
                    break;
                case 13: // Mode indicator
                    if(',' == SerTmp){
                        
                        
                        SerialBuffCntLast = SerialBuffCnt;
                        SerialRcvStatus = 255;
                    }
                    break;
                    
                
                    
                
                default:
                    SerialRcvStatus = 0;
                    SerialBuffCnt = 0;
                    break;
                }
                
            }else if(NMEA_GPZDA == RcvMessage){
                
                
                switch(SerialRcvStatus){
                case 2: // UTC time in hhmmss.ss
                    if(',' == SerTmp){
                        UtcTime[1].Hour = 10 * (SerialBuff[SerialBuffCntLast] - '0');
                        UtcTime[1].Hour += 1 * (SerialBuff[SerialBuffCntLast+1] - '0');
                        UtcTime[1].Minute = 10 * (SerialBuff[SerialBuffCntLast+2] - '0');
                        UtcTime[1].Minute += 1 * (SerialBuff[SerialBuffCntLast+3] - '0');
                        UtcTime[1].Sec = 10 * (SerialBuff[SerialBuffCntLast+4] - '0');
                        UtcTime[1].Sec += 1 * (SerialBuff[SerialBuffCntLast+5] - '0');
                        
                        SerialBuffCntLast = SerialBuffCnt;
                        SerialRcvStatus = 3;
                    }
                    break;
                case 3: // UTC time day
                    if(',' == SerTmp){
                        UtcTime[1].Day = 10 * (SerialBuff[SerialBuffCntLast] - '0');
                        UtcTime[1].Day += 1 * (SerialBuff[SerialBuffCntLast+1] - '0');
                        
                        SerialBuffCntLast = SerialBuffCnt;
                        SerialRcvStatus++;
                    }
                    break;
                case 4: // UTC time month
                    if(',' == SerTmp){
                        UtcTime[1].Month = 10 * (SerialBuff[SerialBuffCntLast] - '0');
                        UtcTime[1].Month += 1 * (SerialBuff[SerialBuffCntLast+1] - '0');
                        
                        SerialBuffCntLast = SerialBuffCnt;
                        SerialRcvStatus++;
                    }
                    break;
                case 5: // UTC time year
                    if(',' == SerTmp){
                        UtcTime[1].Year = 1000 * (SerialBuff[SerialBuffCntLast] - '0');
                        UtcTime[1].Year += 100 * (SerialBuff[SerialBuffCntLast+1] - '0');
                        UtcTime[1].Year += 10 * (SerialBuff[SerialBuffCntLast+2] - '0');
                        UtcTime[1].Year += 1 * (SerialBuff[SerialBuffCntLast+3] - '0');
                        
                        SerialBuffCntLast = SerialBuffCnt;
                        SerialRcvStatus = 255;
                    }
                    break;
                
                    
                
                default:
                    SerialRcvStatus = 0;
                    SerialBuffCnt = 0;
                    break;
                
                }
            }else{
                // not exec
                SerialRcvStatus = 255;
            }
        }
        
        if(fSumming){	
	        CheckSum ^= SerTmp;
        }
        
        /*
        // print rcvstatus
        static uint8_t SerialRcvStatusLast;
        if(fDebugOut && (SerialRcvStatus!=SerialRcvStatusLast)){
            SerDebug->print("st");
            SerDebug->print(SerialRcvStatus, DEC);
            SerDebug->print(" ");
            SerDebug->println(SerialBuffCnt, DEC);
        }
        SerialRcvStatusLast = SerialRcvStatus;
        */
        
    }
    
    if(0 != IsTimeValidTmp){
        IsTimeValid = 1;
        IsTimeValidTmp = 0;
        ValidTimeCnt = 0;
    }else{
        if(ValidTimeCnt < CM_GPS_VALID_TIME_CNT){
            ValidTimeCnt++;
        }else{
            IsTimeValid = 0;
	    Ser->celarRcvErr();		/* 受信死んでる系なのかもなのでエラークリア */
        }
    }
    if(0 != IsPosValidTmp){
        IsPosValid = 1;
        IsPosValidTmp = 0;
        ValidPosCnt = 0;
    }else{
        if(ValidPosCnt < CM_GPS_VALID_POS_CNT){
            ValidPosCnt++;
        }else{
            IsPosValid = 0;
	    Ser->celarRcvErr();		/* 受信死んでる系なのかもなのでエラークリア */
        }
    }
    

}


// set the Msg buffer to write.
// input : (return) NMEA message buffer pointer
// return : Num of Message
uint16_t CanMoreGt723F_t::getNmeaBuff(uint8_t **Buff){
	uint16_t idxtmp;
	*Buff = NmeaMsgBuff;
	idxtmp = NmeaMsgBuffWriteIdx;
	NmeaMsgBuffWriteIdx = 0;		// reset
	
	return idxtmp;
};
    
   


void CanMoreGt723F_t::debugprint(uint8_t *buff, uint8_t Num){
    uint8_t i;
    
    if(fDebugOut){
        for(i=0; i<Num; i++){
        //	SerDebug->write(buff[i]);
        }
       // SerDebug->print("\r\n");
    }
}


void CanMoreGt723F_t::testDefaultVal(void){
    
    NowErr = CMGPS_E_TESTMODE;
    
    UtcTime[0].Year = 2017;
    UtcTime[0].Month = 1;
    UtcTime[0].Day = 13;
    UtcTime[0].Hour = 23;
    UtcTime[0].Minute = 7;
    UtcTime[0].Sec = 12;
    
    Position[0].Latitude.d = 36;
    Position[0].Latitude.mm = 11;
    Position[0].Latitude.mmmm = 3100;
    Position[0].LatitudeHemisphere = N;
    
    Position[0].Longitude.d = 140;
    Position[0].Longitude.mm = 13;
    Position[0].Longitude.mmmm = 9639;
    Position[0].LongitudeHemisphere = E;
    
    NumSatellites[0] = 5;
    
}
