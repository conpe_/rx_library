/********************************************/
/*	SakuraのIoTモジュール					*/
/*		for RX63n @ CS+						*/
/*		Wrote by conpe_						*/
/*							2018/02/08		*/
/********************************************/

// update
// 2018.02.08 new



#include "SakuraIot.h"



/*********************
さくらのIoTモジュール
コンストラクタ
引数：	I2C
**********************/
sakuraiot_t::sakuraiot_t(void){
	
	TaskCnt = 0;
	ConnectStatus = CONSTA_DISCONNECT;
	
	fLastAttachComuFin = true;
	I2cAddress = SAKURAIOT_I2C_ADDRESS_DEFAULT;
	I2Cn = NULL;
	LastAttachComu = NULL;

	SignalQuality = 0;
	TxStatus = SAKURAIOT_TXSTA_IDLE;
	TxStatusImm = SAKURAIOT_TXSTA_IDLE;
	
	FirmWareVersionStrLen = 0;
}
/*********************
デストラクタ
**********************/
sakuraiot_t::~sakuraiot_t(void){
}


/*********************
セットアップ
概要：
 モジュール起こす
引数：
 
**********************/
int8_t sakuraiot_t::begin(I2c_t* I2Cn, uint8_t I2cAddress){
	sakuraiot_t::I2Cn = I2Cn;
	sakuraiot_t::I2cAddress = I2cAddress;
	
	// I2C開始
	I2Cn->begin(400, 32);
	I2Cn->sendStopCondBetweenTransmitReceive();
	
	updateGetFirmwareVersion();
	
	return 0;
}



/*********************
タスク
概要：
 周期処理
引数：
 
**********************/
int8_t sakuraiot_t::task_20ms(void){

	// I2c_t::task();		// I2Cタスク
					// つながってるのがこれだけとは限らないので他のところでやる
	// 1secタスク
	if(++TaskCnt >= 5){
		
		updateConnectionStatus();
		updateSignalQuality();
		updateTxStatus();
		
		updateGetFirmwareUpdateStatus();
		/* ファームウェアアップデート後，updateFirmwareVersion();を実行したい */
		updateGetFirmwareVersion();
		
		TaskCnt = 0;
	}
	
	return 0;
}



/*********************
接続ステータス更新
概要：
引数：
返値：
**********************/
uint8_t sakuraiot_t::updateConnectionStatus(void){
	uint8_t dummy;
	return attachI2cComu(SAKURAIOT_GET_STATUS, &dummy, 0, 1);
}

/*********************
信号強度更新
概要：
引数：
返値：
**********************/
uint8_t sakuraiot_t::updateSignalQuality(void){
	uint8_t dummy;
	return attachI2cComu(SAKURAIOT_GET_SIGNAL_QUALITY, &dummy, 0, 1);
}

/*********************
送信キューへの追加
概要：
引数： ch 0x00〜0x7F
返値：
**********************/
uint8_t sakuraiot_t::enqueueTxRaw(uint8_t ch, uint8_t type, uint8_t length, uint8_t *data, uint64_t offset){
	uint8_t txdata[18];
	uint8_t txnum = 10;
	
	if(0x7F < ch){
		return 1;
	}
	txdata[0] = ch;
	txdata[1] = type;
	memcpy(&txdata[2], data, length);
	if(0!=offset){
		txnum = 18;
		memcpy(&txdata[10], (uint8_t *)&offset, 8);
	}
	
	return attachI2cComu(SAKURAIOT_TX_ENQUEUE, txdata, txnum, 0);
}

uint8_t sakuraiot_t::enqueueTx(uint8_t ch, int32_t value, uint64_t offset){
	return enqueueTxRaw(ch, 'i', 4, (uint8_t *)&value, offset);
}
uint8_t sakuraiot_t::enqueueTx(uint8_t ch, uint32_t value, uint64_t offset){
	return enqueueTxRaw(ch, 'I', 4, (uint8_t *)&value, offset);
}
uint8_t sakuraiot_t::enqueueTx(uint8_t ch, int64_t value, uint64_t offset){
	return enqueueTxRaw(ch, 'l', 8, (uint8_t *)&value, offset);
}
uint8_t sakuraiot_t::enqueueTx(uint8_t ch, uint64_t value, uint64_t offset){
	return enqueueTxRaw(ch, 'L', 8, (uint8_t *)&value, offset);
}
uint8_t sakuraiot_t::enqueueTx(uint8_t ch, float value, uint64_t offset){
	return enqueueTxRaw(ch, 'f', 4, (uint8_t *)&value, offset);
}
uint8_t sakuraiot_t::enqueueTx(uint8_t ch, double value, uint64_t offset){
	return enqueueTxRaw(ch, 'd', 8, (uint8_t *)&value, offset);
}
uint8_t sakuraiot_t::enqueueTx(uint8_t ch, uint8_t value[8], uint64_t offset){
	return enqueueTxRaw(ch, 'b', 8, (uint8_t *)value, offset);
}




/*********************
送信キュークリア
概要：
引数：
返値：
**********************/
uint8_t sakuraiot_t::flushTxQueue(void){
	uint8_t dummy;
	return attachI2cComu(SAKURAIOT_TX_QUEUE_FLUSH, &dummy, 0, 0);
}

/*********************
送信キュー送信
概要：
引数：
返値：
**********************/
uint8_t sakuraiot_t::sendTxQueue(void){
	uint8_t dummy;
	
	TxStatus = SAKURAIOT_TXSTA_TRANSMITTING;
	
	return attachI2cComu(SAKURAIOT_TX_QUEUE_SEND, &dummy, 0, 0);
}



/*********************
送信ステータス取得
概要：
引数：
返値：
**********************/
uint8_t sakuraiot_t::updateTxStatus(void){
	uint8_t dummy;
	return attachI2cComu(SAKURAIOT_GET_TX_STATUS, &dummy, 0, 2);
}



/*********************
ファームウェアバージョン取得
概要：
引数：
返値： 取得済みなら0
**********************/
uint8_t sakuraiot_t::getFirmwareVersion(uint8_t** VerStr, uint8_t* VerStrLen){
	*VerStr = FirmWareVersionStr;
	*VerStrLen = FirmWareVersionStrLen;
	
	return 0==FirmWareVersionStrLen;
}

/*********************
ファームウェアバージョン取得
概要：
引数：
返値：
**********************/
uint8_t sakuraiot_t::updateGetFirmwareVersion(void){
	uint8_t dummy;
	return attachI2cComu(SAKURAIOT_GET_FIRMWARE_VERSION, &dummy, 0, 32);	/* 最大32バイト */
}

/*********************
Unlockコマンド送信
概要：
引数：
返値：
**********************/
uint8_t sakuraiot_t::sendUnlock(void){
	uint8_t UnlockCode[4] = {0x53, 0x6B, 0x72, 0x61};
	return attachI2cComu(SAKURAIOT_SEND_UNLOCK, UnlockCode, 4, 0);
}

/*********************
ファームウェアアップデート
概要：
引数：
返値：
**********************/
uint8_t sakuraiot_t::updateFirmware(void){
	uint8_t dummy;
	
	sendUnlock();		/* ファームウェアアップデートにはアンロックが必要 */
	return attachI2cComu(SAKURAIOT_UPDATE_FIRMWARE, &dummy, 0, 0);
}

/*********************
ファームウェアアップデート状況取得
概要：
引数：
返値：
**********************/
uint8_t sakuraiot_t::updateGetFirmwareUpdateStatus(void){
	uint8_t dummy;
	return attachI2cComu(SAKURAIOT_GET_FIRMWARE_UPDATE_STATUS, &dummy, 0, 1);
}


/*********************
ソフトウェアリセット
概要：
引数：
返値：
**********************/
uint8_t sakuraiot_t::reqSoftwareReset(void){
	uint8_t dummy;
	
	sendUnlock();		/* リセットにはアンロックが必要 */
	return attachI2cComu(SAKURAIOT_REQ_SOFTWARE_RESET, &dummy, 0, 0);
}


/*********************
省電力モードセット
概要：
引数：
	0: 省電力無効 (起動，WAKE_INするたびこの値に戻る)
	1: 自動スリープモード
返値：
**********************/
uint8_t sakuraiot_t::setPowerSaveMode(uint8_t mode){
	return attachI2cComu(SAKURAIOT_SET_POWER_SAVE_MODE, &mode, 1, 0);
}

/*********************
省電力モード取得
概要：
引数：
返値：
**********************/
uint8_t sakuraiot_t::updateGetPowerSaveMode(void){
	uint8_t dummy;
	return attachI2cComu(SAKURAIOT_GET_POWER_SAVE_MODE, &dummy, 0, 1);
}

/*********************
I2Cクラスに通信をアタッチする
概要：
 通信を開始する。
 終わるとfetchI2cRcvDataが呼ばれる。
引数：
	ComuType
	TxData データ部
	TxNum 送信データ長(そのうちなくす)
	RxNum 送信データ長(そのうちなくす)
	Seq：0 送信のみ，1 受信のみ，それ以外 両方連続
返値：
 0 成功
 それ以外 失敗
**********************/
int8_t sakuraiot_t::attachI2cComu(sakuraiot_comu_content ComuType, uint8_t* TxData, uint16_t TxDataNum, uint16_t RxDataNum){
	int8_t AttachedIndex = 0;
	sakuraiot_comus_t* NewComu;
	uint8_t TxDataPari[31] = {0};
	uint8_t i;
	uint8_t Pari = 0x00;
	
	/* 送受信情報をテーブルからセット */
	if(NULL == TxData){
		// TxData
	}
	if(0xFF == TxDataNum){
		// TxNum = 0;
	}
	if(0xFF == RxDataNum){
		// RxNum = 0;
	}
	
	
	/* 定形部セット */
	TxDataPari[0] = (uint8_t)ComuType;		/* 要求種別 */
	TxDataPari[1] = TxDataNum;			/* 引数データ長 */
	/* データ部コピー */
	memcpy(&TxDataPari[2], TxData, TxDataNum);
	/* パリティ付加 */
	for(i=0; i<TxDataNum+2; ++i){
		Pari ^= TxDataPari[i];
	}
	//if(TxDataNum<1){	// 最低1
	//	TxDataNum = 1;
	//}
	TxDataPari[TxDataNum+2] = Pari;
	
	// データ内容，データ長，パリティの分を足す
	TxDataNum += 3;
	RxDataNum += 3;
	
	NewComu = new sakuraiot_comus_t(this, I2cAddress, ComuType, TxDataPari, TxDataNum, RxDataNum);
	if(NULL == NewComu){
		
		/* 多分通信死んでるのでリセットかける */
		reqSoftwareReset();
		
		return -1;	// ヒープ足りない？
	}
	
	AttachedIndex = I2Cn->attach(NewComu);		// 送受信登録
	if(AttachedIndex<0){	// 登録失敗したらなかったことに
		
		/* 多分通信死んでるのでリセットかける */
		reqSoftwareReset();
		
		delete NewComu;
	}
	LastAttachComu = NewComu;
	fLastAttachComuFin = false;
	
	return 0 > AttachedIndex;
}


/*********************
I2C読み終わったので捕獲
概要：
引数：	
**********************/
int8_t sakuraiot_t::fetchI2cRcvData(const sakuraiot_comus_t* Comu){
	uint8_t* RcvData = &Comu->RxData[2];
	uint8_t ErrCode;
	bool_t IsConnect;
	
	if(NULL!=Comu){
		
		// 通信完了フラグ立てる
		if(LastAttachComu == Comu){
			fLastAttachComuFin = true;
		}
		
		if(!Comu->Err){
			
			// 通信内容によって受信データを処理する
			switch(Comu->ComuType){
			case SAKURAIOT_GET_STATUS:		/* ステータス取得 */
				ErrCode = *RcvData & 0x7F;
				IsConnect = (*RcvData & 0x80) >> 7;
				
				if(1==IsConnect){
					ConnectStatus = CONSTA_CONNECT;		/* 接続ok */
				}else{
					ConnectStatus = (connection_status_e)ErrCode;	/* 接続NG(エラーコードを格納) */
				}
				break;
			case SAKURAIOT_GET_SIGNAL_QUALITY:		/* ステータス取得 */
				SignalQuality = *RcvData;
				break;
			case SAKURAIOT_GET_TX_STATUS:		/* ステータス取得 */
				TxStatus = (sakuraiot_tx_status)RcvData[0];
				TxStatusImm = (sakuraiot_tx_status)RcvData[1];
				break;
			case SAKURAIOT_GET_FIRMWARE_VERSION:
				FirmWareVersionStrLen = Comu->RxData[1];
				memcpy(FirmWareVersionStr, RcvData, FirmWareVersionStrLen);
				FirmWareVersionStr[FirmWareVersionStrLen] = '\0';
				break;
			case SAKURAIOT_SEND_UNLOCK:
				SendUnlockResult = Comu->RxData[0];
				break;
			case SAKURAIOT_UPDATE_FIRMWARE:
				FirmwareUpdateCmdResult = Comu->RxData[0];
				break;
			case SAKURAIOT_GET_FIRMWARE_UPDATE_STATUS:
				FirmwareUpdateStatusCmdResult = Comu->RxData[0];
				FirmwareUpdateStatus = RcvData[0];
				break;
			case SAKURAIOT_SET_POWER_SAVE_MODE:
				if(0x01 == Comu->RxData[0]){	/* 送信正常ならば */
					PowerSaveMode = Comu->TxData[2];	/* 内部変数更新 */
				}
				break;
			case SAKURAIOT_GET_POWER_SAVE_MODE:
				PowerSaveMode = RcvData[0];
				break;
			default:
				break;
			}
		}else{
		}
		
		return 0;
	}else{
		return -1;
	}
}

/*********************
概要：
引数：
	TxDataNum：送信データ数
	RxDataNum：受信データ数
		いずれもデータ部のみ。実際に送受信するのは，種別，データ長，パリティで+3byte。
**********************/
sakuraiot_comus_t::sakuraiot_comus_t(sakuraiot_t* Parent, uint8_t DestAddress, sakuraiot_comu_content ComuType, uint8_t* TxData, uint16_t TxNum, uint16_t RxNum)
	:I2c_comu_t(DestAddress, TxData, TxNum, RxNum){
		
	this->Parent = Parent;
	this->ComuType = ComuType;
}


