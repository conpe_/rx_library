/********************************************/
/*	TexasInstruments TCA**** (I2C IO Expander)	*/
/*		for RX63n @ CS+			*/
/*		Wrote by conpe_			*/
/*			2017/05/31		*/
/********************************************/

//【使い方】
// 

// 角度はdeg

// デフォルト設定
// I2Cアドレス 0b11100xx (デバイス，ADピン設定による)
// 

//【更新履歴】
// 2017.05.31 新規作成


#ifndef __TEXASINSTRUMENTS_TCA_H__
#define __TEXASINSTRUMENTS_TCA_H__

#include "RIIC.h"
#include "CommonDataType.h"
extern uint32_t getTime_ms(void);	// 時間計測


// I2Cアドレス
#define TITCA_I2C_ADDRESS_DEFAULT 0x70	// TCA9538


enum titca_comu_content{
	TITCA_CMD_INPUT = 0,
	TITCA_CMD_OUTPUT = 1,
	TITCA_CMD_POLINV = 2,
	TITCA_CMD_CONFIG = 3,
};

class titca_comus_t;

class ti_tca_t{
public:
	// コンストラクタ
	ti_tca_t(I2c_t* I2Cn, uint8_t I2cAddress = TITCA_I2C_ADDRESS_DEFAULT, uint8_t IoNum = 8);
	~ti_tca_t(void);
	

	// モジュールセットアップ
	int8_t begin(uint16_t PortDir);
	
	// IO方向設定
	int8_t setDir(uint16_t PortDir);
	
	// ポート入力
	int8_t readReqPins(void);	// 受信要求
	int16_t readPins(void);		// 受信データを返す
	bool_t readPin(uint8_t Port);	// 受信データを返す(ピン選択)
	
	// ポート出力
	int8_t writePins(uint16_t dat);
	int8_t writePin(uint8_t Port, bool_t dat);
	
	
	// 割り込み用
	// I2C受信データを取得して通信内容ごとに処理するよ
	int8_t fetchI2cRcvData(const titca_comus_t* Comu);
	
private:
	I2c_t *I2Cn;
	uint8_t I2cAddress;		// I2Cアドレス
	uint8_t IoNum;			// 入出力ピン数
	uint16_t PortDir;		// ポート方向(1:input, 0:output)
	uint16_t PortDat;		// ポート状態
	
	
	// I2Cクラスに通信内容をアタッチ
	int8_t attachI2cComu(titca_comu_content ComuType, uint8_t* TxData, uint16_t TxNum, uint16_t RxNum);
	titca_comus_t* LastAttachComu;		// 最後に通信要求した通信
	bool_t fLastAttachComuFin;		// 最後に通信要求した通信が終わったかフラグ
};

class titca_comus_t:public I2c_comu_t{
public:
	titca_comus_t(uint8_t DestAddress, uint8_t* TxData, uint16_t TxNum, uint16_t RxNum)	: I2c_comu_t(DestAddress, TxData, TxNum, RxNum){};	// 送受信
	
	ti_tca_t* TITCA;
	titca_comu_content ComuType;

	void callBack(void){TITCA->fetchI2cRcvData(this);};
	
};

#endif
