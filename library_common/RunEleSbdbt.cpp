/********************************************/
/*	ランニングエレクトロニクス SBDBT		*/
/*					for RX63n @ CS+			*/
/*					Wrote by conpe_			*/
/*							2017/01/31		*/
/********************************************/

#include "RunEleSbdbt.h"

#define SBDBT_PS3CON_LONGPUSH_CNT 50	// 50 : 20msec周期で2secくらい

/*********************
SBDBT
**********************/
runele_sbdbt_t::runele_sbdbt_t(Sci_t *SciObject){
	Sci = SciObject;
	Mode = PC_THROUGH;
	Ps3ConAvailable = false;
	Ps3ConDisableCnt = 0;
};

/*********************
通信データ解析
概要：
	モードに応じて受信バッファを解釈する
	定期的に呼び出すこと
**********************/
void runele_sbdbt_t::task(void){
	uint8_t cnt;
	uint8_t tmp[8];
	uint8_t Sum = 0;
	
	switch(Mode){
	case PS3:
		if(Ps3ConDisableCnt>SBDBT_PS3CON_DISABLECNT){
			Ps3ConAvailable = false;
		}else{
			Ps3ConDisableCnt ++;
		}
		// 受信データをコントローラデータにぶっこむ
		while(SBDBT_PS3CON_DATALENGTH <= Sci->available()){	// 受信数がコントローラ全データ数以上だったら
			// データ先頭候補を見つける
			tmp[0] = 0xFF;
			while(0x80 != tmp[0]){
				if(Sci->receive(&tmp[0])){
					return;		// 先頭バイト見つかることなく受信バッファ無くなった
							// 正常な限りありえない
				}
			}
			
			// RCB3フォーマット分読む
			Sum = 0;
			for(cnt=1;cnt<7;cnt++){		// 1~6byte
				Sci->receive(&tmp[cnt]);
				Sum += tmp[cnt];
			}
			Sci->receive(&tmp[cnt]);	// 7byte
			Sum = Sum&0x7F;	// チェックサムは下位7bit
			
			
			// チェックサム確認
			if(tmp[7] == Sum){
				// サムok
				Ps3ConAvailable = true;
				Ps3ConDisableCnt = 0;		// コントローラ無効カウントリセット
				// 残りのデータを突っ込む
				for(cnt=8;cnt<SBDBT_PS3CON_DATALENGTH;cnt++){
					Sci->receive(&ControllerData.Data[cnt]);
				}
				
				setLongPushCnt();
				
			}else{
				// サムng	
				// 次のデータ先頭候補まで削って終わる
				while(1){
					if(Sci->watch(&tmp[0])){
						return;		// バッファなし
					}
					if(0x80 == tmp[0]){	// 先頭バイト発見
						break;		// 
					}else{			// 先頭バイトじゃない
						Sci->receive(&tmp[0]);	// 消す
					}
				}
			}
		}
		
		break;
	case PC:
		
		break;
	case PC_THROUGH:
		// nothing to do
		break;
	default:
		// nothing to do
		break;
	}
	
}


void runele_sbdbt_t::setLongPushCnt(void){
	setLongPushCntSub(ControllerData.button.Select,		PS3_SELECT);
	setLongPushCntSub(ControllerData.button.Start,		PS3_START);
	setLongPushCntSub(ControllerData.button.L1,		PS3_L1);
	setLongPushCntSub(ControllerData.button.R1,		PS3_R1);
	setLongPushCntSub(ControllerData.button.L2,		PS3_L2);
	setLongPushCntSub(ControllerData.button.R2,		PS3_R2);
	setLongPushCntSub(ControllerData.button.L3, 		PS3_L3);
	setLongPushCntSub(ControllerData.button.R3, 		PS3_R3);
	setLongPushCntSub(ControllerData.button.Up, 		PS3_UP);
	setLongPushCntSub(ControllerData.button.Down, 		PS3_DOWN);
	setLongPushCntSub(ControllerData.button.Left, 		PS3_LEFT);
	setLongPushCntSub(ControllerData.button.Right, 		PS3_RIGHT);
	setLongPushCntSub(ControllerData.button.Triangle, 		PS3_TRIANGLE);
	setLongPushCntSub(ControllerData.button.Circle, 	PS3_CIRCLE);
	setLongPushCntSub(ControllerData.button.Cross,		PS3_CROSS);
	setLongPushCntSub(ControllerData.button.Square, 	PS3_SQUARE);
	setLongPushCntSub(ControllerData.button.Playstation, 	PS3_PLAYSTATION);
	
}
void runele_sbdbt_t::setLongPushCntSub(bool_t ButtonData, runele_ps3_button_e Button){
	if((uint8_t)Button < SBDBT_PS3CON_BUTTON_NUM){
		if(ButtonData){		// 押してたらカウントアップ
			if(LongPushCnt[(uint8_t)Button] < SBDBT_PS3CON_LONGPUSH_CNT){
				LongPushCnt[(uint8_t)Button] ++;
			}
		}else{			// 押してなかったらクリア
			LongPushCnt[(uint8_t)Button] = 0;
		}
	}
}

// モードをセット
void runele_sbdbt_t::setMode(mode_t NewMode){
	
	if(Mode!=NewMode){	// モード変わったらバッファクリア
		Sci->clearRxBuff();
	}
	
	Mode = NewMode;
}

// 押されてる？
// 引数なしならどのボタンが押されていても1
bool_t runele_sbdbt_t::isPush(runele_ps3_button_e Button){
	uint8_t SrcBtn;
	
	if(PS3_ANY != Button){
		if((uint8_t)Button < SBDBT_PS3CON_BUTTON_NUM){
			return 0<LongPushCnt[(uint8_t)Button];
		}
	}else{
		for(SrcBtn = 0; SBDBT_PS3CON_BUTTON_NUM > SrcBtn; ++SrcBtn){
			if(0<LongPushCnt[SrcBtn]){
				return true;
			}
		}
	}
	
	return false;
}

// 長押しされてる？
// 引数なしならどのボタンが長押しされていても1
bool_t runele_sbdbt_t::isLongPush(runele_ps3_button_e Button){
	uint8_t SrcBtn;
	
	if(PS3_ANY != Button){
		if((uint8_t)Button < SBDBT_PS3CON_BUTTON_NUM){
			return SBDBT_PS3CON_LONGPUSH_CNT <= LongPushCnt[(uint8_t)Button];
		}
	}else{
		for(SrcBtn = 0; SBDBT_PS3CON_BUTTON_NUM>SrcBtn; ++SrcBtn){
			if(SBDBT_PS3CON_LONGPUSH_CNT <= LongPushCnt[SrcBtn]){
				return true;
			}
		}
	}
	
	return false;
}


