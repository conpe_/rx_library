/********************************************/
/*	TexasInstruments TCA**** (I2C IO Expander)	*/
/*		for RX63n @ CS+			*/
/*		Wrote by conpe_			*/
/*			2017/05/31		*/
/********************************************/



#include "TiTca.h"


// 通信プロトコル
// アドレスバイト
// コマンドバイト 0x00:read Input, 0x01:write Output, 0x02:PolarityInversion, 0x03:configration(1:input, 0:output)
// データ



/*********************
IOエキスパンダ(I2C初期化)
コンストラクタ
引数：	I2C
	IoNum : 8 or 16
**********************/
ti_tca_t::ti_tca_t(I2c_t* I2Cn, uint8_t I2cAddress, uint8_t IoNum){
	ti_tca_t::I2Cn = I2Cn;
	ti_tca_t::I2cAddress = I2cAddress;
	ti_tca_t::IoNum = IoNum;
	
	PortDir = 0xFFFF;
	PortDat = 0x0000;
	
}
/*********************
デストラクタ
**********************/
ti_tca_t::~ti_tca_t(void){
}


/*********************
セットアップ
概要：
 モジュール起こす
 ISMPU_REG_PWR_MGMT_1に0x00を書く
引数：
 
**********************/
int8_t ti_tca_t::begin(uint16_t PortDir){
	
	// I2C開始
	I2Cn->begin(400, 32);
	
	// ポート入出力方向セット
	return setDir(PortDir);	
}



/*********************
IOピン入出力方向設定
概要：
引数：
**********************/
int8_t ti_tca_t::setDir(uint16_t PortDir){
	uint8_t TxData[3];
	uint16_t TxNum;
	
	// I2Cあたっち準備
	TxData[0] = (uint8_t)TITCA_CMD_CONFIG;
	TxData[1] = PortDir&0x00FF;
	TxData[2] = (PortDir>>8)&0x00FF;
	TxNum = (IoNum-1)/8+2;
	
	ti_tca_t::PortDir = PortDir;
	
	return attachI2cComu(TITCA_CMD_CONFIG, TxData, TxNum, 0);
}
/*********************
IOピン入力
概要：
引数：
**********************/
int8_t ti_tca_t::readReqPins(void){
	uint8_t TxData;
	uint16_t RxNum;
	
	TxData = (uint8_t)TITCA_CMD_CONFIG;
	RxNum = (IoNum-1)/8+1;
	
	return attachI2cComu(TITCA_CMD_INPUT, &TxData, 1, RxNum);
}
int16_t ti_tca_t::readPins(void){
	return PortDat;
}
bool_t ti_tca_t::readPin(uint8_t Port){
	return (PortDat>>Port)&0x01;
}

/*********************
IOピン出力
概要：
引数：
**********************/
int8_t ti_tca_t::writePins(uint16_t dat){
	int8_t ret;
	uint8_t TxData[3];
	uint16_t TxNum;
	
	// Outputに設定されているポートのみ適用する
	dat = (~PortDir) & dat;
	
	TxData[0] = (uint8_t)TITCA_CMD_OUTPUT;
	TxData[1] = dat&0x00FF;
	TxData[2] = (dat>>8)&0x00FF;
	TxNum = (IoNum-1)/8+2;
	
	ret = attachI2cComu(TITCA_CMD_OUTPUT, TxData, TxNum, 0);
	if(0<=ret){
		PortDat = (PortDat&PortDir) | dat;	// Outputに設定されているポートのみ更新
	}
	return ret;
}
// 単ピン
int8_t ti_tca_t::writePin(uint8_t Port, bool_t Data){
	uint16_t PortDat16;
	
	if(IoNum<=Port){	// IO数を超えているよ
		return -1;
	}
	
	if((PortDir>>Port)&0x01){	// そのポートは入力だよ 
		return -1;
	}
	
	PortDat16 = PortDat;
	PortDat16 = (PortDat16&(~(0x0001<<Port))) | (Data<<Port);
	return writePins(PortDat16);
}




/*********************
I2Cクラスに通信をアタッチする
概要：
 通信を開始する。
 終わるとfetchI2cRcvDataが呼ばれる。
引数：

返値：
 AttachedIndex
**********************/
int8_t ti_tca_t::attachI2cComu(titca_comu_content ComuType, uint8_t* TxData, uint16_t TxNum, uint16_t RxNum){
	int8_t AttachedIndex = 0;
	
	titca_comus_t* NewComu;
	
	NewComu = new titca_comus_t(I2cAddress, TxData, TxNum, RxNum);
		if(NULL==NewComu) __heap_chk_fail();
	if(NULL == NewComu){
		return -1;	// ヒープ足りない？
	}
	NewComu->TITCA = this;
	NewComu->ComuType = ComuType;
	
	AttachedIndex = I2Cn->attach(NewComu);		// 送受信登録
	if(AttachedIndex<0){	// 登録失敗したらなかったことに
		delete NewComu;
	}
	LastAttachComu = NewComu;
	fLastAttachComuFin = false;
	
	return AttachedIndex;
}


/*********************
I2C読み終わったので捕獲
概要：
引数：	
**********************/
int8_t ti_tca_t::fetchI2cRcvData(const titca_comus_t* Comu){
	uint8_t* RcvData = Comu->RxData;
	uint8_t i;
	uint16_t PortDatTmp;
	
	if(NULL!=Comu){
		
		// 通信完了フラグ立てる
		if(LastAttachComu == Comu){
			fLastAttachComuFin = true;
		}
		
		if(!Comu->Err){
			
			// 通信内容によって受信データを処理する
			switch(Comu->ComuType){
			case TITCA_CMD_INPUT:
				// 入力値反映
				PortDatTmp = 0x0000;
				for(i = 0; i<Comu->RxNum; ++i){
					PortDatTmp = PortDatTmp | ((uint16_t)RcvData[i]<<8);
				}
				
				// 入力ピンのみ更新
				PortDat = PortDatTmp | (PortDat&PortDir);
				
				break;
			case TITCA_CMD_OUTPUT:			
				break;
			case TITCA_CMD_POLINV:			
				break;
			case TITCA_CMD_CONFIG:			
				break;
			}
		}else{
		}
		
		return 0;
	}else{
		return -1;
	}
}

