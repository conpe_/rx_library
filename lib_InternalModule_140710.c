/********************************************/
/*				カラー液晶関数				*/
/*					for RX621				*/
/*					Wrote by conpe_			*/
/*							2014/07/01		*/
/********************************************/


// 【仕様】
// コントローラ：ST7735S


//【更新履歴】
// Ver.140701
//  2014.07.01 書き始め


//【使い方】
// initPortST7725S();をできるだけ早く実行
// initST7725S();を実行


/*
HardWareSetting.h内で指定するもの

*/

#include "iodefine.h"
#include <HardwareSetting.h>
#include "lib_InternalModule_140710.h"

/********* グローバル変数 *********/





/*==========================================================================
CMTを割込用に使う初期化
	引数:割込処理に用いるCMTモジュールNo. (0〜3)
		割込msec (0〜174)
==========================================================================*/
void setupCmtInterrupt_msec(unsigned int CmtNum, unsigned int msec){
	// 16bit x 4ch

	// レジスタ
	volatile __evenaccess struct st_cmt0 *CMTreg[4] = {&CMT0, &CMT1, &CMT2, &CMT3};
	



	switch(CmtNum){
		case 0:
			MSTP(CMT0) = 0;				// 0:スタンバイ解除
			CMT.CMSTR0.BIT.STR0 = 0;	// カウントストップ
			break;
		case 1:
			MSTP(CMT1) = 0;
			CMT.CMSTR0.BIT.STR1 = 0;	// カウントストップ
			break;
		case 2:
			MSTP(CMT2) = 0;
			CMT.CMSTR1.BIT.STR2 = 0;	// カウントストップ
			break;
		case 3:
			MSTP(CMT3) = 0;
			CMT.CMSTR1.BIT.STR3 = 0;	// カウントストップ
			break;
		default:
			break;
	}
	
	CMTreg[CmtNum]->CMCR.BIT.CKS = 2;		// 0:PCLK/8 1:PCLK/32 2:PCLK/128 3:PCLK/512

	CMTreg[CmtNum]->CMCOR = msec * PCLK/128/1000;	// コンペアマッチまでの期間
	//CMTreg[CmtNum]->CMCOR = 12000;			// 1msec
	
	CMTreg[CmtNum]->CMCNT = 0;				// カウンタ
	CMTreg[CmtNum]->CMCR.BIT.CMIE = 1;		// 割り込みon
	
	switch(CmtNum){
		case 0:
			IEN(CMT0,CMI0) = 1;
			IPR(CMT0,CMI0) = 2;
			CMT.CMSTR0.BIT.STR0 = 1;	// カウントスタート
			break;
		case 1:
			IEN(CMT1,CMI1) = 1;
			IPR(CMT1,CMI1) = 2;
			CMT.CMSTR0.BIT.STR1 = 1;
			break;
		case 2:
			IEN(CMT2,CMI2) = 1;
			IPR(CMT2,CMI2) = 2;
			CMT.CMSTR1.BIT.STR2 = 1;
			break;
		case 3:
			IEN(CMT3,CMI3) = 1;
			IPR(CMT3,CMI3) = 2;
			CMT.CMSTR1.BIT.STR3 = 1;
			break;
		default:
			break;
	}
	
}

/*==========================================================================
CMTを割込用に使う初期化
	引数:割込処理に用いるCMTモジュールNo. (0〜3)
		割込usec (0〜10922)
==========================================================================*/
void setupCmtInterrupt_usec(unsigned int CmtNum, unsigned int usec){
	// 16bit x 4ch

	// レジスタ
	volatile __evenaccess struct st_cmt0 *CMTreg[4] = {&CMT0, &CMT1, &CMT2, &CMT3};
	



	switch(CmtNum){
		case 0:
			MSTP(CMT0) = 0;				// 0:スタンバイ解除
			CMT.CMSTR0.BIT.STR0 = 0;	// カウントストップ
			break;
		case 1:
			MSTP(CMT1) = 0;
			CMT.CMSTR0.BIT.STR1 = 0;	// カウントストップ
			break;
		case 2:
			MSTP(CMT2) = 0;
			CMT.CMSTR1.BIT.STR2 = 0;	// カウントストップ
			break;
		case 3:
			MSTP(CMT3) = 0;
			CMT.CMSTR1.BIT.STR3 = 0;	// カウントストップ
			break;
		default:
			break;
	}
	
	CMTreg[CmtNum]->CMCR.BIT.CKS = 0;		// 0:PCLK/8 1:PCLK/32 2:PCLK/128 3:PCLK/512

	CMTreg[CmtNum]->CMCOR = usec * PCLK/8/1000/1000;	// コンペアマッチまでの期間
	//CMTreg[CmtNum]->CMCOR = 12000;			// 1msec
	
	CMTreg[CmtNum]->CMCNT = 0;				// カウンタ
	CMTreg[CmtNum]->CMCR.BIT.CMIE = 1;		// 割り込みon
	
	switch(CmtNum){
		case 0:
			IEN(CMT0,CMI0) = 1;
			IPR(CMT0,CMI0) = 2;
			CMT.CMSTR0.BIT.STR0 = 1;	// カウントスタート
			break;
		case 1:
			IEN(CMT1,CMI1) = 1;
			IPR(CMT1,CMI1) = 2;
			CMT.CMSTR0.BIT.STR1 = 1;
			break;
		case 2:
			IEN(CMT2,CMI2) = 1;
			IPR(CMT2,CMI2) = 2;
			CMT.CMSTR1.BIT.STR2 = 1;
			break;
		case 3:
			IEN(CMT3,CMI3) = 1;
			IPR(CMT3,CMI3) = 2;
			CMT.CMSTR1.BIT.STR3 = 1;
			break;
		default:
			break;
	}
	
}

/*==========================================================================
AD変換初期化
	引数:	ADCチャンネル(0〜7)
	戻り値:	設定レジスタ(AD0orAD1)ポインタ
	
	モジュール0(AD0〜3)とモジュール1(AD1〜7)は同時に使えるけど、
	各モジュール内の複数ADCHは同時には使えない。その都度この関数で設定し直す必要あり。
==========================================================================*/

volatile __evenaccess struct st_ad * setupADaSingleMode(uint8_t AdCh){
	
	volatile __evenaccess struct st_ad *ADaReg;
	
	
	if(AdCh<4){
		ADaReg = &AD0;
	}else{
		ADaReg = &AD1;
		AdCh = AdCh&0x03;	// 下2桁のみ使う
	}
	
	
	ADaReg->ADCSR.BIT.ADST = 0;	// ADC stop
	ADaReg->ADCSR.BIT.CH = AdCh;	// AD Ch
	ADaReg->ADCSR.BIT.ADIE = 0;	// Interrupt disable
	
	ADaReg->ADCR.BIT.MODE = 0;	// SingleMode
	ADaReg->ADCR.BIT.CKS = 3;	// PCLK
	ADaReg->ADCR.BIT.TRGS = 0;	// Trig:Software
	
	ADaReg->ADDPR.BIT.DPSEL = 0;	// Format:Right
	
	return ADaReg;
	
	// 【使い方】
	// AD0.ADCSR.ADST = 1;でADC開始
	// ADC完了するとADSTが0になる
	// AD0.ADDRA, …, ADDRD, 
	// AD1.ADDRA, …, ADDRD, に結果が格納される
}


/*==========================================================================
AD変換初期化
	引数:ADCチャンネル範囲(0〜7)
			(3ならAD0のCH0〜3、6ならAD1のCH4〜CH6が対象。)
	戻り値:	設定レジスタ(AD0orAD1)ポインタ
==========================================================================*/

volatile __evenaccess struct st_ad * setupADaContinuousScanMode(uint8_t MaxAdCh){
	
	volatile __evenaccess struct st_ad *ADaReg;
	
	
	if(MaxAdCh<4){
		ADaReg = &AD0;
		
		MSTP(AD0) = 0;	//AD0モジュール起こす
	}else{
		ADaReg = &AD1;
		MaxAdCh = MaxAdCh&0x03;	// 下2桁のみ使う
		
		MSTP(AD1) = 0;	//AD0モジュール起こす
	}
	
	
	ADaReg->ADCSR.BIT.ADST = 0;	// ADC stop
	ADaReg->ADCSR.BIT.CH = MaxAdCh;	// AD Ch
	ADaReg->ADCSR.BIT.ADIE = 0;	// Interrupt disable
	
	ADaReg->ADCR.BIT.MODE = 2;	// ContinuousScanMode
	ADaReg->ADCR.BIT.CKS = 3;	// PCLK
	ADaReg->ADCR.BIT.TRGS = 0;	// Trig:Software
	
	ADaReg->ADDPR.BIT.DPSEL = 0;	// Format:Right
	
	ADaReg->ADCSR.BIT.ADST = 1;	// ADC start
	
	return ADaReg;
	
	// 【使い方】
	// AD0.ADDRA, …, ADDRD, 
	// AD1.ADDRA, …, ADDRD, に結果が格納される
}


